# Half-Life Timer

### No gamers here, just people concerned about metabolites

If you're looking for something that will help with the _Half Life_ game(s), you are definitely in the wrong place.

### What is Half-Life Timer (_NOW_)?

I began working on a portfolio piece to help with keeping track of date/dosage of medication history, after noticing that there was a gap in serious medical care, at times, for people who were on contradictory meds that may be habit forming. Take, for instance, the person that needs a pill to wake them up, and a pill to go to sleep. A few bad days on the habit and the person may take a little more of one or the other, depending on the day, and slowly both doses start to rise until the person cannot function without baseline dosages of both drugs.

Eventually this application grew to handle taperings of substances like that, sophisticated record keeping for the single user (on the mobile application) and for an unlimited number of users (on the django web-based 'server' application [LastTime](https://gitlab.com/dgetsman/lasttime-tracker)), dosage scheduling, notification for when a substance and its metabolites have broken down completely, or been fully eliminated from the body, etc. The best documentation that I can offer for either one of these projects is at the GitLab sites for their projects, in the wiki. I strive to keep these documents up-to-date before anything else barring inline code comments.

The mobile app is called Half-life Timer, and the web app with multiple users is called LastTime. You can also upload data from the mobile app into the server, which opens all kinds of doors with their interoperability at a large scale of operation. Please contact me if you have any questions regarding these applications.

## What is Half-Life Timer (_OLD_)?

The **Half-Life Timer** is a project that I thought of while working on a much more involved 'older brother', not for Android, but as a Django web app/project.  It occurred to me that while tracking dosages, administrations, and plasma and/or elimination route concentrations is extremely useful, it may be useful to have a very lobotomized version as an Android app.

I was actually just considering something that I'd been setting the standard _Clock_ app's timer for recently, trying to figure out how far down my plasma concentration was going as I weaned off of a medication, in order to determine when another step down in dosage would be appropriate.  Why not have an application that will, at least, track different medication elimination data, and then upon activation, give you a per-interval breakdown on what percentage of the original med would still be circulating, or even remaining amount in serum/plasma in mg or whatever size unit would be appropriate.

The best thing is that the data models for this and the web app could be pretty similar, leading to an easy route to expand **Half-Life Timer** into more identical functionality with the web app, and recording administrations on your mobile device while you're away from connectivity, etc...

## Algorithm (_NEW_)

As far as lipid soluble substance elimination, rudimentary support has been added for THC, but no other lipid solubles are implemented.  The mathematics is proving a bit more than I want to deal with just yet...  It's actually implemented much better in the server (**LastTime**) than in the mobile app right now.  The mobile app (very stupidly) deals with things just by throwing the worst case scenario at you.  IE if you smoke it once, you aren't 'clean' according to the app until 30 days have passed, despite the fact that due to lipid solubility issues and time away from a high blood concentration of the substance you would be clean in roughly 3-5 days, tops.  The better adipose soluble algorithm is under development at a top priority.

## Algorithm (_OLD_)

The alpha version is only going to work with water soluble substances, so that we don't have to worry about adipose concentrations and the like.  So it will be a straight up half-life equation being implemented on the basis of a per-second (or maybe user settings could determine the interval) countdown.  I would not want to bother with throwing calculus in there for an accurate running amount with any initial version, but should things head towards a release dependably, it would certainly be worth learning how to implement an integral in java.

## See also

  * [Half-Life Timer's Usage & Description Page](https://gitlab.com/dgetsman/half-life-timer/-/wikis/usage-n-description)
  * [Half-Life Timer's Releases Page](https://gitlab.com/dgetsman/half-life-timer/-/releases)
  * [LastTime's Wiki (with usage & description bits)](https://gitlab.com/dgetsman/lasttime-tracker/-/wikis/home)


