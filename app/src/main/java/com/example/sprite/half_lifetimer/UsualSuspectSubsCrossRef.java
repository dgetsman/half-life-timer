package com.example.sprite.half_lifetimer;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(tableName="UsualSuspectSubsCrossRef", primaryKeys={"usualSuspect", "sub"})
//we'll have to figure out how to use composite primary keys later on
//@Entity(tableName="UsualSuspectSubsCrossRef")
public class UsualSuspectSubsCrossRef {
    //@PrimaryKey(autoGenerate = true)
    //private int         id;

    @ColumnInfo(name="usualSuspect")
    private int usualSuspect;

    @ColumnInfo(name="sub")
    private int sub;

    @ColumnInfo(name="dosage")
    private float dosage;

    public UsualSuspectSubsCrossRef() { }

    public UsualSuspectSubsCrossRef(int usualSuspect, int sId, float dose) {
        this.usualSuspect = usualSuspect;
        this.sub = sId;
        this.dosage = dose;
    }

    /*public int getId() { return id; }

    public void setId(int id) {
        this.id = id;
    }*/

    public float getDosage() {
        return dosage;
    }

    public void setDosage(float dose) {
        this.dosage = dose;
    }

    public int getUsualSuspect() {
        return usualSuspect;
    }

    public void setUsualSuspect(int usualSuspect) {
        this.usualSuspect = usualSuspect;
    }

    public int getSub() {
        return sub;
    }

    public void setSub(int substance) {
        this.sub = substance;
    }

    public String toString() {
        return Permanence.US.loadUSByID(this.usualSuspect).getName() + " (" + this.usualSuspect +
                ") <--> " + Permanence.Subs.loadSubstanceById(this.sub) + " (" + this.sub +
                ")  -- Dosage: " + this.dosage;
    }
}
