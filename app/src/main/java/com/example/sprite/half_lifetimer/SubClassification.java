package com.example.sprite.half_lifetimer;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Class provides the structure for the categories of substances that exist.
 */
@Entity
public class SubClassification {
    @PrimaryKey(autoGenerate = true)
    private int         id;
    @ColumnInfo(name="name")
    private String      name;
    @ColumnInfo(name="description")
    private String      desc;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public SubClassification() { }

    public String toString() {
        return name + " \t" + desc;
    }
}
