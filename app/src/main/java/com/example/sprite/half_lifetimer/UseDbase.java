package com.example.sprite.half_lifetimer;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database(entities = {Usage.class, Substance.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class UseDbase extends RoomDatabase {
    public abstract UsageDao usageDao();
}
