package com.example.sprite.half_lifetimer;

import android.graphics.Color;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.ValueDependentColor;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.time.LocalDateTime;
import java.util.List;

public class DosageGraphing extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosage_graphing);

        updateDisplay(getIntent().getExtras().getInt("SUB_NDX"));
    }

    /**
     * As in the other classes, this method is to simply update the display whenever the activity
     * is first called or data may have changed.
     *
     * @param subNdx int 'id' of the substance currently being graphed
     */
    public void updateDisplay(int subNdx) {
        GraphView hourlyDosageGraph = findViewById(R.id.dosageGraph);
        GraphView dailyDosageGraph = findViewById(R.id.dailyGraph);

        if (Permanence.Admins.countPostToTimestampForSub(
                Converters.fromLocalDateTime(LocalDateTime.now().minusHours(24)), subNdx) <= 0) {
            GlobalMisc.showSimpleDialog(DosageGraphing.this, "No Results to Show!",
                    "There have been no usages in the past 24 hours for " +
                    Permanence.Subs.loadSubstanceById(subNdx).getCommon_name() +
                    ".  There is no data to graph.");

            return;
        }

        dailyDosageGraph.getViewport().setXAxisBoundsManual(true);
        dailyDosageGraph.getViewport().setMinX(GlobalMisc.fromLDTToDate(
                LocalDateTime.now().minusDays(30)
                        .withHour(0)
                        .withMinute(0)
                        .withSecond(0)).getTime());
        dailyDosageGraph.getViewport().setMaxX(GlobalMisc.fromLDTToDate(
                LocalDateTime.now().withHour(23)    //not so sure we want this @ 23:59:59, that has
                        .withMinute(59)             //painfully skewed visual results in other graph
                        .withSecond(59)).getTime());    //areas
        dailyDosageGraph.getViewport().setScrollableY(true);
        dailyDosageGraph.getViewport().setScalableY(true);

        //note we need to modularize both of these calls to get stats so that we're not hitting
        //the database twice for the same information, as these listings may get pretty long
        hourlyDosageGraph.addSeries(GlobalMisc.getPastDayGraphSeries(subNdx));
        dailyDosageGraph.addSeries(getDailyTotalDosageGraphDataPoints(subNdx));

        //do we have a taper goal for this graph to be added?
        Taper subTaper = Permanence.Tapers.loadOneValidTaperBySid(subNdx);
        if (subTaper != null) {
            dailyDosageGraph.addSeries(
                    DecayGraphingSupport.getDosageTaperLevelDataPoints(subTaper));
        }

        //more for the labels on the dailyDosageGraph, at least to keep them readable
        dailyDosageGraph.getGridLabelRenderer().setLabelFormatter(
                new DateAsXAxisLabelFormatter(this));
        dailyDosageGraph.getGridLabelRenderer().setNumHorizontalLabels(3);
    }

    private BarGraphSeries<DataPoint> getHourlyTotalDosageGraphDataPoints(int subNdx) {
        //let's just try displaying the current 24 hour period at this point; later on we'll expand
        //things to be able to scroll this backwards with a larger set
        LocalDateTime oneDayAgo =  LocalDateTime.now().minusDays(1);
        LocalDateTime todayMidnight = LocalDateTime.of(oneDayAgo.getYear(), oneDayAgo.getMonth(),
                oneDayAgo.getDayOfMonth(), 0, 0, 0);

        List<Usage> dayUsages = Permanence.Admins.getPostToTSUsageRecords(subNdx,
                Converters.fromLocalDateTime(todayMidnight));

        for (Usage usage : dayUsages) {
            GlobalMisc.debugMsg("getHourlyTotalDosageGraphDataPoints",
                    usage.getDosage() + "@ " + usage.getTimestamp());
        }

        float[] hourlyDosages = new float[24];
        for (int cntr = 0; cntr < 24; cntr++) {
            hourlyDosages[cntr] = 0;
        }
        for (int cntr = 0; cntr < dayUsages.size(); cntr++) {
            hourlyDosages[dayUsages.get(cntr).getTimestamp().getHour()] +=
                    dayUsages.get(cntr).getDosage();

            GlobalMisc.debugMsg("getHourlyTotalDosageGraphDataPoints",
                    "hourlyDosages[" + dayUsages.get(cntr).getTimestamp().getHour()
                            + "] += " + dayUsages.get(cntr).getDosage());
        }

        DataPoint[] points = new DataPoint[24];
        for (int cntr = 0; cntr < 24; cntr++) {
            points[cntr] = new DataPoint(cntr, hourlyDosages[cntr]);
        }

        return new BarGraphSeries<>(points);
    }

    /**
     * Method processes the usages' dosages for individual administrations' proper graphing.
     *
     * @param subNdx int 'id' of the substance currently being graphed
     * @return LineGraphSeries of the dosages
     */
    private LineGraphSeries<DataPoint> getDosageGraphDataPoints(int subNdx) {
        //construct a date 30 days in the past
        LocalDateTime thirtyDayStart = LocalDateTime.now().minusDays(GlobalMisc.DaysToGraph);

        //only use the ones in our relevant frame from the past now
        List<Usage> usages = Permanence.Admins.getPostToTSOrderedUsageRecords(subNdx,
                Converters.fromLocalDateTime(thirtyDayStart));

        DataPoint[] points = new DataPoint[usages.size()];

        for (int cntr = 0; cntr < usages.size(); cntr++) {
            points[cntr] = new DataPoint(cntr, usages.get(cntr).getDosage());
        }

        return new LineGraphSeries<>(points);   //dosages;
    }

    /**
     * Method processes the usages' dosages for a daily dosage graph.
     *
     * @param subNdx int 'id' of the substance currently being graphed
     * @return LineGraphSeries of the daily dosage totals
     */
    private BarGraphSeries<DataPoint> getDailyTotalDosageGraphDataPoints(int subNdx) {
        LocalDateTime xDayEndLDT = LocalDateTime.now()
                .withHour(23)   //again, not so sure that we want this @ 23:59:59
                .withMinute(59)
                .withSecond(59);
        LocalDateTime xDaysAgoZero =
                LocalDateTime.now()
                        .minusDays(GlobalMisc.DaysToGraph)
                        .withHour(0)
                        .withMinute(0)
                        .withSecond(0);
        List<Usage> applicableUsages;
        int cntr = 0;
        float curRunningTotal = 0.0f;
        DataPoint[] point = new DataPoint[GlobalMisc.DaysToGraph + 1];

        //iterate through each day from xDaysAgoZero (@ 00:00:00hrs) until curFromXDayLDT comes up
        //as being after xDayEndLDT, our ending point (@ 23:59:59)
        for (LocalDateTime curFromXDayLDT = xDaysAgoZero; curFromXDayLDT.isBefore(xDayEndLDT);
             curFromXDayLDT = curFromXDayLDT.plusDays(1)) {
            applicableUsages = Permanence.Admins.getBetweenSpanValidUsagesForSub(subNdx,
                    Converters.fromLocalDateTime(curFromXDayLDT),
                    Converters.fromLocalDateTime(
                            curFromXDayLDT.withHour(23).withMinute(59).withSecond(59)));

            for (Usage use : applicableUsages) {
                curRunningTotal += use.getDosage();
            }

            point[cntr++] =
                    new DataPoint(GlobalMisc.convertLocalDateTimeToDate(curFromXDayLDT),
                            curRunningTotal);

            curRunningTotal = 0.0f;
        }

        final Taper fTaper = Permanence.Tapers.loadOneValidTaperBySid(subNdx);

        BarGraphSeries<DataPoint> bSeries = new BarGraphSeries<>(point);
        bSeries.setValueDependentColor(new ValueDependentColor<DataPoint>() {
            @Override
            public int get(DataPoint data) {
                if (fTaper == null) {
                    //return Color.BLUE;
                    return ContextCompat.getColor(DosageGraphing.this,
                            R.color.colorPrimary);
                } else if (data.getY() == fTaper.findTodaysDosageMax()) {
                    return ContextCompat.getColor(DosageGraphing.this,
                            R.color.colorWarnYellow);
                } else if (data.getY() > fTaper.findTodaysDosageMax()) {
                    return Color.RED;
                } else {
                    //we're under the taper target
                    return ContextCompat.getColor(DosageGraphing.this,
                            R.color.colorDarkGreen);
                }
            }
        });
        bSeries.setDrawValuesOnTop(true);
        bSeries.setValuesOnTopColor(Color.BLACK);

        return bSeries;
    }


}

