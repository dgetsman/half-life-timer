package com.example.sprite.half_lifetimer;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Class provides the view and nuts and bolts for working with different SubClassifications.
 */
public class SubClass extends AppCompatActivity {
    private final SubClassification tmpSubClass = new SubClassification();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_class);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Substance Classes");
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GlobalMisc.showDialogGetString(SubClass.this,
                        getResources().getString(R.string.add_new_subclass_name), "",
                        new GlobalMisc.OnDialogResultListener() {
                            @Override
                            public void onResult(String result) {
                                tmpSubClass.setName(result);

                                GlobalMisc.showDialogGetString(SubClass.this,
                                        getResources().getString(
                                                R.string.add_new_classification_description),
                                        "",
                                        new GlobalMisc.OnDialogResultListener() {
                                    public void onResult(String result) {
                                        tmpSubClass.setDesc(result);

                                        Permanence.SC.saveSC(tmpSubClass);
                                        tmpSubClass.setName(null);
                                        tmpSubClass.setDesc(null);

                                        updateDisplay();
                                    }
                                });
                            }
                        });
            }
        });

        updateDisplay();
    }

    /**
     * Method updates the display for SubClass activity whenever it is
     * necessary.  Note that this does not have anything to do with the
     * populateGraph() method, below.
     */
    private void updateDisplay() {
        LinearLayout lloSubClasses = findViewById(R.id.lloSubClassListing);
        lloSubClasses.removeAllViewsInLayout();

        List<SubClassification> subClasses = Permanence.SC.loadAllSC();

        TextView[] sClassesTextView = new TextView[subClasses.size()];

        for (int cntr = 0; cntr < subClasses.size(); cntr++) {
            final SubClassification sClass = subClasses.get(cntr);
            sClassesTextView[cntr] = new TextView(SubClass.this);
            sClassesTextView[cntr].setText(subClasses.get(cntr).getName());
            sClassesTextView[cntr].setTextSize(22);
            sClassesTextView[cntr].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    populateGraph(sClass.getId());
                }
            });
            sClassesTextView[cntr].setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(SubClass.this);
                    builder.setPositiveButton("See Class Tabulated Data",
                            new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(SubClass.this, SubClassTabbedData.class);
                            intent.putExtra("CLASS_ID", sClass.getId());
                            startActivity(intent);
                        }
                    });
                    builder.setNegativeButton("Delete Classification",
                            new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            GlobalMisc.showDialogGetConfirmation(SubClass.this,
                                    "Delete substance class?",
                                    "Delete the " + sClass.getName() + " substance " +
                                    "classification?  Are you sure?",
                                    new GlobalMisc.OnDialogConfResultListener() {
                                        @Override
                                        public void onResult(boolean result) {
                                            if (result) {
                                                Permanence.SC.wipeSC(sClass);

                                                updateDisplay();
                                            }
                                        }
                                    });
                        }
                    });
                    builder.show();

                    return false;
                }
            });

            lloSubClasses.addView(sClassesTextView[cntr]);
        }
    }

    /**
     * Method handles populating the graph visual data after a substance class
     * has been clicked on/selected.
     *
     * @param classID int substance classification ID #
     */
    private void populateGraph(final int classID) {
        Toast.makeText(SubClass.this, "Crunching data to populate the graph, please wait. . .",
                Toast.LENGTH_LONG).show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                LocalDateTime today =
                        LocalDateTime.now();
                LocalDateTime thirtyDaysAgo =
                        today.minusDays(30).withHour(0)
                                .withMinute(0)
                                .withSecond(0);
                List<Substance> classSubstances =
                        Permanence.Subs.loadUnarchivedSubstanceBySClass(classID);
                HashMap<Substance, Double> highestDosages = new HashMap<>();
                HashMap<Integer, Double[]> substanceLevels = new HashMap<>();
                HashMap<Integer, DataPoint[]> dataPoints = new HashMap<>();
                HashMap<Substance, Integer> subToColorMap = new HashMap<>();
                List<Integer> rainbow = GlobalMisc.getRoyGBiv(SubClass.this);

                //obtain substance levels for each substance, determine each sub's highest dosage
                boolean classHasEntries = false;
                for (Substance curSub : classSubstances) {
                    substanceLevels.put(curSub.getId(), DecayGraphingSupport.plotXDaysSubLevels(
                            curSub.getId(), thirtyDaysAgo, today));

                    GlobalMisc.debugMsg("populateGraph",
                            "substanceLevels.get(curSub.getId()): " +
                                    Arrays.toString(substanceLevels.get(curSub.getId())));

                    //determine the highest value in this array
                    highestDosages.put(curSub, 0.0);
                    for (Double subLevel : substanceLevels.get(curSub.getId())) {
                        if (subLevel > highestDosages.get(curSub)) {
                            highestDosages.replace(curSub, subLevel);
                        }
                    }

                    if (Permanence.Admins.getUsageCountForSubstance(curSub.getId()) > 0) {
                        classHasEntries = true;
                    }
                }

                final FrameLayout floGraphPane = findViewById(R.id.floGraphFrame);
                if (!classHasEntries) {
                    //we just want to display our message about no entries and then be done with it
                    removeGraphDisplayNotice();
                    return;
                } else if (floGraphPane.getChildCount() == 0 ||
                        (floGraphPane.getChildAt(
                                0).getClass().getName().contains("TextView"))) {
                    removeNoticeDisplayGraph();
                }

                LocalDateTime curLDT = thirtyDaysAgo;
                double highestDoseOnGraph = DecayGraphingSupport.getHighestDosage(highestDosages);
                //apply multipliers and construct our DataPoints
                int ouah = 0;
                for (Substance curSub : classSubstances) {
                    DataPoint[] subLevelPoints =
                            new DataPoint[substanceLevels.get(curSub.getId()).length];
                    double multiplier = highestDoseOnGraph / highestDosages.get(curSub);
                    Double[] substanceLevel = substanceLevels.get(curSub.getId());

                    GlobalMisc.debugMsg("populateGraph", "multiplier: " +
                            multiplier + "\tsubstanceLevel: " + Arrays.toString(substanceLevel));

                    if (multiplier != 1.0) {
                        for (int cntr = 0; cntr < subLevelPoints.length; cntr++) {
                            GlobalMisc.debugMsg("populateGraph",
                                    "new DataPoint(x, y) -> x: " +
                                            GlobalMisc.fromLDTToDate(curLDT) + "\ty: " +
                                            substanceLevel[cntr] * multiplier);

                            subLevelPoints[cntr] =
                                    new DataPoint(GlobalMisc.fromLDTToDate(curLDT),
                                            substanceLevel[cntr] * multiplier);

                            curLDT = curLDT.plusHours(1);
                        }
                    } else {
                        //avoid that unnecessary multiplication
                        for (int cntr = 0; cntr < subLevelPoints.length; cntr++) {
                            subLevelPoints[cntr] =
                                    new DataPoint(GlobalMisc.fromLDTToDate(curLDT),
                                            substanceLevel[cntr]);

                            curLDT = curLDT.plusHours(1);
                        }
                    }

                    GlobalMisc.debugMsg("populateGraph", "curSub: " +
                            curSub.toString() + "\tsubLevelPoints: " +
                            Arrays.toString(subLevelPoints));

                    dataPoints.put(curSub.getId(), subLevelPoints);
                    subToColorMap.put(curSub, ouah++);

                    curLDT = thirtyDaysAgo;
                }

                GraphView classSubLevelsGraph = findViewById(R.id.classMembersGraph);
                if (!subToColorMap.isEmpty()) {
                    initGraph(GlobalMisc.fromLDTToDate(thirtyDaysAgo),
                            GlobalMisc.fromLDTToDate(today));
                    ouah = 0;
                    for (Substance curSub : classSubstances) {
                        GlobalMisc.debugMsg("populateGraph",
                                "adding series for: " + curSub.getId() +
                                        "\tdataPoints.get(curSub.getId())[-1].getX(): " +
                                        dataPoints.get(
                                                curSub.getId())[dataPoints.get(
                                                        curSub.getId()).length - 1].getX());

                        LineGraphSeries<DataPoint> newSeries =
                                new LineGraphSeries<>(dataPoints.get(curSub.getId()));
                        newSeries.setColor(rainbow.get(ouah));
                        if (ouah > 6) {
                            Paint dottedPaint = new Paint();
                            dottedPaint.setStyle(Paint.Style.STROKE);
                            dottedPaint.setStrokeWidth(10);
                            dottedPaint.setPathEffect(new DashPathEffect(new float[]{8, 5}, 0));
                            dottedPaint.setColor(rainbow.get(ouah - 6));
                            newSeries.setDrawAsPath(true);
                            newSeries.setCustomPaint(dottedPaint);
                        }

                        final GraphView fClassSubLevelsGraph = classSubLevelsGraph;
                        final LineGraphSeries<DataPoint> fNewSeries = newSeries;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                fClassSubLevelsGraph.addSeries(fNewSeries);
                            }
                        });

                        ouah++;
                    }

                    final HashMap<Substance, Integer> fSubToColorMap = subToColorMap;
                    final List<Integer> fRainbow = rainbow;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            writeLegend(fSubToColorMap, fRainbow);
                        }
                    });
                } else {
                    TextView tvwNoResults = new TextView(SubClass.this);

                    tvwNoResults.setText(getString(R.string.no_results_found,
                            Permanence.SC.loadSCById(classID).getName()));
                    tvwNoResults.setTextSize(20);
                    tvwNoResults.setTextColor(getResources().getColor(R.color.colorDarkOrange));

                    //final FrameLayout floGraphFrame = findViewById(R.id.floGraphFrame);
                    final GraphView fClassSubLevelsGraph = classSubLevelsGraph;
                    final TextView fTvwNoResults = tvwNoResults;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            floGraphPane.removeView(fClassSubLevelsGraph);
                            floGraphPane.addView(fTvwNoResults);
                        }
                    });
                }
            }

            /**
             * Method writes the legend data (substance : color) at the bottom of the
             * graph.
             *
             * @param subToColorMap HashMap<Substance, Integer> substance : color map
             * @param rainbow List<Integer> colors of the rainbow to use
             */
            private void writeLegend(HashMap<Substance, Integer> subToColorMap,
                                     List<Integer> rainbow) {
                //make the color blocks 55x55, when we get around to that
                LinearLayout lloGraphLegend = findViewById(R.id.lloClassMembersGraphLegend);

                lloGraphLegend.removeAllViewsInLayout();

                TextView legendLabel = new TextView(getApplicationContext());
                legendLabel.setText(getResources().getString(R.string.legend));
                legendLabel.setTextColor(Color.BLACK);
                lloGraphLegend.addView(legendLabel);
                for (Substance clearingSub : subToColorMap.keySet()) {
                    TextView legendMember = new TextView(getApplicationContext());
                    legendMember.setText(getResources().getString(R.string.tab_buffered_string,
                            clearingSub.getCommon_name()));
                    legendMember.setTextColor(rainbow.get(subToColorMap.get(clearingSub)));

                    lloGraphLegend.addView(legendMember);
                }
            }

            /**
             * Method just handles the boilerplate as far as getting the graph set up.
             * Just pulled it out of plotDecayCurves to avoid that method having too
             * much clutter.
             *
             * @param startDate Date starting date for graph
             * @param endDate Date ending date for graph
             */
            private void initGraph(Date startDate, Date endDate) {
                GraphView classMembersGraph = findViewById(R.id.classMembersGraph);

                //graph setup
                classMembersGraph.removeAllSeries();

                StaticLabelsFormatter staticLabelsFormatter =
                        new StaticLabelsFormatter(classMembersGraph,
                                new DateAsXAxisLabelFormatter(getApplicationContext()));

                staticLabelsFormatter.setVerticalLabels(new String[] {"zero", "mid", "peak"});

                classMembersGraph.getViewport().setXAxisBoundsManual(true);
                classMembersGraph.getViewport().setMinX(startDate.getTime());
                classMembersGraph.getViewport().setMaxX(endDate.getTime());
                classMembersGraph.getViewport().setScrollableY(true);
                classMembersGraph.getViewport().setScalableY(true);

                classMembersGraph.getGridLabelRenderer().setNumHorizontalLabels(3);
                classMembersGraph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
                classMembersGraph.setTitle("Class Members Over Previous 30 Days");
            }

            /**
             * Method handles removing the GraphView and displaying a TextView instead,
             * explaining that there are no results for the current substance found.
             */
            private void removeGraphDisplayNotice() {
                final TextView tvwNoResults = new TextView(SubClass.this);

                tvwNoResults.setText(getString(R.string.no_results_found,
                        Permanence.SC.loadSCById(classID).getName()));
                tvwNoResults.setTextSize(20);
                tvwNoResults.setTextColor(getResources().getColor(R.color.colorDarkOrange));
                tvwNoResults.setGravity(Gravity.CENTER);

                final FrameLayout floGraphFrame = findViewById(R.id.floGraphFrame);
                floGraphFrame.removeAllViewsInLayout();
                runOnUiThread(new Runnable() {
                    public void run() {
                        floGraphFrame.addView(tvwNoResults);
                    }
                });
            }

            /**
             * Method handles removing the notice that (may have been/was) previously
             * displayed and puts the GraphView up where it should be.
             */
            private void removeNoticeDisplayGraph() {
                final FrameLayout graphContainer = findViewById(R.id.floGraphFrame);

                //remove the TextView notification that we've got 0 results and plot our shit
                runOnUiThread(new Runnable() {
                    public void run() {
                        View graphView = LayoutInflater.from(
                                SubClass.this).inflate(R.layout.graph_container, null);
                        graphContainer.removeAllViewsInLayout();
                        graphContainer.addView(graphView);
                    }
                });
            }
        }).start();

    }




}
