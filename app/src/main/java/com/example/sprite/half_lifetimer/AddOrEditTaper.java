package com.example.sprite.half_lifetimer;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.time.LocalDateTime;
import java.time.LocalTime;

public class AddOrEditTaper extends AppCompatActivity
        implements AdapterView.OnItemSelectedListener, ChooseDateFragment.DateListener,
        ChooseTimeFragment.TimeListener {
    boolean settingStartTaper, settingStartHour;
    Taper newTaper = new Taper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_or_edit_taper);

        if (getIntent().hasExtra("TAPER_ID")) {
            setUpForEdit(getIntent().getExtras().getInt("TAPER_ID"));
        }

        Button btnSetConstraintStart = findViewById(R.id.btnSetConstraintStart);
        Button btnSetConstraintEnd = findViewById(R.id.btnSetConstraintEnd);

        //set up the Spinner
        Spinner spnSubstanceToTaper = findViewById(R.id.spnAddSubstanceForTaper);
        String[] allSubNames = new String[SubData.subList.size()];
        for (int cntr = 0; cntr < SubData.subList.size(); cntr++) {
            allSubNames[cntr] = SubData.subList.get(cntr).getCommon_name();
        }

        ArrayAdapter<String> subsAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, allSubNames);
        subsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnSubstanceToTaper.setAdapter(subsAdapter);
        spnSubstanceToTaper.setOnItemSelectedListener(this);

        //we don't want these buttons enabled unless the checkbox is set
        btnSetConstraintStart.setEnabled(false);
        btnSetConstraintEnd.setEnabled(false);
    }

    /**
     * This method handles setting all of the activity's form elements to the values that are in
     * the taper object that we're editing, should we be in editing mode.
     *
     * @param taperID int ID of the taper to edit
     */
    private void setUpForEdit(int taperID) {
        newTaper = Permanence.Tapers.loadTaperById(taperID);

        EditText edtTaperName = findViewById(R.id.edtTaperName);
        Spinner spnSubstanceForTaper = findViewById(R.id.spnAddSubstanceForTaper);
        EditText edtTaperStartDose = findViewById(R.id.edtTaperStartDosage);
        EditText edtTaperEndDose = findViewById(R.id.edtTaperGoalDosage);
        EditText edtTaperDosageStep = findViewById(R.id.edtTaperDosageStepAmount);
        EditText edtTaperAdminsPerDay = findViewById(R.id.edtTaperAdminsPerDay);
        CheckBox cbxConstrained = findViewById(R.id.cbxConstrainHoursOfTaper);

        edtTaperName.setText(newTaper.getName());
        spnSubstanceForTaper.setSelection(
                SubData.subList.indexOf(Permanence.Subs.loadSubstanceById(newTaper.getSid())));
        edtTaperStartDose.setText(Float.toString(newTaper.getDailyDosageStart()));
        edtTaperEndDose.setText(Float.toString(newTaper.getDailyDosageTarget()));
        if (newTaper.getDosageInterval() == 0) {
            edtTaperDosageStep.setText(getResources().getString(R.string.automatic));
        } else {
            edtTaperDosageStep.setText(Float.toString(newTaper.getDosageInterval()));
        }
        edtTaperAdminsPerDay.setText(Integer.toString(newTaper.getAdminsPerDay()));
        if (newTaper.getStartHour() != null && newTaper.getEndHour() != null) {
            cbxConstrained.setChecked(true);
        } else {
            cbxConstrained.setChecked(false);
        }
    }

    /**
     * This activates the ChooseDateFragment in order to pick the starting date for the taper.
     *
     * @param v View
     */
    public void onClickSetTaperStartDate(View v) {
        settingStartTaper = true;

        ChooseDateFragment chooseDateFragment = new ChooseDateFragment();
        chooseDateFragment.show(getSupportFragmentManager(), "");
        Bundle args = new Bundle();
        args.putBoolean("startCompleted", false);    //hackity hack hack
        chooseDateFragment.setArguments(args);
    }

    /**
     * This method activates the ChooseDateFragment in order to pick the ending date for the taper.
     *
     * @param v View
     */
    public void onClickSetTaperEndDate(View v) {
        settingStartTaper = false;

        ChooseDateFragment chooseDateFragment = new ChooseDateFragment();
        chooseDateFragment.show(getSupportFragmentManager(), "");
        Bundle args = new Bundle();
        args.putBoolean("startCompleted", true);    //hackity hack hack
        chooseDateFragment.setArguments(args);
    }

    /**
     * This method activates the ChooseTimeFragment in order to pick the starting hour for the
     * constrained tapering periods per day.
     *
     * @param v View
     */
    public void onClickSetConstraintStartHour(View v) {
        settingStartHour = true;

        ChooseTimeFragment chooseTimeFragment = new ChooseTimeFragment();
        chooseTimeFragment.show(getSupportFragmentManager(), "");
        Bundle args = new Bundle();
        args.putBoolean("editing", false);
        chooseTimeFragment.setArguments(args);
    }

    /**
     * This method activates ChooseTimeFragment to pick the ending hour for constrained tapering
     * period of the day.
     *
     * @param v View
     */
    public void onClickSetConstraintEndHour(View v) {
        settingStartHour = false;

        ChooseTimeFragment chooseTimeFragment = new ChooseTimeFragment();
        chooseTimeFragment.show(getSupportFragmentManager(), "");
        Bundle args = new Bundle();
        args.putBoolean("editing", false);
        chooseTimeFragment.setArguments(args);
    }

    /**
     * This method handles validating the input within the different activity fields, depending on
     * which criteria are currently being utilized in the taper.  Provided everything validates,
     * this method will save the new taper to the database and update the status line appropriately
     * whether it has succeeded or failed.
     *
     * @param v View
     */
    public void onClickCommit(View v) {
        //validate input fields and values that were [hopefully] previously set, then it's time
        //to stuff the new Taper object and try to save it
        if (!validateName() || !validateBothDates() || !validateDosageAmounts() ||
                !validateAdminsPerDay() || !validateConstraints()) {
            return;
        }

        EditText edtTaperName = findViewById(R.id.edtTaperName);
        EditText edtDosageTarget = findViewById(R.id.edtTaperGoalDosage);
        EditText edtDosageStart = findViewById(R.id.edtTaperStartDosage);
        EditText edtAdminsPerDay = findViewById(R.id.edtTaperAdminsPerDay);
        EditText edtDosageInterval = findViewById(R.id.edtTaperDosageStepAmount);


        newTaper.setName(edtTaperName.getText().toString().trim());
        newTaper.setDailyDosageStart(Float.parseFloat(edtDosageStart.getText().toString()));
        newTaper.setDailyDosageTarget(Float.parseFloat(edtDosageTarget.getText().toString()));
        newTaper.setAdminsPerDay(Integer.parseInt(edtAdminsPerDay.getText().toString()));
        if (edtDosageInterval.getText().toString().trim().contentEquals("auto")) {
            newTaper.setDosageInterval(0);
        } else {
            newTaper.setDosageInterval(Float.parseFloat(edtDosageInterval.getText().toString()));
        }

        TextView tvwStatusLine = findViewById(R.id.tvwTaperStatusLine);

        try {
            Permanence.Tapers.saveTaper(newTaper);
        } catch (Exception ex) {
            tvwStatusLine.setText(getResources().getString(R.string.unable_to_save_taper,
                    ex.toString()));
            tvwStatusLine.setTextColor(getResources().getColor(R.color.colorDarkRed));
            tvwStatusLine.setTextSize(18);

            return;
        }

        tvwStatusLine.setText(getResources().getString(R.string.taper_saved_successfully));
        tvwStatusLine.setTextColor(getResources().getColor(R.color.colorDarkGreen));
        tvwStatusLine.setTextSize(20);

        clearForm();
    }

    private void clearForm() {
        EditText edtTaperName = findViewById(R.id.edtTaperName);
        EditText edtDosageTarget = findViewById(R.id.edtTaperGoalDosage);
        EditText edtDosageStart = findViewById(R.id.edtTaperStartDosage);
        EditText edtAdminsPerDay = findViewById(R.id.edtTaperAdminsPerDay);
        EditText edtDosageInterval = findViewById(R.id.edtTaperDosageStepAmount);
        CheckBox cbxConstrainHours = findViewById(R.id.cbxConstrainHoursOfTaper);

        edtTaperName.setText("");
        edtDosageTarget.setText("");
        edtDosageStart.setText("");
        edtAdminsPerDay.setText("");
        edtDosageInterval.setText("");
        cbxConstrainHours.setChecked(false);
    }

    /**
     * This method is SUPPOSED to remove the 'auto' from the dosage step amount (er dosage interval,
     * I think I referred to it in different ways in different areas of the source here).  This is
     * so that a numeric dosage amount can be entered instead of 'automatic' determination.  For
     * some reason it is still not working, though, and you have to manually backspace over the
     * 'auto' which populates the EditText box.
     *
     * @param v View
     */
    public void onClickDosageStepAmt(View v) {
        EditText edtDosageStepAmt = findViewById(R.id.edtTaperDosageStepAmount);

        edtDosageStepAmt.setText("");
    }

    /**
     * This method toggles whether or not the constrained start & ending buttons are active,
     * depending on whether or not the user has selected constrainment of dosages.  If the
     * constraint parameter is removed, it also wipes any previous constraint values that were in
     * place in the new taper in memory, just to make sure that we don't end up with a horked
     * database.
     *
     * @param v View
     */
    public void onClickConstraintCheckbox(View v) {
        CheckBox cbxConstrainedHours = findViewById(R.id.cbxConstrainHoursOfTaper);
        Button btnSetConstraintStart = findViewById(R.id.btnSetConstraintStart);
        Button btnSetConstraintEnd = findViewById(R.id.btnSetConstraintEnd);

        if (cbxConstrainedHours.isChecked()) {
            btnSetConstraintStart.setEnabled(true);
            btnSetConstraintEnd.setEnabled(true);
        } else {
            //this will wipe the values that they had there before, but I think that's probably for
            //the best; maybe with a dialog to the user about it, though
            btnSetConstraintStart.setEnabled(false);
            btnSetConstraintEnd.setEnabled(false);

            newTaper.setStartHour(null);
            newTaper.setEndHour(null);
        }
    }

    /**
     * This method handles validation of the taper's name, making sure that it's not an empty string
     * and that it does not match any other tapers' names.
     *
     * @return boolean true for valid & vice versa
     */
    private boolean validateName() {
        EditText edtTaperName = findViewById(R.id.edtTaperName);

        if (edtTaperName.getText().toString().trim().length() == 0) {
            GlobalMisc.showSimpleDialog(AddOrEditTaper.this, "Unnamed Taper",
                    "You need to give your taper a name!");

            return false;
        }

        for (Taper taper : Permanence.Tapers.loadAllTapers()) {
            if (edtTaperName.getText().toString().trim().toLowerCase().contentEquals(
                    taper.getName().toLowerCase())) {
                GlobalMisc.showSimpleDialog(AddOrEditTaper.this, "Duplicate Taper",
                    "A taper with that name already exists!");

                return false;
            }
        }

        return true;
    }

    /**
     * Method validates that the starting & ending dates for the taper are not null, and that they
     * are in the appropriate chronological order.
     *
     * @return boolean true for valid & vice versa
     */
    private boolean validateBothDates() {
        if (newTaper.getStartDate() == null || newTaper.getTargetDate() == null) {
            GlobalMisc.showSimpleDialog(AddOrEditTaper.this, "Dates Not Set",
            "Your starting and/or ending dates for the taper are not set yet!");

            return false;
        } else if (newTaper.getStartDate().isAfter(newTaper.getTargetDate()) ||
                newTaper.getStartDate().isEqual(newTaper.getTargetDate())) {
            GlobalMisc.showSimpleDialog(AddOrEditTaper.this, "Dates Make No Sense",
                    "Your starting date is the same as or later than your ending date!");

            return false;
        }

        return true;
    }

    /**
     * Method validates that there is a significant difference in starting & ending dosages; in the
     * future it should also parse the values within a try/catch loop just in case they aren't able
     * to be parsed for Float.parseFloat(), in which case we would want to raise an issue with a
     * malformed number, as well...
     *
     * @return boolean true for valid & vice versa
     */
    private boolean validateDosageAmounts() {
        EditText edtStartDosage = findViewById(R.id.edtTaperStartDosage);
        EditText edtTargetDosage = findViewById(R.id.edtTaperGoalDosage);
        float start, target, difference;
        start = Float.parseFloat(edtStartDosage.getText().toString());
        target = Float.parseFloat(edtTargetDosage.getText().toString());
        difference = start - target;

        if ((difference > 0 && difference < GlobalMisc.DoseRangeMin) ||
                (difference < 0 && difference < (GlobalMisc.DoseRangeMin * -1))) {
            GlobalMisc.showSimpleDialog(AddOrEditTaper.this, "No Change in Dosage",
                    "Your starting dosage is too close to your ending dosage, no " +
                            "tapering would be taking place!");

            return false;
        }

        return true;
    }

    /**
     * Method simply validates that administrations per day has been set for the taper.
     *
     * @return boolean true for valid & vice versa
     */
    private boolean validateAdminsPerDay() {
        EditText edtAdminsPerDay = findViewById(R.id.edtTaperAdminsPerDay);

        if (edtAdminsPerDay.getText().toString().trim().isEmpty()) {
            GlobalMisc.showSimpleDialog(AddOrEditTaper.this, "No Administrations Per Day",
                    "You have not set how many administrations per day for your taper!");

            return false;
        }

        return true;
    }

    /**
     * Method checks to make sure that if the user wants the constraints that they are properly set.
     *
     * @return boolean true for valid & vice versa
     */
    private boolean validateConstraints() {
        CheckBox cbxConstrained = findViewById(R.id.cbxConstrainHoursOfTaper);

        if (cbxConstrained.isChecked() &&
                (newTaper.getStartHour() == null || newTaper.getEndHour() == null)) {
            GlobalMisc.showSimpleDialog(AddOrEditTaper.this, "No Constraints Set",
                    "You indicated with the checkbox that you want constraints set but have not " +
                    "yet set the beginning & ending hours!");

            return false;
        }

        return true;
    }

    /**
     * Spinner's listener (used for setting the Substance of the taper properly).
     *
     * @param parent AdapterView
     * @param view View
     * @param position int position selected in the Spinner's list
     * @param id long
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        /*if (firstItemSelected) {
            firstItemSelected = false;

            return;
        }*/

        newTaper.setSid(SubData.subList.get(position).getId());
    }

    /**
     * Not used in this code.
     *
     * @param parent AdapterView
     */
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //ouah ouah ouah
    }

    /**
     * Listener for the ChooseDateFragment which sets the taper's starting & target dates.
     *
     * @param year int
     * @param month int
     * @param day int
     */
    @Override
    public void onDateSelected(int year, int month, int day) {
        LocalDateTime newLDT;

        newLDT = LocalDateTime.of(year, month, day, 0, 0, 0);

        //note that these still need to be sanity checked
        if (settingStartTaper) {
            newTaper.setStartDate(newLDT);
        } else {
            newTaper.setTargetDate(newLDT);
        }
    }

    /**
     * Listener for the ChooseTimeFragment which sets the taper's constraint hours if they have been
     * selected for usage in this taper.
     *
     * @param hour int
     * @param minute int
     * @param second int (not used)
     */
    @Override
    public void onTimeSelected(int hour, int minute, int second) {
        LocalTime newLT;

        newLT = LocalTime.of(hour, minute);

        if (settingStartHour) {
            newTaper.setStartHour(newLT);
        } else {
            newTaper.setEndHour(newLT);
        }
    }
}