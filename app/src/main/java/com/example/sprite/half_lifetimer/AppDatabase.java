package com.example.sprite.half_lifetimer;

import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.room.migration.Migration;
import android.content.Context;

@Database(entities = {Substance.class, Usage.class, UsualSuspect.class, SubClassification.class,
                      SubstanceGoal.class, UsualSuspectSubsCrossRef.class, Taper.class},
          version = 20)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase INSTANCE;

    public abstract SubstanceDao getSubstanceDao();
    public abstract UsageDao getUsageDao();
    public abstract UsualSuspectDao getUsualSuspectDao();
    public abstract SubClassificationDao getSubClassificationDao();
    public abstract SubstanceGoalDao getSubstanceGoalDao();
    public abstract UsualSuspectSubsCrossRefDao getUsualSuspectSubsCrossRefDao();
    public abstract TaperDao getTaperDao();

    public static AppDatabase getDatabase(Context ctxt) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(ctxt.getApplicationContext(), AppDatabase.class,
                                            GlobalMisc.DbName)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    //.addMigrations(MIGRATION_1_2)
                    //.addMigrations(MIGRATION_2_3)
                    //upward migrations follow
                    .addMigrations(MIGRATION_3_4)
                    .addMigrations(MIGRATION_4_5)
                    .addMigrations(MIGRATION_5_6)
                    .addMigrations(MIGRATION_6_7)
                    .addMigrations(MIGRATION_7_8)
                    .addMigrations(MIGRATION_8_9)
                    .addMigrations(MIGRATION_9_10)
                    .addMigrations(MIGRATION_10_11)
                    .addMigrations(MIGRATION_11_12)
                    .addMigrations(MIGRATION_12_13)
                    .addMigrations(MIGRATION_13_14)
                    .addMigrations(MIGRATION_14_15)
                    .addMigrations(MIGRATION_15_16)
                    .addMigrations(MIGRATION_16_17)
                    .addMigrations(MIGRATION_17_18)
                    .addMigrations(MIGRATION_18_19)
                    .addMigrations(MIGRATION_19_20)
                    //downward migrations follow
                    .addMigrations(MIGRATION_5_4)
                    .addMigrations(MIGRATION_7_6)
                    .addMigrations(MIGRATION_8_7)   //wipes SubstanceGoal
                    .addMigrations(MIGRATION_9_8)
                    .addMigrations(MIGRATION_16_15)
                    .build();
        }
        return INSTANCE;
    }

    //upward migrations
    static final Migration MIGRATION_3_4 = new Migration(3, 4) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            //database.execSQL("CREATE TABLE IF NOT EXISTS `UsualSuspect` (`id` INTEGER, `name` " +
            //        "VARCHAR NOT NULL, `sid` " + "INTEGER NOT NULL, `dosage` REAL NOT NULL, " +
            //        "`notes` VARCHAR NOT NULL, PRIMARY KEY (`id`))");
            database.execSQL("CREATE TABLE IF NOT EXISTS `UsualSuspect` " +
                    "(`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "`name` TEXT, `sid` INTEGER NOT NULL, `dosage` REAL NOT NULL, " +
                    "`notes` TEXT)");
        }
    };

    static final Migration MIGRATION_4_5 = new Migration(4, 5) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            //drop the table if it exists due to previous creation attempts
            database.execSQL("DROP TABLE IF EXISTS `SubClassification`");
            //now create things (again, maybe)
            database.execSQL("CREATE TABLE IF NOT EXISTS `SubClassification` " +
                    "(`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "`name` TEXT, `description` TEXT)");
            database.execSQL("ALTER TABLE `Substance` ADD `sub_class` INTEGER NOT NULL " +
                    "DEFAULT (-1)");
        }
    };

    static final Migration MIGRATION_5_6 = new Migration(5, 6) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            //create new table (ugh)
            database.execSQL("CREATE TABLE IF NOT EXISTS `new_Substance` " +
                    "(`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "`units` INTEGER, `cname` TEXT, `sname` TEXT, " +
                    "`halflife` FLOAT NOT NULL, `detectable_halflife` FLOAT NOT NULL, " +
                    "`lipid_soluble` INTEGER NOT NULL, `sub_class` INTEGER NOT NULL " +
                    "DEFAULT (-1))");

            //insert old table's data into the new one
            database.execSQL("INSERT INTO `new_Substance` " +
                    "SELECT * FROM `Substance`");
            //delete the old table
            database.execSQL("DROP TABLE IF EXISTS `Substance`");
            //rename to the original table's name (talk about a pain in the ass here)
            database.execSQL("ALTER TABLE `new_Substance` RENAME TO `Substance`");
        }
    };

    static final Migration MIGRATION_6_7 = new Migration(6, 7) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            //just in case of a previous migration attempt that failed *cough*
            database.execSQL("DROP TABLE IF EXISTS `new_Usage`");
            //all we should have to do here is add the new column (valid_entry)
            database.execSQL("ALTER TABLE `Usage` ADD `valid_entry` INTEGER NOT NULL " +
                    "DEFAULT (1)");
        }
    };

    static final Migration MIGRATION_7_8 = new Migration(7, 8) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("DROP TABLE IF EXISTS `SubstanceGoal`");
            database.execSQL("CREATE TABLE IF NOT EXISTS `SubstanceGoal` " +
                    "(`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "`sub_id` INTEGER NOT NULL, `target_units_per_day` REAL NOT NULL)");
        }
    };

    //this is fairly foolish, but I really borked the migration numbers/schema somehow, and had to
    //make a bogus upgrade/downgrade to & from 9 because of an error I caused.  :-?(beep)  Room has
    //some work to go in some areas I guess
    static final Migration MIGRATION_8_9 = new Migration(8, 9) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {

        }
    };

    //this one is going to be a little more complicated; working with changing relationships between
    //tables to a many-to-many relationship involving a new crossref table, etc
    static final Migration MIGRATION_9_10 = new Migration(9, 10) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE IF NOT EXISTS `UsualSuspectSubsCrossRef`" +
                    //"(`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "(`usualSuspect` INTEGER NOT NULL, `sub` INTEGER NOT NULL, " +
                    "`dosage` REAL NOT NULL, PRIMARY KEY(`usualSuspect`, `sub`))");
            database.execSQL("INSERT INTO `UsualSuspectSubsCrossRef` " +
                    "SELECT `id`, `sid`, `dosage` FROM `UsualSuspect`");
            database.execSQL("DROP TABLE IF EXISTS `new_UsualSuspect`");
            database.execSQL("CREATE TABLE IF NOT EXISTS `new_UsualSuspect` " +
                    "(`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "`name` TEXT, `notes` TEXT)");
            database.execSQL("INSERT INTO `new_UsualSuspect` " +
                    "SELECT `id`, `name`, `notes` FROM `UsualSuspect`");
            database.execSQL("DROP TABLE `UsualSuspect`");
            database.execSQL("ALTER TABLE `new_UsualSuspect` RENAME TO `UsualSuspect`");
        }
    };

    //yet another migration to add the Taper table to our database in support of Tapering.java
    static final Migration MIGRATION_10_11 = new Migration(10, 11) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE IF NOT EXISTS `Taper`" +
                    "(`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "`name` TEXT, `target_date` INTEGER, " +
                    "`substance_id` INTEGER NOT NULL, `daily_dosage_target` REAL NOT NULL)");
        }
    };

    //ouah
    static final Migration MIGRATION_11_12 = new Migration(11, 12) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            //ouah ouah ouah
        }
    };

    //forgot a few fields in the last one; at least there's nothing worth saving
    static final Migration MIGRATION_12_13 = new Migration(12, 13) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("DROP TABLE IF EXISTS `Taper`");
            database.execSQL("CREATE TABLE IF NOT EXISTS `Taper`" +
                    "(`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "`name` TEXT, `target_date` INTEGER, `start_date` INTEGER, " +
                    "`substance_id` INTEGER NOT NULL, `daily_dosage_target` REAL NOT NULL, " +
                    "`daily_dosage_start` REAL NOT NULL)");
        }
    };

    //need to add a validity flag to the Taper records
    static final Migration MIGRATION_13_14 = new Migration(13, 14) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE `Taper` ADD COLUMN `valid` INTEGER DEFAULT (1) NOT NULL");
        }
    };

    //need to add an archival flag to Substance records
    static final Migration MIGRATION_14_15 = new Migration(14, 15) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE `Substance` ADD COLUMN `archived` " +
                    "INTEGER DEFAULT (0) NOT NULL");
        }
    };

    //add tapering schedule data to Taper
    static final Migration MIGRATION_15_16 = new Migration(15, 16) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            //let's try using this previously bungled Migration attempt to add what we need to
            //Taper in order to save taper scheduling data & add that capability (see also
            //git branch: feature/tapering-capability)
            //NOTE: there is nothing worth saving in this table
            /*database.execSQL("DROP TABLE IF EXISTS `Taper`");
            database.execSQL("CREATE TABLE IF NOT EXISTS `Taper`" +
                    "(`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "`name` TEXT, `target_date` INTEGER, `start_date` INTEGER, " +
                    "`substance_id` INTEGER NOT NULL, `daily_dosage_target` REAL NOT NULL, " +
                    "`daily_dosage_start` REAL NOT NULL, `average_interval` REAL NOT NULL, " +
                    "`average_dosage` REAL NOT NULL)");*/
            database.execSQL("ALTER TABLE `Taper` ADD COLUMN `average_interval` REAL " +
                    "DEFAULT (0) NOT NULL");
            database.execSQL("ALTER TABLE `Taper` ADD COLUMN `average_dosage` REAL " +
                    "DEFAULT (0) NOT NULL");
        }
    };

    //remove the previous bogus columns from Taper
    static final Migration MIGRATION_16_17 = new Migration(16, 17) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("DROP TABLE IF EXISTS `new_Taper`");
            database.execSQL("CREATE TABLE IF NOT EXISTS `new_Taper` " +
                    "(`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "`name` TEXT, `target_date` INTEGER, `start_date` INTEGER, " +
                    "`substance_id` INTEGER NOT NULL, `daily_dosage_target` REAL NOT NULL, " +
                    "`daily_dosage_start` REAL NOT NULL, `valid` INTEGER DEFAULT (1) NOT NULL)");
            database.execSQL("INSERT INTO `new_Taper` " +
                    "SELECT `id`, `name`, `target_date`, `start_date`, `substance_id`, " +
                    "`daily_dosage_target`, `daily_dosage_start`, `valid` FROM `Taper`");
            database.execSQL("DROP TABLE `Taper`");
            database.execSQL("ALTER TABLE `new_Taper` RENAME TO `Taper`");
        }
    };

    //add some new information that we need in there
    static final Migration MIGRATION_17_18 = new Migration(17, 18) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE `Taper` ADD COLUMN `admins_per_day` INTEGER " +
                    "DEFAULT (1) NOT NULL");
        }
    };

    //and now we're going to need to add the columns for constraining taper admins solely to
    //specific portions of the day
    static final Migration MIGRATION_18_19 = new Migration(18, 19) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE `Taper` ADD COLUMN `start_hour` INTEGER");
            database.execSQL("ALTER TABLE `Taper` ADD COLUMN `end_hour` INTEGER");
        }
    };

    //add discrete dosage steps capability for certain tapers
    static final Migration MIGRATION_19_20 = new Migration(19, 20) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE `Taper` ADD COLUMN `dosage_interval` REAL DEFAULT (0) " +
                    "NOT NULL");
        }
    };

    //downward migrations
    static final Migration MIGRATION_5_4 = new Migration(5, 4) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            //we are unable to DROP a COLUMN in SQLite, so we're going to have to create a new table
            //and copy everything to it sans that column, then delete the old table, and finally
            //renaming the new table to the old table :|  great feature, guys!
            //if we had a failed migration previously, wipe new_Substance
            database.execSQL("DROP TABLE IF EXISTS `new_Substance`");
            //create new table
            database.execSQL("CREATE TABLE IF NOT EXISTS `new_Substance` " +
                    "(`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "`units` INTEGER, `cname` TEXT, `sname` TEXT, " +
                    "`halflife` INTEGER NOT NULL, `detectable_halflife` INTEGER NOT NULL, " +
                    "`lipid_soluble` INTEGER NOT NULL)");
            //insert relevant old table's data into the new one
            database.execSQL("INSERT INTO `new_Substance` " +
                    "SELECT `id`, `units`, `cname`, `sname`, `halflife`, `detectable_halflife`, " +
                    "`lipid_soluble` FROM `Substance`");
            //delete the old table
            database.execSQL("DROP TABLE IF EXISTS `Substance`");
            //rename the new one to the name of the old one
            database.execSQL("ALTER TABLE `new_Substance` RENAME TO `Substance`");
            //hopefully that'll do it :|
        }
    };

    static final Migration MIGRATION_7_6 = new Migration(7, 6) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            //create our new temporary table (just going to insert the VALID entries in  here to
            //avoid putting our database into a way invalid state with duplicate consolidated
            //entries
            database.execSQL("DROP TABLE IF EXISTS `new_old_Usage`");
            //new table
            database.execSQL("CREATE TABLE IF NOT EXISTS `new_old_Usage` " +
                    "(`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "`sub_id` INTEGER NOT NULL, `dosage` FLOAT NOT NULL, " +
                    "`timestamp` FLOAT NOT NULL, `notes` TEXT)");
            //insert only the valid old new table's data into the new old one (I know, confusing)
            database.execSQL("INSERT INTO `new_old_Usage` " +
                    "SELECT `id`, `sub_id`, `dosage`, `timestamp`, `notes` FROM `Usage` " +
                    "WHERE `valid_entry` LIKE (1)");    //1 is true, right?
            //delete the old new table
            database.execSQL("DROP TABLE IF EXISTS `Usage`");
            //rename the temp table to our new 'old' table name
            database.execSQL("ALTER TABLE `new_old_Usage` RENAME TO `Usage`");
            //god SQLite sucks
        }
    };

    static final Migration MIGRATION_8_7 = new Migration(8, 7) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("DROP TABLE IF EXISTS `SubstanceGoal`");
        }
    };

    //this is fairly foolish, but I really borked the migration numbers/schema somehow, and had to
    //make a bogus upgrade/downgrade to & from 9 because of an error I caused.  :-?(beep)  Room has
    //some work to go in some areas I guess
    static final Migration MIGRATION_9_8 = new Migration(9, 8) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {

        }
    };

    /*static final Migration MIGRATION_10_9 = new Migration(10, 9) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("DROP TABLE IF EXISTS `new_UsualSuspect`");
            database.execSQL("CREATE TABLE IF NOT EXISTS `new_UsualSuspect` " +
                    "(`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "`name` TEXT, `sid` INTEGER NOT NULL, `dosage` REAL NOT NULL, " +
                    "`notes` TEXT)");
            database.execSQL("INSERT INTO `new_UsualSuspect` " +
                    "")
        }
    };*/

    static final Migration MIGRATION_16_15 = new Migration(16, 15) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            //ouah ouah ouah - nothing changed in this migration in the first place
        }
    };

    public static void destroyInstance() { INSTANCE = null; }
}
