package com.example.sprite.half_lifetimer;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database(entities = {UsualSuspect.class, Substance.class, UsualSuspectSubsCrossRef.class},
          version = 2)
@TypeConverters({Converters.class})
public abstract class UsualSuspectDbase extends RoomDatabase {
    public abstract UsualSuspectDao usualSuspectDao();
}
