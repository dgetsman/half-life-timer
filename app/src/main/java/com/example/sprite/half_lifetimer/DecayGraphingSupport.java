package com.example.sprite.half_lifetimer;

import android.content.Context;
import android.graphics.Color;
import android.widget.Toast;

import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.time.LocalDateTime;
import java.time.Duration;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class DecayGraphingSupport {
    /**
     * Method scans the highestDosages HashMap and determines the highest value
     * in the structure, then returns that value.
     *
     * @param highestDosages HashMap<Substance, Double> highest dosages per sub
     * @return double highestValueOnGraph
     */
    public static double getHighestDosage(HashMap<Substance, Double> highestDosages) {
        double highestValueOnGraph = 0.0;

        for (Substance sub : highestDosages.keySet()) {
            if (highestDosages.get(sub) > highestValueOnGraph) {
                highestValueOnGraph = highestDosages.get(sub);
            }
        }

        return highestValueOnGraph;
    }

    /**
     * Calculates the substance level dosage taken, and associated decay curve
     * levels in the body afterwards, throughout the user-specified time
     * period.
     *
     * @param sid int Substance ID
     * @param startLDT LocalDateTime start of period
     * @param endLDT LocalDateTime end of period
     * @return Double[] substance levels present per hour starting @ startLDT
     */
    public static Double[] plotXDaysSubLevels(int sid, LocalDateTime startLDT,
                                              LocalDateTime endLDT) {
        Substance sub = Permanence.Subs.loadSubstanceById(sid);

        if (sub.getLipid_soluble() && !sub.isTHC()) {
            //we have no algorithm for working with this sub yet
            return null;
        }

        int cntr = 0;
        startLDT = startLDT.withHour(0)
                           .withMinute(0)
                           .withSecond(0);
        endLDT = endLDT.withHour(23)
                       .withMinute(59)
                       .withSecond(59);
        Double[] plotLevels = new Double[(int)Duration.between(startLDT, endLDT).toHours() + 1];
        float halflife;
        double dosagePresent = 0.0;
        if (sub.isTHC()) {
            halflife = 24f * 1.4f;
        } else {
            halflife = Math.max(sub.getdHalflife(), sub.getHalflife());
        }
        int dayHours = 0;

        //hourly loop from xDaysAgo until today
        for (LocalDateTime curLDT = startLDT;
             curLDT.isBefore(endLDT);
             curLDT = curLDT.plusHours(1)) {
            List<Usage> applicableUsages =
                    Permanence.Admins.getBetweenSpanValidUsagesForSub(sid,
                            Converters.fromLocalDateTime(curLDT),
                            Converters.fromLocalDateTime(curLDT.plusHours(1)));
            //plotLevels[cntr] = 0.0;

            if (!sub.isTHC()) {
                //determine our current body levels/saturation for this hour
                //first let's drop our plot level by whatever metabolism has taken place this hour
                dosagePresent = dosagePresent / Math.pow(2f, (1f / halflife));

                //get our dosage total for this hour
                for (Usage use : applicableUsages) {
                    dosagePresent += use.getDosage();
                }

                //wipe it to 0 if we're below the threshold
                if (dosagePresent < GlobalMisc.PlasmaSolubleAllGone) {
                    dosagePresent = 0.0;
                }
            } else {
                //working with THC, at least we have a bit of an algorithm for that
                double administrationBump = (1f / 8f);

                dosagePresent = dosagePresent / Math.pow(2f, (1f / halflife));

                if (dayHours == 0 && (applicableUsages.size() > 0)) {
                    //we've had our administration for this 24 hour block
                    dayHours = 24;  //start the countdown until we can add another if necessary
                    dosagePresent += administrationBump;

                    if (dosagePresent > 1) {
                        dosagePresent = 1;  //it's a percentage, no point in going over 100%
                    } else if (dosagePresent < GlobalMisc.LipidSolubleAllGone) {
                        dosagePresent = 0;  //metabolism sopped all of the rest up
                    }
                }
            }

            if (dayHours > 0) {
                dayHours--;
            }
            plotLevels[cntr++] = dosagePresent;
        }

        return plotLevels;
    }

    /**
     * Method goes through the values in decayCurves, determining the highest
     * value on the graph and the multiplier through that information, applying
     * the multiplier while it creates the DataPoint[][] structure for eventual
     * addition to the graph.  Method then returns that structure.
     *
     * TODO: rework this so that it's not passing/needing duplicate level info
     *
     * @param substanceLevels Double[][] substanceLevels
     * @param highestValueOnGraph double highest value on the entire graph
     * @param highestDosages HashMap<Substance, Double> highest dosage per substance
     * @param startLDT LocalDateTime starting point for the graph
     * @param endLDT LocalDateTime ending point for the graph
     * @return DataPoint[][] ready for graphing
     */
    public static DataPoint[][] getAndApplyMultipliers(List<Double[]> substanceLevels,
                                                       double highestValueOnGraph,
                                                       HashMap<Substance, Double> highestDosages,
                                                       LocalDateTime startLDT,
                                                       LocalDateTime endLDT) {
        //create datapoints structure
        DataPoint[][] eliminationPoints =
                new DataPoint[highestDosages.keySet().size()][(int)Duration.between(
                        startLDT, endLDT).toHours()];

        //fill structure while applying multiplier
        int cntr = 0;
        for (Substance clearingSub : highestDosages.keySet()) {
            //set up our LDTs for this substance
            LocalDateTime applicableLDT = startLDT;
            Date applicableDate = GlobalMisc.getProperMidnightDateFromLDT(applicableLDT);

            //determine multiplier & apply
            double multiplier = highestValueOnGraph / highestDosages.get(clearingSub);
            if (multiplier != 1.0) {
                for (int cntr3 = 0; cntr3 < substanceLevels.get(cntr).length; cntr3++) {
                    //tack onto our DataPoints array
                    double tmpSubLevel = substanceLevels.get(cntr)[cntr3];

                    if (tmpSubLevel < 0) {
                        eliminationPoints[cntr][cntr3] = new DataPoint(applicableDate, -1);
                    } else if (tmpSubLevel < 0.0009) {
                        eliminationPoints[cntr][cntr3] = new DataPoint(applicableDate, 0);
                    } else {
                        eliminationPoints[cntr][cntr3] = new DataPoint(applicableDate,
                                substanceLevels.get(cntr)[cntr3] * multiplier);
                    }

                    applicableLDT = applicableLDT.plusHours(1);
                    applicableDate = GlobalMisc.fromLDTToDate(applicableLDT);
                }
            } else {
                for (int cntr3 = 0; cntr3 < substanceLevels.get(cntr).length; cntr3++) {
                    eliminationPoints[cntr][cntr3] = new DataPoint(applicableDate,
                            substanceLevels.get(cntr)[cntr3]/* * multiplier*/);

                    applicableLDT = applicableLDT.plusHours(1);
                    applicableDate = GlobalMisc.fromLDTToDate(applicableLDT);
                }
            }

            GlobalMisc.debugMsg("plotDecayCurves", "using multiplier: " +
                    multiplier + " for substance: " + clearingSub.getCommon_name() + "'s graph");

            cntr++;
        }

        return eliminationPoints;
    }

    /**
     * Method simply uses a long toast notification to let the user know that
     * the graph data is being crunched and the results will be in shortly.
     *
     * @param ctxt Context current application context
     */
    public static void notifyUserOfGraphCrunchingWait(Context ctxt) {
        Toast.makeText(ctxt, "Crunching graph data, please wait...",
                Toast.LENGTH_LONG).show();
    }

    /**
     * Method uses a user-specified time interval notification to let the user
     * know that the graph data is being crunched and the results will be in
     * shortly.
     *
     * @param ctxt Context current application context
     * @param toastLength int either Toast.LENGTH_LONG or Toast.LENGTH_SHORT
     */
    public static void notifyUserOfGraphCrunchingWait(Context ctxt, int toastLength) {
        Toast.makeText(ctxt, "Crunching graph data, please wait...", toastLength).show();
    }


    public static LineGraphSeries<DataPoint> getDosageTaperLevelDataPoints(Taper applicableTaper) {
        //we'll have to iterate through each day between the start & end of the taper
        long taperDays = Duration.between(applicableTaper.getStartDate(),
                applicableTaper.getTargetDate()).toDays();

        DataPoint[] points = new DataPoint[(int) taperDays];

        for (int cntr = 0; cntr < (int)taperDays; cntr++) {
            points[cntr] = new DataPoint(GlobalMisc.convertLocalDateTimeToDate(
                    applicableTaper.getStartDate().plusDays((long)cntr)),
                    applicableTaper.findXDayDosageMax(
                            applicableTaper.getStartDate().plusDays((long)cntr)));
        }

        LineGraphSeries<DataPoint> lgSeries = new LineGraphSeries<>(points);
        lgSeries.setColor(Color.BLACK);

        return new LineGraphSeries<>(points);
    }
}
