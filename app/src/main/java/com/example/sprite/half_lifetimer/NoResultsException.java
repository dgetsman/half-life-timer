package com.example.sprite.half_lifetimer;

public class NoResultsException extends Exception {
    public String toString() {
        return "No results were found for your query.";
    }
}
