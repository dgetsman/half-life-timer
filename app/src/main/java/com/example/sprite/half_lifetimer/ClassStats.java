package com.example.sprite.half_lifetimer;

import android.annotation.SuppressLint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@SuppressLint("UseSparseArrays")
public class ClassStats {
    private int     numberOfSubs;
    private int     totalUses;
    private List<Integer> sIdList = new ArrayList<>();

    private HashMap<Integer, Float> totalDosagePerSub = new HashMap<>();
    private HashMap<Integer, Float> averageDosagePerSub = new HashMap<>();
    private HashMap<Integer, Integer> numUsesPerSub = new HashMap<>();
    private HashMap<Integer, String> subNames = new HashMap<>();

    public ClassStats(int classId) {
        List<Substance> classSubs = Permanence.Subs.loadUnarchivedSubstanceBySClass(classId);
        this.numberOfSubs = classSubs.size();

        for (Substance sub : classSubs) {
            this.sIdList.add(sub.getId());

            this.subNames.put(sub.getId(), sub.getCommon_name());
        }

        calculateTotalDosagePerSub();   //populates totalDosagePerSub, numUsesPerSub, and
                                        //totalUses
        calculateAverageDosesPerSub();  //just populates averageDosagePerSub
    }

    public int getNumberOfSubs() {
        return numberOfSubs;
    }

    public void setNumberOfSubs(int numberOfSubs) {
        this.numberOfSubs = numberOfSubs;
    }

    public int getTotalUses() {
        return totalUses;
    }

    public void setTotalUses(int totalUses) {
        this.totalUses = totalUses;
    }

    public List<Integer> getSIdList() {
        return sIdList;
    }

    public void setSIdList(List<Integer> sIds) {
        this.sIdList = sIds;
    }

    public HashMap<Integer, Float> getTotalDosagePerSub() {
        return totalDosagePerSub;
    }

    public void setTotalDosagePerSub(int subId, float totalDosage) {
        this.totalDosagePerSub.put(subId, totalDosage);
    }

    public HashMap<Integer, Integer> getNumUsesPerSub() {
        return numUsesPerSub;
    }

    public void setNumUsesPerSub(int subId, int numUses) {
        this.numUsesPerSub.put(subId, numUses);
    }

    public HashMap<Integer, String> getSubNames() {
        return subNames;
    }

    public void setSubNames(HashMap<Integer, String> subNames) {
        this.subNames = subNames;
    }

    public String getSubName(int sId) {
        return this.subNames.get(sId);
    }

    public HashMap<Integer, Float> getAverageDosagePerSub() {
        return averageDosagePerSub;
    }

    /**
     * Method will handle calculating the average dosages per sub; note that
     * this requires totalDosagePerSub and numUsesPerSub to be set with sane
     * values prior to utilization here.
     *
     * @return boolean true if successful, else false
     */
    private boolean calculateAverageDosesPerSub() {
        if (this.totalDosagePerSub == null || this.numUsesPerSub == null) {
            return false;   //error condition; I guess we should probably throw
                            //an exception, but I'll implement that l8r
        }

        for (int subId : totalDosagePerSub.keySet()) {
            this.averageDosagePerSub.put(subId,
                    this.totalDosagePerSub.get(subId) / this.numUsesPerSub.get(subId));
        }

        return true;
    }

    /**
     * Method will handle calculating the total dosages per sub; note that this
     * requires sIdList to be properly initialized.  This also handles tallying
     * all of the usages for the class.
     *
     * @return boolean true if successful, else false
     */
    private boolean calculateTotalDosagePerSub() {
        if (sIdList == null) {
            return false;
        }

        int tmpTotalUsesAllSubs = 0;
        for (int sid : sIdList) {
            int tmpUsesTotal = 0;
            float runningTotal = (float) 0.0;

            for (Usage use : Permanence.Admins.loadValidUsagesBySubId(sid)) {
                tmpTotalUsesAllSubs++;
                tmpUsesTotal++;
                runningTotal += use.getDosage();
            }

            this.totalDosagePerSub.put(sid, runningTotal);
            this.numUsesPerSub.put(sid, tmpUsesTotal);
            this.totalUses = tmpTotalUsesAllSubs;
        }

        return true;
    }
}
