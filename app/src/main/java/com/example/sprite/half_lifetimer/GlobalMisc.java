package com.example.sprite.half_lifetimer;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import androidx.core.content.ContextCompat;
import android.text.InputType;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Class provides static methods and other globally useful routines.
 */
public class GlobalMisc {
    public static Boolean           Debugging = true;
    public static String            DbName = "halflife-db";
    public static int               ConsolidateMin = 1;
    public static int               ConsolidateMax = 120;
    public static int               DaysToGraph = 30;
    public static int               MaxConnectionAttempts = 10;
    public static float             HalflifeFullEliminationMultiplicand = 5.7f;
    public static float             AverageHumanWeight = 177.5f;    //this value taken from
                                // https://www.theaveragebody.com/average-weight-for-men-and-women/
    public static ArrayList<Integer>   RoyGBivInts = null;
    public static double            LipidSolubleAllGone = 0.035;
    public static double            PlasmaSolubleAllGone = 0.01;
    public static double            DoseRangeMax = 1500;
    public static double            DoseRangeMin = .0001;


    /**
     * Method shows an AlertDialog with whatever title and waits for the user to enter a text
     * response, then returning aforementioned response via the listener.
     *
     * @param ctxt Context
     * @param title String title of the dialog
     * @param prePopulateText text to prepopulate the EditText box with (null if not to be used)
     * @param listener OnDialogResultListener
     */
    public static void showDialogGetString(Context ctxt, String title, String prePopulateText,
                                           OnDialogResultListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctxt);
                builder.setTitle(title);

                final EditText input = new EditText(ctxt);
                final OnDialogResultListener fListener = listener;

                if (prePopulateText != null) {
                    input.setText(prePopulateText);
                }

                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

                //buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //m_Text = input.getText().toString();

                        fListener.onResult(input.getText().toString());

                        //not sure what to do with the callback here in order to get m_Text where
                        //it needs to be
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
    }

    /**
     * Listener for the showDialogGetString() method above.
     */
    public interface OnDialogResultListener {
        void onResult(String result);
    }

    /**
     * Logs a message to Log.d with the method string prepended, should Debugging be true.
     *
     * @param method String name of the method from which debugMsg was called
     * @param notification String/message notification text
     */
    public static void debugMsg(String method, String notification) {
        if (Debugging) {
            //Toast.makeText(context, notification, Toast.LENGTH_LONG).show();
            Log.d(method, notification);
        }
    }

    public static void debugLog(String method, String notification) {
        if (Debugging) {
            Log.d(method, notification);
        }
    }

    /**
     * Method converts a DosageUnit enum value to a String for the user.
     *
     * @param units DosageUnit what we need the string for
     * @return String
     */
    public static String dUnitsToString(DosageUnit units) {
        switch (units) {
            case MCG:
                return "mcg";
            case MG:
                return "mg";
            case G:
                return "g";
            case ML:
                return "ml";
            case TSP:
                return "tsp";
            case TBSP:
                return "Tbsp";
            case DOSE:
                return "dose";
            default:
                return "error";
        }
    }

    /**
     * Method shows a simple OK/cancel dialog box with the following parameters set.
     *
     * @param ctxt Context
     * @param title String title of the dialog
     * @param message Message text for the dialog
     */
    public static void showSimpleDialog(Context ctxt, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctxt);
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //ouah
                        dialog.dismiss();
                    }
                })
                .show();
    }

    /**
     * Method shows a dialog, much like showSimpleDialog, but then obtains confirmation that the
     * message was seen, with an option to dismiss instead of clicking OK.
     *
     * @param ctxt Context
     * @param title Message box title
     * @param message Body of the message
     * @param listener Returns value on whether or not OK or dismiss was selected by the user
     */
    public static void showDialogGetConfirmation(Context ctxt, String title, String message,
                                                 OnDialogConfResultListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctxt);
        builder.setTitle(title);
        builder.setMessage(message);

        final OnDialogConfResultListener fListener = listener;

        //buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                fListener.onResult(true);
                dialog.dismiss();

                debugMsg("showDialogGetConfirmation",
                        "Still able to perform actions after dialog is closed");
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                fListener.onResult(false);
                dialog.cancel();
            }
        });
        builder.show();
    }

    public static void showDialogGetConfirmation(Context ctxt, String title, String message,
                                                 String cancelMessage,
                                                 OnDialogConfResultListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctxt);
        builder.setTitle(title);
        builder.setMessage(message);

        final OnDialogConfResultListener fListener = listener;

        //buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                fListener.onResult(true);
                dialog.dismiss();

                debugMsg("showDialogGetConfirmation",
                        "Still able to perform actions after dialog is closed");
            }
        });
        builder.setNegativeButton(cancelMessage, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                fListener.onResult(false);
                dialog.cancel();
            }
        });
        builder.show();
    }

    /**
     * Listener for the showDialogGetConfirmation() method above.
     */
    public interface OnDialogConfResultListener {
        void onResult(boolean result);
    }

    /**
     * Method determines whether or not the LDT passed is during the same day's interval
     *
     * @param currentLDT LocalDateTime for the current day to be tested against
     * @param usageLDT LocalDateTime for the usage to test against the currentLDT
     * @return boolean
     */
    public static boolean isSameDay(LocalDateTime currentLDT, LocalDateTime usageLDT) {
        return (usageLDT.getYear() == currentLDT.getYear() &&
                usageLDT.getMonth() == currentLDT.getMonth() &&
                usageLDT.getDayOfMonth() == currentLDT.getDayOfMonth());
    }

    /**
     * Method provides a simple way to break down a Duration into a [more] human-readable string
     *
     * @param span Duration to break down
     * @return String value of the human readable string
     */
    public static String durationToString(Duration span) {
        Duration tmpSpan = Duration.ZERO;
        long days = span.toDays();
        long hours;
        long minutes;

        tmpSpan = tmpSpan.plus(days, ChronoUnit.DAYS);
        hours = span.minus(tmpSpan).toHours();
        tmpSpan = tmpSpan.plus(hours, ChronoUnit.HOURS);
        minutes = span.minus(tmpSpan).toMinutes();

        return days + "d " + hours + "h " + minutes + "m ";
    }

    /**
     * Method presumably converts a LocalDateTime to just a Date.
     *
     * @param dateToConvert LDT to convert to a Date
     * @return Date from the aforementioned LDT
     */
    public static Date convertLocalDateTimeToDate(LocalDateTime dateToConvert) {
        return java.util.Date.from(dateToConvert.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * This method returns a BarGraphSeries for the datapoints for usages since midnight of the
     * current day, for the substance index given.
     *
     * @param subNdx substance's index
     * @return BarGraphSeries for the aforementioned data, listed by hour
     */
    public static BarGraphSeries<DataPoint> getPastDayGraphSeries(int subNdx) {
        float runningTotal;
        int cntr = 0;
        DataPoint[] points = new DataPoint[25];

        LocalDateTime nowHour = LocalDateTime.now()
                .withHour(0)
                .withMinute(0)
                .withSecond(0);

        for (LocalDateTime curHour = nowHour.minusHours(24); !curHour.isAfter(nowHour);
             curHour = curHour.plusHours(1)) {
            runningTotal = 0.0f;
            List<Usage> applicableUsages = Permanence.Admins.getBetweenSpanValidUsagesForSub(subNdx,
                    Converters.fromLocalDateTime(curHour),
                    Converters.fromLocalDateTime(curHour.withMinute(59).withSecond(59)));

            for (Usage use : applicableUsages) {
                runningTotal += use.getDosage();
            }

            //points[cntr++] = new DataPoint(convertLocalDateTimeToDate(curHour), runningTotal);
            points[cntr++] = new DataPoint((24 - cntr) * -1, runningTotal);
        }

        return new BarGraphSeries<>(points);

        /*LocalDateTime todayMidnight = LocalDateTime.of(now.getYear(), now.getMonth(),
                now.getDayOfMonth(), 0, 0);

        List<Usage> applicableUsages =
                Permanence.Admins.getPostToTSOrderedUsageRecords(subNdx,
                        Converters.fromLocalDateTime(todayMidnight));

        float[] hourlyDosages = new float[24];
        for (int cntr = 0; cntr < 24; cntr++) {
            hourlyDosages[cntr] = 0;
        }

        for (int cntr = 0; cntr < applicableUsages.size(); cntr++) {
            hourlyDosages[applicableUsages.get(cntr).getTimestamp().getHour()] +=
                    applicableUsages.get(cntr).getDosage();
        }

        DataPoint[] points = new DataPoint[24];
        //int firstHour = usages.get(0).getTimestamp().getHour();

        for (int cntr = 0; cntr < 24; cntr++) {
            //convert the first datapoint to a scale matching the hour of the administration
            /*int thisHour = cntr + firstHour;
            if (thisHour > 24) {
                thisHour -= 24;
            }

            points[cntr] = new DataPoint(thisHour, hourlyDosages[cntr]);*/
            /*points[cntr] = new DataPoint(cntr, hourlyDosages[cntr]);
        }

        return new LineGraphSeries<>(points);*/
    }

    /**
     * Method takes a specific substance's index #, and the number of days to
     * go back for data, then graphing the dosage on a per-hour basis for
     * however many days were specified.  Returns the BarGraphSeries needed for
     * graphing.
     *
     * @param subNdx substance's index
     * @param days number of days to go back in data
     * @return
     */
    public static BarGraphSeries<DataPoint> getPastXDaysGraphSeries(int subNdx, int days) {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime todayMidnight = LocalDateTime.of(now.getYear(), now.getMonth(),
                now.getDayOfMonth(), 0, 0);
        LocalDateTime pastXDayMidnight = todayMidnight.minusDays(days - 1);

        GlobalMisc.debugMsg("getPastXDaysGraphSeries", pastXDayMidnight.toString());

        List<Usage> applicableUsages =
                Permanence.Admins.getPostToTSOrderedUsageRecords(subNdx,
                        Converters.fromLocalDateTime(pastXDayMidnight));

        GlobalMisc.debugMsg("getPastXDaysGraphSeries", "size of " +
                "applicableUsages: " + applicableUsages.size());

        float[][] hourlyDosages = new float[days][24];
        for (int cntr = 0; cntr < days; cntr++) {
            for (int cntr2 = 0; cntr2 < 24; cntr2++) {
                hourlyDosages[cntr][cntr2] = 0;

                GlobalMisc.debugMsg("getPastXDaysGraphSeries", "set " +
                        "hourlyDosages[" + cntr + "][" + cntr2 + "] to 0");
            }
        }

        for (Usage use : applicableUsages) {
            long daysFromDayX; /*daysToNow, hoursFromDayBegin;*/
            //LocalDateTime currentMidnight;

            //daysToNow = use.getTimestamp().until(now, ChronoUnit.DAYS);
            daysFromDayX = pastXDayMidnight.until(use.getTimestamp(), ChronoUnit.DAYS);
            //currentMidnight = pastXDayMidnight.plusDays(daysFromDayX);

            hourlyDosages[(int)daysFromDayX][use.getTimestamp().getHour()] += use.getDosage();

            /* GlobalMisc.debugMsg("getPastXDaysGraphSeries",
                    "set hourlyDosages[" + (int)daysFromDayX + "][" +
                            use.getTimestamp().getHour() + "] to: " + use.getDosage()); */
        }

        //flatten our array into the format that we need
        DataPoint[] points = new DataPoint[24 * days];
        int cntr = 0;
        for (int cntr2 = 0; cntr2 < days; cntr2++) {
            for (int cntr3 = 0; cntr3 < 24; cntr3++) {
                points[cntr] = new DataPoint(cntr++, hourlyDosages[cntr2][cntr3]);
            }
        }

        /*GlobalMisc.debugMsg("getPastXDaysGraphSeries", "length of points[]: " +
                points.length);
        GlobalMisc.debugMsg("getPastXDaysGraphSeries", "points[]: " +
                Arrays.toString(points)); */

        BarGraphSeries<DataPoint> bGraphSeries = new BarGraphSeries<>(points);

        return bGraphSeries;
    }

    public static final HashMap<Integer, String> getSubIndexNNames(Context ctxt) {
        //I think that we're going to load all of the substances into a hashmap for easier lookup
        //within memory instead of pulling from the database for each result; otherwise this is
        //going to take forever with a lot of hits
        HashMap<Integer, String> subsList = new HashMap<>();
        for (Substance sub : Permanence.Subs.loadSubstances(ctxt)) {
            subsList.put(sub.getId(), sub.getCommon_name());
        }
        final HashMap<Integer, String> fSubsList = subsList;

        return fSubsList;
    }

    /**
     * Method goes through the different existing tapers and marks them as
     * invalid if the last date of the taper has been reached.  Perhaps at
     * some point we can go through and ask if they should be extended, but
     * for now this should suffice.
     */
    public static void markInvalidTapers(Context ctxt) {
        for (Taper taper : Permanence.Tapers.loadAllValidTapers(ctxt)) {
            if (taper.getTargetDate().isBefore(LocalDateTime.now())) {
                //our taper is expired, let's mark it as invalid and resave it
                taper.setValid(false);

                Permanence.Tapers.updateTaper(taper);
            }
        }
    }

    /**
     * Method takes a substance ID #, and returns that particular substance's
     * index into the List at SubData.subList, since this is used for a few
     * different purposes throughout the code.  Almost certainly not the best
     * style or practice, but what the hell, at least it works.
     *
     * @param sid substance ID #
     * @return int substance's position in SubData.subList
     */
    public static int getSubListPositionBySid(int sid) {
        for (int cntr = 0; cntr < SubData.subList.size(); cntr++) {
            if (sid == SubData.subList.get(cntr).getId()) {
                return cntr;
            }
        }

        return -1;  //not found
    }

    public static Date getProperMidnightDateFromLDT(LocalDateTime toModifyLDT) {
        Date newDate = Date.from(toModifyLDT.atZone(ZoneId.systemDefault()).toInstant());

        newDate.setHours(0);
        newDate.setMinutes(0);
        newDate.setSeconds(0);

        return newDate;
    }

    /**
     * Method initializes and/or returns the colors of the rainbow.
     *
     * @param ctxt Context current application context
     * @return ArrayList of Colors
     */
    public static ArrayList<Integer> getRoyGBiv(Context ctxt) {
        if (RoyGBivInts == null) {
            RoyGBivInts = new ArrayList<>(Arrays.asList(
                    ContextCompat.getColor(ctxt, R.color.colorDarkRed),
                    ContextCompat.getColor(ctxt, R.color.colorDarkOrange),
                    ContextCompat.getColor(ctxt, R.color.colorWarnYellow),
                    Color.GREEN, Color.BLUE, ContextCompat.getColor(ctxt, R.color.colorIndigo),
                    ContextCompat.getColor(ctxt, R.color.colorViolet)));
        }

        return RoyGBivInts;
    }

    /**
     * Converts a Date to a LocalDateTime (in the default TZ)
     *
     * @param ouah Date
     * @return LocalDateTime
     */
    public static LocalDateTime fromDateToLDT(Date ouah) {
        LocalDateTime ouahLDT = Instant.ofEpochMilli(ouah.getTime()).atZone(
                ZoneId.systemDefault()).toLocalDateTime();

        return ouahLDT;
    }

    /**
     * Converts a LocalDateTime to a Date (in the default TZ)
     * @param ouah LocalDateTIme
     * @return Date
     */
    public static Date fromLDTToDate(LocalDateTime ouah) {
        Date ouahDate = Date.from(ouah.atZone(ZoneId.systemDefault()).toInstant());

        return ouahDate;
    }

    /**
     * Method just shows a Toast notification that the results to the user's
     * query are being compiled.
     *
     * @param ctxt Context current application context
     */
    public static void showResultsBeingCompiledMessage(Context ctxt) {
        Toast.makeText(ctxt, "Please wait, your results are being compiled...",
                Toast.LENGTH_LONG).show();
    }

    /**
     * Method takes the dosage to be validated and returns true or false
     * depending on the sanity of the dosage as per values set in GlobalMisc.
     *
     * @param dosage float dosage claimed to be administered
     * @return boolean T/F based on whether or not it is valid
     */
    public static boolean validateDosageAmount(float dosage) {
        if (dosage > DoseRangeMax || dosage < DoseRangeMin) {
            return false;
        }

        return true;
    }
}
