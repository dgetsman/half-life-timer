package com.example.sprite.half_lifetimer;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class Tapering extends AppCompatActivity {
     /**
     * Handles normal AppCompatActivity setup; also checks to see if this
     * invocation is due to a NotificationService intent, and then handles
     * toggling the appropriate bit in NotificationService.firedNotifications
     * if so.
     *
     * @param savedInstanceState no friggin' clue
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tapering);

        updateDisplay();

        //this conditional is just for working hand-in-hand with NotificationService
        if (getIntent().hasExtra("SUB_NDX")) {
            //notification was wiped
            NotificationService.firedNotifications.replace(
                    Permanence.Tapers.loadOneValidTaperBySid(
                            getIntent().getExtras().getInt("SUB_NDX")).getId(), false);

            if (GlobalMisc.Debugging) {
                Log.i("Halflife.Tapering.onCreate", "Tried to reset " +
                        "firedNotifications with a false for this taper.\nfiredNotifications: "
                        + NotificationService.firedNotifications.toString());
            }
        }
    }

    @Override
    public void onRestart() {
        super.onRestart();

        updateDisplay();
    }

    /**
     * onClick handler for the add new taper button; starts the chain of method
     * calls that will populate the taper information and, ideally, end up
     * saving the record to the database.
     *
     * @param v View
     */
    public void onAddTaperSchedule(View v) {
        Intent addTaper = new Intent(Tapering.this, AddOrEditTaper.class);
        startActivity(addTaper);
    }

    /**
     * Method updates the main activity view with information on all tapers
     * found in the database.
     */
    private void updateDisplay() {
        LinearLayout lloAllTapers = findViewById(R.id.lloTaperingSchedules);
        lloAllTapers.removeAllViewsInLayout();

        List<Taper> tapers = Permanence.Tapers.loadAllValidUnarchivedTapers();

        final List<Taper> fTapers = tapers;

        TextView[] tvwTapers = new TextView[tapers.size()];

        for (int cntr = 0; cntr < tapers.size(); cntr++) {
            tvwTapers[cntr] = new TextView(Tapering.this);
            tvwTapers[cntr].setText(getString(R.string.tapering_entry_line,
                    tapers.get(cntr).getName(),
                    Permanence.Subs.loadSubstanceById(tapers.get(cntr).getSid()).getCommon_name(),
                    tapers.get(cntr).findTodaysDosageMax()));
            tvwTapers[cntr].setTextSize(22);

            if (tapers.get(cntr).isExpired()) {
                tvwTapers[cntr].setTextColor(Color.RED);

                if (tapers.get(cntr).isExpired()) {
                    //we need to mark this invalid
                    tapers.get(cntr).setValid(false);

                    Permanence.Tapers.updateTaper(tapers.get(cntr));
                }
            } else {
                tvwTapers[cntr].setTextColor(getResources().getColor( R.color.colorDarkGreen));

                GlobalMisc.debugMsg("updateDisplay", "current taper's valid " +
                        "flag sez: " + tapers.get(cntr).getValid());
            }

            final int fCntr = cntr;
            final LinearLayout fLloAllTapers = lloAllTapers;

            //display details for the taper on a standard click
            tvwTapers[cntr].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GlobalMisc.showSimpleDialog(Tapering.this, fTapers.get(fCntr).getName() +
                            " Details", fTapers.get(fCntr).toString() +
                            "\nNext dosage due: " +
                            fTapers.get(fCntr).findNextScheduledDosageLDT().toString());
                }
            });

            //run delete confirmation dialog if the user long clicks an entry
            tvwTapers[cntr].setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    String[] choices = {
                            getResources().getString(R.string.edit_taper_data),
                            getResources().getString(R.string.delete_taper),
                            getResources().getString(R.string.view_taper),
                            getResources().getString(R.string.cancel)
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(Tapering.this);
                    builder.setTitle("Edit or Delete Taper?");
                    builder.setItems(choices, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int choice) {
                            switch (choice) {
                                case 0: //edit taper information
                                    //editTaperStart(fTapers.get(fCntr));
                                    Intent editIntent =
                                            new Intent(Tapering.this, AddOrEditTaper.class);
                                    editIntent.putExtra("TAPER_ID", fTapers.get(fCntr).getId());
                                    startActivity(editIntent);

                                    break;

                                case 1: //delete taper
                                    GlobalMisc.showDialogGetConfirmation(Tapering.this,
                                            "Delete Taper?",
                                            "Are you certain that you want to delete " +
                                                    fTapers.get(fCntr).getName() + "?",
                                            new GlobalMisc.OnDialogConfResultListener() {
                                                @Override
                                                public void onResult(boolean result) {
                                                    if (result) {
                                                        Permanence.Tapers.deleteTaper(
                                                                fTapers.get(fCntr));

                                                        fLloAllTapers.invalidate();
                                                        updateDisplay();
                                                    }
                                                }
                                            });

                                    break;

                                case 2:
                                    GlobalMisc.showSimpleDialog(Tapering.this,
                                            fTapers.get(fCntr).getName() + " Details",
                                            fTapers.get(fCntr).toString());

                                break;

                                case 3: //abort messing with this taper

                                    break;
                            }
                        }
                    });
                    builder.show();

                    return true;
                }
            });

            lloAllTapers.addView(tvwTapers[cntr]);
        }
    }
}
