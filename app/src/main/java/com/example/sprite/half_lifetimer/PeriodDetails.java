package com.example.sprite.half_lifetimer;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class PeriodDetails extends AppCompatActivity implements ChooseDateFragment.DateListener {
    private int stYear, stMonth, stDay, eYear, eMonth, eDay;
    private boolean startCompleted = false, endCompleted = false, firstRun = true;
    private ArrayList<Integer> includedSubIDs = new ArrayList<>();
    private final HashMap<Substance, Double> highestDosages = new HashMap<>();
    final private ConcurrentHashMap<Substance, LineGraphSeries<DataPoint>> includedSerii =
            new ConcurrentHashMap<>();
    private final HashMap<Substance, List<Integer>> subToColorMap = new HashMap<>();
    private Date startPeriod, endPeriod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_period_details);

        TextView tvwStartDate = findViewById(R.id.tvwStartDate);
        TextView tvwEndDate = findViewById(R.id.tvwEndDate);
        tvwStartDate.setText(getString(R.string.start_date));
        tvwEndDate.setText(getString(R.string.end_date));

        if (!GlobalMisc.Debugging) {
            GlobalMisc.showDialogGetConfirmation(PeriodDetails.this, "Enter your" +
                            " period range","You will be shown two date selection" +
                            " fragments; please select the starting date for your range in the " +
                            "first one and ending date of your range in the second.",
                    new GlobalMisc.OnDialogConfResultListener() {
                        @Override
                        public void onResult(boolean result) {
                            if (result) {
                                ChooseDateFragment chooseDateFragment = new ChooseDateFragment();
                                chooseDateFragment.show(getSupportFragmentManager(), "");
                                Bundle args = new Bundle();
                                args.putBoolean("startCompleted", startCompleted);
                                chooseDateFragment.setArguments(args);
                            }
                        }
                    });
        } else {
            ChooseDateFragment chooseDateFragment = new ChooseDateFragment();
            chooseDateFragment.show(getSupportFragmentManager(), "");
            Bundle args = new Bundle();
            args.putBoolean("startCompleted", startCompleted);
            chooseDateFragment.setArguments(args);
        }
    }

    /**
     * Method handles the onClick action for the date selection button.  This
     * will first set up a calendar fragment to pick out the starting date and
     * then the ending date for the time period selection.
     *
     * @param view View requiem for an onClick()
     */
    public void ShowDatePicker(View view) {
        if (startCompleted && endCompleted) {
            LinearLayout lloSubsList = findViewById(R.id.lloSubstancesList);
            LinearLayout lloAdminsList = findViewById(R.id.lloAdministrationsList);
            TextView tvwStartDate = findViewById(R.id.tvwStartDate);
            TextView tvwEndDate = findViewById(R.id.tvwEndDate);

            //reset everything and let's have another go
            startCompleted = false;
            endCompleted = false;

            lloSubsList.removeAllViews();
            lloAdminsList.removeAllViews();
            tvwStartDate.setText(getString(R.string.start_date));
            tvwEndDate.setText(getString(R.string.end_date));
        }

        ChooseDateFragment chooseDateFragment = new ChooseDateFragment();
        chooseDateFragment.show(getSupportFragmentManager(), "");
        Bundle args = new Bundle();
        args.putBoolean("startCompleted", startCompleted);
        chooseDateFragment.setArguments(args);
    }

    /**
     * Listener provided for the DatePicker.
     *
     * @param year int year picked
     * @param month int month picked
     * @param day int day of month picked
     */
    public void onDateSelected(int year, int month, int day) {
        TextView tvwStartDate = findViewById(R.id.tvwStartDate);
        TextView tvwEndDate = findViewById(R.id.tvwEndDate);

        if (!startCompleted) {
            this.stDay = day;
            this.stMonth = month;
            this.stYear = year;

            startCompleted = true;

            tvwStartDate.setText(getString(R.string.date_display, month, day, year));

            ShowDatePicker(null);
        } else {
            this.eDay = day;
            this.eMonth = month;
            this.eYear = year;

            endCompleted = true;
            firstRun = true;

            tvwEndDate.setText(getString(R.string.date_display, month, day, year));
            if (LocalDateTime.of(stYear, stMonth, stDay, 0, 0, 0).isBefore(
                    LocalDateTime.of(eYear, eMonth, eDay, 0, 0, 0).plusDays(1))) {
                GlobalMisc.showResultsBeingCompiledMessage(getApplicationContext());
                DecayGraphingSupport.notifyUserOfGraphCrunchingWait(getApplicationContext(),
                        Toast.LENGTH_SHORT);

                displayDynamicData();
            } else {
                GlobalMisc.showSimpleDialog(getApplicationContext(), "Start date too late!",
                        "Start date must be before ending date");
            }
        }
    }

    /**
     * Method displays the substances used and the actual usages used during
     * the time period selected.
     */
    private void displayDynamicData() {
        DecayGraphingSupport.notifyUserOfGraphCrunchingWait(PeriodDetails.this);

        new Thread(new Runnable() {
            /**
             * Background thread process runs displayCompiledResults() and then
             * plotLevelGraphs().
             */
            @Override
            public void run() {
                try {
                    displayCompiledResults();
                } catch (NoResultsException ex) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            displayNoResults();
                        }
                    });

                    return;
                }
                plotLevelGraphs();
            }
        }).start();
    }

    /**
     * Method takes care of all of the boring initializations for the
     * graph such as dates as axis labels, size requirements, etc...
     *
     * @param startPeriod Date starting date
     * @param endPeriod Date ending date
     */
    private void initGraph(Date startPeriod, Date endPeriod) {
        //graph initialization
        GraphView saturationLevelsGraph = findViewById(R.id.periodDetailsGraph);
        saturationLevelsGraph.removeAllSeries();

        StaticLabelsFormatter staticLabelsFormatter =
                new StaticLabelsFormatter(saturationLevelsGraph,
                        new DateAsXAxisLabelFormatter(getApplicationContext()));

        staticLabelsFormatter.setVerticalLabels(new String[] {"zero", "mid", "peak"});

        saturationLevelsGraph.getViewport().setXAxisBoundsManual(true);
        saturationLevelsGraph.getViewport().setMinX(startPeriod.getTime());
        saturationLevelsGraph.getViewport().setMaxX(endPeriod.getTime());
        saturationLevelsGraph.getViewport().setScrollableY(true);
        saturationLevelsGraph.getViewport().setScalableY(true);

        saturationLevelsGraph.getGridLabelRenderer().setNumHorizontalLabels(3);
        saturationLevelsGraph.getGridLabelRenderer().setLabelFormatter(
                staticLabelsFormatter);
    }

    /**
     * Packs applicableSubs with all of the substances referenced in
     * includedSubIDs
     *
     * @return List<Substance> applicableSubs
     */
    private List<Substance> initApplicableSubs() {
        //initialize applicableSubs
        List<Substance> applicableSubs = new ArrayList<>();

        for (int subID : includedSubIDs) {
            Substance sub = Permanence.Subs.loadSubstanceById(subID);

            //skip this if it's a lipid soluble that we can't deal with yet
            if (sub.getLipid_soluble() && !sub.isTHC()) {
                continue;
            }

            applicableSubs.add(sub);
        }

        return applicableSubs;
    }

    /**
     * Method determines the eliminationLevels (per hour throughout the
     * referenced duration) of whatever substance entry that is correlated with
     * THC.
     *
     * @param startLDT LocalDateTime start date/time
     * @param endLDT LocalDateTime ending date/time
     * @param sub Substance
     * @param interim Duration between start & end
     * @return Double[] substance lipid saturation %age [hourly]
     */
    private Double[] determineEliminationLevelsTHC(LocalDateTime startLDT, LocalDateTime endLDT,
                                                   Substance sub, Duration interim) {
        List<Usage> applicableUsages;

        double administrationBump = (1f / 8f);
        float halflife = 24 * 1.4f;
        int cntr = 0;
        int dayHours = 0;
        double currentDosagePresent = 0;
        Double[] eliminationLevel = new Double[(int)interim.toHours() + 1];

        for (LocalDateTime applicableLDT = startLDT; applicableLDT.isBefore(endLDT);
             applicableLDT = applicableLDT.plusHours(1)) {
            //start for loop body
            applicableUsages = Permanence.Admins.getBetweenSpanValidUsagesForSub(
                    sub.getId(), Converters.fromLocalDateTime(applicableLDT),
                    Converters.fromLocalDateTime(applicableLDT.plusHours(1)));

            //note that current dosage present for this substance is 0-1.0,
            //representing the percentage of complete lipid saturation present at
            // the time, not a flat dosage in a standard measurement
            currentDosagePresent = currentDosagePresent /
                    Math.pow(2f, (1f / halflife));

            //bottom floor
            if (currentDosagePresent < GlobalMisc.LipidSolubleAllGone) {
                currentDosagePresent = 0;
            }

            if (dayHours == 0) {    //we haven't already recorded a day's usage yet
                //add our factor, level off at 100% if we reach or exceed 1.0
                if (applicableUsages.size() > 0) {
                    if ((currentDosagePresent + administrationBump) > 1) {
                        //bump lipidConcentration to 100% for the day
                        currentDosagePresent = 1;
                    } else {
                        //bump lipidConcentration closer to 100% for the day
                        currentDosagePresent += administrationBump;
                    }

                    dayHours = 23;
                }
            } else {
                dayHours--;
            }

            //fill eliminationLevels
            eliminationLevel[cntr++] = currentDosagePresent;

            //determine if this is the highest dosage for the THC
            if (currentDosagePresent > highestDosages.get(sub)) {
                highestDosages.put(sub, currentDosagePresent);
            }
        }

        //eliminationLevels.put(sub, eliminationLevel);
        return eliminationLevel;
    }

    /**
     * Method determines the eliminationLevels (per hour throughout the
     * referenced duration) of whatever plasma soluble substance entry that
     * is specified.
     *
     * @param startLDT LocalDateTime start date/time
     * @param endLDT LocalDateTime ending date/time
     * @param sub Substance that we're working with
     * @param interim Duration between start & end
     * @param halflife float highest half-life of the substance referenced
     * @return Double[] substance remaining in the body [hourly]
     */
    private Double[] determineEliminationLevelsPlasmaSoluble(LocalDateTime startLDT,
                                                             LocalDateTime endLDT,
                                                             Substance sub, Duration interim,
                                                             float halflife) {
        //code for dealing with a plasma soluble substance
        List<Usage> applicableUsages;
        double currentDosagePresent = 0;
        Double[] eliminationLevel = new Double[(int)interim.toHours() + 1];
        int cntr = 0;

        //for loop iterates over each day in the startLDT-endLDT interim, compiling
        //usages for that time, taking off the day's metabolism from a running total
        //of the current dosage present, adding the current day's total dosage to
        //that, filling the eliminationLevels data structure, and determining
        //whether or not to place today's dosage in the highestDosages data
        //structure
        for (LocalDateTime applicableLDT = startLDT; applicableLDT.isBefore(endLDT);
             applicableLDT = applicableLDT.plusHours(1)) {
            //start for loop body
            applicableUsages = Permanence.Admins.getBetweenSpanValidUsagesForSub(
                    sub.getId(), Converters.fromLocalDateTime(applicableLDT),
                    Converters.fromLocalDateTime(applicableLDT.plusHours(1)));

            //take off the day's metabolism from the current dosage present
            currentDosagePresent = currentDosagePresent /
                    Math.pow(2f, (1f / halflife));

            //compile today's total dosage
            for (Usage use : applicableUsages) {
                currentDosagePresent += use.getDosage();
            }

            //fill eliminationLevels
            eliminationLevel[cntr++] = currentDosagePresent;

            //determine if this is a new highestDosage
            if (currentDosagePresent > highestDosages.get(sub)) {
                highestDosages.put(sub, currentDosagePresent);
            }
        }

        return eliminationLevel;
    }

    /**
     * Method handles determining the appropriate multiplier for the current
     * Substance's data set and applies it to each of the saturationLevel
     * array members, normalizing the data for the combined graph.
     *
     * @param highestValueOnGraph double this substance's highest graphed value
     * @param sub Substance that we're working with
     * @param startLDT LocalDateTime beginning of the duration
     * @param endLDT LocalDateTime end of the duration
     * @param eliminationLevel Double[] array of our original levels [hourly]
     * @param interim Duration between starting & ending LDTs
     * @return DataPoint[] DataPoint array holding the new, normalized levels
     */
    private DataPoint[] determineAndApplyMultiplier(double highestValueOnGraph, Substance sub,
                                                    LocalDateTime startLDT, LocalDateTime endLDT,
                                                    Double[] eliminationLevel, Duration interim) {
        //determine multiplier & apply
        DataPoint[] saturationLevel = new DataPoint[(int)interim.toHours()];

        double multiplier = highestValueOnGraph / highestDosages.get(sub);
        int cntr = 0;

        for (LocalDateTime applicableLDT = startLDT; applicableLDT.isBefore(endLDT);
             applicableLDT = applicableLDT.plusHours(1)) {
            saturationLevel[cntr] =
                    new DataPoint(Date.from(
                            applicableLDT.atZone(ZoneId.systemDefault()).toInstant()),
                            eliminationLevel[cntr] * multiplier);

            cntr++;
        }

        return saturationLevel;
    }

    /**
     * Handles determination of the attributes for each of the lines plotted on
     * our graph, included dotted line status if the plot is #7 or above.
     *
     * @param fCntr final int position in the rainbow
     * @param rainbow ArrayList<Integer> RoyGBiv integer color codes
     * @param colorCntr like fCntr, but wraps @ entry #6 (0 based index)
     * @param saturationLevels DataPoint[] data to create our LineGraphSeries
     * @return HashMap<LineGraphSeries<DataPoint>, List<Integer>> our severely
     *         convoluted structure for returning the LineGraphSeries &
     *         attributes for the color and/or dotted line plot
     */
    private HashMap<LineGraphSeries<DataPoint>, List<Integer>> determineGraphPlotAttributes(
            final int fCntr, ArrayList<Integer> rainbow, final int colorCntr,
            DataPoint[] saturationLevels) {
        LineGraphSeries<DataPoint> tmpSeries =
                new LineGraphSeries<>(saturationLevels);
        List<Integer> colorNLineStatus = new ArrayList<>();
        HashMap<LineGraphSeries<DataPoint>, List<Integer>> returnValues = new HashMap<>();

        if (fCntr > 6) {
            Paint dottedPaint = new Paint();
            dottedPaint.setStyle(Paint.Style.STROKE);
            dottedPaint.setStrokeWidth(10);
            dottedPaint.setPathEffect(new DashPathEffect(new float[]{8, 5}, 0));
            dottedPaint.setColor(rainbow.get(colorCntr));
            tmpSeries.setDrawAsPath(true);
            tmpSeries.setCustomPaint(dottedPaint);

            colorNLineStatus.add(rainbow.get(colorCntr));
            colorNLineStatus.add(1);
        } else {
            tmpSeries.setColor(rainbow.get(colorCntr));

            colorNLineStatus.add(rainbow.get(colorCntr));
            colorNLineStatus.add(0);
        }

        returnValues.put(tmpSeries, colorNLineStatus);
        return returnValues;
    }

    /**
     * Method goes all the way from initializing the graph to filling it with
     * the data, which it also gathers from the applicableSubIDs.  Obviously,
     * this is too much to have handled in one method and it should be broken
     * up significantly.
     */
    private void plotLevelGraphs() {
        GraphView saturationLevelsGraph = findViewById(R.id.periodDetailsGraph);
        initGraph(startPeriod, endPeriod);

        //general initializations
        ArrayList<Integer> rainbow = GlobalMisc.getRoyGBiv(getApplicationContext());

        //LDT initializations
        LocalDateTime startLDT = GlobalMisc.fromDateToLDT(startPeriod);
        LocalDateTime endLDT = GlobalMisc.fromDateToLDT(endPeriod);
        Duration interim = Duration.between(startLDT, endLDT);
        HashMap<Substance, Double[]> eliminationLevels = new HashMap<>();
        saturationLevelsGraph.setTitle(startLDT.toString() + " To " + endLDT.toString());

        int cntr;
        float halflife;
        List<Substance> applicableSubs = initApplicableSubs();

        //the following for loop iterates over each sub in our applicable subs ID listing,
        //determining the maximum halflife for the sub, then determining whether or not the
        //sub is THC or alternatively not lipid soluble (if it is a lipid soluble non-THC
        //we'll return an error code).  Code then branches between choices of different for
        //loops based on the preceding condition.
        for (Substance sub : applicableSubs) {
            halflife = Math.max(sub.getdHalflife(), sub.getHalflife());

            highestDosages.put(sub, 0.0);

            if (sub.isTHC()) {
                eliminationLevels.put(sub,
                        determineEliminationLevelsTHC(startLDT, endLDT, sub, interim));
            } else if (!sub.getLipid_soluble()) {
                eliminationLevels.put(sub,
                        determineEliminationLevelsPlasmaSoluble(
                                startLDT, endLDT, sub, interim, halflife));
            }
        }

        double highestValueOnGraph = DecayGraphingSupport.getHighestDosage(highestDosages);

        //create datapoints structure
        DataPoint[][] saturationLevels =
                new DataPoint[eliminationLevels.keySet().size()][(int)interim.toHours()];
        cntr = 0;
        int colorCntr = 0;

        //for each substance, determine the appropriate multiplier and apply whilst building
        //the datapoints array (saturationLevels) for addition to the graph, add that series
        //of datapoints to the graph & fill subToColorMap appropriately
        for (Substance sub : applicableSubs) {
            HashMap<LineGraphSeries<DataPoint>, List<Integer>> returnedResults;
            saturationLevels[cntr] = determineAndApplyMultiplier(highestValueOnGraph, sub,
                    startLDT, endLDT, eliminationLevels.get(sub), interim);

            if (colorCntr > 6) {
                colorCntr = 0;
            }
            final GraphView fSaturationLevelsGraph = saturationLevelsGraph;
            final int fCntr = cntr;

            returnedResults =
                    determineGraphPlotAttributes(
                            fCntr, rainbow, colorCntr, saturationLevels[fCntr]);

            includedSerii.put(sub,
                    (LineGraphSeries<DataPoint>) returnedResults.keySet().toArray()[0]);
            subToColorMap.put(sub, returnedResults.get(includedSerii.get(sub)));

            final Substance fSub = sub;
            runOnUiThread(new Runnable() {
                public void run() {
                    fSaturationLevelsGraph.addSeries(includedSerii.get(fSub));
                }
            });

            cntr++;
            colorCntr++;
        }

        writeLegend(subToColorMap);
    }

    /**
     * OnClick handler for the button activating selection of filter criteria.
     * This enables the user to 'de-clutter' what can get to be a very packed
     * graph on a tiny mobile display.
     *
     * @param v View (requiem for an onClick handler)
     */
    public void pickFilterCriteria(View v) {
        String[] choices = new String[includedSubIDs.size()];

        int cntr = 0;
        for (Integer includedSubID : includedSubIDs) {
            choices[cntr++] =
                    Permanence.Subs.loadSubstanceById(includedSubID).getCommon_name();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(PeriodDetails.this);
        builder.setTitle("Pick a substance to remove from the results.");
        builder.setItems(choices, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final int fWhich = which;
                Toast.makeText(PeriodDetails.this,
                        "Removing selection from displayed results", Toast.LENGTH_SHORT).show();

                new Thread(new Runnable() {
                    public void run() {
                        removePlotAndOtherInstancesOfSub(includedSubIDs.get(fWhich));
                    }
                }).start();
            }
        });

        builder.show();
    }

    /**
     * Displays the compiled results on substances administered and
     * administration times/notes.  Note that this should be run from a
     * background thread.
     */
    public void displayCompiledResults() throws NoResultsException {
        LocalDateTime startLDT =
                LocalDateTime.of(stYear, stMonth, stDay, 0, 0, 0);
        LocalDateTime endLDT =
                LocalDateTime.of(eYear, eMonth, eDay, 0, 0, 0).plusDays(1);
        final LinearLayout lloSubsList = findViewById(R.id.lloSubstancesList);
        final LinearLayout lloAdminsList = findViewById(R.id.lloAdministrationsList);
        List<Usage> usages;

        startPeriod = Date.from(startLDT.atZone(ZoneId.systemDefault()).toInstant());
        endPeriod = Date.from(endLDT.atZone(ZoneId.systemDefault()).toInstant());

        usages = Permanence.Admins.getBetweenSpanValidUnarchivedUsages(
                startLDT.toEpochSecond(ZoneOffset.UTC),
                endLDT.toEpochSecond(ZoneOffset.UTC));

        if (usages.size() <= 0) {
            throw new NoResultsException();
        }

        //put together our substance lists to graph & not graph - actually we're going to
        //switch to just working with one list for both
        if (firstRun) {
            includedSubIDs = Permanence.Misc.getSubIDsUseThroughDuration(
                    Converters.fromLocalDateTime(startLDT),
                    Converters.fromLocalDateTime(endLDT));
            firstRun = false;
        }

        //compile substances & usages list
        TextView[] usagesList =
                new TextView[Permanence.Misc.countSubstancesUsedThroughDuration(
                        getApplicationContext(),
                        Converters.fromLocalDateTime(startLDT),
                        Converters.fromLocalDateTime(endLDT))];
        int cntr0 = 0;
        for (Usage use : usages) {
            //first let's see if this is a new substance to be listed in subsList
            //this should be replaced by proper database queries
            if (!includedSubIDs.contains((Integer)use.getSub_id())) {
                continue;
            }

            //now let's just get everything together for the adminsList
            usagesList[cntr0] = new TextView(getApplicationContext());
            int subID = use.getSub_id();
            usagesList[cntr0++].setText(getString(R.string.administration_summary_line,
                    Permanence.Subs.loadSubstanceById(subID).getCommon_name(),
                    use.getDosage(), Permanence.Subs.getUnitsBySID(subID),
                    use.getTimestamp().toString()));
        }

        //prepare and add subsList children
        TextView[] subsList =
                new TextView[includedSubIDs.size()];

        //add to the subs listing linear layout
        lloSubsList.removeAllViewsInLayout();
        for (int cntr = 0; cntr < (includedSubIDs.size()); cntr++) {

            Substance curSub = Permanence.Subs.loadSubstanceById(includedSubIDs.get(cntr));

            subsList[cntr] = new TextView(getApplicationContext());
            subsList[cntr].setText(curSub.getCommon_name());

            final TextView[] fSubsList = subsList;
            final int fCntr = cntr;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    lloSubsList.addView(fSubsList[fCntr]);
                }
            });
        }

        //add usagesList children
        lloAdminsList.removeAllViewsInLayout();
        for (int cntr = 0; cntr < usagesList.length; cntr++) {
            final TextView[] fUsagesList = usagesList;
            final int fCntr = cntr;

            //this is a sad way to do this, but I'm gonna deal with it for now
            if (fUsagesList[fCntr] == null) {
                break;  //they'll all be null beyond this point
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    lloAdminsList.addView(fUsagesList[fCntr]);
                }
            });
        }
    }

    /**
     * Method takes the information in subToColorMap and generates a legend
     * (preferably in a horizontal scrollview's linearlayout) to explain the
     * colors used on the graph.
     *
     * @param subToColorMap HashMap<Substance, List<Integer>> substance to
     *                      color/line attribute mapping
     */
    private void writeLegend(HashMap<Substance, List<Integer>> subToColorMap) {
        //make the color blocks 55x55, when we get around to that
        final LinearLayout fLloGraphLegend = findViewById(R.id.lloPeriodGraphLegend);
        final TextView legendLabel = new TextView(getApplicationContext());
        legendLabel.setText(getResources().getString(R.string.legend));
        legendLabel.setTextColor(Color.BLACK);

        runOnUiThread(new Runnable() {
            public void run() {
                fLloGraphLegend.removeAllViewsInLayout();
                fLloGraphLegend.addView(legendLabel);
            }
        });

        //int cntr = 0;
        for (Substance clearingSub : subToColorMap.keySet()) {
            final TextView legendMember = new TextView(getApplicationContext());
            legendMember.setText(getResources().getString(R.string.tab_buffered_string,
                    clearingSub.getCommon_name()));
            legendMember.setTextColor(subToColorMap.get(clearingSub).get(0));
            //if (cntr++ > 6) {
            if (subToColorMap.get(clearingSub).get(1) == 1) {
                legendMember.setPaintFlags(legendMember.getPaintFlags() |
                        Paint.UNDERLINE_TEXT_FLAG);
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    fLloGraphLegend.addView(legendMember);
                }
            });
        }
    }

    /**
     * Method removes the substance at the included list position from the
     * plots on the active graph, as well as removing from includedSubIDs,
     * subToColorMap, and then calls the appropriate methods to display the
     * compiled results/no results and to rewrite the legend.
     *
     * @param includedSubID int substance ID that we're working with
     */
    private void removePlotAndOtherInstancesOfSub(int includedSubID) {
        final GraphView periodDetailsGraph = findViewById(R.id.periodDetailsGraph);
        final Substance sub = Permanence.Subs.loadSubstanceById(includedSubID);

        //remove from the visible graph
        runOnUiThread(new Runnable() {
            public void run() {
                periodDetailsGraph.removeSeries(includedSerii.get(sub));
            }
        });

        //remove from the inclusion list
        //includedSubIDs.remove(includedListPosition);
        includedSubIDs.remove((Object)includedSubID);

        //remove from the legend
        subToColorMap.remove(sub);

        writeLegend(subToColorMap);

        //display our remaining results
        try {
            displayCompiledResults();
        } catch (NoResultsException ex) {
            displayNoResults();
        }
    }

    /**
     * Method just removes the graph and instead displays a notice that there
     * were no results available for this duration/substance to graph.
     */
    private void displayNoResults() {
        FrameLayout floPeriodDetailsGraphHolder =
                findViewById(R.id.floPeriodDetailsGraphHolder);
        floPeriodDetailsGraphHolder.removeAllViewsInLayout();

        TextView tvwNoResults = new TextView(PeriodDetails.this);
        tvwNoResults.setText(getString(R.string.no_results_found,
                "this duration"));
        tvwNoResults.setTextColor(
                getResources().getColor(R.color.colorDarkOrange));
        tvwNoResults.setTextSize(20);

        floPeriodDetailsGraphHolder.addView(tvwNoResults);
    }
}