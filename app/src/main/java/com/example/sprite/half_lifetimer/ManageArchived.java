package com.example.sprite.half_lifetimer;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class ManageArchived extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_archived);

        updateDisplay();
    }

    private void updateDisplay() {
        List<Substance> archivedSubs = Permanence.Subs.loadArchivedSubstances();
        LinearLayout lloArchivedSubs = findViewById(R.id.lloArchivedEntries);

        lloArchivedSubs.removeAllViewsInLayout();

        TextView[] archivedSubsView = new TextView[archivedSubs.size()];
        for (int cntr = 0; cntr < archivedSubs.size(); cntr++) {
            final List<Substance> fArchivedSubs = archivedSubs;
            final int fCntr = cntr;
            final LinearLayout fLloArchivedSubs = lloArchivedSubs;

            archivedSubsView[cntr] = new TextView(getApplicationContext());
            archivedSubsView[cntr].setText(archivedSubs.get(cntr).getCommon_name());
            archivedSubsView[cntr].setTextSize(22);

            archivedSubsView[cntr].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GlobalMisc.showDialogGetConfirmation(ManageArchived.this,
                            "Confirm Unarchival",
                            "Are you sure that you want to unarchive " +
                                    fArchivedSubs.get(fCntr).getCommon_name() + "?",
                            new GlobalMisc.OnDialogConfResultListener() {
                                @Override
                                public void onResult(boolean result) {
                                    if (result) {
                                        //unarchive this substance
                                        Substance unarcSub = fArchivedSubs.get(fCntr);

                                        /*Log.d("updateDisplay()", "Unarchiving " +
                                                unarcSub.getCommon_name());*/

                                        unarcSub.setArchived(false);
                                        Permanence.Subs.updateSub(unarcSub);

                                        //add this back to our primary substance listing
                                        SubData.subList =
                                                Permanence.Subs.loadUnarchivedSubstances(
                                                        ManageArchived.this);

                                        fLloArchivedSubs.invalidate();
                                        updateDisplay();
                                    }
                                }
                            });
                }
            });

            archivedSubsView[cntr].setOnLongClickListener(new View.OnLongClickListener() {
               @Override
               public boolean onLongClick(View v) {
                   GlobalMisc.showDialogGetConfirmation(ManageArchived.this,
                           "Confirm Substance Deletion",
                           "Are you certain that you want to delete this substance?  " +
                           "Please note that this will cascade and delete all of the substance's " +
                           "administrations, as well.  All data _will_ be lost.",
                           new GlobalMisc.OnDialogConfResultListener() {
                                @Override
                               public void onResult(boolean result) {
                                    if (result) {
                                        //delete the substance
                                        int subToWipeID = fArchivedSubs.get(fCntr).getId();

                                        Permanence.Subs.wipeSubByID(subToWipeID);

                                        Toast.makeText(ManageArchived.this,
                                                fArchivedSubs.get(fCntr).getCommon_name() +
                                                " has been deleted!", Toast.LENGTH_LONG).show();

                                        fLloArchivedSubs.invalidate();
                                        updateDisplay();
                                    }
                                }
                           });

                   return true;
               }
            });


            lloArchivedSubs.addView(archivedSubsView[cntr]);
        }
    }
}