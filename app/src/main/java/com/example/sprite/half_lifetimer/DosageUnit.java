package com.example.sprite.half_lifetimer;

public enum DosageUnit {
    MCG,
    MG,
    G,
    ML,
    TSP,
    TBSP,
    DOSE
}
