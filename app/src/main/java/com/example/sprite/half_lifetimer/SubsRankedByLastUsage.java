package com.example.sprite.half_lifetimer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.PointsGraphSeries;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * Class contains the activity that shows substances ranked by last usage, with an option to toggle
 * the display of substances that are no longer (or have never been) clearing.
 */
public class SubsRankedByLastUsage extends AppCompatActivity {
    private ArrayList<Integer> rainbow;
    private HashMap<Substance, Integer> subToColorMap = new HashMap<>();
    private ArrayList<Integer> doNotDisplay = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subs_ranked_by_last_usage);

        updateDisplay();
    }

    /**
     * Retrieves the last valid usage of each substance that is currently
     * clearing from the user.
     *
     * @return List<Usage>
     * @throws NoResultsException thrown if there are no results to report
     */
    private List<Usage> getLastUsages() throws NoResultsException {
        List<Usage> latestUsages = new ArrayList<>();
        List<Substance> applicableSubstances;

        applicableSubstances = Substance.getCurrentlyClearingSubs(getApplicationContext());

        for (Substance sub : applicableSubstances) {
            if (doNotDisplay.contains(sub)) {
                //we're not going to mess with this one
                continue;
            }

            //will definitely have to test and make sure that this database query is return only
            //the most recent usage for each sub; not sure if access is sequential from the last
            //like that or not
            Usage use = Permanence.Misc.loadLastValidUsageOfSub(sub.getId());
            if (use != null) {
                latestUsages.add(Permanence.Misc.loadLastValidUsageOfSub(sub.getId()));
            }
        }

        //if there are no results, we want to throw the exception
        if (latestUsages.size() < 1) {
            throw new NoResultsException();
        }

        //now sort these guys
        Collections.sort(latestUsages, new Comparator<Usage>() {
            public int compare(Usage u1, Usage u2) {
                return u2.getTimestamp().compareTo(u1.getTimestamp());
            }
        });

        return latestUsages;
    }

    /**
     * Method updates the display upon return from the non-clearing subs toggle.
     */
    private void updateDisplay() {
        Context ctxt = getApplicationContext();
        List<Usage> latestUsages;

        try {
            latestUsages = getLastUsages();
        } catch (NoResultsException ex) {
            FrameLayout floGraphHolder = findViewById(R.id.floLastUsageGraphHolder);
            floGraphHolder.removeAllViewsInLayout();

            TextView tvwNoResultsMessage = new TextView(SubsRankedByLastUsage.this);
            tvwNoResultsMessage.setText(getString(R.string.no_results_found,
                    "eliminating substances"));
            tvwNoResultsMessage.setTextSize(20);
            tvwNoResultsMessage.setTextColor(getResources().getColor(R.color.colorDarkOrange));

            floGraphHolder.addView(tvwNoResultsMessage);

            return;
        }

        LinearLayout lloSubsRankedByLast = this.findViewById(R.id.lloRankedSubs);
        LinearLayout lloLegend = this.findViewById(R.id.lloGraphLegend);
        TextView tvwHeading = new TextView(ctxt);

        lloLegend.removeAllViewsInLayout();
        lloSubsRankedByLast.removeAllViewsInLayout();
        
        tvwHeading.setText(getResources().getString(R.string.subs_ranked_by_last));
        tvwHeading.setTextSize(25);
        lloSubsRankedByLast.addView(tvwHeading);

        TextView[] tvwSubsRanked = new TextView[latestUsages.size()];
        int cntr = 0;

        subToColorMap.clear();

        //ah due to the fact that we're looping through usages we won't have an entry for a sub that
        //has never been administered.  wish I would've looked at that a lot sooner here...
        for (final Usage use : latestUsages) {
            Substance sub = Permanence.Subs.loadSubstanceById(use.getSub_id());

            //not so sure that this is necessary; isn't this taken care of in getLastUsages()?
            if (sub.isCleared() || doNotDisplay.contains(sub.getId())) {
                GlobalMisc.debugMsg("updateDisplay", "skipping: " +
                        sub.getCommon_name());

                continue;
            }

            final String cname = sub.getCommon_name();
            final Substance fSub = sub;

            tvwSubsRanked[cntr] = new TextView(ctxt);
            LocalDateTime subFullEliminationLDT = sub.getFullEliminationLDT();
            if (sub.getLipid_soluble() && !sub.isTHC()) {
                //we can't do projections for lipid solubles yet, though we actually CAN do some for
                //THC based substances if we take the time to code that properly
                tvwSubsRanked[cntr].setText(getString(R.string.projection_unavailable, cname));
                tvwSubsRanked[cntr].setTextColor(ContextCompat.getColor(
                        SubsRankedByLastUsage.this, R.color.colorWarnYellow));
            } else if (subFullEliminationLDT.isBefore(LocalDateTime.now())) {
                //substance is not lipid soluble, and has already been eliminated
                tvwSubsRanked[cntr].setText(cname);
                tvwSubsRanked[cntr].setTextColor(ContextCompat.getColor(
                        SubsRankedByLastUsage.this, R.color.colorDarkGreen));
            } else {
                //substance is still currently being eliminated from the body
                tvwSubsRanked[cntr].setText(getString(R.string.subname_elimination_at, cname,
                        subFullEliminationLDT.toString()));
                tvwSubsRanked[cntr].setTextColor(Color.RED);
            }
            tvwSubsRanked[cntr].setTextSize(20);
            tvwSubsRanked[cntr].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent =
                            new Intent(SubsRankedByLastUsage.this, AdminData.class);
                    intent.putExtra("SUB_NDX",
                            GlobalMisc.getSubListPositionBySid(fSub.getId()));
                    startActivity(intent);
                }
            });
            tvwSubsRanked[cntr].setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    GlobalMisc.showDialogGetConfirmation(SubsRankedByLastUsage.this,
                            "Removing " + fSub.getCommon_name() + " from graph . . .",
                            "Removing " + fSub.getCommon_name() + " from graph to " +
                                    "improve readability of the remaining plots.",
                            new GlobalMisc.OnDialogConfResultListener() {
                                @Override
                                public void onResult(boolean result) {
                                    doNotDisplay.add(fSub.getId());

                                    updateDisplay();
                                }
                            });

                    return true;
                }
            });

            lloSubsRankedByLast.addView(tvwSubsRanked[cntr++]);
        }

        DecayGraphingSupport.notifyUserOfGraphCrunchingWait(SubsRankedByLastUsage.this);

        new Thread(new Runnable() {
            public void run() {
                HashMap<Substance, Double[]> satPlots = getSaturationPlots();
                final HashMap<Substance, DataPoint[]> usagePlotPoints = new HashMap<>();
                DataPoint[][] decayCurves = plotDecayCurves(satPlots);
                final HashMap<Substance, Double[]> fSatPlots = satPlots;
                final DataPoint[][] fDecayCurves = decayCurves;

                //modify our PointsGraphData (one of these things is not like the other!)
                int sCntr = 0;
                for (Substance clearingSub : satPlots.keySet()) {
                    if (clearingSub.getLipid_soluble()) {
                        LocalDateTime applicableLDT =
                                LocalDateTime.now().minusDays(GlobalMisc.DaysToGraph);
                        Date applicableDate =
                                GlobalMisc.fromLDTToDate(applicableLDT);

                        List<DataPoint> dataPointList = new ArrayList<>();
                        /*for (int cntr = 0; cntr < satPlots.get(clearingSub).length; cntr++) {
                            GlobalMisc.debugLog("updateDisplay.run", cntr +
                                    ": " + satPlots.get(clearingSub)[cntr]);

                            if (satPlots.get(clearingSub)[cntr] > -0.001) {
                                dataPointList.add(new DataPoint(applicableDate,
                                        satPlots.get(clearingSub)[cntr]));

                                GlobalMisc.debugLog("updateDisplay.run",
                                        "Added (" + applicableDate.toString() + ", " +
                                        satPlots.get(clearingSub)[cntr].toString() + ") to " +
                                        "dataPointList (" + dataPointList.toString() + ")");
                            }

                            applicableLDT = applicableLDT.plusHours(1);
                            applicableDate = GlobalMisc.fromLDTToDate(applicableLDT);
                        }*/
                        for (int cntr = 0; cntr < decayCurves[sCntr].length; cntr++) {
                            if (decayCurves[sCntr][cntr].getY() > 0) {
                                dataPointList.add(new DataPoint(applicableDate,
                                        decayCurves[sCntr][cntr].getY()));
                            }

                            applicableLDT = applicableLDT.plusHours(1);
                            applicableDate = GlobalMisc.fromLDTToDate(applicableLDT);
                        }

                        DataPoint[] dataPointArray = new DataPoint[dataPointList.size()];
                        /*for (int cntr = 0; cntr < dataPointList.size(); cntr++) {
                            dataPointArray[cntr] =
                        }*/
                        dataPointArray = dataPointList.toArray(dataPointArray);

                        GlobalMisc.debugLog("updateDisplay.run",
                                "dataPointArray: " + dataPointArray.toString() + "\t\t" +
                                "dataPointList: " + dataPointList.toString());

                        usagePlotPoints.put(clearingSub, dataPointArray);
                    }

                    sCntr++;
                }

                rainbow = GlobalMisc.getRoyGBiv(SubsRankedByLastUsage.this);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        GraphView subCurrentLevelsGraph = findViewById(R.id.lastUsageGraph);
                        PointsGraphSeries newPSeries = null;
                        LineGraphSeries newLSeries = null;
                        int cntr = 0;
                        int colorCntr = 0;
                        for (Substance clearingSub : fSatPlots.keySet()) {
                            if (colorCntr > 6) {
                                colorCntr = 0;
                            }

                            if (clearingSub.getLipid_soluble()) {
                                newPSeries =
                                        new PointsGraphSeries(usagePlotPoints.get(clearingSub));
                                newPSeries.setSize(3);
                                newPSeries.setColor(rainbow.get(colorCntr));
                            } else {
                                newLSeries = new LineGraphSeries(fDecayCurves[cntr]);
                                newLSeries.setColor(rainbow.get(colorCntr));
                                newLSeries.setThickness(2);
                            }

                            //we need to make sure we don't run out of colors at some point in here
                            if (cntr > 6) {
                                Paint dottedPaint = new Paint();
                                dottedPaint.setStyle(Paint.Style.STROKE);
                                dottedPaint.setStrokeWidth(3);
                                dottedPaint.setPathEffect(
                                        new DashPathEffect(new float[]{8, 5}, 0));
                                dottedPaint.setColor(rainbow.get(colorCntr));
                                newLSeries.setDrawAsPath(true);
                                newLSeries.setCustomPaint(dottedPaint);
                            }

                            if (clearingSub.getLipid_soluble()) {
                                subCurrentLevelsGraph.addSeries(newPSeries);
                            } else {
                                subCurrentLevelsGraph.addSeries(newLSeries);
                            }
                            subToColorMap.put(clearingSub, colorCntr++);
                            cntr++;
                        }

                        writeLegend(subToColorMap);
                    }
                });
            }

            /**
             * Method compiles the substance body level curves and maps those to the
             * substance objects, returning the values in a HashMap
             *
             * @return HashMap<Substance, Double[]> substances and their decay curves
             */
            private HashMap<Substance, Double[]> getSaturationPlots() {
                List<Substance> clearingSubs =
                        Substance.getCurrentlyClearingSubs(getApplicationContext());
                HashMap<Substance, Double[]> clearingPlot = new HashMap<>();
                Double[] decayCurveData = null;

                //do we have any lipid soluble subs to add to clearingSubs?
                for (Substance lSub : Permanence.Subs.loadUnarchivedLipidSolubleSubstances()) {
                    if (lSub.isOnGraph(GlobalMisc.DaysToGraph)) {
                        clearingSubs.add(lSub);
                    }
                }

                for (Substance sub : clearingSubs) {
                    if (doNotDisplay.contains(sub.getId())) {
                        //we don't want this one in the results any more
                        continue;
                    }

                    try {
                        if (sub.getLipid_soluble()) {
                            decayCurveData = sub.getUsagePlot();

                            if (GlobalMisc.Debugging) {
                                GlobalMisc.debugLog("getSaturationPlots",
                                        "decayCurveData: " +
                                                Arrays.toString(decayCurveData));
                            }
                        } else {
                            decayCurveData = sub.getDecayCurve(false);
                        }
                    } catch (NoResultsException ex) {
                        //ouah
                        GlobalMisc.debugMsg("getSaturationPlots",
                                "not really sure what's going on here, talk to admin");
                    }

                    clearingPlot.put(sub, decayCurveData);
                }

                if (clearingPlot.size() <= 0) {
                    return null;
                }

                return clearingPlot;
            }

            /**
             * Method takes care of far too much shit and really needs to be broken up
             * a bit.  This handles setting up the graph, normalizing the values for
             * graphing (on the Y-axis), filling the DataPoints for the graph, and then
             * creation of the LineGraphSeries and addition of it to the actual graph.
             * Oh it also takes care of color mapping for the different substances
             * being graphed.  It then calls writeLegend() to create the graph's
             * legend underneath.
             *
             * @param decayCurves HashMap<Substance, Double[]> map of Substance to a
             *                    'double' array holding the former & current body
             *                    levels of the sub.
             */
            private DataPoint[][] plotDecayCurves(HashMap<Substance, Double[]> decayCurves) {
                int cntr2 = 0;
                HashMap<Substance, Double> highestDosages = new HashMap<>();
                ArrayList<Double[]> eliminationLevels = new ArrayList<>();
                LocalDateTime applicableLDT = LocalDateTime.now().minusDays(GlobalMisc.DaysToGraph);
                Date applicableDate = GlobalMisc.fromLDTToDate(applicableLDT);
                Date endApplicableDate =
                        GlobalMisc.fromLDTToDate(applicableLDT.plusDays(GlobalMisc.DaysToGraph));

                initGraph(applicableDate, endApplicableDate);

                List<Substance> subsToGraph = new ArrayList<Substance>();
                for (Substance sub : decayCurves.keySet()) {
                    subsToGraph.add(sub);
                }

                //do we have lipid solubles to add to it?
                for (Substance lSub : Permanence.Subs.loadUnarchivedLipidSolubleSubstances()) {
                    if (lSub.isOnGraph(GlobalMisc.DaysToGraph)) {
                        subsToGraph.add(lSub);
                    }
                }

                //for (Substance clearingSub : decayCurves.keySet()) {
                for (Substance clearingSub : subsToGraph) {
                    Double[] eliminationLevel = new Double[GlobalMisc.DaysToGraph * 24];
                    //init highestDosages
                    highestDosages.put(clearingSub, 0.0);

                    if (clearingSub.getLipid_soluble()) {
                        if (decayCurves.get(clearingSub).length > 0) {
                            //we'll never run into more than one smoke sesh per hour or whatever
                            //recorded for a lipid soluble, so 1.0 goes into highest dosages for
                            //this particular substance

                            //I think the logic in the above comment is mistaken, I'm swapping out
                            //the 1.0 for 23.99 which is close to the max value that I think the
                            //y-value will take on a lipid soluble as it's going by hours in the day
                            highestDosages.put(clearingSub, 23.99);
                        }

                        eliminationLevel = decayCurves.get(clearingSub);
                    } else {
                        for (; cntr2 < Objects.requireNonNull(decayCurves.get(clearingSub)).length;
                             cntr2++) {
                            double currentDosage = Objects.requireNonNull(
                                    decayCurves.get(clearingSub))[cntr2];

                            //record highest dosage, if this is it
                            if (currentDosage > highestDosages.get(clearingSub)) {
                                highestDosages.put(clearingSub, currentDosage);
                            }

                            eliminationLevel[cntr2] = currentDosage;
                        }
                    }

                    eliminationLevels.add(eliminationLevel);

                    cntr2 = 0;
                }

                //for determining multipliers
                double highestValueOnGraph = DecayGraphingSupport.getHighestDosage(highestDosages);

                //ugly hacking
                /*for (Substance clearingSub : subsToGraph) {
                    if (GlobalMisc.Debugging) {
                        GlobalMisc.debugLog("plotDecayCurves",
                                "Working on clearing sub: " + clearingSub.getCommon_name());
                    }

                    if (clearingSub.getLipid_soluble()) {
                        Double[] newElimLevel = new Double[GlobalMisc.DaysToGraph * 24];
                        double multiplier = highestValueOnGraph / 23.99;  //since our highest is 1.0
                        int eliminationIndex =
                                eliminationLevels.indexOf(decayCurves.get(clearingSub));

                        double tmpLevel = 0.0;
                        for (int cntr = 0; cntr < (GlobalMisc.DaysToGraph * 24); cntr++) {
                            tmpLevel = eliminationLevels.get(eliminationIndex)[cntr];
                            if (tmpLevel != -1.0) {
                                newElimLevel[cntr] = tmpLevel * multiplier;
                            } else {
                                newElimLevel[cntr] = -1.0;
                            }
                        }

                        GlobalMisc.debugLog("plotDecayCurves",
                                "Setting eliminationLevels[" + eliminationIndex + "] " +
                                "to: " + newElimLevel.toString());

                        eliminationLevels.set(eliminationIndex, newElimLevel);
                    }
                }*/

                //create datapoints structure
                return DecayGraphingSupport.getAndApplyMultipliers(
                        eliminationLevels, highestValueOnGraph, highestDosages, applicableLDT,
                        Instant.ofEpochMilli(endApplicableDate.getTime()).atZone(
                                ZoneId.systemDefault()).toLocalDateTime());
            }

            /**
             * Method writes the legend for the graph, mapping substances to the colors
             * in a human readable format.
             *
             * @param subToColorMap HashMap<Substance, Integer> Substance to Color's
             *                      int mapping
             */
            private void writeLegend(HashMap<Substance, Integer> subToColorMap) {
                //make the color blocks 55x55, when we get around to that
                LinearLayout lloGraphLegend = findViewById(R.id.lloGraphLegend);

                lloGraphLegend.removeAllViewsInLayout();

                TextView legendLabel = new TextView(getApplicationContext());
                legendLabel.setText(getResources().getString(R.string.legend));
                legendLabel.setTextColor(Color.BLACK);
                lloGraphLegend.addView(legendLabel);

                int cntr = 0;
                for (Substance clearingSub : subToColorMap.keySet()) {
                    TextView legendMember = new TextView(getApplicationContext());
                    legendMember.setText(getResources().getString(R.string.tab_buffered_string,
                            clearingSub.getCommon_name()));
                    legendMember.setTextColor(rainbow.get(subToColorMap.get(clearingSub)));
                    if (cntr++ > 6) {
                        legendMember.setPaintFlags(legendMember.getPaintFlags() |
                                Paint.UNDERLINE_TEXT_FLAG);
                    }

                    lloGraphLegend.addView(legendMember);
                }
            }
        }).start();
    }

    /*
     * Method handles the toggling of the showing of just clearing vs. all substances.
     *
     * @param v View
     */
    /*public void toggleJustClearing(View v) {
        Button btnToggleJustClearing = findViewById(R.id.btnToggleJustClearing);

        justShowClearing = !justShowClearing;

        if (justShowClearing) {
            btnToggleJustClearing.setText(getResources().getString(R.string.show_all_subs));
        } else {
            btnToggleJustClearing.setText(getResources().getString(R.string.show_clearing_subs));
        }

        updateDisplay();
    }*/

    /**
     * Method just handles the boilerplate as far as getting the graph set up.
     * Just pulled it out of plotDecayCurves to avoid that method having too
     * much clutter.
     *
     * @param startDate Date starting date for graph
     * @param endDate Date ending date for graph
     */
    private void initGraph(Date startDate, Date endDate) {
        GraphView subCurrentLevelsGraph = findViewById(R.id.lastUsageGraph);

        //graph setup
        subCurrentLevelsGraph.removeAllSeries();

        StaticLabelsFormatter staticLabelsFormatter =
                new StaticLabelsFormatter(subCurrentLevelsGraph,
                        new DateAsXAxisLabelFormatter(getApplicationContext()));

        staticLabelsFormatter.setVerticalLabels(new String[] {"zero", "mid", "peak"});

        subCurrentLevelsGraph.getViewport().setXAxisBoundsManual(true);
        subCurrentLevelsGraph.getViewport().setMinX(startDate.getTime());
        subCurrentLevelsGraph.getViewport().setMaxX(endDate.getTime());

        subCurrentLevelsGraph.getGridLabelRenderer().setNumHorizontalLabels(3);
        subCurrentLevelsGraph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        subCurrentLevelsGraph.setTitle("Currently Metabolizing Relative Substance Levels");
    }
}
