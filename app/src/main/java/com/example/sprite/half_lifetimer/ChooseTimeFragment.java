package com.example.sprite.half_lifetimer;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TimePicker;

public class ChooseTimeFragment extends DialogFragment implements View.OnClickListener {
    private TimePicker timePicker;
    private int hour;
    private int minute;
    private TimeListener timeListener;
    private boolean editing;

    public interface TimeListener {
        public void onTimeSelected(int hour, int minute, int second);
    }

    public ChooseTimeFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        editing = getArguments().getBoolean("editing");

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_choose_time, container, false);

        timePicker = (TimePicker) rootView.findViewById(R.id.timepicker);
        Button button = (Button) rootView.findViewById(R.id.btnSetStart);
        button.setOnClickListener(this);

        if (editing) {
            int[] hm = getArguments().getIntArray("hm");

            timePicker.setHour(hm[0]);
            timePicker.setMinute(hm[1]);
        }

        return rootView;
    }

    @Override
    public void onClick(View view) {
        int hour = timePicker.getHour();
        int minute = timePicker.getMinute();

        timeListener.onTimeSelected(hour, minute, 0);

        this.dismiss();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        timeListener = (TimeListener) context;
    }
}
