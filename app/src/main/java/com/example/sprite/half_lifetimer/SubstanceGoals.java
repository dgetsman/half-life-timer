package com.example.sprite.half_lifetimer;

import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class SubstanceGoals extends AppCompatActivity {

    final SubstanceGoal tmpGoal = new SubstanceGoal();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_substance_goals);

        updateDisplay();
    }

    public void onAddGoalClick(View v) {
        //add a goal
        String[] substanceNamesArray = new String[SubData.subList.size()];

        for (int cntr = 0; cntr < SubData.subList.size(); cntr++) {
            substanceNamesArray[cntr] = SubData.subList.get(cntr).getCommon_name();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(SubstanceGoals.this);
        builder.setTitle(getResources().getString(R.string.applicable_substance))
                .setItems(substanceNamesArray, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // of the selected item
                        tmpGoal.setSubId(SubData.subList.get(which).getId());

                        getGoalDosage();
                    }

                });

        builder.show();
    }

    private void getGoalDosage() {
        //set the goal's target dosage
        AlertDialog.Builder builder = new AlertDialog.Builder(SubstanceGoals.this);
        builder.setTitle("Add Daily Target Dosage in " +
                GlobalMisc.dUnitsToString(tmpGoal.getUnits()));

        final EditText input = new EditText(SubstanceGoals.this);

        input.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        builder.setView(input);

        //buttons
        builder.setPositiveButton(getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        tmpGoal.setUnitsPerDay(Float.parseFloat(input.getText().toString()));

                        //save the goal
                        Permanence.SG.saveGoal(tmpGoal);

                        //now we just need to add this to the display
                        updateDisplay();
                    }
                });
        builder.setNegativeButton(getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        builder.show();
    }

    private void updateDisplay() {
        final List<SubstanceGoal> subGoalList = Permanence.SG.loadAllUnarchivedSubstanceGoals();

        LinearLayout lloGoalList = findViewById(R.id.lloSubGoalsListing);
        TextView[] tvwSubGoals = new TextView[subGoalList.size()];

        lloGoalList.removeAllViews();

        for (int cntr = 0; cntr < subGoalList.size(); cntr++) {
            Substance sub = Permanence.Subs.loadSubstanceById(subGoalList.get(cntr).getSubId());
            final int fCntr = cntr;

            tvwSubGoals[cntr] = new TextView(SubstanceGoals.this);
            tvwSubGoals[cntr].setText(getString(R.string.substance_goal_textview,
                    sub.getCommon_name(), subGoalList.get(cntr).getUnitsPerDay(),
                    subGoalList.get(cntr).getUnits()));
            tvwSubGoals[cntr].setTextSize(18);

            tvwSubGoals[cntr].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GlobalMisc.showSimpleDialog(SubstanceGoals.this,
                            "Percentage of Goal Administered",
                            "You have administered " +
                                    subGoalList.get(fCntr).getPercentageOfDaysGoal() * 100 +
                            "% of today's target.");
                }
            });
            tvwSubGoals[cntr].setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    GlobalMisc.showDialogGetConfirmation(SubstanceGoals.this,
                            "Delete Goal?",
                            "This will toast " + subGoalList.get(fCntr).toString() +
                                    " irreversibly!", new GlobalMisc.OnDialogConfResultListener() {
                                @Override
                                public void onResult(boolean result) {
                                    if (result) {
                                        //delete the goal
                                        Permanence.SG.wipeGoal(subGoalList.get(fCntr));

                                        updateDisplay();
                                    }
                                }
                            });

                    return false;
                }
            });

            lloGoalList.addView(tvwSubGoals[cntr]);
        }
    }
}
