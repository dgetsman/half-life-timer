package com.example.sprite.half_lifetimer;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Class provides the UI and nuts 'n bolts for the UsualSuspects view.
 */
public class UsualSuspects extends AppCompatActivity {
    private final UsualSuspect newUS = new UsualSuspect();
    private final UsualSuspect editedUS = new UsualSuspect();
    private final List<UsualSuspectSubsCrossRef> newCrossRefs = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usual_suspects);

        List<UsualSuspect> unarchivedUS = Permanence.US.loadAllUnarchivedUS();
        GlobalMisc.debugMsg("onCreate()", "unarchivedUS contains:\n" +
                unarchivedUS);
        updateDisplay(UsualSuspects.this, unarchivedUS);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        //hopefully this takes care of proper updating of our display
        if (hasFocus) {
            updateDisplay(UsualSuspects.this, Permanence.US.loadAllUnarchivedUS());
        }
    }

    /**
     * Method updates the display; too much logic is in here, it should probably be broken up a bit
     * at some point.  Basically it shows all of the usual suspect entries and registers the click
     * event handlers so that they're sma'at.
     *
     * @param ctxt Context for the display
     * @param USList ArrayList of UsualSuspects to be displayed
     */
    private void updateDisplay(Context ctxt, List<UsualSuspect> USList) {
        LinearLayout lloUS = findViewById(R.id.lloUsualSuspects);
        TextView[] usTextView = new TextView[USList.size()];
        final List<UsualSuspect> fUSList = USList;

        lloUS.removeAllViewsInLayout();

        for (int cntr = 0; cntr < USList.size(); cntr++) {
            //make sure that nothing in this usual suspect is archived
            boolean archivedSubstance = false;
            for (UsualSuspectSubsCrossRef cx :
                    Permanence.CrossRef.loadCrossRefsPerUsualSuspect(USList.get(cntr).getId())) {
                if (Permanence.Subs.loadSubstanceById(cx.getSub()).getArchived()) {
                    //we need to skip this UsualSuspect, it has an archived component
                    archivedSubstance = true;
                    break;
                }
            }

            //skip this loop iteration
            if (archivedSubstance) {
                continue;
            }

            SubstanceGoal goal = null;
            try {
                goal = getHighestGoalForUsualSuspect(fUSList.get(cntr).getId());
            } catch (Exception ex) {
                //some testing should probably be made here to make sure we're catching the right
                //kinds of exceptions :P
            }

            usTextView[cntr] = new TextView(ctxt);
            if (goal != null) {
                usTextView[cntr].setText(getResources().getString(R.string.sub_and_percentage_float,
                        USList.get(cntr).getName(), goal.getPercentageOfDaysGoal() * 100));
            } else {
                usTextView[cntr].setText(USList.get(cntr).getName());
            }
            usTextView[cntr].setTag(cntr);
            usTextView[cntr].setTextSize(20);

            //do some color coding in case we're close to our usual suspect substance's daily goals
            //here
            if (goal != null) {
                float percentageAdminned = goal.getPercentageOfDaysGoal();

                if (percentageAdminned >= 1) {
                    usTextView[cntr].setTextColor(Color.RED);
                } else if (percentageAdminned >= 0.75) {
                    usTextView[cntr].setTextColor(
                            ContextCompat.getColor(UsualSuspects.this, R.color.colorWarnYellow));
                } else {
                    usTextView[cntr].setTextColor(
                            ContextCompat.getColor(UsualSuspects.this, R.color.colorDarkGreen));
                }
            }

            final int fCntr = cntr;

            usTextView[cntr].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //add a usage for this substance
                    final UsualSuspect fCurUsualSuspect = fUSList.get(fCntr);

                    GlobalMisc.debugMsg("UsualSuspects.updateDisplay()",
                            "curUsualSuspect: " + fCurUsualSuspect);

                    AlertDialog.Builder sBuilder = new AlertDialog.Builder(UsualSuspects.this);
                    sBuilder.setTitle(getResources().getString(R.string.current_or_past_usage) + " (" + fCurUsualSuspect.getName() +
                            ")");
                    sBuilder.setPositiveButton(getResources().getString(R.string.current),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    doCurrentUsage(fCurUsualSuspect);

                                    GlobalMisc.debugMsg("UsualSuspects.updateDisplay()->" +
                                            "onClickListener()", "fCurUsualSuspect: " +
                                            fCurUsualSuspect);

                                    //switchToAdminData(fCurUsualSuspect);
                                }
                            });

                    sBuilder.setNeutralButton(getResources().getString(R.string.past),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    doPastUsage(fUSList, fCntr);
                                }
                            });

                    sBuilder.setNegativeButton(getResources().getString(R.string.cancel),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                    sBuilder.show();
                }
            });
            usTextView[cntr].setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    String[] choices = {
                            getResources().getString(R.string.view_usual_suspect),
                            getResources().getString(R.string.edit_usual_suspect),
                            getResources().getString(R.string.delete_usual_suspect)
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(UsualSuspects.this);
                    builder.setTitle("Usual Suspect Details Menu");
                    builder.setItems(choices, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    //view details
                                    GlobalMisc.showSimpleDialog(UsualSuspects.this,
                                            fUSList.get(fCntr).getName() + " Details",
                                            fUSList.get(fCntr).toString());

                                    break;

                                case 1:
                                    editUsualSuspect(fUSList, fCntr);

                                    break;

                                case 2:
                                    deleteUsualSuspect(fUSList, fCntr);

                                    break;
                            }
                        }
                    });

                    builder.show();

                    return false;
                }
            });

            lloUS.addView(usTextView[cntr]);
        }
    }

    /**
     * Handler for the 'add usual suspect' button
     *
     * @param v View
     */
    public void addUsualSuspectOnClick(View v) {
        /*GlobalMisc.showDialogGetString(UsualSuspects.this, "Name", null,
                new GlobalMisc.OnDialogResultListener() {
                    @Override
                    public void onResult(String result) {
                        newUS.setName(result);

                        setUSSub();
                    }
                });*/

        Intent addUsualSuspect = new Intent(UsualSuspects.this, AddUsualSuspect.class);
        startActivity(addUsualSuspect);
    }

    /**
     * This method is called when the user dialog for adding a new usual suspect has been initiated,
     * and the substance that it falls under is being determined by user input.
     */
    private void setUSSub() {
        final List<Substance> allSubs = Permanence.Subs.loadSubstances(UsualSuspects.this);
        String [] subArray = new String[allSubs.size()];
        int cntr = 0;


        for (Substance sub : allSubs) {
            subArray[cntr++] = sub.getCommon_name();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(UsualSuspects.this);
        builder.setTitle(getResources().getString(R.string.substance))
            .setItems(subArray, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    newCrossRefs.add(new UsualSuspectSubsCrossRef());
                    newCrossRefs.get(newCrossRefs.size() - 1).setSub(allSubs.get(which).getId());

                    setUSDosage();
                }
            });

        builder.show();
    }

    /**
     * Again part of the user dialog in setting up a new usual suspect, this method sets the dosage
     * of the freshly added usual suspect.
     */
    private void setUSDosage() {
        final MyBoolean myBoolean = new MyBoolean();
        myBoolean.setValue(true);

        AlertDialog.Builder builder = new AlertDialog.Builder(UsualSuspects.this);
        builder.setTitle(getResources().getString(R.string.dosage_sans_colon) + " in " +
                GlobalMisc.dUnitsToString(
                        Permanence.Subs.loadSubstanceById(newCrossRefs.get(
                                newCrossRefs.size() - 1).getSub()).getUnits()) + ":");

        final EditText input = new EditText(UsualSuspects.this);

        input.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        builder.setView(input);

        builder.setPositiveButton(getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        float tmpDosage = Float.parseFloat(input.getText().toString());

                        if (!GlobalMisc.validateDosageAmount(tmpDosage)) {
                            GlobalMisc.showDialogGetConfirmation(UsualSuspects.this,
                                    "Invalid Dosage Amount",
                                    "Your dosage amount must be between 0.001 and " +
                                            GlobalMisc.DoseRangeMax + "!",
                                    new GlobalMisc.OnDialogConfResultListener() {
                                        @Override
                                        public void onResult(boolean result){
                                            if (result) {
                                                setUSDosage();
                                            } else {
                                                Toast.makeText(UsualSuspects.this,
                                                    "Cancelling!", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                        }

                        newCrossRefs.get(newCrossRefs.size() - 1).setDosage(tmpDosage);

                        if (!myBoolean.isValue()) {
                            setUSNotes();
                        }

                        GlobalMisc.showDialogGetConfirmation(
                            UsualSuspects.this, getString(R.string.add_another), getString(
                                    R.string.add_another_substance),
                            new GlobalMisc.OnDialogConfResultListener() {
                                @Override
                                public void onResult(boolean result) {
                                    if (result) {
                                        myBoolean.setValue(result);

                                        setUSSub();
                                    } else {
                                        setUSNotes();
                                    }
                                }
                            });
                    }
                });
        builder.setNegativeButton(getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        builder.show();
    }

    /**
     * The final part of the user dialog for adding a usual suspect is here.  This one sets the
     * notes for each entry of this usual suspect and then proceeds to save the usual suspect
     * record into the database (then calling updateDisplay()).
     */
    private void setUSNotes() {
        GlobalMisc.showDialogGetString(UsualSuspects.this, "Notes", null,
                new GlobalMisc.OnDialogResultListener() {
                    @Override
                    public void onResult(String result) {
                        newUS.setNotes(result);

                        Permanence.US.saveUS(newUS);
                        UsualSuspect newerUS = Permanence.US.loadUSByName(newUS.getName());

                        GlobalMisc.debugMsg("setUSNotes", newerUS.toString());

                        for (UsualSuspectSubsCrossRef crossRef : newCrossRefs) {
                            crossRef.setUsualSuspect(Permanence.US.loadUSByName(
                                    newerUS.getName()).getId());
                            Permanence.CrossRef.saveCrossRef(crossRef);
                        }

                        //get rid of these crossrefs in case of adding another new US
                        newCrossRefs.clear();

                        //updateDisplay(UsualSuspects.this, Permanence.US.loadAllUS());
                        updateDisplay(UsualSuspects.this, Permanence.US.loadAllUnarchivedUS());
                    }
                });
    }

    /**
     * Method locates the different substance goals which may be in effect for the given
     * UsualSuspect and then determines which is the highest percentage, then returning the goal.
     *
     * @param USId UsualSuspect ID
     */
    private SubstanceGoal getHighestGoalForUsualSuspect(int USId) {
        List<UsualSuspectSubsCrossRef> crossRefs =
                Permanence.CrossRef.loadCrossRefsPerUsualSuspect(USId);

        float highestPercentage = 0.0f;
        SubstanceGoal highestGoal = null;

        for (UsualSuspectSubsCrossRef crossRef : crossRefs) {
            SubstanceGoal curGoal = Permanence.SG.loadSubGoalBySID(crossRef.getSub());

            if (curGoal.getPercentageOfDaysGoal() > highestPercentage) {
                highestPercentage = curGoal.getPercentageOfDaysGoal();
                highestGoal = curGoal;
            }
        }

        return highestGoal;
    }

    /**
     * Method handles addition of a current usage of a usual suspect, linked by the onClick handler
     * of updateDisplay()'s listing of UsualSuspects.
     *
     * @param curUS current UsualSuspect
     */
    private void doCurrentUsage(UsualSuspect curUS) {
        List<Usage> newUses = new ArrayList<>();
        HashMap<Integer, Float> newUsageData =
                curUS.getSIDsAndDosages();

        Log.d("doCurrentUsage()", "curUS.getSIDsAndDosages() returns: " +
                newUsageData);

        Set newUsageDataSet = newUsageData.entrySet();
        Iterator iterator = newUsageDataSet.iterator();
        while (iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry)iterator.next();

            newUses.add(new Usage((int)mentry.getKey(),
                    (float)mentry.getValue(), LocalDateTime.now(),
                    curUS.getNotes()));
        }

        /*
         * TODO:
         * Note that the following for loop is not yet optimized for the
         * new task that we're using it for.  Most of the code inside of
         * the for-each loop is taken directly from the period in which
         * we were only adding one Substance per UsualSuspect.  Now that
         * we're potentially adding more than one, this loop will ask
         * about adding each substance if getting too close to the goal
         * in turn, individual adding (or declining to) on a per-
         * substance basis instead of for the entire UsualSuspect's
         * contents.  This will need to be fixed in the future.
         */
        for (Usage newUsage : newUses) {
            SubstanceGoal goal = Permanence.SG.loadSubGoalBySID(newUsage.getSub_id());

            if (goal != null && goal.getPercentageOfDaysGoal(
                    newUsage.getDosage()) > .5) {
                final Usage fNewUse = newUsage;
                final MyInteger fMultiplier = new MyInteger();

                AlertDialog.Builder builder = new AlertDialog.Builder(
                        UsualSuspects.this);
                builder.setTitle(R.string.get_multiplier);

                final EditText input = new EditText(UsualSuspects.this);
                final GlobalMisc.OnDialogResultListener fListener =
                        new GlobalMisc.OnDialogResultListener() {
                            @Override
                            public void onResult(String result) {
                                fMultiplier.setValue(Integer.parseInt(result));
                            }
                        };

                input.setText("1");
                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                builder.setView(input);

                final SubstanceGoal fGoal = goal;
                final UsualSuspect fCurUS = curUS;

                //buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        fListener.onResult(input.getText().toString());

                        GlobalMisc.showDialogGetConfirmation(
                                UsualSuspects.this,
                                getString(R.string.are_you_sure), getString(
                                        R.string.this_will_bring_you_to_x_percentage,
                                        fGoal.getPercentageOfDaysGoal(
                                                fNewUse.getDosage() *
                                                        fMultiplier.getValue()) * 100),
                                new GlobalMisc.OnDialogConfResultListener() {
                                    @Override
                                    public void onResult(boolean result) {
                                        if (result) {
                                            fNewUse.setDosage(fNewUse.getDosage() *
                                                    fMultiplier.getValue());
                                            Permanence.Admins.saveUsage(fNewUse);

                                            Toast.makeText(UsualSuspects.this,
                                                    fCurUS.getName() +
                                                            " " + getResources().getString(
                                                                    R.string.us_added),
                                                    Toast.LENGTH_LONG).show();

                                            switchToAdminData(fCurUS);
                                        } else {
                                            Toast.makeText(UsualSuspects.this,
                                                    getString(R.string.canceling_administration),
                                                    Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();

            } else {    //no goal exists for this substance
                Permanence.Admins.saveUsage(newUsage);

                Toast.makeText(UsualSuspects.this,
                        curUS.getName() + " " + getResources().getString(R.string.us_added),
                        Toast.LENGTH_LONG).show();

                switchToAdminData(curUS);
            }
        }
    }

    /**
     * Method handles addition of a past usage of a UsualSuspect, linked from the onClick handler
     * from updateDisplay()'s listing of UsualSuspects.
     *
     * @param fUSList final UsualSuspect listing
     * @param fCntr final counter for the position in that list
     */
    private void doPastUsage(final List<UsualSuspect> fUSList, final int fCntr) {
        Intent intent = new Intent(UsualSuspects.this, AddPastUsage.class);

        intent.putExtra("SUB_NDX", -1);
        intent.putExtra("US_NDX", fUSList.get(fCntr).getId());
        intent.putExtra("DOSAGE", -1);
        intent.putExtra("NOTES", fUSList.get(fCntr).getNotes());

        startActivity(intent);
    }

    /**
     * Method handles editing a UsualSuspect, linked by the onLongClick() handler from
     * updateDisplay()'s listing of UsualSuspects.
     *
     * @param fUSList final UsualSuspect listing
     * @param fCntr final counter for the position in that list
     */
    private void editUsualSuspect(final List<UsualSuspect> fUSList, final int fCntr) {
        //edit details
        UsualSuspect currentlyEditingUS = fUSList.get(fCntr);
        editedUS.setId(currentlyEditingUS.getId());
        editedUS.setName(currentlyEditingUS.getName());
        editedUS.setNotes(currentlyEditingUS.getNotes());

        //prepare the data for the next few method calls
        //now editing suspect's substances capability (more complex now
        //with many-to-many relationship)
        List<UsualSuspectSubsCrossRef> crossRefs = Permanence.CrossRef.loadCrossRefsPerUsualSuspect(
                fUSList.get(fCntr).getId());
        final String[] currentSubstanceNamesArray = new String[crossRefs.size()];
        String currentSubstanceNamesString = "";

        //load currentSubstanceNamesString with the appropriate
        //concatenated strings delimited by commas, subs with substances
        //from this particular UsualSuspect, currentSubstanceNamesArray
        //with the names
        for (int cntr = 0; cntr < crossRefs.size(); cntr++) {
            Substance sub = Permanence.Subs.loadSubstanceById(crossRefs.get(cntr).getSub());
            currentSubstanceNamesString += sub.getCommon_name();
            currentSubstanceNamesArray[cntr] = sub.getCommon_name();

            if (cntr != (crossRefs.size() - 1)) {
                currentSubstanceNamesString += ", ";
            }
        }

        final String fCurrentSubstanceNamesString = currentSubstanceNamesString;
        final List<UsualSuspectSubsCrossRef> fCrossRefs = crossRefs;

        GlobalMisc.showDialogGetString(UsualSuspects.this,
                "Change " + currentlyEditingUS.getName() +
                        "'s Name", currentlyEditingUS.getName(),
                new GlobalMisc.OnDialogResultListener() {
                    @Override
                    public void onResult(String result) {
                        editedUS.setName(result);

                        Permanence.US.updateUS(editedUS);

                        editingUSAddSubstance(fCurrentSubstanceNamesString,
                                currentSubstanceNamesArray, fUSList, fCntr, fCrossRefs);
                    }
                });
    }

    private void editingUSChangeNotes(final List<UsualSuspect> fUSList, final int fCntr) {
        //change notes?
        GlobalMisc.showDialogGetString(UsualSuspects.this,
                "Change " + newUS.getName() + "'s Notes",
                fUSList.get(fCntr).getNotes(),
                new GlobalMisc.OnDialogResultListener() {
                    @Override
                    public void onResult(String result) {
                        editedUS.setNotes(result);

                        Permanence.US.updateUS(editedUS);

                        Toast.makeText(UsualSuspects.this,
                                editedUS.getName() + " Updated!",
                                Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void editingUSDeleteSubstance(String currentSubstanceNamesString,
                                          final String[] fCurrentSubstanceNamesArray,
                                          final List<UsualSuspectSubsCrossRef> fCrossRefs,
                                          final List<UsualSuspect> fUSList, final int fCntr) {
        //this is an attempt at being able to cycle through the delete
        //substance loop multiple times; however it does not appear that
        //fFlag is properly conveying what we need on down the line
        final MyBoolean fFlag = new MyBoolean();
        fFlag.setValue(true);

        while (fFlag.isValue()) {
            fFlag.setValue(false);

            GlobalMisc.showDialogGetConfirmation(UsualSuspects.this,
                    "Delete Associated Substance Details?",
                    "Current substances: " +
                            currentSubstanceNamesString,
                    new GlobalMisc.OnDialogConfResultListener() {
                        //user has confirmed crossreference deletion
                        @Override
                        public void onResult(boolean result) {
                            if (result) {
                                AlertDialog.Builder builder =
                                        new AlertDialog.Builder(UsualSuspects.this);
                                builder.setTitle(
                                        "Delete Substance from Lineup?");
                                builder.setItems(fCurrentSubstanceNamesArray,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Permanence.CrossRef.deleteCrossRef(
                                                        fCrossRefs.get(which));

                                                //TODO: remove from currentSubstancesNamesArray, as well

                                                fFlag.setValue(true);

                                                editingUSChangeNotes(fUSList, fCntr);
                                            }
                                        });

                                builder.show();
                            } else {
                                editingUSChangeNotes(fUSList, fCntr);
                            }
                        }
                    });
        }
    }

    private void editingUSAddSubstance(final String currentSubstanceNamesString,
                                       final String[] fCurrentSubstanceNamesArray,
                                       final List<UsualSuspect> fUSList, final int fCntr,
                                       final List<UsualSuspectSubsCrossRef> fCrossRefs) {
        //here we add a substance to the lineup, if required
        GlobalMisc.showDialogGetConfirmation(UsualSuspects.this,
                "Add Substance To Usual Suspect?", "Current substances: " +
                        currentSubstanceNamesString,
                new GlobalMisc.OnDialogConfResultListener() {
                    //user has confirmed addition of a crossreferenced substance
                    @Override
                    public void onResult(boolean result) {
                        if (result) {
                            //add a substance
                            List<Substance> subs = Permanence.Subs.loadSubstances(
                                    UsualSuspects.this);

                            //remove instances of the substances already
                            //listed for this particular UsualSuspect
                            List<Substance> snippedSubs = new ArrayList<>();
                            for (Substance sub : subs) {
                                if (!Arrays.asList(fCurrentSubstanceNamesArray).contains(
                                        sub.getCommon_name())) {
                                    snippedSubs.add(sub);
                                }
                            }

                            String[] newSubstanceNamesArray = new String[snippedSubs.size()];

                            for (int cntr = 0; cntr < snippedSubs.size(); cntr++) {
                                newSubstanceNamesArray[cntr] =
                                        snippedSubs.get(cntr).getCommon_name();
                            }

                            GlobalMisc.debugMsg("updateDisplay",
                                    "newSubstanceNamesArray: " + newSubstanceNamesArray);

                            final List<Substance> fSubs = snippedSubs;
                            final String fCurrentSubstanceNamesString = currentSubstanceNamesString;

                            AlertDialog.Builder builder = new AlertDialog.Builder(
                                    UsualSuspects.this);
                            builder.setTitle("Add Substance to Usual Suspect");
                            builder.setItems(newSubstanceNamesArray,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            //now we add the cross reference and the dosage
                                            AlertDialog.Builder builder = new AlertDialog.Builder(
                                                    UsualSuspects.this);
                                            builder.setTitle("Dosage?");
                                            builder.setMessage("Dosage in " +
                                                    GlobalMisc.dUnitsToString(
                                                            fSubs.get(which).getUnits()) + "?");

                                            final int fWhich = which;
                                            final EditText input = new EditText(
                                                    UsualSuspects.this);
                                            final GlobalMisc.OnDialogResultListener fListener =
                                                    new GlobalMisc.OnDialogResultListener() {
                                                        @Override
                                                        public void onResult(String result) {
                                                            //save the new cross-reference record
                                                            Permanence.CrossRef.saveCrossRef(
                                                                    new UsualSuspectSubsCrossRef(
                                                                            fUSList.get(fCntr).getId(),
                                                                            fSubs.get(fWhich).getId(),
                                                                            Float.parseFloat(result)));

                                                            editingUSDeleteSubstance(
                                                                    fCurrentSubstanceNamesString,
                                                                    fCurrentSubstanceNamesArray,
                                                                    fCrossRefs, fUSList, fCntr);
                                                        }
                                                    };

                                            input.setInputType(InputType.TYPE_CLASS_NUMBER |
                                                    InputType.TYPE_NUMBER_FLAG_DECIMAL);
                                            builder.setView(input);

                                            //buttons
                                            builder.setPositiveButton("OK",
                                                    new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog,
                                                                            int which) {
                                                            fListener.onResult(
                                                                    input.getText().toString());
                                                        }
                                                    });
                                            builder.setNegativeButton("Cancel",
                                                    new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog,
                                                                            int which) {
                                                            dialog.cancel();
                                                        }
                                                    });

                                            builder.show();
                                        }
                                    });

                            builder.show();
                        } else {
                            editingUSDeleteSubstance(
                                    currentSubstanceNamesString,
                                    fCurrentSubstanceNamesArray,
                                    fCrossRefs, fUSList, fCntr);
                        }
                    }
                });
    }

    /**
     * Method handles deletion of a UsualSuspect, linked from the onLongClick handler via
     * updateDisplay()'s listing of UsualSuspects.
     *
     * @param fUSList final UsualSuspect listing
     * @param fCntr final counter for the position in that list
     */
    private void deleteUsualSuspect(final List<UsualSuspect> fUSList, final int fCntr) {
        //delete usual suspect totally, including cross references to
        //substances (now handled in Permanence)
        AlertDialog.Builder dBuilder = new AlertDialog.Builder(
                UsualSuspects.this);
        dBuilder.setTitle("Delete Usual Suspect " +
                fUSList.get(fCntr).getName() + "?");
        dBuilder.setMessage("Are you sure that you want to delete " +
                fUSList.get(fCntr).getName() + "?");
        dBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Permanence.US.deleteUS(fUSList.get(fCntr));

                        Toast.makeText(UsualSuspects.this,
                                fUSList.get(fCntr).getName() + " deleted!",
                                Toast.LENGTH_LONG).show();
                    }
                });
        dBuilder.setNegativeButton("No!",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        dBuilder.show();
    }

    /**
     * Just a bit of a wrapper for the code to build an intent and switch to
     * another activity (in this case AdminData) in order to be able to see
     * the results of the UsualSuspect administration.
     *
     * @param currentUS UsualSuspect current UsualSuspect being administered
     */
    private void switchToAdminData(UsualSuspect currentUS) {
        int subNdx = GlobalMisc.getSubListPositionBySid(
                (int)currentUS.getSIDsAndDosages().keySet().toArray()[0]);

        GlobalMisc.debugMsg("switchToAdminData", "working with subNdx: " +
                subNdx);

        Intent intent = new Intent(UsualSuspects.this, AdminData.class);
        intent.putExtra("SUB_NDX", subNdx);
        startActivity(intent);
    }
}
