package com.example.sprite.half_lifetimer;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import android.util.Log;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class NotificationService extends Service {
    public static HashMap<Integer, Boolean> firedNotifications = new HashMap<>();
    public static LocalDateTime lastNotificationLoopLDT = null;

    @Nullable
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Method handles creation of a NotificationChannel and database
     * initialization (for this particular subset of the code), then passing
     * control off to notificationLoop().
     */
    public void onCreate() {
        startForeground(31337, buildForegroundNotification());

        if (GlobalMisc.Debugging) {
            Log.i("Halflife.NotificationService.onCreate", "Started NotificationService");
        }

        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            /*CharSequence name = "Half-life Timer";
            String description = "Here there goes the shit for Half-Life Timer";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;*/

            NotificationChannel chan = new NotificationChannel(
                    "taper-n-clearing-talk", "taper-n-clearing",
                    NotificationManager.IMPORTANCE_NONE);
            chan.setDescription("Notifications for Taper dosages and Substance clearance");

            //NotificationChannel channel = new NotificationChannel("halflife", name, importance);
            //channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(chan);
        }

        //get the database ready
        try {
            Permanence.Misc.init(NotificationService.this);
        } catch (Exception ex) {
            Log.e("Halflife.notificationLoop", "Unable to init database: " +
                    ex.toString());
        }

        if (GlobalMisc.Debugging) {
            Log.i("Halflife.onCreate", "all valid tapers: " +
                    Permanence.Tapers.loadAllValidTapers(getApplicationContext()).toString());

        }

        //notificationLoop();
        PeriodicWorkRequest notificationsRequest =
                new PeriodicWorkRequest.Builder(NotificationWorker.class, 15, TimeUnit.MINUTES)
                        .build();
        WorkManager.getInstance()
                .enqueue(notificationsRequest);
    }

    private Notification buildForegroundNotification() {
        NotificationCompat.Builder b=new NotificationCompat.Builder(this);

        b.setOngoing(true)
                .setContentTitle("HLT Foreground Service")
                .setContentText("Giving Half-life Timer foreground priority")
                .setChannelId("taper-n-clearing-talk")
                .setSmallIcon(getApplicationContext().getResources().getIdentifier(
                        "plus_medical_blue","drawable",
                        getApplicationContext().getPackageName()));

        return(b.build());
    }
}
