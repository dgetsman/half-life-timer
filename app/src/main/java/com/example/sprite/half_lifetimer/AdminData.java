package com.example.sprite.half_lifetimer;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.PointsGraphSeries;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Class provides the code for the AdminData activity.
 */
public class AdminData extends AppCompatActivity {
    private Usage tmpUsage = new Usage();
    private int subNdx;
    private boolean notesView = false;
    private boolean sortAscending = false;
    private boolean fullEliminationCurve = false;
    private boolean problematicLipidSoluble = false;
    private boolean updating = false;

    /**
     * Method initializes the UI for the activity, sets the class-level variable subNdx to the
     * selected substance's index, and calls updateDisplay() with the appropriate parameters for an
     * ascending or descending sort in order to populate administration entries in the display.
     *
     * @param savedInstanceState requiem for an onCreate
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_data);

        //this is only for a notification based intent switch here
        try {
            if (getIntent().getExtras().containsKey("NOTIFICATION_BASED") &&
                    getIntent().getExtras().getBoolean("NOTIFICATION_BASED")) {
                //we need to fix NotificationService.firedNotifications
                Taper thisTaper = Permanence.Tapers.loadOneValidTaperBySid(
                        SubData.subList.get(subNdx).getId());

                NotificationService.firedNotifications.replace(thisTaper.getId(), false);

                if (GlobalMisc.Debugging) {
                    Log.i("Halflife.AdminData.onCreate",
                            "firedNotifications contains: " +
                                    NotificationService.firedNotifications.toString());
                }
            }
        } catch (Exception ex) {
            Log.i("Halflife.AdminData.onCreate", "issue trying to reset " +
                    "firedNotifications");
        }

        //here starts the stuff that we're interested in for this activity
        TextView tvwSubHeading = findViewById(R.id.tvwSubHeading);

        subNdx = getIntent().getExtras().getInt("SUB_NDX");

        //set up our display here
        tvwSubHeading.setText(SubData.subList.get(subNdx).getCommon_name());
        tvwSubHeading.setTextSize(25);

        updating = true;
        updateDisplay();
        if (Permanence.Admins.getUsageCountForSubstance(SubData.subList.get(subNdx).getId()) > 0) {
            plotSaturationData();
        } else {
            removeGraphDisplayNotice();
        }
    }

    /**
     * Method handles repopulating the display after the window focus has changed.  IE after a user
     * has selected to go to another activity and then comes back after having manipulated entries
     * that otherwise would be showing incorrect values.
     *
     * @param hasFocus requiem for an onWindowFocusChanged
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        //hopefully this takes care of proper updating of our display
        if (hasFocus && !updating) {
            updating = true;
            updateDisplay();
            if (Permanence.Admins.getUsageCountForSubstance(
                    SubData.subList.get(subNdx).getId()) > 0) {
                plotSaturationData();
            } else {
                removeGraphDisplayNotice();
            }
        }
    }

    /**
     * Method triggers the user dialog required for adding a new usage administration for whatever
     * substance whose index was passed to the activity as it was initiated.
     *
     * @param v View
     */
    public void addUsageOnClick(View v) {
        //here's where we'll trigger adding a new usage administration
        AlertDialog.Builder sBuilder = new AlertDialog.Builder(AdminData.this);
        sBuilder.setTitle(getResources().getString(R.string.current_or_past_usage));
        sBuilder.setPositiveButton(getResources().getString(R.string.current),
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if this isn't a generic dosage, in which case the amount will always be 1.0
                if (SubData.subList.get(subNdx).getUnits() != DosageUnit.DOSE) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(AdminData.this);
                    builder.setTitle("Add Usage Record (dosage) in " +
                            GlobalMisc.dUnitsToString(SubData.subList.get(subNdx).getUnits()));

                    final EditText input = new EditText(AdminData.this);

                    input.setInputType(InputType.TYPE_CLASS_NUMBER |
                            InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    builder.setView(input);

                    //buttons
                    builder.setPositiveButton(getResources().getString(R.string.ok),
                            new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            tmpUsage.setSub_id(
                                    SubData.subList.get(subNdx).getId());
                            try {
                                float tmpDose = Float.parseFloat(input.getText().toString());

                                if (!GlobalMisc.validateDosageAmount(tmpDose)) {
                                    String errorMessage = "You need to enter a value from " +
                                            /*String.format("%.3f", GlobalMisc.DoseRangeMin)*/
                                            "0.001 to " + GlobalMisc.DoseRangeMax + "!";
                                    GlobalMisc.showSimpleDialog(AdminData.this,
                                            "Invalid Dosage Value", errorMessage);

                                    return;
                                } else {
                                    tmpUsage.setDosage(tmpDose);
                                }
                            } catch (java.lang.NumberFormatException ex) {
                                tmpUsage = null;
                                GlobalMisc.showSimpleDialog(AdminData.this,
                                        "Number Format Exception",
                                        "You need to enter a numeric dosage in " +
                                                SubData.subList.get(subNdx).getUnits() + "!");

                                return;
                            }
                            tmpUsage.setTimestamp(LocalDateTime.now());

                            checkTarget(SubData.subList.get(subNdx).getId());
                        }
                    });
                    builder.setNegativeButton(getResources().getString(R.string.cancel),
                            new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder.show();
                } else {
                    //generic dosage, skip the dialog and fill in what we know
                    tmpUsage.setSub_id(SubData.subList.get(subNdx).getId());
                    tmpUsage.setDosage((float) 1.0);
                    tmpUsage.setTimestamp(LocalDateTime.now());

                    checkTarget(SubData.subList.get(subNdx).getId());
                }
            }
        });
        sBuilder.setNegativeButton(getResources().getString(R.string.past),
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(AdminData.this, AddPastUsage.class);
                intent.putExtra("SUB_NDX", subNdx);
                intent.putExtra("DOSAGE", (String) null);
                intent.putExtra("NOTES", (String) null);
                startActivity(intent);
            }
        });

        sBuilder.show();
    }

    /**
     * Method initiates the TabbedData activity.
     *
     * @param v View
     */
    public void seeTabulatedData(View v) {
        //well this is inefficient as all hell
        if (!Permanence.Admins.loadValidOrderedUsagesBySubid(
                SubData.subList.get(
                        getIntent().getExtras().getInt("SUB_NDX")).getId()).isEmpty()) {
            Intent intent = new Intent(AdminData.this, TabbedData.class);
            intent.putExtra("SUB_NDX", SubData.subList.get(subNdx).getId());
            startActivity(intent);
        } else {
            Toast.makeText(AdminData.this, "No usages to tabulate data from!",
                    Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Method checks whether or not the dosage specified for the substance being added is going to
     * exceed a certain percentage of the substance's target dosage, if it exists.  If it's going to
     * put things above 75% or 100%, a warning and/or confirmation dialog will be shown.
     *
     * @param subId Substance ID #
     */
    private void checkTarget(int subId) {
        SubstanceGoal goal = null;
        try {
            goal = Permanence.SG.loadSubGoalBySID(subId);
        } catch (Exception ex) {
            Toast.makeText(AdminData.this, "OMG something completely unexpected happened!",
                    Toast.LENGTH_LONG).show();
        }

        float percentOfTarget = 0.0f;
        if (goal != null) {
            percentOfTarget = goal.getPercentageOfDaysGoal(tmpUsage.getDosage());
        } else {
            percentOfTarget = -1.0f;
        }

        if (percentOfTarget > 1) {
            GlobalMisc.showDialogGetConfirmation(AdminData.this, "Are You Sure?",
                    "This will put you over your target dosage by " +
                            ((percentOfTarget - 1) * 100) + "% for the day.",
                    new GlobalMisc.OnDialogConfResultListener() {
                        @Override
                        public void onResult(boolean result) {
                            if (result) {
                                //go ahead and get the notes and save the administration
                                getNotes();
                            } else {
                                Toast.makeText(AdminData.this, "Cancelling Administration\n" +
                                        "(good choice!)", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        } else if (percentOfTarget >= 0.75) {
            GlobalMisc.showDialogGetConfirmation(AdminData.this, "Are You Sure?",
                    "This will put you at " + (percentOfTarget * 100) +
                            "% of your target dosage for the day.",
                    new GlobalMisc.OnDialogConfResultListener() {
                        @Override
                        public void onResult(boolean result) {
                            if (result) {
                                //go ahead and get the notes and save the administration
                                getNotes();
                            } else {
                                Toast.makeText(AdminData.this, "Cancelling Administration\n" +
                                        "(good choice!)", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        } else if (percentOfTarget >= 0.0) {
            //show them where they'll be after this administration
            Toast.makeText(AdminData.this, "You will be at " + (percentOfTarget * 100) +
                    "% of your goal for the day after this.", Toast.LENGTH_LONG).show();

            getNotes();
        } else {
            //just grab the notes, we don't need to pester the use (user?) right now
            getNotes();
        }
    }

    /**
     * Method is part of the user dialog chain used for recording the usage/administration's
     * particular details, in this case particularly the notes aspect.  Being the final link in the
     * chain, this also saves the Usage.
     */
    private void getNotes() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AdminData.this);
        builder.setTitle(getResources().getString(R.string.add_usage_record_notes));
        //reference the sub's units l8r

        final EditText input = new EditText(AdminData.this);

        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        //buttons
        builder.setPositiveButton(getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tmpUsage.setNotes(input.getText().toString().trim());

                Permanence.Admins.saveUsage(tmpUsage);
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    /**
     * Method takes the saturation/half-life data from the Substance's
     * getDecayCurve method and plots it onto the saturationGraph available
     * on this activity's view.
     */
    private void plotSaturationData() {
        FrameLayout graphContainer = findViewById(R.id.floAdminDataGraphFrame);

        //determine whether or not we need to change the layout based on the occasional
        //change due to 0 results to display
        if ((graphContainer.getChildCount() == 0 &&
                Permanence.Admins.getUsageCountForSubstance(
                        SubData.subList.get(subNdx).getId()) > 0) ||
                (graphContainer.getChildAt(0).getClass().getName().contains("TextView") &&
                        Permanence.Admins.getUsageCountForSubstance(
                                SubData.subList.get(subNdx).getId()) > 0)) {
            //add the graph since we've got none
            removeNoticeDisplayGraph();
        }

        //I'm going to put this in the background but not with any notification that it's going on
        //because it should only take a short bit to parse the 30*24 hours worth of entries for one
        //substance
        new Thread(new Runnable() {
            @Override
            public void run() {
                GraphView saturationDataGraph = findViewById(R.id.saturationGraph);
                Double[] rawSaturationData = new Double[30 * 24];
                try {
                    if (SubData.subList.get(subNdx).getLipid_soluble()) {
                        rawSaturationData =
                                SubData.subList.get(subNdx).getUsagePlot();
                    } else {
                        rawSaturationData =
                                SubData.subList.get(subNdx).getDecayCurve(fullEliminationCurve);
                    }
                } catch (NoResultsException ex) {
                    //this really shouldn't be an issue as we're checking for it before ever
                    //spawning this thread
                    GlobalMisc.debugMsg("plotSaturationData.run",
                            "Exception trying to fill rawSaturationData");
                }

                LocalDateTime applicableLDT = LocalDateTime.now().minusDays(30);
                Date applicableDate = GlobalMisc.fromLDTToDate(applicableLDT);

                saturationDataGraph.removeAllSeries();

                saturationDataGraph.getViewport().setXAxisBoundsManual(true);
                saturationDataGraph.getViewport().setMinX(applicableDate.getTime());
                saturationDataGraph.getViewport().setMaxX(GlobalMisc.fromLDTToDate(
                        applicableLDT.plusHours(rawSaturationData.length)).getTime());

                saturationDataGraph.getGridLabelRenderer().setLabelFormatter(
                        new DateAsXAxisLabelFormatter(AdminData.this));
                saturationDataGraph.getGridLabelRenderer().setNumHorizontalLabels(4);

                DataPoint[] eliminationPoints = new DataPoint[rawSaturationData.length];
                List<DataPoint> pointsData = null;
                int cntr;
                int cntr2 = 0;
                //count how many members of rawSaturationData need to be accounted for if we use
                //fPointsData
                for (cntr = 0; cntr < rawSaturationData.length; cntr++) {
                    if (rawSaturationData[cntr] > -0.001) { //a little buffer in case of double
                        cntr2++;                            //debauchery
                    }
                }
                final DataPoint[] fPointsData = new DataPoint[cntr2];

                if (SubData.subList.get(subNdx).getLipid_soluble()) {
                    //lipid soluble substance
                    //set up the viewport in the graph
                    saturationDataGraph.getViewport().setMinY(0.0);
                    saturationDataGraph.getViewport().setMaxY(23.0);
                    saturationDataGraph.getViewport().setYAxisBoundsManual(true);

                    //set up our eliminationPoints
                    //how many do we have?
                    pointsData = new ArrayList<>();
                    for (cntr = 0; cntr < rawSaturationData.length; cntr++) {
                        if (rawSaturationData[cntr] > -0.001) {
                            //remove the <= -0.001 values
                            pointsData.add(new DataPoint(applicableDate, rawSaturationData[cntr]));

                            GlobalMisc.debugLog("plotSaturationData.run",
                                    "Added (" + applicableDate.toString() + ", " +
                                            rawSaturationData[cntr] + ") to pointsData.");
                        }

                        applicableLDT = applicableLDT.plusHours(1);
                        applicableDate = GlobalMisc.fromLDTToDate(applicableLDT);
                    }

                    for (cntr = 0; cntr < pointsData.size(); cntr++) {
                       fPointsData[cntr] = pointsData.get(cntr);

                        GlobalMisc.debugLog("plotSaturationData.run",
                                "Putting (" + cntr + ", " + pointsData.get(cntr) +
                                ") into fPointsData[]");
                    }
                } else {
                    for (cntr = 0; cntr < rawSaturationData.length; cntr++) {
                        eliminationPoints[cntr] = new DataPoint(applicableDate,
                                rawSaturationData[cntr]);

                        applicableLDT = applicableLDT.plusHours(1);
                        applicableDate = GlobalMisc.fromLDTToDate(applicableLDT);
                    }
                }

                saturationDataGraph.getViewport().setScrollableY(true);
                saturationDataGraph.getViewport().setScalableY(true);

                final GraphView fSaturationDataGraph = saturationDataGraph;
                final DataPoint[] fEliminationPoints = eliminationPoints;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        fSaturationDataGraph.removeAllSeries();

                        if (SubData.subList.get(subNdx).getLipid_soluble()) {
                            fSaturationDataGraph.setTitle("Lipid Soluble Usages");

                            PointsGraphSeries<DataPoint> pointsGraph =
                                    new PointsGraphSeries<>(fPointsData);
                            pointsGraph.setSize(3);

                            fSaturationDataGraph.addSeries(pointsGraph);
                        } else {
                            fSaturationDataGraph.setTitle("Plasma Concentration");

                            LineGraphSeries<DataPoint> lineGraph =
                                    new LineGraphSeries<>(fEliminationPoints);
                            lineGraph.setThickness(2);

                            fSaturationDataGraph.addSeries(lineGraph);
                        }
                    }
                });

            }
        }).start();
    }

    /**
     * Method is called whenever the display needs to be updated for AdminData
     * (ie whenever the activity is first initiated, a dialog that could have
     * potentially added a new record to the previous listing is initiated, or
     * another activity has taken over for a period.
     */
    private void updateDisplay() {
        final LinearLayout fLloAdminsList = findViewById(R.id.lloAdminsList);
        fLloAdminsList.removeAllViewsInLayout();

        if (Permanence.Admins.getUsageCountForSubstance(SubData.subList.get(subNdx).getId()) > 0) {
            GlobalMisc.showResultsBeingCompiledMessage(AdminData.this);
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                List<Usage> usageList;
                Context ctxt = AdminData.this;

                if (sortAscending) {
                    usageList = Permanence.Admins.loadValidOrderedUsagesBySubid(
                            SubData.subList.get(subNdx).getId());
                } else {
                    usageList = Permanence.Admins.loadValidDescOrderedUsagesBySubid(
                            SubData.subList.get(
                                    getIntent().getExtras().getInt("SUB_NDX")).getId());
                }

                TextView[] usesTextView = new TextView[usageList.size() * 3];

                final List<Usage> uList = usageList;

                //new for[each] loop consolidating showing with and without notes
                int cntr = 0;
                int ouah = 0;
                for (Usage use : usageList) {
                    //our heading is going to be the timestamp
                    usesTextView[cntr] = new TextView(ctxt);
                    usesTextView[cntr].setText(use.getTimestamp().toString());
                    usesTextView[cntr].setTextSize(17);
                    usesTextView[cntr].setTypeface(Typeface.DEFAULT_BOLD);
                    usesTextView[cntr].setPaintFlags(usesTextView[cntr].getPaintFlags()|
                            Paint.UNDERLINE_TEXT_FLAG);

                    cntr++;
                    final int fOuah = ouah;

                    //now the usage data (don't forget to strip the timestamp from this)
                    usesTextView[cntr] = new TextView(ctxt);
                    usesTextView[cntr].setText(usageList.get(fOuah).toStringSansDT());
                    usesTextView[cntr].setTag(fOuah);
                    usesTextView[cntr].setTextSize(16);
                    if (!usageList.get(fOuah).isValid_entry()) {
                        usesTextView[cntr].setTextColor(Color.RED);
                    }

                    usesTextView[cntr].setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            AlertDialog.Builder builder =
                                    new AlertDialog.Builder(AdminData.this);

                            builder.setTitle(
                                    getResources().getString(R.string.view_details_or_delete));
                            builder.setMessage((uList.get(fOuah)).getTimestamp().toString());
                            builder.setPositiveButton(getResources().getString(R.string.delete),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Usage.wipeEntry(AdminData.this, uList.get(fOuah));

                                            if (GlobalMisc.Debugging) {
                                                Log.d("updateDisplay1", uList.toString() +
                                                        " ul size: " + uList.size());
                                            }

                                            uList.remove(fOuah);

                                            if (GlobalMisc.Debugging) {
                                                Log.d("updateDisplay2", uList.toString() +
                                                        " ul size: " + uList.size());
                                            }

                                            updateDisplay();
                                        }
                                    });
                            builder.setNeutralButton(getResources().getString(R.string.view_details_title),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            AlertDialog.Builder builder =
                                                    new AlertDialog.Builder(AdminData.this);

                                            //these are not loading a substance properly upon the
                                            //first addition of one to the substance base and then
                                            //immediately adding an administration
                                            Usage use = uList.get(fOuah);
                                            Substance sub =
                                                    Permanence.Subs.loadSubstanceById(
                                                            use.getSub_id());

                                            builder.setTitle(sub.getCommon_name() + " @ " +
                                                    use.getTimestamp().toString());
                                            builder.setMessage("ID:\t" + use.getId() +
                                                    "\nDosage:\t\t" + use.getDosage() +
                                                    sub.getUnits() + "\n\nNotes:\n" +
                                                    use.getNotes());

                                            builder.show();
                                        }
                                    });
                            builder.setNegativeButton(getResources().getString(R.string.cancel),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });

                            builder.show();

                            return false;
                        }
                    });

                    cntr++;

                    if (notesView) {
                        //this should probably have a click/onclick listener linked to the same shit
                        //as the textviews are above
                        //cntr++;
                        usesTextView[cntr] = new TextView(AdminData.this);
                        usesTextView[cntr].setText(usageList.get(fOuah).getNotes());
                        usesTextView[cntr++].setTextSize(17);
                    }

                    ouah++;
                }

                final TextView[] fUsesTextView = usesTextView;

                //and now we can finally write it all to the display
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        for (TextView currentTextView : fUsesTextView) {
                            if (currentTextView != null) {
                                fLloAdminsList.addView(currentTextView);
                            } else {
                                break;
                            }
                        }
                    }
                });

                updating = false;
            }
        }).start();
    }

    /**
     * Method toggles the main AdminData activity's display back and forth between the standard/
     * more compact view and a larger version that displays each administration's notes in-line
     * with the usage/administration information.
     *
     * @param v requiem for an onCreate handler
     */
    public void toggleNotesInMainView(View v) {
        Button btnToggleNotes = findViewById(R.id.btnViewNotes);

        notesView = !notesView;
        if (notesView) {
            btnToggleNotes.setText(getResources().getString(R.string.view_notes_off));
        } else {
            btnToggleNotes.setText(getResources().getString(R.string.view_notes_on));
        }

        updateDisplay();
    }

    /**
     * Method handles reversing the sort order in the primary display.
     *
     * @param v onClick requirement
     */
    public void onClickButtonReverse(View v) {
        sortAscending = !sortAscending;

        updateDisplay();
    }

    /**
     * Toggles whether or not entries without the proper validEntry boolean flag will be displayed
     * along with the valid entries.
     *
     * @param v onClick requirement
     */
    /*public void onViewInvalidEntries(View v) {
        showInvalid = !showInvalid;

        loadUsages();
    }*/

    /**
     * Much as the method says, this handles the click on the 'search notes'
     * button and starts the NotesSearch activity.
     *
     * @param v View
     */
    public void onSearchNotes(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AdminData.this);
        builder.setTitle("Search String");

        final EditText input = new EditText(AdminData.this);

        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(AdminData.this, NotesSearch.class);
                intent.putExtra("SUBSTR", input.getText().toString());
                intent.putExtra("SUB_NDX",
                        SubData.subList.get(
                                getIntent().getExtras().getInt("SUB_NDX")).getId());

                startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    /**
     * Method handles the click handler for the view full elimination curve
     * option.  It just toggles the 'fullEliminationCurve' class-wide variable
     * here and then re-hits plotSaturationData().
     *
     * @param v View
     */
    public void onClickFullEliminationCurve(View v) {
        if (Permanence.Admins.getUsageCountForSubstance(SubData.subList.get(subNdx).getId()) > 0) {
            Toast.makeText(AdminData.this, "Re-crunching graph data, please wait . . .",
                    Toast.LENGTH_LONG).show();

            fullEliminationCurve = !fullEliminationCurve;
            Button fullEliminationCurveToggle = findViewById(R.id.btnToggleFullEliminationPlot);

            if (fullEliminationCurve) {
                fullEliminationCurveToggle.setText(getString(R.string.current_elimination_curve));
            } else {
                fullEliminationCurveToggle.setText(getString(R.string.full_elimination_curve));
            }

            plotSaturationData();
        }
    }

    /**
     * Method handles removing the GraphView and displaying a TextView instead,
     * explaining that there are no results for the current substance found.
     */
    private void removeGraphDisplayNotice() {
        //GraphView saturationDataGraph = findViewById(R.id.saturationGraph);
        TextView tvwNoResults = new TextView(AdminData.this);

        tvwNoResults.setText(getString(R.string.no_results_found,
                Permanence.Subs.loadSubstanceById(
                        SubData.subList.get(subNdx).getId()).getCommon_name()));
        tvwNoResults.setTextSize(20);
        tvwNoResults.setTextColor(getResources().getColor(R.color.colorDarkOrange));
        tvwNoResults.setGravity(Gravity.CENTER);

        FrameLayout floGraphFrame = findViewById(R.id.floAdminDataGraphFrame);
        //floGraphFrame.removeView(saturationDataGraph);
        floGraphFrame.removeAllViewsInLayout();
        floGraphFrame.addView(tvwNoResults);
    }

    /**
     * Method handles removing the notice that (may have been/was) previously
     * displayed and puts the GraphView up where it should be.
     */
    private void removeNoticeDisplayGraph() {
        FrameLayout graphContainer = findViewById(R.id.floAdminDataGraphFrame);

        //remove the TextView notification that we've got 0 results and plot our shit
        graphContainer.removeAllViewsInLayout();
        View graphView = LayoutInflater.from(this).inflate(R.layout.graph_container, null);
        graphContainer.addView(graphView);
    }


}
