package com.example.sprite.half_lifetimer;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.HashMap;
import java.util.List;

@Entity(tableName="UsualSuspect")
public class UsualSuspect {
    @PrimaryKey(autoGenerate = true)
    private int         id;
    @ColumnInfo(name="name")
    private String      name;
    @ColumnInfo(name="notes")
    private String      notes;

    public UsualSuspect(String name, String notes) {
        this.name = name;
        this.notes = notes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getSids() {
        List<Integer> tmpSids = null;

        for (Substance tmpSub : Permanence.US.loadUSSubs(this.id)) {
            tmpSids.add(tmpSub.getId());
        }

        return tmpSids;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String toString() {
        boolean first = true;

        String subInfo = "";

        GlobalMisc.debugMsg("UsualSuspect.toString", "Looking up CrossRefs for" +
                " Usual Suspect ID: " + this.id);

        for (UsualSuspectSubsCrossRef tmpCX :
                Permanence.CrossRef.loadCrossRefsPerUsualSuspect(this.id)) {
            if (!first) {
                subInfo += ", ";
            }

            Substance tmpSub = Permanence.Subs.loadSubstanceById(tmpCX.getSub());
            subInfo += tmpSub.getCommon_name() + ": " + tmpCX.getDosage() +
                    GlobalMisc.dUnitsToString(tmpSub.getUnits()) + " Sub ID: " +
                    tmpSub.getId();

            first = false;
        }

        return this.id + ": " + this.name + " (" + subInfo + ") " + "Notes: " + this.notes + "\n";
    }

    /**
     * Method loads the UsualSuspect's goals by UsualSuspect ID.
     *
     * @return List of SubstanceGoals
     */
    public List<SubstanceGoal> loadGoalsPerUsualSuspect() {
        List<UsualSuspectSubsCrossRef> crossRefs =
                Permanence.CrossRef.loadCrossRefsPerUsualSuspect(this.id);
        List<SubstanceGoal> applicableGoals = null;

        for (UsualSuspectSubsCrossRef crossRef : crossRefs) {
            applicableGoals.add(Permanence.SG.loadSubGoalBySID(crossRef.getSub()));
        }

        return applicableGoals;
    }

    /**
     * Method returns the SIDs and dosages of the current UsualSuspect.
     *
     * @return HashMap of SIDs to [float] dosages
     */
    public HashMap<Integer, Float> getSIDsAndDosages() {
        GlobalMisc.debugMsg("UsualSuspect.getSIDsAndDosage",
                "Looking up SIDs and dosages for US id #: " + this.id);

        HashMap<Integer, Float> tmpMap = new HashMap<>();

        List<UsualSuspectSubsCrossRef> crossRefs =
                Permanence.CrossRef.loadCrossRefsPerUsualSuspect(this.id);

        for (UsualSuspectSubsCrossRef crossref : crossRefs) {
            tmpMap.put(crossref.getSub(), crossref.getDosage());
        }

        return tmpMap;
    }

    public UsualSuspect() { }
}
