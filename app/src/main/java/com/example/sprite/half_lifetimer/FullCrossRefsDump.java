package com.example.sprite.half_lifetimer;

import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class FullCrossRefsDump extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_cross_refs_dump);

        updateDisplay(FullCrossRefsDump.this, Permanence.CrossRef.loadCrossRefs());
    }

    /**
     * Method updates the damn display.
     *
     * @param ctxt current context
     * @param crossRefs list of UsualSuspectSubsCrossRef'ed
     */
    private void updateDisplay(Context ctxt, List<UsualSuspectSubsCrossRef> crossRefs) {
        LinearLayout lloCXD = findViewById(R.id.lloCXDump);
        TextView[] txtCXs = new TextView[crossRefs.size()];

        for (int cntr = 0; cntr < crossRefs.size(); cntr++) {
            txtCXs[cntr] = new TextView(ctxt);
            try {
                txtCXs[cntr].setText(
                        Permanence.US.loadUSByID(crossRefs.get(cntr).getUsualSuspect()).getName() +
                                ": " +
                                Permanence.Subs.loadSubstanceById(crossRefs.get(cntr).getSub()).getSci_name() +
                                " @ " + crossRefs.get(cntr).getDosage() +
                                GlobalMisc.dUnitsToString(
                                        Permanence.Subs.loadSubstanceById(
                                                crossRefs.get(cntr).getSub()).getUnits()));
            } catch (Exception ex) {
                txtCXs[cntr].setText("no usual suspect: " +
                        Permanence.Subs.loadSubstanceById(crossRefs.get(cntr).getSub()).getSci_name() +
                        " @ " + crossRefs.get(cntr).getDosage() +
                        GlobalMisc.dUnitsToString(
                                Permanence.Subs.loadSubstanceById(
                                        crossRefs.get(cntr).getSub()).getUnits()));
            }

            txtCXs[cntr].setTextSize(18);

            lloCXD.addView(txtCXs[cntr]);
        }
    }
}
