package com.example.sprite.half_lifetimer;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import android.content.Context;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import static com.example.sprite.half_lifetimer.SubData.subList;

/**
 * Class provides the structure for the different substance records utilized for administration
 * recording purposes.
 */
@Entity
public class Substance {
    @PrimaryKey(autoGenerate = true)
    private int         id;
    @ColumnInfo(name="units")
    private DosageUnit  units;
    @ColumnInfo(name="cname")
    private String      common_name;
    @ColumnInfo(name="sname")
    private String      sci_name;
    @ColumnInfo(name="halflife")
    private float       halflife = 0;
    @ColumnInfo(name = "detectable_halflife")
    public float        dHalflife = 0;      //um, why was this again?  We need to fix this
    @ColumnInfo(name="lipid_soluble")
    private boolean     lipid_soluble = false;
    @ColumnInfo(name="sub_class")
    private int         sClass = -1;
    @ColumnInfo(name="archived")
    private boolean     archived = false;

    public Substance() { }

    public Substance(DosageUnit measure, String cname, String sname) {
        this.units = measure;
        this.common_name = cname;
        this.sci_name = sname;
    }

    public Substance(String cname, String sname, DosageUnit units, float halflife, float dHalflife,
                     boolean lipid_soluble, int sClass) {
        this.common_name = cname;
        this.sci_name = sname;
        this.units = units;
        this.halflife = halflife;
        this.dHalflife = dHalflife;
        this.lipid_soluble = lipid_soluble;
        this.sClass = sClass;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DosageUnit getUnits() {
        return this.units;
    }

    public void setUnits(DosageUnit newUnits) {
        this.units = newUnits;
    }

    public String getCommon_name() {
        return this.common_name;
    }

    public void setCommon_name(String cname) {
        this.common_name = cname;
    }

    public String getSci_name() {
        return this.sci_name;
    }

    public void setSci_name(String sname) {
        this.sci_name = sname;
    }

    public float getHalflife() {
        return halflife;
    }

    public void setHalflife(float halflife) {
        this.halflife = halflife;
    }

    public float getdHalflife() {
        return dHalflife;
    }

    public void setdHalflife(float dHalflife) {
        this.dHalflife = dHalflife;
    }

    public boolean getLipid_soluble() {
        return this.lipid_soluble;
    }

    public void setLipid_soluble(boolean l_soluble) {
        this.lipid_soluble = l_soluble;
    }

    public int getSClass() {
        return sClass;
    }

    public void setSClass(int sClass) {
        this.sClass = sClass;
    }

    public boolean getArchived() {
        return this.archived;
    }

    public void setArchived(boolean arced) {
        this.archived = arced;
    }

    public String toString() {
        //based on the order we define these with user dialog, this _should_
        //work and avoid the null toString() errors.  heh
        //TODO: remove the redundancy here
        if (this.sci_name == null) {
            return this.common_name;
        } else if (this.units == null) {
            return this.common_name + " (" + this.sci_name + ")";
        } else if (this.halflife == 0 || this.dHalflife == 0) {
            return this.common_name + " (" + this.sci_name + "), dosage administered in " +
                    this.units.toString() + " lipid soluble: " + this.lipid_soluble;
        } else {
            return this.common_name + " (" + this.sci_name + "), dosage administered in " +
                    this.units.toString() + " halflife (total/detected in hours): " +
                    this.halflife + "/" + this.dHalflife +
                    " classification: " + this.sClass +
                    " lipid soluble: " + lipid_soluble;
        }
    }

    /**
     * Method returns the amount of this substance taken during the current 24
     * hour period.
     *
     * @return dosage taken today
     */
    public float getTotalDosageToday() {
        float tmpTotalDosage = 0.0f;

        LocalTime midnight = LocalTime.MIDNIGHT;
        LocalDate today = LocalDate.now();
        LocalDateTime todayMidnight = LocalDateTime.of(today, midnight);

        List<Usage> todaysUses = Permanence.Admins.getPostToTSUsageRecords(this.id,
                Converters.fromLocalDateTime(todayMidnight));

        for (Usage use : todaysUses) {
            tmpTotalDosage += use.getDosage();
        }

        return tmpTotalDosage;
    }

    /**
     * Method wipes an entry from the internal data structure that we're
     * currently working with, making sure to cleanse it in a way that will
     * leave the List suitable for updateDisplay().
     *
     * @param position List index to toast
     * @return new substance list
     */
    public static List<Substance> wipeEntry(Context ctxt, int position) {
        Permanence.Subs.wipeSub(subList.get(position));
        SubData.subList = Permanence.Subs.loadUnarchivedSubstances(ctxt);

        return SubData.subList;
    }

    /**
     * Method determines whether or not a substance is fully cleared.
     *
     * @return boolean signifying whether or not substance has been metabolized
     */
    public boolean isCleared() {
        boolean cleared;

        try {
            cleared = this.getFullEliminationLDT().isBefore(LocalDateTime.now());
        } catch (NullPointerException ex) {
            //it looks like this substance hasn't had any usages at all
            cleared = true; //since it's never been in the body anyway
        }

        if (cleared) {
            GlobalMisc.debugMsg("isCleared", this.getCommon_name() +
                    " has been cleared from the body");
        } else {
            GlobalMisc.debugMsg("isCleared", this.getCommon_name() +
                    " has not been cleared from the body yet");
        }

        return cleared;
    }

    /**
     * Method takes the number of days being graphed and, if there are any
     * usages for the substance during this many days ago and now, returns
     * true, otherwise returns false.
     *
     * To be used primarily to determine whether or not a lipid soluble
     * substance (currently with no decay data) should be included on a graph,
     * since we can't really use isCleared() above.
     *
     * @param daysBeingGraphed int number of days to go back into admin history
     * @return boolean true for include in graph, false for opposite
     */
    public boolean isOnGraph(int daysBeingGraphed) {
        LocalDateTime startGraphLDT = LocalDateTime.now().minusDays(daysBeingGraphed);

        if (Permanence.Admins.countPostToTimestampForSub(
                Converters.fromLocalDateTime(startGraphLDT), this.getId()) > 0) {
            return true;
        }

        return false;
    }

    /**
     * Method very crudely checks to see if this substance is THC, returning
     * true of false depending on whether or not the appropriate substrings
     * are found within the common and/or scientific names of the substance.
     *
     * @return boolean true if this is THC, false otherwise
     */
    public boolean isTHC() {
        if (this.getLipid_soluble() &&
                (this.getSci_name().toUpperCase().contains("THC") ||
                        this.getSci_name().toUpperCase().contains("TETRAHYDROCANNABINOL") ||
                        this.getCommon_name().toUpperCase().contains("MARIJUANA"))) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Method calculates the time that it will take for the substance to be
     * fully eliminated/undetectable in the body based on the half-life of the
     * substance and the last date that it was administered.
     *
     * @return LocalDateTime projection of full elimination
     */
    public LocalDateTime getFullEliminationLDT() {
        if (!this.getLipid_soluble()) {
            if (Permanence.Admins.getUsageCountForSubstance(this.getId()) == 0) {
                return null;
            }

            LocalDateTime lastUsageLDT = Converters.toLocalDateTime(
                    Permanence.Admins.getLatestUsageTimestampBySid(this.getId()));

            float longestHalflife = Math.max(this.getHalflife(), this.getdHalflife());

            GlobalMisc.debugMsg("getFullEliminationLDT",
                    this.getCommon_name() + " longestHalflife contains: " + longestHalflife +
                            "\nlongestHalflife * 60f yields: " +
                            Math.round(longestHalflife * 60f));

            Duration fullEliminationSpan =
                    Duration.ofMinutes(
                            Math.round((longestHalflife * 60f *
                                    GlobalMisc.HalflifeFullEliminationMultiplicand)));

            GlobalMisc.debugMsg("getFullEliminationLDT", "fullEliminationSpan: " +
                    fullEliminationSpan.toString() + "\nfull elimination at: " +
                    lastUsageLDT.plus(fullEliminationSpan));
            GlobalMisc.debugMsg("getFullEliminationLDT", "lastUsageLDT: " +
                    lastUsageLDT.toString());

            return lastUsageLDT.plus(fullEliminationSpan);
        } else if (this.getLipid_soluble() && this.isTHC()) {
            //now this is the algorithm that really works on things here; estimating 30 days as 4.29
            //weeks here
            //1-2x/wk will register as 'single usage'
            double singleUsageTotal = 2 * 4.29;
            //4-5x/wk will register as 'moderate use'
            double moderateUseTotal = 5 * 4.29;
            //6-7x/wk will register as 'heavy use'
            double heavyUse = 7 * 4.29;
            //prolonged stretches of 'heavy use' will register as chronic heavy use
            List<Usage> pastMonthTHCUsages = Permanence.Admins.getPostToTSOrderedUsageRecords(
                    this.getId(), Converters.fromLocalDateTime(LocalDateTime.now().minusDays(30)));
            int numUses = pastMonthTHCUsages.size();

            if (numUses > 0) {
                LocalDateTime lastUsageLDT =
                        pastMonthTHCUsages.get(pastMonthTHCUsages.size() - 1).getTimestamp();

                //we're just going to use an average count algorithm right now, instead of breaking
                //down the weeks and trying to detail things further...  This would be so much
                //easier with the goddamned calc algorithm.  So let it be known that this will be
                //again erring on the margin of safety, but will be more granular than '30 days' for
                //any usage
                if (numUses >= heavyUse) {
                    //chronic heavy usage
                    return lastUsageLDT.plusDays(30);
                } else if (numUses >= moderateUseTotal) {
                    //heavy usage
                    return lastUsageLDT.plusDays(10);
                } else if (numUses >= singleUsageTotal) {
                    //moderate usage
                    return lastUsageLDT.plusDays(5);
                } else {
                    //single usage (or spread out single usages)
                    return lastUsageLDT.plusDays(5);
                }
            } else {
                return null;   //clear since nao/forever/whatever
            }
        } else {
            //we're working with a lipid soluble which we don't have an algorithm for yet

            return null;
        }
    }

    /**
     * This method plots the amount of the substance/metabolites remaining in
     * the body after the day's metabolism/excretion of the substance, also
     * taking into account any further administrations of the substance.
     *
     * @param projectToFullElimination boolean calculate to full elimination
     *                                 or just to the present day
     * @return HashMap day:amount remaining in the body
     */
    public Double[] getDecayCurve(boolean projectToFullElimination) throws NoResultsException {
        //I think we'll just default to working with hours right now, since that's what the half-
        //life values are given in, too

        Double[] decayCurve = new Double[30 * 24];
        List<Usage> applicableUsages;
        LocalDateTime currentTimestamp = LocalDateTime.now().minusDays(30);
        double lipidConcentration = 0;
        double totalDosagePresent = 0;
        boolean gotResults = false;

        if (getLipid_soluble()) {
            //just do the scatterplot for lipid solubles, so they can at least see their history of
            //usage, if not decay curves
            return getUsagePlot();

            //graph the scatterplot
            //double administrationBump = (1f / 8f);
            //float halflife = 24f * 1.4f;
            //int cntr = 0;
            //int dayHours = 0;

            /*while (currentTimestamp.isBefore(LocalDateTime.now().minusHours(1))) {
                applicableUsages = Permanence.Admins.getBetweenSpanValidUsagesForSub(this.getId(),
                        Converters.fromLocalDateTime(currentTimestamp),
                        Converters.fromLocalDateTime(currentTimestamp.plusHours(1)));

                //subtract the day's metabolism of THC-COOH
                lipidConcentration = lipidConcentration / Math.pow(2f, (1f / halflife));
                if (lipidConcentration < GlobalMisc.LipidSolubleAllGone) {
                    //let's not try going into negative concentrations here, this is complicated
                    //enough already; also the reason that we're checking whether or not we're below
                    //0.035 as opposed to just 0 is because the division would go on infinitely,
                    //otherwise, and at a certain point, as Sam said, the body is just going to
                    //sweep up the rest
                    lipidConcentration = 0;
                }

                //add the day's usage to the lipidConcentraiton here, unless we've had another
                //dosage within the preceding 24 hours
                if (dayHours == 0) {
                    if (applicableUsages.size() > 0) {
                        if ((lipidConcentration + administrationBump) > 1) {
                            //bump lipidConcentration to 100% for the day
                            lipidConcentration = 1;
                        } else {
                            //bump lipidConcentration closer to 100% for the day
                            lipidConcentration += administrationBump;
                        }

                        dayHours = 23;
                    }
                } else {
                    dayHours--;
                }

                //add our plot data
                //Date applicableDate = GlobalMisc.convertLocalDateTimeToDate(currentTimestamp);
                decayCurve[cntr++] = lipidConcentration;

                //increment our timestamp
                currentTimestamp = currentTimestamp.plusHours(1);

                //modify our flag (if needed)
                if (applicableUsages.size() > 0) {
                    gotResults = true;
                }
            }*/
        } else {
            //plasma soluble only
            float halflife = Math.max(this.getdHalflife(), this.getHalflife());
            int cntr = 0;

            while (currentTimestamp.isBefore(LocalDateTime.now().minusHours(1))) {
                //note that this is not a percentage of saturation, so we do not have an upper bound
                //for totalDosagePresent; we will, however, still need to watch for a lower
                //threshold, and/or add this detectability threshold to the Substance database
                //record, perhaps
                applicableUsages = Permanence.Admins.getBetweenSpanValidUsagesForSub(this.getId(),
                        Converters.fromLocalDateTime(currentTimestamp),
                        Converters.fromLocalDateTime(currentTimestamp.plusHours(1)));

                totalDosagePresent = totalDosagePresent / Math.pow(2f, (1f / halflife));

                if (totalDosagePresent < GlobalMisc.PlasmaSolubleAllGone) {
                    totalDosagePresent = 0;
                }

                for (Usage usage : applicableUsages) {
                    totalDosagePresent += usage.getDosage();
                }

                //add the plot data
                //Date applicableDate = GlobalMisc.fromLDTToDate(currentTimestamp);
                decayCurve[cntr++] = totalDosagePresent;

                //increment our timestamp
                currentTimestamp = currentTimestamp.plusHours(1);

                //modify our flag (if needed)
                if (applicableUsages.size() > 0) {
                    gotResults = true;
                }
            }
        }

        if (!gotResults) {
            throw(new NoResultsException());
        }

        //if we've decided to project this to full elimination, then we just need to keep on going
        //with the projections (at least for lipid solubles) until the time elapsed since the last
        //dosage is >= 5.7 * its halflife
        HashMap<Integer, Double> eliminationFuture = new HashMap<>();

        if (projectToFullElimination && !getLipid_soluble()) {
            int cntr = 30 * 24;
            float halflife;
            double amtPresent;

            halflife = Math.max(this.getdHalflife(), this.getHalflife());
            amtPresent = totalDosagePresent;

            do {
                amtPresent = amtPresent / Math.pow(2f, (1f / halflife));

                eliminationFuture.put(cntr++, amtPresent);
            } while (amtPresent >= GlobalMisc.PlasmaSolubleAllGone);
        } else if (projectToFullElimination && this.isTHC()) {
            int cntr = 30 * 24;
            float stepDown = 1f / 30f;

            //TODO: add note that explains that this is a crude algorithm on activity view
            while (lipidConcentration > 0.0) {
                lipidConcentration -= stepDown;

                if (lipidConcentration < GlobalMisc.LipidSolubleAllGone) {
                    lipidConcentration = 0.0;
                }

                eliminationFuture.put(cntr++, lipidConcentration);
            }
        }

        if (projectToFullElimination) {
            Double[] decayAndEliminationCurve =
                    new Double[eliminationFuture.size() + decayCurve.length];
            int cntr;

            for (cntr = 0; cntr < decayCurve.length; cntr++) {
                decayAndEliminationCurve[cntr] = decayCurve[cntr];
            }

            for (; cntr < (eliminationFuture.size() + decayCurve.length); cntr++) {
                decayAndEliminationCurve[cntr] = eliminationFuture.get(cntr);
            }

            return decayAndEliminationCurve;
        }

        return decayCurve;
    }

    /**
     *
     * @return
     */
    public Double[] getUsagePlot() {
        LocalDateTime currentTimestamp = LocalDateTime.now().minusDays(GlobalMisc.DaysToGraph);
        List<Usage> applicableUsages;
        Double[] usePlot = new Double[GlobalMisc.DaysToGraph * 24];
        int cntr = 0;

        while (currentTimestamp.isBefore(LocalDateTime.now().minusHours(1))) {
            applicableUsages = Permanence.Admins.getBetweenSpanValidUsagesForSub(this.getId(),
                    Converters.fromLocalDateTime(currentTimestamp),
                    Converters.fromLocalDateTime(currentTimestamp.plusHours(1)));

            //add our plot data
            if (applicableUsages.size() > 0) {
                GlobalMisc.debugLog("Substance.getUsagePlot",
                        "Adding usage @ " + currentTimestamp.toString());

                usePlot[cntr] = (double) currentTimestamp.getHour();
            } else {
                usePlot[cntr] = -1.0;
            }

            //increment our timestamp
            currentTimestamp = currentTimestamp.plusHours(1);
            cntr++;
        }

        return usePlot;
    }

    /**
     * Method handles determining which substances are still present in the
     * body and still need to be fully cleared.
     *
     * @return List<Substance> subs that are still present in the body
     */
    public static List<Substance> getCurrentlyClearingSubs(Context ctxt) {
        List<Substance> stillClearingSubs = new ArrayList<>();

        for (Substance sub : Permanence.Subs.loadUnarchivedSubstances(ctxt)) {
            if (sub.isCleared()) {
                continue;
            }

            stillClearingSubs.add(sub);
        }

        return stillClearingSubs;
    }

    /**
     * Method overrides the standard Object .equals() method to be used in
     * comparing for equality; this has become a problem for us in
     * PeriodDetails and will no doubt haunt us in other locations when we are
     * trying to compare objects for equality which are not precisely the same
     * instance.
     */
    @Override
    public boolean equals(Object o) {
        //self check
        if (this == o) {
            return true;
        }

        //null check
        if (o == null) {
            return false;
        }

        //type check & cast
        if (getClass() != o.getClass()) {
            return false;
        }

        Substance substance = (Substance) o;
        return Objects.equals(id, substance.id); // && Objects.equals(units, substance.units) &&
                //Objects.equals(common_name, substance.common_name) &&
                //Objects.equals(sci_name, substance.sci_name) &&
                //Objects.equals(halflife, substance.halflife) &&
                //Objects.equals(dHalflife, substance.dHalflife) &&
                //Objects.equals(lipid_soluble, substance.lipid_soluble) &&
                //Objects.equals(sClass, substance.sClass) &&
                //Objects.equals(archived, substance.archived);
    }

    /**
     * Method ovverrides the standard Object .hashCode() method to be used in
     * the shortcut route for determining equality.  As in .equals(), this is
     * being overriden due to the fact that comparing objects for equality
     * which are not precisely the same instance is now giving us grief.
     */
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
