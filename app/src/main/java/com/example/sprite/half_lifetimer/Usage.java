package com.example.sprite.half_lifetimer;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import android.content.Context;

import java.time.LocalDateTime;

@Entity
public class Usage {
    @PrimaryKey(autoGenerate = true)
    private int             id;
    @ColumnInfo(name="sub_id")
    private int             sub_id;
    @ColumnInfo(name="dosage")
    private float           dosage;
    @ColumnInfo(name="timestamp")
    private LocalDateTime   timestamp;
    @ColumnInfo(name="notes")
    private String          notes;
    @ColumnInfo(name="valid_entry")
    private boolean         valid_entry;

    public Usage() {
        this.valid_entry = true;
    }

    /*public Usage(int sid, float dose, String note) {
        this.sub_id = sid;
        this.dosage = dose;
        this.timestamp = null;
        this.notes = note;
        this.valid_entry = true;
    }*/

    public Usage(int sid, float dose, LocalDateTime ts, String note) {
        this.sub_id = sid;
        this.dosage = dose;
        this.timestamp = ts;
        this.notes = note;
        this.valid_entry = true;
    }

    public Usage(int sid, float dose, LocalDateTime ts, String note, boolean valid) {
        this.sub_id = sid;
        this.dosage = dose;
        this.timestamp = ts;
        this.notes = note;
        this.valid_entry = valid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSub_id() {
        return sub_id;
    }

    public void setSub_id(int sub_id) {
        this.sub_id = sub_id;
    }

    public float getDosage() {
        return dosage;
    }

    public void setDosage(float dosage) {
        this.dosage = dosage;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public boolean isValid_entry() {
        return valid_entry;
    }

    public void setValid_entry(boolean valid_entry) {
        this.valid_entry = valid_entry;
    }

    public static void wipeEntry(Context ctxt, Usage use) {
        Permanence.Admins.wipeUsage(use);
    }

    public String obtainUnitString() {
        return GlobalMisc.dUnitsToString(Permanence.Subs.loadSubstanceById(this.sub_id).getUnits());
    }

    public String obtainSubName() {
        return Permanence.Subs.loadSubstanceById(this.sub_id).getCommon_name();
    }

    public void setInvalid() {
        this.valid_entry = false;

        Permanence.Admins.updateUsage(this);
    }

    public String toString() {
        try {
            return obtainSubName() + " (admin ID: " + this.id + ") at " + this.timestamp +
                    " \tDose: " + this.dosage + obtainUnitString() + " Valid: " +
                    this.valid_entry;
        } catch (Exception ex) {
            return "invalid data" /*+ ex.toString()*/;
        }
    }

    /**
     * Method is another version of the above toString() method, basically, but without the date/
     * time string so that this can be used under the heading given by the date/time.
     *
     * @return String value of the Usage (sans DT value)
     */
    public String toStringSansDT() {
        try {
            return obtainSubName() + " (admin ID: " + this.id + ") Dose: " + this.dosage +
                    obtainUnitString();
        } catch (Exception ex) {
            return "invalid data";
        }
    }
}
