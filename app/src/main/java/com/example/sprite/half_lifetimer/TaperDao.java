package com.example.sprite.half_lifetimer;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface TaperDao {
    @Query("SELECT * FROM Taper")
    List<Taper> getAll();

    @Query("SELECT * FROM Taper WHERE valid LIKE 1")
    List<Taper> getAllValid();

    @Query("SELECT Taper.* FROM Taper INNER JOIN Substance WHERE Taper.substance_id LIKE " +
           "Substance.id AND Substance.archived LIKE 0")
    List<Taper> getAllUnarchived();

    @Query("SELECT Taper.* FROM Taper INNER JOIN Substance WHERE Taper.substance_id LIKE " +
           "Substance.id AND Taper.valid LIKE 1 AND Substance.archived LIKE 0")
    List<Taper> getAllValidUnarchived();

    @Query("SELECT * FROM Taper WHERE valid LIKE 1 AND substance_id LIKE :sid")
    List<Taper> getAllValidBySid(int sid);

    @Query("SELECT * FROM Taper WHERE valid LIKE 1 AND substance_id LIKE :sid LIMIT 1")
    Taper getOneValidBySid(int sid);

    @Query("SELECT * FROM Taper WHERE id LIKE :tid")
    Taper getTaperById(int tid);

    @Insert
    void insertTaper(Taper taper);

    @Update
    void updateTaper(Taper taper);

    @Delete
    void delete(Taper taper);
}
