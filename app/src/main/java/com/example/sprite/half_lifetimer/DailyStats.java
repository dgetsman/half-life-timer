package com.example.sprite.half_lifetimer;

import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;

public class DailyStats extends AppCompatActivity {

    /**
     * Method is the onCreate handler; takes care of seeing the heading and calls updateDisplay()
     * to add relevant information to the display.
     *
     * @param savedInstanceState requiem for onCreate
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_stats);

        TextView tvwMainHeading = findViewById(R.id.tvwDailyStatsHeader);
        tvwMainHeading.setText(getString(R.string.daily_stats_header,
                Permanence.Subs.loadSubstanceById(
                        getIntent().getExtras().getInt("SUB_NDX")).getCommon_name()));

        updateDisplay();
    }

    /**
     * Method fills in DailyStats activity's tabulated data.
     */
    private void updateDisplay() {
        Context ctxt = DailyStats.this;

        Substance sub =
                Permanence.Subs.loadSubstanceById(getIntent().getExtras().getInt("SUB_NDX"));
        GlobalMisc.debugMsg("updateDisplay", "SUB_NDX: " +
                getIntent().getExtras().getInt("SUB_NDX") + "\tsub: " + sub.toString());

        LinearLayout lloUsages = findViewById(R.id.lloDailyUsages);

        //now we need two LDTs, one for now and one for 'x' days ago
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime xDaysAgo = LocalDateTime.now().minus(Duration.ofDays(
                getIntent().getExtras().getInt("DAYS_AGO")));
        GlobalMisc.debugMsg("updateDisplay", "DAYS_AGO: " +
                getIntent().getExtras().getInt("DAYS_AGO") + "\txDaysAgo: " +
                xDaysAgo.toString());

        List<Usage> durationUsages = Permanence.Admins.getBetweenSpanValidUsagesForSub(sub.getId(),
                Converters.fromLocalDateTime(xDaysAgo), Converters.fromLocalDateTime(now));

        if (durationUsages.size() == 0) {
            GlobalMisc.showSimpleDialog(DailyStats.this, "No Usages to Graph",
                    "There have been no usages found for this time period; unable to " +
                    "summarize.  Return to the previous activity.");

            return;
        }

        TextView[] usesTextView = new TextView[durationUsages.size()];
        Duration[] durationsBetwixtUses = new Duration[durationUsages.size() - 1];

        LinearLayout lloDurations = findViewById(R.id.lloDurationsListing);
        TextView[] durationsTextView = new TextView[durationUsages.size() - 1];

        int cntr = 0;
        float totalAdminned = 0f;
        float highDosage = 0f;
        float lowDosage = Float.MAX_VALUE;

        for (Usage use : durationUsages) {
            //set up the display of usages for this 24 hour period
            usesTextView[cntr] = new TextView(ctxt);
            usesTextView[cntr].setText(use.toString());

            lloUsages.addView(usesTextView[cntr++]);

            //and do our math to figure out the total and averages here, along with other stats
            totalAdminned += use.getDosage();
            if (highDosage < use.getDosage()) {
                highDosage = use.getDosage();
            }
            if (lowDosage > use.getDosage()) {
                lowDosage = use.getDosage();
            }

            //figure out our durations
            if (cntr > 0 && cntr < durationUsages.size()) {
                durationsBetwixtUses[cntr - 1] = Duration.between(
                        durationUsages.get(cntr - 1).getTimestamp(),
                        durationUsages.get(cntr).getTimestamp()
                );

                durationsTextView[cntr - 1] = new TextView(ctxt);
                durationsTextView[cntr - 1].setText(GlobalMisc.durationToString(
                        durationsBetwixtUses[cntr - 1]));
                lloDurations.addView(durationsTextView[cntr - 1]);
            }
        }

        float averageAdminned = totalAdminned / durationUsages.size();
        float averageAdminnedPerDay = totalAdminned / getIntent().getExtras().getInt("DAYS_AGO");

        TableLayout tloUsageStats = findViewById(R.id.tloStatsTable);
        TableRow trwCountNAverage = new TableRow(ctxt);

        TextView tvwCountLabel = new TextView(ctxt);
        tvwCountLabel.setText(getString(R.string.count));
        TextView tvwCountContent = new TextView(ctxt);
        tvwCountContent.setText(String.format(Locale.getDefault(), "%1$d", durationUsages.size()));
        TextView tvwAvgLabel = new TextView(ctxt);
        tvwAvgLabel.setText(getString(R.string.average_administered));
        TextView tvwAvgContent = new TextView(ctxt);
        tvwAvgContent.setText(String.format(Locale.getDefault(), "%1$f", averageAdminned));

        TableRow.LayoutParams lp = new TableRow.LayoutParams();
        lp.weight = 0.25f;

        tvwCountLabel.setLayoutParams(lp);
        tvwCountContent.setLayoutParams(lp);
        tvwAvgLabel.setLayoutParams(lp);
        tvwAvgContent.setLayoutParams(lp);
        trwCountNAverage.addView(tvwCountLabel);
        trwCountNAverage.addView(tvwCountContent);
        trwCountNAverage.addView(tvwAvgLabel);
        trwCountNAverage.addView(tvwAvgContent);
        tloUsageStats.addView(trwCountNAverage);

        TableRow trwHighNLow = new TableRow(ctxt);
        TextView tvwHighDosageLabel = new TextView(ctxt);
        tvwHighDosageLabel.setText(getString(R.string.high_dose));
        TextView tvwHighDosageContent = new TextView(ctxt);
        tvwHighDosageContent.setText(String.format(Locale.getDefault(), "%1$f", highDosage));
        TextView tvwLowDosageLabel = new TextView(ctxt);
        tvwLowDosageLabel.setText(getResources().getString(R.string.lowest_dosage_taken));
        TextView tvwLowDosageContent = new TextView(ctxt);
        tvwLowDosageContent.setText(String.format(Locale.getDefault(), "%1$f", lowDosage));

        tvwHighDosageLabel.setLayoutParams(lp);
        tvwHighDosageContent.setLayoutParams(lp);
        tvwLowDosageLabel.setLayoutParams(lp);
        tvwLowDosageContent.setLayoutParams(lp);
        trwHighNLow.addView(tvwHighDosageLabel);
        trwHighNLow.addView(tvwHighDosageContent);
        trwHighNLow.addView(tvwLowDosageLabel);
        trwHighNLow.addView(tvwLowDosageContent);
        tloUsageStats.addView(trwHighNLow);

        TableRow trwTotal = new TableRow(ctxt);
        TextView tvwTotalAdminLabel = new TextView(ctxt);
        tvwTotalAdminLabel.setText(getString(R.string.total_administered));
        TextView tvwTotalAdminContent = new TextView(ctxt);
        tvwTotalAdminContent.setText(String.format(Locale.getDefault(), "%1$f", totalAdminned));
        TextView tvwDailyAvgLabel = new TextView(ctxt);
        tvwDailyAvgLabel.setText(getResources().getString(R.string.daily_average));
        TextView tvwDailyAvg = new TextView(ctxt);
        tvwDailyAvg.setText(Float.toString(averageAdminnedPerDay));

        tvwTotalAdminLabel.setLayoutParams(lp);
        tvwTotalAdminContent.setLayoutParams(lp);
        trwTotal.addView(tvwTotalAdminLabel);
        trwTotal.addView(tvwTotalAdminContent);
        tvwDailyAvgLabel.setLayoutParams(lp); tvwDailyAvg.setLayoutParams(lp);
        trwTotal.addView(tvwDailyAvgLabel); trwTotal.addView(tvwDailyAvg);
        tloUsageStats.addView(trwTotal);
    }
}
