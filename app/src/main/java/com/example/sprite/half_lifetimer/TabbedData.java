package com.example.sprite.half_lifetimer;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

/**
 * Class provides routines that support a view of tabulated data based on the Substance whose index
 * has been passed to the activity via the extra 'SUB_NDX'.
 */
public class TabbedData extends AppCompatActivity {
    private int totalAdminCount;    //, subNdx;
    private float avgAdmin, highestAdmin, lowestAdmin, totalAdmin;
    private Duration longSpan, shortSpan, avgSpan, spanSinceLast;
    private List<Duration> spanList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabbed_data);

        int subID = getIntent().getExtras().getInt("SUB_NDX");

        //ugh, layout
        TextView tvwHeading = findViewById(R.id.txtSubHeading);
        tvwHeading.setText(Permanence.Subs.loadSubstanceById(subID).getCommon_name());
        tvwHeading.setTextSize(25);

        populateData(subID);

        //tabulated data
        TableRow trwOne = findViewById(R.id.trwTotals);
        TextView tvwTotAdminLbl = findViewById(R.id.tvwTotalAdminsLabel);
        TextView tvwTotAdminData = findViewById(R.id.tvwTotalAdmins);
        TextView tvwTotAdminAmtLbl = findViewById(R.id.tvwTotalAdminAmtLabel);
        TextView tvwTotAdminAmt = findViewById(R.id.tvwTotalAdminAmt);

        trwOne.removeAllViewsInLayout();
        tvwTotAdminData.setText(String.format("%1$d", totalAdminCount));
        trwOne.addView(tvwTotAdminLbl);
        trwOne.addView(tvwTotAdminData);
        tvwTotAdminAmt.setText(String.format("%1$f", totalAdmin));
        trwOne.addView(tvwTotAdminAmtLbl);
        trwOne.addView(tvwTotAdminAmt);

        TableRow trwTwo = findViewById(R.id.trwAverages);
        TextView tvwAvgDose = findViewById(R.id.tvwAvgAdmin);
        TextView tvwAvgDoseLbl = findViewById(R.id.tvwAvgAdminLbl);
        TextView tvwAvgSpan = findViewById(R.id.tvwAvgSpan);
        TextView tvwAvgSpanLbl = findViewById(R.id.tvwAvgSpanLbl);

        trwTwo.removeAllViewsInLayout();
        tvwAvgDose.setText(String.format("%1$f", avgAdmin));
        trwTwo.addView(tvwAvgDoseLbl);
        trwTwo.addView(tvwAvgDose);
        trwTwo.addView(tvwAvgSpanLbl);
        if (avgSpan != null) {
            tvwAvgSpan.setText(GlobalMisc.durationToString(avgSpan));
        } else {
            tvwAvgSpan.setText(" - ");
        }
        trwTwo.addView(tvwAvgSpan);

        TableRow trwThree = findViewById(R.id.trwHighLow);
        TextView tvwHighDoseLbl = findViewById(R.id.tvwHighDoseLbl);
        TextView tvwHighDose = findViewById(R.id.tvwHighDose);
        TextView tvwLowDoseLbl = findViewById(R.id.tvwLowDoseLbl);
        TextView tvwLowDose = findViewById(R.id.tvwLowDose);

        trwThree.removeAllViewsInLayout();
        tvwHighDose.setText(String.format("%1$f", highestAdmin));
        tvwLowDose.setText(String.format("%1$f", lowestAdmin));
        trwThree.addView(tvwHighDoseLbl);
        trwThree.addView(tvwHighDose);  //add dosage units
        trwThree.addView(tvwLowDoseLbl);
        trwThree.addView(tvwLowDose);   //add dosage units

        TableRow trwFour = findViewById(R.id.trwLongShort);
        TextView tvwLongSpanLbl = findViewById(R.id.tvwLongSpanLbl);
        TextView tvwLongSpan = findViewById(R.id.tvwLongSpan);
        TextView tvwShortSpanLbl = findViewById(R.id.tvwShortSpanLbl);
        TextView tvwShortSpan = findViewById(R.id.tvwShortSpan);

        trwFour.removeAllViewsInLayout();
        if (longSpan != null) {
            tvwLongSpan.setText(GlobalMisc.durationToString(longSpan));
            tvwShortSpan.setText(GlobalMisc.durationToString(shortSpan));
        } else {
            tvwLongSpan.setText(" - ");
            tvwShortSpan.setText(" - ");
        }
        trwFour.addView(tvwLongSpanLbl);
        trwFour.addView(tvwLongSpan);
        trwFour.addView(tvwShortSpanLbl);
        trwFour.addView(tvwShortSpan);

        //spans listing
        LinearLayout lloSpansList = findViewById(R.id.lloSpansList);
        lloSpansList.removeAllViewsInLayout();

        if (spanList != null) {
            TextView[] spansTextView = new TextView[spanList.size()];

            for (int cntr = 0; cntr < spanList.size(); cntr++) {
                spansTextView[cntr] = new TextView(TabbedData.this);
                spansTextView[cntr].setText(getString(R.string.spanlist_item, cntr + 1,
                        GlobalMisc.durationToString(spanList.get(cntr))));
                lloSpansList.addView(spansTextView[cntr]);
            }
        } else {
            TextView notEnoughSpans = new TextView(TabbedData.this);
            notEnoughSpans.setText(getResources().getString(R.string.spans_not_enough_data));
            lloSpansList.addView(notEnoughSpans);
        }

        TextView tvwHalfLifeData = findViewById(R.id.tvwHalfLifeData);
        LocalDateTime eliminationLDT =
                Permanence.Subs.loadSubstanceById(subID).getFullEliminationLDT();

        tvwHalfLifeData.setText(getString(R.string.elimination_specifics_item, eliminationLDT));

        if (eliminationLDT.isAfter(LocalDateTime.now())) {
            tvwHalfLifeData.setTextColor(Color.RED);
        } else {
            tvwHalfLifeData.setTextColor(ContextCompat.getColor(TabbedData.this,
                    R.color.colorDarkGreen));
        }

        TextView tvwTimeSinceLast = findViewById(R.id.tvwTimeSinceLastUsage);

        if (spanSinceLast != Duration.of(Long.MAX_VALUE, ChronoUnit.SECONDS)) {
            tvwTimeSinceLast.setText(GlobalMisc.durationToString(spanSinceLast));
        } else {
            tvwTimeSinceLast.setText(getString(R.string.invalid_data));
        }

    }

    /**
     * This method populates the different data (found in class-level variables) that will be used
     * in further calculation of displayed to the user on the tabulated data results activity.
     *
     * @param sid int substance ID #
     */
    private void populateData(int sid) {
        totalAdmin = (float) 0.0;
        highestAdmin = 0;
        lowestAdmin = 10000;
        spanSinceLast = null;
        LocalDateTime prevDT = null;
        List<Usage> usages = Permanence.Admins.loadValidOrderedUsagesBySubid(sid);

        totalAdminCount = usages.size();
        for (Usage use : usages) {
            float tmpDose = use.getDosage();
            totalAdmin += tmpDose;
            if (tmpDose > highestAdmin) { highestAdmin = tmpDose; }
            if (tmpDose < lowestAdmin) { lowestAdmin = tmpDose; }

            if (prevDT != null) {
                spanList.add(Duration.between(prevDT, use.getTimestamp()));
            }

            prevDT = use.getTimestamp();
        }
        avgAdmin = totalAdmin / totalAdminCount;

        if (spanList != null && spanList.size() >= 1) {
            avgSpan = Duration.ZERO;
            longSpan = Duration.ZERO;
            shortSpan = Duration.of(Long.MAX_VALUE, ChronoUnit.SECONDS);

            for (Duration span : spanList) {
                if (span.compareTo(longSpan) > 0) {
                    longSpan = span;
                }
                if (span.compareTo(shortSpan) < 0) {
                    shortSpan = span;
                }

                avgSpan = avgSpan.plus(span);
            }
            avgSpan = avgSpan.dividedBy(spanList.size());
        } else {
            //be sure to add code that displays the 'not enough information recorded yet' message
            //should these nulls be passed along for usage
            spanList = null;
            avgSpan = null;
            longSpan = null;
            shortSpan = null;
        }

        try {
            spanSinceLast = Duration.between(usages.get(usages.size() - 1).getTimestamp(),
                    LocalDateTime.now());
        } catch (Exception ex) {
            spanSinceLast = Duration.of(Long.MAX_VALUE, ChronoUnit.SECONDS);
        }
    }

    /**
     * Method determines whether or not a LocalDateTime is within the last day (24 hours).
     *
     * @param ldt LocalDateTime to analyze
     * @return boolean value on whether or not the LocalDateTime was within the previous 24 hours
     */
    private static boolean ldtWithinLastDay(LocalDateTime ldt) {
        LocalDateTime twentyFourAgo = LocalDateTime.now().minusDays(1);

        return ldt.compareTo(twentyFourAgo) > 0;
    }

    /**
     * Method uses the above 24 hour determination to put together a List of Usages that have
     * occurred within the last 24 hours.
     *
     * @param uses Usages to determine relevancy of and conditionally compile into a list
     * @return List of Usages that have occurred within the past 24 hours
     */
    private static List<Usage> gatherPastDayUsages(List<Usage> uses) {
        List<Usage> pastDayUsages = new ArrayList<>();

        for (Usage use : uses) {
            if (ldtWithinLastDay(use.getTimestamp())) {
                pastDayUsages.add(use);
            }
        }

        return pastDayUsages;
    }

    /**
     * Method displays a simple dialog displaying the past 24hrs usage statistics.
     *
     * @param v View
     */
    public void seeIntervalStatsOnClick(View v) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final Intent intent = new Intent(TabbedData.this, DailyStats.class);
                intent.putExtra("SUB_NDX", getIntent().getExtras().getInt("SUB_NDX"));

                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        //open the previous day stats view
                        intent.putExtra("DAYS_AGO", 1);
                        startActivity(intent);

                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        AlertDialog.Builder builder = new AlertDialog.Builder(TabbedData.this);
                        builder.setTitle("How many days to view statistics for?");

                        final EditText input = new EditText(TabbedData.this);
                        final GlobalMisc.OnDialogResultListener fListener =
                                new GlobalMisc.OnDialogResultListener() {
                            @Override
                            public void onResult(String result) {
                                int daysAgo = Integer.parseInt(result);
                                intent.putExtra("DAYS_AGO", daysAgo);
                                startActivity(intent);
                            }
                        };

                        input.setInputType(InputType.TYPE_CLASS_NUMBER);
                        builder.setView(input);

                        //buttons
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                fListener.onResult(input.getText().toString());
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        builder.show();

                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.day_or_variable_stats))
                .setPositiveButton(getResources().getString(R.string.past_day), dialogClickListener)
                .setNegativeButton(getResources().getString(R.string.variable_interval),
                        dialogClickListener).show();
    }

    /**
     * Method starts the activity for viewing dosage graphing.
     *
     * @param v View
     */
    public void seeDosageGraph(View v) {
        Intent intent = new Intent(TabbedData.this, DosageGraphing.class);
        intent.putExtra("SUB_NDX", getIntent().getExtras().getInt("SUB_NDX"));
        startActivity(intent);
    }
}
