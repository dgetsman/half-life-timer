package com.example.sprite.half_lifetimer;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import android.util.Log;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
public class Taper {
    @PrimaryKey(autoGenerate=true)
    private int             id;
    @ColumnInfo(name="name")
    private String          name;
    @ColumnInfo(name="target_date")
    private LocalDateTime   targetDate;
    @ColumnInfo(name="start_date")
    private LocalDateTime   startDate;
    @ColumnInfo(name="substance_id")
    private int             sid;
    @ColumnInfo(name="daily_dosage_target")
    private float           dailyDosageTarget;
    @ColumnInfo(name="daily_dosage_start")
    private float           dailyDosageStart;
    @ColumnInfo(name="valid")
    private boolean         valid = true;
    @ColumnInfo(name="admins_per_day")
    private int             adminsPerDay;
    @ColumnInfo(name="start_hour")
    private LocalTime       startHour;
    @ColumnInfo(name="end_hour")
    private LocalTime       endHour;
    @ColumnInfo(name="dosage_interval")
    private float           dosageInterval;

    public Taper() { }

    public Taper(String name, LocalDateTime tDate, int sid, float dosageTarget) {
        this.name = name;
        this.targetDate = tDate;
        this.sid = sid;
        this.dailyDosageTarget = dosageTarget;
    }

    public Taper(String name, LocalDateTime tDate, LocalDateTime sDate, int sid, float dosageTarget,
                 float dosageStart, boolean valid, int adminsPerDay, LocalTime startHour,
                 LocalTime endHour, float dosageInterval) {
        this.name = name;
        this.targetDate = tDate;
        this.startDate = sDate;
        this.sid = sid;
        this.dailyDosageTarget = dosageTarget;
        this.dailyDosageStart = dosageStart;
        this.valid = valid;
        this.adminsPerDay = adminsPerDay;
        this.startHour = startHour;
        this.endHour = endHour;
        this.dosageInterval = dosageInterval;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.trim();
    }

    public LocalDateTime getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(LocalDateTime targetDate) {
        this.targetDate = targetDate;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public float getDailyDosageTarget() {
        return dailyDosageTarget;
    }

    public void setDailyDosageTarget(float dailyDosageTarget) {
        this.dailyDosageTarget = dailyDosageTarget;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public float getDailyDosageStart() {
        return dailyDosageStart;
    }

    public void setDailyDosageStart(float dailyDosageStart) {
        this.dailyDosageStart = dailyDosageStart;
    }

    public boolean getValid() {
        return valid;
    }

    public void setValid(boolean flag) {
        this.valid = flag;
    }

    public int getAdminsPerDay() {
        return adminsPerDay;
    }

    public void setAdminsPerDay(int adminsPerDay) {
        this.adminsPerDay = adminsPerDay;
    }

    public LocalTime getStartHour() {
        return startHour;
    }

    public void setStartHour(LocalTime startHour) {
        this.startHour = startHour;
    }

    public LocalTime getEndHour() {
        return endHour;
    }

    public void setEndHour(LocalTime endHour) {
        this.endHour = endHour;
    }

    public float getDosageInterval() {
        return dosageInterval;
    }

    public void setDosageInterval(float dosageInterval) {
        this.dosageInterval = dosageInterval;
    }

    public boolean isExpired() {
        if (this.targetDate.compareTo(LocalDateTime.now()) <= 0) {
            return true;
        }

        return false;
    }

    public String toString() {
        Substance sub = Permanence.Subs.loadSubstanceById(this.sid);

        String string =  this.name + " (id: " + this.id + "): " + sub.getCommon_name() + " from " +
                this.dailyDosageStart + sub.getUnits() + " to " +
                this.dailyDosageTarget + sub.getUnits() + " by " + this.targetDate +
                " (started on " + this.getStartDate() + ") with " + this.adminsPerDay +
                " administrations per day";

        if (((this.getStartHour() != null) && (this.getEndHour() != null)) &&
                ((this.getStartHour() != LocalTime.of(0, 0)) &&
                        (this.getEndHour() != LocalTime.of(0, 0)))) {
            string = string + "; admins constrained to between " + this.getStartHour().toString() +
                    " and " + this.getEndHour().toString();
        }

        if (this.getDosageInterval() != 0) {
            string = string + " dosage step size: " + this.getDosageInterval();
        }

        return string;
    }

    /**
     * Calculates the maximum dosage that would be administered today, should
     * the taper be followed.  Note that this does not calculate the amount
     * per administration, that is handled below.
     *
     * @return dosageToday
     */
    public float findTodaysDosageMax() {
        Duration curDuration = Duration.between(this.startDate, LocalDateTime.now());
        Duration fullDuration = Duration.between(this.startDate, this.targetDate);

        long fullDays, curDays;
        float taperAmount, perDayDosageDrop;

        fullDays = fullDuration.getSeconds() / (3600 * 24);
        curDays = curDuration.getSeconds() / (3600 * 24);

        taperAmount = this.dailyDosageStart - this.dailyDosageTarget;
        perDayDosageDrop = taperAmount / fullDays;

        float dosageToday = this.dailyDosageStart - (perDayDosageDrop * curDays);

        //debugging information
        GlobalMisc.debugMsg("findTodaysDosageMax", "curDuration (days): " +
                curDays + " fullDuration (days): " + fullDays + " perDayDosageDrop: " +
                perDayDosageDrop + " dosageToday: " + dosageToday);

        return dosageToday;
    }

    /**
     * Calculates the maximum dosage that would be administered on a LDT
     * specified day.
     *
     * @param xDay LocalDateTIme contains the date to calculate for
     * @return dosageAtDayX
     */
    public float findXDayDosageMax(LocalDateTime xDay) {
        Duration xDuration = Duration.between(this.startDate, xDay);
        Duration fullDuration = Duration.between(this.startDate, this.targetDate);

        long fullDays, xDays;
        float taperAmount, perDayDosageDrop;

        fullDays = fullDuration.getSeconds() / (3600 * 24);
        xDays = xDuration.getSeconds() / (3600 * 24);

        taperAmount = this.dailyDosageStart - this.dailyDosageTarget;
        perDayDosageDrop = taperAmount / fullDays;

        return this.dailyDosageStart - (perDayDosageDrop * xDays);
    }

    /**
     * Calculates the LocalDateTime of the next dosage to be administered.  If
     * this LDT, based on the previous last usage, would be in the past, it
     * defaults to now.
     *
     * @return LocalDateTime date & time of next administration
     */
    public LocalDateTime findNextScheduledDosageLDT() {
        Duration administrationDuration;
        if (!isConstrained()) {
            GlobalMisc.debugMsg("findNextScheduledDosageLDT", "non-constrained");

            administrationDuration = Duration.ofDays(1).dividedBy(getAdminsPerDay());
        } else {
            GlobalMisc.debugMsg("findNextScheduledDosageLDT", "constrained");

            administrationDuration =
                    Duration.between(this.getStartHour(),
                            this.getEndHour()).dividedBy(getAdminsPerDay());
        }

        //make sure that we've started off the taper with an administration if one recent enough
        //hasn't been found
        long lastTimestamp = Permanence.Admins.getLatestUsageTimestampBySid(this.sid);
        LocalDateTime lastUsage = Converters.toLocalDateTime(lastTimestamp);

        Log.i("findNextScheduledDosageLDT", "lastUsage: " + lastUsage.toString());
        Log.i("findNextScheduledDosageLDT", "isBefore argument: " +
                LocalDateTime.now().minus(administrationDuration).toString());

        if (lastTimestamp == 0 ||
                lastUsage.isBefore(LocalDateTime.now().minus(administrationDuration))) {
            //we need an administration now or at next available instant w/in constraints
            if (!this.isConstrained()) {
                return LocalDateTime.now();
            } else {
                if (!this.inConstraintHours()) {
                    //we need to return the next instance within our hourly constraints
                    LocalDateTime nowLDT = LocalDateTime.now();
                    LocalDateTime nextApplicableLDT = nowLDT;
                    LocalDateTime todayEndOfConstraint = nowLDT;

                    nextApplicableLDT = nextApplicableLDT.withHour(this.getStartHour().getHour());
                    nextApplicableLDT = nextApplicableLDT.withMinute(
                            this.getStartHour().getMinute());

                    todayEndOfConstraint = todayEndOfConstraint.withHour(
                            this.getEndHour().getHour());
                    todayEndOfConstraint = todayEndOfConstraint.withMinute(
                            this.getEndHour().getMinute());

                    if (nextApplicableLDT.isBefore(nowLDT) &&
                            nowLDT.isAfter(todayEndOfConstraint)) {
                        nextApplicableLDT = nextApplicableLDT.plusDays(1);
                    }

                    //I'm a little confused due to lack of sleep, this code will probably have to be
                    //cleaned up after a good rest
                    if (ltInConstraintHours(nextApplicableLDT.toLocalTime())) {
                        return nextApplicableLDT;
                    } else {
                        return LocalDateTime.now().plusDays(1)
                                .withHour(this.getStartHour().getHour())
                                .withMinute(this.getStartHour().getMinute());
                    }
                } else {
                    return LocalDateTime.now();
                }
            }
        } else {
            return lastUsage.plus(administrationDuration);
        }
    }

    /**
     * Calculates the dosage of the substance that will next be administered
     * according to the taper levels.
     *
     * NOTE: This should be named findCurrnetScheduledDosageAmount() as it
     * goes off of now, not the point at which the next dosage is scheduled.
     *
     * TODO: Verify that the dosage interval conditional works properly
     *
     * @return float amount in [units] to be administered
     */
    public Float findCurrentScheduledDosageAmount() {
        LocalDateTime nowLDT = LocalDateTime.now();
        Duration taperStartUntilNow = Duration.between(getStartDate(), nowLDT);
        Duration taperDuration = Duration.between(this.getStartDate(),
                this.getTargetDate());
        float dosageChange = this.getDailyDosageStart() - this.getDailyDosageTarget();
        float percentageComplete = (float)taperStartUntilNow.toMillis() /
                (float)taperDuration.toMillis();
        float increment = dosageChange * percentageComplete;

        GlobalMisc.debugMsg("findNextScheduledDosageAmount",
                "dosageChange: " + dosageChange + "\npercentageComplete: " +
                        (percentageComplete * 100) + "\nincrement: " + increment);

        float nextScheduledDosage = (
                (getDailyDosageStart() - increment - Permanence.Subs.loadSubstanceById(
                        this.getSid()).getTotalDosageToday()) / getAdminsPerDay());

        if (nextScheduledDosage <= (this.getDosageInterval() / 2)) {
            //we are below the 50% mark of a dosage interval; the user does not need a dosage
            Log.d("findCurrentScheduledDosageAmount", "No dosage is required due to " +
                    "taper amount being below 50% of a min dosage interval");

            return -1f;
        } else if ((getDosageInterval() == 0) ||
                ((nextScheduledDosage % getDosageInterval()) == 0)) {
            Log.d("findCurrentScheduledDosageAmount", "No dosage interval or next " +
                    "scheduled dosage is perfectly divisible");
            Log.d("findCurrentScheduledDosageAmount", "dosage interval: " +
                    getDosageInterval() + " nextScheduledDosage: " + nextScheduledDosage);

            return nextScheduledDosage;
        } else {
            int dosageSteps = Math.round(nextScheduledDosage / getDosageInterval());

            Log.d("findCurrentScheduledDosageAmount", dosageSteps + " steps of " +
                    getDosageInterval() + " in " + nextScheduledDosage);
            Log.d("findCurrentScheduledDosageAmount", "dosage to administer: " +
                    dosageSteps * getDosageInterval());

            return dosageSteps * getDosageInterval();
        }
    }

    /**
     * Method determines whether or not this taper is constrained to specific
     * hours during the day.
     *
     * @return boolean
     */
    public boolean isConstrained() {
        //we're not constrained, the 24 hours are wide open
        //stick between the guideposts
        return (this.getStartHour() != null || this.getEndHour() != null) &&
                (this.getStartHour() != LocalTime.of(0, 0) ||
                        this.getEndHour() != LocalTime.of(0, 0));
    }

    /**
     * Method determines whether or not a constrained taper is currently being
     * used during the valid constraint hours.
     *
     * @return boolean
     */
    public boolean inConstraintHours() {
        /*Instant ouahInstant = Instant.now();
        ZoneId systemZone = ZoneId.systemDefault();*/
        LocalTime now = LocalTime.now();

        GlobalMisc.debugLog("inConstraintHours", "now: " + now.toString() +
                "\tgetStartHour(): " + getStartHour().toString() + "\tgetEndHour(): " +
                getEndHour().toString());

        return now.isAfter(this.getStartHour()) && now.isBefore(this.getEndHour());
    }

    /**
     * Determines whether or not a specific LocalTime is within our valid
     * constraint hours.
     *
     * @param ltInQuestion LocalTime to be checked
     * @return boolean
     */
    public boolean ltInConstraintHours(LocalTime ltInQuestion) {
        Instant ouahInstant = Instant.now();

        if (ltInQuestion.isAfter(this.getStartHour()) && ltInQuestion.isBefore(this.getEndHour())) {
            GlobalMisc.debugMsg("ltInConstraintHours",
                    ltInQuestion.toString() + " is in our constraint criteria");

            return true;
        } else {
            return false;
        }
    }

    /**
     * Method returns the total duration during which the substance can be
     * administered as per the constrained taper.
     *
     * @return
     */
    public Duration getTotalConstraintDuration() {
        if (isConstrained()) {
            return Duration.between(getStartHour(), getEndHour());
        } else {
            return null;
        }
    }
}
