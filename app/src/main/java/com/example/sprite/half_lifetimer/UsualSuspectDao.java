package com.example.sprite.half_lifetimer;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface UsualSuspectDao {
    @Query("SELECT * FROM UsualSuspect")
    List<UsualSuspect> getAll();

    @Query("SELECT DISTINCT UsualSuspect.id, UsualSuspect.name, UsualSuspect.notes " +
            " FROM UsualSuspect INNER JOIN UsualSuspectSubsCrossRef ON " +
            "UsualSuspect.id = UsualSuspectSubsCrossRef.usualSuspect INNER JOIN Substance ON " +
            "UsualSuspectSubsCrossRef.sub = Substance.id WHERE Substance.archived = 0")
    List<UsualSuspect> getAllUnarchived();

    @Query("SELECT * FROM UsualSuspect WHERE id LIKE :sid")
    UsualSuspect getUSByID(int sid);

    @Query("SELECT * FROM UsualSuspect WHERE name LIKE :name")
    UsualSuspect getUSByName(String name);

    @Delete
    void delete(UsualSuspect us);

    @Insert
    void insertUsualSuspect(UsualSuspect us);

    @Update
    void updateUsualSuspect(UsualSuspect us);
}
