package com.example.sprite.half_lifetimer;

import android.content.Intent;
import android.graphics.Paint;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Class provides the view as well as the nuts and bolts for SubClassTabbedData UI.
 */
public class SubClassTabbedData extends AppCompatActivity {
    List<Substance> subList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_class_tabbed_data);

        ClassStats compiledStats = new ClassStats(getIntent().getExtras().getInt("CLASS_ID"));

        TextView tvwTotalUses = findViewById(R.id.tvwTotalUses);
        LinearLayout lloDosagesPerSub = findViewById(R.id.lloPerSubTotals);

        tvwTotalUses.setText(Integer.toString(compiledStats.getTotalUses()));

        HashMap<Integer, Float> perSubTotalDosages = compiledStats.getTotalDosagePerSub();
        TextView[] tvwTotalDosagesPerSubHeadings = new TextView[compiledStats.getNumberOfSubs()];
        TextView[] tvwTotalDosagesPerSub = new TextView[perSubTotalDosages.size()];
        TextView[] tvwAverageDosagePerSub = new TextView[perSubTotalDosages.size()];

        //add the headings & dosages
        int cntr = 0;
        //for (int cntr = 0; cntr < compiledStats.getNumberOfSubs(); cntr++) {
        for (int subId : compiledStats.getSIdList()) {
            final int fSubId = subId;

            tvwTotalDosagesPerSubHeadings[cntr] = new TextView(SubClassTabbedData.this);
            tvwTotalDosagesPerSubHeadings[cntr].setText(compiledStats.getSubName(subId));
            tvwTotalDosagesPerSubHeadings[cntr].setTextSize(18);
            tvwTotalDosagesPerSubHeadings[cntr].setPaintFlags(
                    tvwTotalDosagesPerSubHeadings[cntr].getPaintFlags() |
                            Paint.UNDERLINE_TEXT_FLAG);

            tvwTotalDosagesPerSubHeadings[cntr].setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(SubClassTabbedData.this,
                                    TabbedData.class);
                            intent.putExtra("SUB_NDX", fSubId);

                            startActivity(intent);
                        }
                    });

            lloDosagesPerSub.addView(tvwTotalDosagesPerSubHeadings[cntr]);

            //usage total dosages for each substance
            tvwTotalDosagesPerSub[cntr] = new TextView(SubClassTabbedData.this);
            tvwTotalDosagesPerSub[cntr].setText(getString(R.string.total_administered_dosage,
                    perSubTotalDosages.get(subId), Permanence.Subs.getUnitsBySID(subId)));
            /*tvwTotalDosagesPerSub[cntr].setText("Total Administered: " +
                    perSubTotalDosages.get(subList.get(cntr).getId()));*/
            lloDosagesPerSub.addView(tvwTotalDosagesPerSub[cntr]);

            //average dosages for each substance
            tvwAverageDosagePerSub[cntr] = new TextView(SubClassTabbedData.this);
            tvwAverageDosagePerSub[cntr].setText(getString(R.string.avg_admined,
                    compiledStats.getAverageDosagePerSub().get(subId),
                    Permanence.Subs.getUnitsBySID(subId)));
            /*tvwAverageDosagePerSub[cntr].setText("Average Administered Per Dosage: " +
                    compiledStats.getAverageDosagePerSub().get(subList.get(cntr).getId()));*/
            lloDosagesPerSub.addView(tvwAverageDosagePerSub[cntr]);

            cntr++;
        }

    }
}
