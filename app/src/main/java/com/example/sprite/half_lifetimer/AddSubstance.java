package com.example.sprite.half_lifetimer;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

public class AddSubstance extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    DosageUnit dUnits = null;
    Integer subClassificationID = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_substance2);

        //populate the spinners with the data they require
        Spinner spnSubDUnits = findViewById(R.id.spnAddSubstanceDUnits);
        Spinner spnSubClass = findViewById(R.id.spnAddSubstanceClass);

        //working on sub classification first
        //prepare a list in order to populate the ArrayAdapter that our spinner will need :-?(beep)
        List<SubClassification> sClasses = Permanence.SC.loadAllSC();
        String[] sClassNames = new String[sClasses.size() + 2];
        sClassNames[0] = " --- ";
        sClassNames[1] = "None";
        for (int cntr = 2; cntr < (sClasses.size() + 2); cntr++) {
            sClassNames[cntr] = sClasses.get(cntr - 2).getName();
        }

        //now bind this shit all up or whatever this does
        ArrayAdapter<String> classAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, sClassNames);
        classAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnSubClass.setAdapter(classAdapter);

        //and now the same for the dosage units spinner
        String[] dUnitNames = new String[DosageUnit.values().length + 1];
        dUnitNames[0] = " --- ";
        int cntr = 1;
        for (DosageUnit dunit : DosageUnit.values()) {
            dUnitNames[cntr++] = GlobalMisc.dUnitsToString(dunit);
        }

        //bind it up
        ArrayAdapter<String> unitAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, dUnitNames);
        unitAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnSubDUnits.setAdapter(unitAdapter);

        //register the listeners
        spnSubClass.setOnItemSelectedListener(this);
        spnSubDUnits.setOnItemSelectedListener(this);
    }

    /**
     * Requiem for an onClick handler; this one handles passing off the task of
     * validation of proper fields being filled out, then tries to save the
     * Substance if valid input is found, displaying a status message regarding
     * how that went in either case.
     *
     * @param v View requiem for an onClick
     */
    public void onCommitClick(View v) {
        EditText edtCName = findViewById(R.id.edtSubstanceCommonName);
        EditText edtSName = findViewById(R.id.edtSubstanceScientificName);
        EditText edtPrimaryHL = findViewById(R.id.edtSubstanceHalflife);
        EditText edtDetectableHL = findViewById(R.id.edtSubstanceDHalflife);
        CheckBox cbxLipidSoluble = findViewById(R.id.cbxAddSubstanceLipidSoluble);

        //there will be more validation needed
        int success = validateInputFields();
        if (success == -2) {
            GlobalMisc.showSimpleDialog(AddSubstance.this, "Incomplete Entry",
                    "You need to fill out all of the fields for the new substance!");

            return;
        } else if (success == -1) {
            GlobalMisc.showSimpleDialog(AddSubstance.this, "Duplicate Entry",
                    "You need to fill out the form for a substance not already in the " +
                    "database!");

            return;
        }

        Substance newSub = new Substance(//cname, sname, units, hl, dhl, lipid soluble, sclass
            edtCName.getText().toString().trim(), edtSName.getText().toString().trim(),
            dUnits, Float.parseFloat(edtPrimaryHL.getText().toString()),
            Float.parseFloat(edtDetectableHL.getText().toString()),
            cbxLipidSoluble.isChecked(), subClassificationID
        );

        TextView tvwStatus = findViewById(R.id.tvwStatusMessage);
        if (commitSubstance(newSub)) {
            tvwStatus.setText(getString(R.string.commit_substance_successful));
            tvwStatus.setTextSize(20);
            tvwStatus.setTextColor(Color.GREEN);
        }
    }

    /**
     * Validates that all fields have been filled out properly in order to give
     * values to each of the fields for the Substance constructor.
     *
     * @return int 0 for valid, -1 for duplicate, -2 for form not completed
     */
    private int validateInputFields() {
        EditText edtCName = findViewById(R.id.edtSubstanceCommonName);
        EditText edtSName = findViewById(R.id.edtSubstanceScientificName);
        EditText edtPrimaryHL = findViewById(R.id.edtSubstanceHalflife);
        EditText edtDetectableHL = findViewById(R.id.edtSubstanceDHalflife);

        String cName = edtCName.getText().toString().trim();
        String sName = edtSName.getText().toString().trim();
        String pHalflife = edtPrimaryHL.getText().toString().trim();
        String dHalflife = edtDetectableHL.getText().toString().trim();

        //validation against current substances (archived and not)
        for (Substance oldSub : Permanence.Subs.loadSubstances(this)) {
            if (cName.toLowerCase().contentEquals(oldSub.getCommon_name().toLowerCase()) ||
                sName.toLowerCase().contentEquals(oldSub.getSci_name().toLowerCase())) {
                //duplicate substance
                return -1;
            }
        }

        if (dUnits == null || subClassificationID == null) {
            return -2;  //(form not completed)
        } else {
            if (cName.length() == 0 || sName.length() == 0 || pHalflife.length() == 0 ||
                dHalflife.length() == 0) {
                return -2;  //(form not completed)
            }
        }

        return 0;
    }

    /**
     * Method commits the new Substance to the database; if it runs into an
     * error it displays an error message and returns the appropriate boolean
     * status code to show error, as well.
     *
     * @param newSub Substance substance to be committed to the database
     * @return boolean true for success & vice versa
     */
    private boolean commitSubstance(Substance newSub) {
        try {
            Permanence.Subs.saveSubstance(AddSubstance.this, newSub);
        } catch (Exception ex) {
            TextView tvwStatus = findViewById(R.id.tvwStatusMessage);

            tvwStatus.setText(getString(R.string.commit_substance_error, ex.toString()));
            tvwStatus.setTextSize(20);
            tvwStatus.setTextColor(Color.MAGENTA);

            return false;
        }

        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        //the 'position - x' bit is just because of the blank fields inserted into the Spinners in
        //order to force the user to select something to invoke this method
        if (parent.getId() == R.id.spnAddSubstanceDUnits) {
            //set the dUnits
            if (!parent.getItemAtPosition(position).toString().contentEquals(" --- ")) {
                dUnits = DosageUnit.values()[position - 1];
            }
        } else if (parent.getId() == R.id.spnAddSubstanceClass) {
            //set the sClass
            if (parent.getItemAtPosition(position).toString().contentEquals("None")) {
                subClassificationID = -1;
            } else if (!parent.getItemAtPosition(position).toString().contentEquals(" --- ")) {
                //probably not the most efficient way to do this, but it'll work in a pinch
                subClassificationID = Permanence.SC.loadAllSC().get(position - 2).getId();
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //ouah
    }
}