package com.example.sprite.half_lifetimer;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Class contains slightly beautified versions of the different database access routines that exist
 * in raw form in the different DAOs.  This class contains all of the different methods used for
 * accessing the database records.
 *
 * TODO: Remove all context/logging from debugging procedures done previously to reduce overhead
 */
public class Permanence {
    private static AppDatabase sDb;
    private static boolean initialized = false;

    /**
     * substance methods
     */
    public static class Subs {
        /**
         * Saves a substance record.
         *
         * @param ctxt Context
         * @param sub  Substance record to save
         */
        public static void saveSubstance(Context ctxt, Substance sub) {
            Misc.init(ctxt);

            Log.d("saveSubstance", "trying to save " + sub.toString());
            sDb.getSubstanceDao().insertSub(sub);
        }

        /**
         * Method handles loading all substances from the database.
         *
         * @param ctxt Context
         * @return List of all Substances
         */
        public static List<Substance> loadSubstances(Context ctxt) {
            if (!initialized) {
                Misc.init(ctxt);
            }

            return sDb.getSubstanceDao().getAll();
        }

        /**
         * Method handles loading all substances from the database which have not been archived.
         *
         * @return List of Unarchived Substances
         */
        public static List<Substance> loadUnarchivedSubstances(Context ctxt) {
            if (!initialized) {
                Misc.init(ctxt);
            }

            return sDb.getSubstanceDao().getAllUnarchived();
        }

        /**
         * Method puts all of the unarchived substances that are lipid soluble into a list to be
         * returned.
         *
         * @return List of unarchived, lipid-soluble Substances
         */
        public static List<Substance> loadUnarchivedLipidSolubleSubstances() {
            return sDb.getSubstanceDao().getUnarchivedLipidSolubleSubs();
        }

        /**
         * Method handles loading all archived substances for management.
         *
         * @return List of Archived Substances
         */
        public static List<Substance> loadArchivedSubstances() {
            return sDb.getSubstanceDao().getAllArchived();
        }

        /**
         * Method returns a single Substance by ID #.
         *
         * @param sid Substance ID number
         * @return Substance object
         */
        public static Substance loadSubstanceById(int sid) {
            return sDb.getSubstanceDao().findSubByID(sid);
        }

        /**
         * Method returns a single Substance by ID, only if it is not archived.
         *
         * @param sid Substance ID number
         * @return Substance object or null
         */
        public static Substance loadUnarchivedSubstanceById(int sid) {
            return sDb.getSubstanceDao().findUnarchivedSubByID(sid);
        }

        /**
         * Loads all Substances within a given SubClassification
         *
         * @param sClass SubClassification ID number
         * @return List of Substances within that class
         */
        public static List<Substance> loadUnarchivedSubstanceBySClass(int sClass) {
            return sDb.getSubstanceDao().findUnarchivedSubsBySClass(sClass);
        }

        /**
         * Method handles wiping a Substance from the database, cascading through the different Usages
         * to remove all applicable Usages from the database as well.  This should assist in keeping the
         * database in a valid state, though we do have options to check the database for Usages with
         * invalid substance ID #s, as well.
         *
         * @param sub Substance object to be wiped
         */
        public static void wipeSub(Substance sub) {
            //don't leave the database horked
            Admins.wipeAllSubUsagesCascade(sub.getId());

            sDb.getSubstanceDao().delete(sub);
        }

        public static void wipeSubByID(int sid) {
            sDb.getSubstanceDao().deleteById(sid);
        }

        /**
         * Method updates a previously existing Substance record with its modified version.
         *
         * @param sub new Substance object to modify within the database
         */
        public static void updateSub(Substance sub) {
            sDb.getSubstanceDao().update(sub);
        }

        public static DosageUnit getUnitsBySID(int sid) {
            return sDb.getSubstanceDao().getUnitsBySID(sid);
        }
    }

    /**
     * usage/administration methods
     */
    public static class Admins {
        /**
         * Method loads all Usages from the database.
         *
         * @return List of all Usages
         */
        public static List<Usage> loadUsages() {
            List<Usage> ouah = sDb.getUsageDao().getAll();

            return ouah;
        }

        /**
         * Method loads all Usages for Substances that are not currently archived from the database.
         *
         * @return List of Unarchived Usages
         */
        public static List<Usage> loadUnarchivedUsages() {
            return sDb.getUsageDao().getAllUnarchived();
        }

        /**
         * Method saves a new Usage object to the database.
         *
         * @param use new Usage object
         */
        public static void saveUsage(Usage use) {
            sDb.getUsageDao().insertUsage(use);
        }

        /**
         * Method wipes an existing Usage object from the database.
         *
         * @param use existing Usage object to wipe from the database
         */
        public static void wipeUsage(Usage use) {
            sDb.getUsageDao().delete(use);
        }

        public static int wipeUsageWithResultRows(Usage use) {
            return sDb.getUsageDao().deleteWithResultRows(use);
        }

        public static int wipeUsageByID(int usageID) {
            return sDb.getUsageDao().deleteByUsageID(usageID);
        }

        public static int wipePriorToTimestamp(long timestamp) {
            return sDb.getUsageDao().deletePriorToTimestamp(timestamp);
        }

        public static int countPriorToTimestamp(long timestamp) {
            return sDb.getUsageDao().countPriorToTimestamp(timestamp);
        }

        public static int countPostToTimestampForSub(long timestamp, int sid) {
            return sDb.getUsageDao().countPostToTimestampForSubstance(timestamp, sid);
        }

        public static int countInDurationForSub(int sid, long sts, long ets) {
            return sDb.getUsageDao().countInDurationForSubstance(sid, sts, ets);
        }

        /**
         * Method modifies an existing Usage object in the database.
         *
         * @param use Usage object to modify within the database
         */
        public static void updateUsage(Usage use) {
            sDb.getUsageDao().update(use);
        }

        /**
         * Method handles wiping all Usages with a given Substance ID #, used primarily when deleting
         * a Substance from the database to prevent invalid database states.
         *
         * @param sid Substance ID # whose Usages should be wiped
         */
        public static void wipeAllSubUsagesCascade(int sid) {
            Log.d("wipeAllSubUsagesCascade", sDb.getUsageDao().getValidBySid(sid).toString());
            sDb.getUsageDao().deleteAllBySID(sid);
        }

        /**
         * Method returns the dosage units used by a given Usage.
         *
         * @param use Usage object
         * @return DosageUnit utilized in the aforementioned Usage object
         */
        public static DosageUnit getUnitsByUsage(Usage use) {
            return sDb.getSubstanceDao().findSubByID(use.getId()).getUnits();
        }

        /**
         * Method returns a Usage object by its ID #.
         *
         * @param id ID number for the Usage
         * @return Usage object
         */
        public static Usage getUsageById(int id) {
            return sDb.getUsageDao().getById(id);
        }

        /**
         * Method returns all Usages with a given Substance ID #.
         *
         * @param sid Substance ID number
         * @return List of Usages with the given Substance ID number
         */
        public static List<Usage> loadValidUsagesBySubId(int sid) {
            return sDb.getUsageDao().getValidBySid(sid);
        }

        /**
         * Method returns all Usages with a given Substance ID, ordering them chronologically along the
         * way.
         *
         * @param sid Substance ID number
         * @return List of Usages with the given Substance ID number in chrono order
         */
        public static List<Usage> loadOrderedUsagesBySubid(int sid) {
            return sDb.getUsageDao().getUsagesByChrono(sid);
        }

        /**
         * Method returns all valid Usages with a given Substance ID number, ordering them
         * chronologically along the way.
         *
         * @param sid Substance ID number
         * @return List of valid Usages with the given Substance ID number in chrono order
         */
        public static List<Usage> loadValidOrderedUsagesBySubid(int sid) {
            return sDb.getUsageDao().getValidUsagesByChrono(sid);
        }

        /**
         * Method returns all Usages with a given Substance ID in reverse chronological order.
         *
         * @param sid Substance ID number
         * @return List of Usages in reverse chronological order
         */
        public static List<Usage> loadDescOrderedUsagesBySubid(int sid) {
            return sDb.getUsageDao().getDescUsagesByChrono(sid);
        }

        public static List<Usage> loadValidUnarchivedOrderedUsagesByUID() {
            return sDb.getUsageDao().getValidUnarchivedUsagesByUID();
        }

        public static List<Usage> loadValidUnarchivedOrderedDescUsagesByUID() {
            return sDb.getUsageDao().getValidUnarchivedUsagesDescByUID();
        }

        public static List<Usage> getValidUnarchivedUsagesBySID(int sid) {
            return sDb.getUsageDao().getValidUnarchivedUsagesBySID(sid);
        }

        public static List<Usage> getValidUnarchivedUsagesDescBySID(int sid) {
            return sDb.getUsageDao().getValidUnarchivedUsagesDescBySID(sid);
        }

        /**
         * Method returns all valid Usages with a given Substance ID in reverse chronological order.
         *
         * @param sid Substance ID number
         * @return List of valid Usages in reverse chronological order
         */
        public static List<Usage> loadValidDescOrderedUsagesBySubid(int sid) {
            return sDb.getUsageDao().getValidDescUsagesByChrono(sid);
        }

        /**
         * Method returns the total number of Usages in the database.
         *
         * @return number of Usages in the database
         */
        public static int getTotalUsageCount() {
            return sDb.getUsageDao().getTotalUsageCount();
        }

        /**
         * Method obtains the number of Usages prior to a given timestamp for a given Substance.
         *
         * @param sid Substance ID number
         * @param ts  timestamp
         * @return count of Usages prior to timestamp for Substance ID number
         */
        public static int getUsagePriorToCount(int sid, long ts) {
            return sDb.getUsageDao().getPriorToDateUsageCount(sid, ts);
        }

        public static int getUsageFromAllPriorToCount(long ts) {
            return sDb.getUsageDao().getPriorToTSUOrderedUsageRecordsFromAllSubsCount(ts);
        }

        /**
         * Method obtains a List of all timestamps for Usages with a given Substance ID number prior to
         * a specified timestamp.
         *
         * @param sid Substance ID number
         * @param ts  timestamp
         * @return List of timestamps prior to that given for Usages with Substance ID number
         */
        public static List<Long> getPriorToTimestamps(int sid, long ts) {
            return sDb.getUsageDao().getPriorToDateTimestamps(sid, ts);
        }

        /**
         * Method returns all Usages with a given Substance ID # prior to a given timestamp.
         *
         * @param sid Substance ID number
         * @param ts  timestamp
         * @return List of Usages with Substance ID number prior to the timestamp
         */
        public static List<Usage> getPriorToUsageRecords(int sid, long ts) {
            return sDb.getUsageDao().getPriorToTSUsageRecords(sid, ts);
        }

        /**
         * Method returns all Usages from all Substances prior to a given timestamp.
         *
         * @param ts  timestamp
         * @return List of Usages from all subs prior to the timestamp passed
         */
        public static List<Usage> getPriorToUsageRecordsFromAll(long ts) {
            return sDb.getUsageDao().getPriorToTSOrderedUsageRecordsFromAllSubs(ts);
        }

        /**
         * Method returns all usages with a given Substance ID # at or beyond a given timestamp.
         *
         * @param sid Substance ID number
         * @param ts  timestamp
         * @return List of Usages w/Sub ID # at or beyond the timestamp
         */
        public static List<Usage> getPostToTSUsageRecords(int sid, long ts) {
            return sDb.getUsageDao().getPostToTSUsageRecords(sid, ts);
        }

        /**
         * Method returns all usages with a given Substance ID # at or beyond a given timestamp;
         * usages are ordered in ascending date order.
         *
         * @param sid Substance ID #
         * @param ts  timestamp
         * @return List of Usages w/Sub ID # at or beyond the timestamp
         */
        public static List<Usage> getPostToTSOrderedUsageRecords(int sid, long ts) {
            return sDb.getUsageDao().getPostToTSOrderedUsageRecords(sid, ts);
        }

        /**
         * Method searches for valid Usages with a given Substance ID # between the start timestamp
         * and ending timestamp, then returning the found List.
         *
         * @param sid Substance ID number
         * @param sts starting timestamp
         * @param ets ending timestamp
         * @return List of Usages
         */
        public static List<Usage> getBetweenSpanValidUsagesForSub(int sid, long sts, long ets) {
            //TODO: check and see where this is used and whether or not signs need to be reversed
            //in the Dao
            return sDb.getUsageDao().getDurationValidUsageRecordsForSub(sid, sts, ets);
        }

        /**
         * Method searches for valid Usages of any substance between the start & ending timestamps,
         * then returning the found List.
         *
         * @param sts starting timestamp
         * @param ets ending timestamp
         * @return List of Usages
         */
        public static List<Usage> getBetweenSpanValidUsages(long sts, long ets) {
            return sDb.getUsageDao().getDurationValidUsageRecords(sts, ets);
        }

        /**
         * Method searches for valid Usages of any unarchived substance between the start & ending
         * timestamps, then returning the found List.
         *
         * @param sts starting timestamp
         * @param ets ending timestamp
         * @return List of Usages
         */
        public static List<Usage> getBetweenSpanValidUnarchivedUsages(long sts, long ets) {
            return sDb.getUsageDao().getDurationValidUnarchivedUsageRecords(sts, ets);
        }

        /**
         * Method searches for a substring within the notes of a particular substance's usages,
         * then returning any matching hits.
         *
         * @param sid Substance ID to be searching
         * @param substring substring to search for in the usage's notes
         * @return List of matching usages
         */
        public static List<Usage> getUsagesWithSubstring(int sid, String substring) {
            //had to throw a little hack in here for working with the '%' operator w/Room
            GlobalMisc.debugMsg("Permanence.getUsagesWithSubstring",
                    "called with " + sid + " and " + substring);

            return sDb.getUsageDao().getUsagesWithNotesSubstr(sid, "%" + substring + "%");
        }

        /**
         * Method searches for a substring in the notes of _all_ usages, then returning any
         * matching results.
         *
         * @param substring substring to search for in the usages's notes
         * @return List of matching usages
         */
        public static List<Usage> getAllUsagesWithSubstring(String substring) {
            //same hack as above
            return sDb.getUsageDao().getAllUsagesWithNotesSubstr("%" + substring + "%");
        }

        public static long getEarliestUsageTimestamp() {
            return sDb.getUsageDao().getEarliestUsageTimestamp();
        }

        public static long getEarliestUsageTimestampBySid(int sid) {
            return sDb.getUsageDao().getEarliestUsageTimestampBySID(sid);
        }

        public static long getLatestUsageTimestampBySid(int sid) {
            return sDb.getUsageDao().getLatestUsageTimestampBySID(sid);
        }

        public static int getUsageCountForSubstance(int sid) {
            return sDb.getUsageDao().getUsageCountForSubstance(sid);
        }
    }

    /**
     * UsualSuspects methods
     */
    public static class US {
        /**
         * Method saves a new UsualSuspect.
         *
         * @param us UsualSuspect object
         */
        public static void saveUS(UsualSuspect us) {
            sDb.getUsualSuspectDao().insertUsualSuspect(us);
        }

        /**
         * Method loads all UsualSuspects.
         *
         * @return List of UsualSuspects
         */
        public static List<UsualSuspect> loadAllUS() {
            return sDb.getUsualSuspectDao().getAll();
        }

        /**
         * Method loads all UsualSuspects with associated Substances that are unarchived.
         *
         * @return List of all unarchived UsualSuspects
         */
        public static List<UsualSuspect> loadAllUnarchivedUS() {
            return sDb.getUsualSuspectDao().getAllUnarchived();
        }

        /**
         * Method updates an existing record for an existing UsualSuspect object within the database.
         *
         * @param us modified UsualSuspect object
         */
        public static void updateUS(UsualSuspect us) {
            sDb.getUsualSuspectDao().updateUsualSuspect(us);
        }

        /**
         * Method wipes an existing UsualSuspect object/record from the database, and now toasts all
         * related crossreferences from the appropriate table, as well.
         *
         * @param us existing UsualSuspect object to wipe
         */
        public static void deleteUS(UsualSuspect us) {
            for (UsualSuspectSubsCrossRef crossRef :
                    Permanence.CrossRef.loadCrossRefsPerUsualSuspect(us.getId())) {
                //wipe the crossrefs
                sDb.getUsualSuspectSubsCrossRefDao().delete(crossRef);
            }

            //wipe the UsualSuspect
            sDb.getUsualSuspectDao().delete(us);
        }

        /**
         * Method snags a UsualSuspect's corresponding substance(s) from the cross reference table.
         *
         * @param USId UsualSuspect ID
         */
        public static List<Substance> loadUSSubs(int USId) {
            List<Substance> tmpSubs = null;

            for (UsualSuspectSubsCrossRef crossRef :
                    sDb.getUsualSuspectSubsCrossRefDao().loadUsualSuspectAndSubsByUS(USId)) {
                tmpSubs.add(Permanence.Subs.loadSubstanceById(crossRef.getSub()));
            }

            return tmpSubs;
        }

        /**
         * Method snags a UsualSuspect by ID.
         *
         * @param USId
         */
        public static UsualSuspect loadUSByID(int USId) {
            return sDb.getUsualSuspectDao().getUSByID(USId);
        }

        /**
         * Method snags a UsualSuspect by name.
         *
         * @param USName
         */
        public static UsualSuspect loadUSByName(String USName) {
            return sDb.getUsualSuspectDao().getUSByName(USName);
        }
    }

    /**
     * SubClassification methods
     */
    public static class SC {
        /**
         * Method saves a new SubClassification.
         *
         * @param sc SubClassification object to save
         */
        public static void saveSC(SubClassification sc) {
            sDb.getSubClassificationDao().insertSubClass(sc);
        }

        /**
         * Method loads all SubClassifications.
         *
         * @return List of existing SubClassifications
         */
        public static List<SubClassification> loadAllSC() {
            return sDb.getSubClassificationDao().getAll();
        }

        /**
         * Method loads SubClassification with a given ID #.
         *
         * @param id SubClassification ID number
         * @return SubClassification object
         */
        public static SubClassification loadSCById(int id) {
            return sDb.getSubClassificationDao().findClassById(id);
        }

        public static SubClassification loadSCByName(String name) {
            return sDb.getSubClassificationDao().findClassByName(name);
        }

        public static void wipeSC(SubClassification sc) {
            sDb.getSubClassificationDao().delete(sc);
        }
    }

    /**
     * SubstanceGoal methods
     */
    public static class SG {

        public static List<SubstanceGoal> loadAllSubstanceGoals() {
            return sDb.getSubstanceGoalDao().getAll();
        }

        public static List<SubstanceGoal> loadAllUnarchivedSubstanceGoals() {
            return sDb.getSubstanceGoalDao().getAllUnarchived();
        }

        public static SubstanceGoal loadSubGoalBySID(int sid) {
            return sDb.getSubstanceGoalDao().getSubstanceGoalBySid(sid);
        }

        public static void saveGoal(SubstanceGoal sGoal) {
            sDb.getSubstanceGoalDao().insertSubstanceGoal(sGoal);
        }

        public static void wipeGoal(SubstanceGoal sGoal) {
            sDb.getSubstanceGoalDao().delete(sGoal);
        }
    }

    /**
     * UsualSuspectSubsCrossRef methods
     */
    public static class CrossRef {
        /**
         * Method loads all cross references.
         *
         * @return List of UsualSuspectSubsCrossRef'ed
         */
        public static List<UsualSuspectSubsCrossRef> loadCrossRefs() {
            return sDb.getUsualSuspectSubsCrossRefDao().loadUsualSuspectCrossRefs();
        }
        /**
         * Method loads the UsualSuspectSubsCrossRef records by UsualSuspect ID.
         *
         * @param USId UsualSuspect ID
         * @return List of UsualSuspectSubsCrossRef'ed
         */
        public static List<UsualSuspectSubsCrossRef> loadCrossRefsPerUsualSuspect(int USId) {
            return sDb.getUsualSuspectSubsCrossRefDao().loadUsualSuspectAndSubsByUS(USId);
        }

        /**
         * Method saves a new UsualSuspectSubsCrossRef record.
         *
         * @param crossRef
         */
        public static void saveCrossRef(UsualSuspectSubsCrossRef crossRef) {
            sDb.getUsualSuspectSubsCrossRefDao().insertUsualSuspectSubsCrossRef(crossRef);
        }

        /**
         * Method deletes a UsualSuspectSubsCrossRef record.
         */
        public static void deleteCrossRef(UsualSuspectSubsCrossRef crossRef) {
            sDb.getUsualSuspectSubsCrossRefDao().delete(crossRef);
        }
    }

    /**
     * Methods for dealing with Taper goals.
     */
    public static class Tapers {
        public static List<Taper> loadAllTapers() {
            return sDb.getTaperDao().getAll();
        }

        public static List<Taper> loadAllValidTapers(Context ctxt) {
            if (!initialized) {
                Misc.init(ctxt);
            }

            return sDb.getTaperDao().getAllValid();
        }

        public static List<Taper> loadAllUnarchivedTapers() {
            return sDb.getTaperDao().getAllUnarchived();
        }

        public static List<Taper> loadAllValidUnarchivedTapers() {
            return sDb.getTaperDao().getAllValidUnarchived();
        }

        public static List<Taper> loadValidTapersBySid( int sid) {
            return sDb.getTaperDao().getAllValidBySid(sid);
        }

        public static Taper loadOneValidTaperBySid(int sid) {
            return sDb.getTaperDao().getOneValidBySid(sid);
        }

        public static Taper loadTaperById(int tid) {
            return sDb.getTaperDao().getTaperById(tid);
        }

        public static void saveTaper(Taper taper) {
            sDb.getTaperDao().insertTaper(taper);
        }

        public static void deleteTaper(Taper taper) {
            sDb.getTaperDao().delete(taper);
        }

        public static void updateTaper(Taper taper) {
            sDb.getTaperDao().updateTaper(taper);
        }
    }

    /**
     * misc methods (or ones focused on more than one of the above categories)
     */
    public static class Misc {
        /**
         * Method loads the last valid Usage for a given Substance.
         *
         * @param sid Substance ID number
         * @return Usage object
         */
        public static Usage loadLastValidUsageOfSub(int sid) {
            return sDb.getUsageDao().getLastValidUsage(sid);
        }

        /**
         * Method initializes the database upon first attempts to work with it.
         *
         * @param ctxt Context
         */
        public static void init(Context ctxt) {
            sDb = AppDatabase.getDatabase(ctxt);

            initialized = true;
        }

        public static int countSubstancesUsedThroughDuration(
                Context ctxt, long sts, long ets) {
            int runningTotal = 0;

            for (Substance sub : Subs.loadUnarchivedSubstances(ctxt)) {
                runningTotal += Permanence.Admins.countInDurationForSub(sub.getId(), sts, ets);
            }

            return runningTotal;
        }

        public static ArrayList<Integer> getSubIDsUseThroughDuration(long sts, long ets) {
            ArrayList<Integer> validSubIDsForDuration = new ArrayList<>();

            for (Substance sub : sDb.getSubstanceDao().getAllUnarchived()) {
                if (sDb.getUsageDao().countDurationValidUsageRecordFlagForSub(
                        sub.getId(), sts, ets) > 0) {
                    validSubIDsForDuration.add(sub.getId());
                }
            }

            return validSubIDsForDuration;

            //not sure why below doesn't work; I'd really like to figure that out
            //return sDb.getSubstanceDao().findUnarchivedSubsUsedInDuration(sts, ets);
        }
    }
}
