package com.example.sprite.half_lifetimer;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * This class provides an activity that can be called from other activities in order to provide a
 * full-screen 'dialog' to obtain the additional information (date/time/notes/etc) required in order
 * to fill in the record information for a prior usage/administration.
 */
public class AddPastUsage extends AppCompatActivity implements ChooseTimeFragment.TimeListener {
    private Substance sub;
    private UsualSuspect us;
    private final Calendar tmpCal = Calendar.getInstance();
    private int hour, minute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_past_usage);

        try {
            sub = SubData.subList.get(getIntent().getExtras().getInt("SUB_NDX"));
        } catch (Exception ex) {
            sub = null;
        }
        us = Permanence.US.loadUSByID(getIntent().getExtras().getInt("US_NDX"));

        SimpleDateFormat timeF = new SimpleDateFormat("HH:mm", Locale.getDefault());
        Calendar calDT = Calendar.getInstance();

        TextView tvwAddPastUsageHead = findViewById(R.id.tvwAddPastUsageHeading);
        if (sub != null) {
            tvwAddPastUsageHead.setText(getString(R.string.add_previous_usage, sub.getCommon_name()));
        } else {
            //we're working with a UsualSuspect, not just a sub
            tvwAddPastUsageHead.setText(getString(R.string.add_previous_usage, us.getName()));
        }
        tvwAddPastUsageHead.setTextSize(25);

        EditText edtTime = findViewById(R.id.edtTime);
        edtTime.setText(timeF.format(calDT.getTime()));
        edtTime.setEnabled(false);

        EditText edtNotes = findViewById(R.id.edtNotes);
        if (getIntent().getExtras().getString("NOTES") != null) {
            edtNotes.setText(getIntent().getExtras().getString("NOTES"));
        } else {    //past usage
            edtNotes.setText(R.string.not_applicable);
        }

        CalendarView cvwDate = findViewById(R.id.cvwDate);

        //get just the current date from calDT
        calDT.set(Calendar.HOUR_OF_DAY, 0);
        calDT.set(Calendar.MINUTE, 0);
        calDT.set(Calendar.SECOND, 0);
        calDT.set(Calendar.MILLISECOND, 0);
        cvwDate.setMaxDate(calDT.getTime().getTime());     //'ywhh
        calDT.set(Calendar.YEAR, (calDT.get(Calendar.YEAR) - 50));
        cvwDate.setMinDate(calDT.getTime().getTime());     //'ywhh * 2
        cvwDate.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {
                tmpCal.set(year, month, dayOfMonth);
            }
        });

        EditText edtDosage = findViewById(R.id.edtDosage);

        if (sub != null) {
            if (sub.getUnits() == DosageUnit.DOSE) {
                edtDosage.setText("1");
                edtDosage.setEnabled(false);
            } else if (getIntent().getExtras().getInt("DOSAGE") != -1) {
                edtDosage.setText(getIntent().getExtras().getString("DOSAGE"));
            } else {
                edtDosage.setText("0");
            }
        } else {
            //working with a UsualSuspect
            edtDosage.setText(R.string.not_applicable);
            edtDosage.setEnabled(false);
        }
    }

    /**
     * This method is utilized to validate that the dosage amount (ie between 0.001 & 999) is
     * within reasonable parameters (these being based off of metric measurements (which have the
     * maximum range, as opposed to imperial units) and their usual maximum span.
     *
     * @param doseText string which SHOULD contain the float dosage
     * @return boolean
     */
    private boolean validateDosage(String doseText) {
        //should this be moved to Usage? -- probably :|
        float dose;

        try {
            dose = Float.parseFloat(doseText);
        } catch (Exception ex) {
            Toast.makeText(this, "Unable to parse dosage text, please check and retry!",
                    Toast.LENGTH_LONG).show();

            return false;
        }

        return (! (dose < GlobalMisc.DoseRangeMin || dose > GlobalMisc.DoseRangeMax));
    }

    /**
     * Method is called when the activity's 'done' button has been pressed.  It handles calling the
     * validation(s) and then handles parsing and saving of the appropriate information.
     *
     * @param v View
     */
    public void doneWithPastUsage(View v) {
        EditText edtDosage = findViewById(R.id.edtDosage);
        EditText edtNotes = findViewById(R.id.edtNotes);

        if (sub != null) {
            //working with a substance, not a US

            if (!validateDosage(edtDosage.getText().toString())) {
                GlobalMisc.showSimpleDialog(AddPastUsage.this,
                        getResources().getString(R.string.invalid_dosage_title),
                        getResources().getString(R.string.invalid_dosage_message));

                return;
            }

            //the next 5 lines of code could all be thrown into the applicable Usage() constructor
            Usage pastUse = new Usage();

            pastUse.setSub_id(sub.getId());
            try {
                pastUse.setDosage(Float.parseFloat(edtDosage.getText().toString()));
            } catch (Exception ex) {
                Toast.makeText(this, "Unable to parse dosage information, please check and retry.",
                        Toast.LENGTH_LONG).show();
                return;
            }
            pastUse.setNotes(edtNotes.getText().toString().trim());

            //NOTE: the + 1 exists in the month field below due to the fact that Calendar uses a
            //      base 0 numbering scheme while the timestamp does not, so this needs to be
            //      included in order to make sure that things are added properly
            pastUse.setTimestamp(LocalDateTime.of(tmpCal.get(Calendar.YEAR),
                    tmpCal.get(Calendar.MONTH) + 1, tmpCal.get(Calendar.DAY_OF_MONTH),
                    this.hour, this.minute));

            Permanence.Admins.saveUsage(pastUse);

            Toast.makeText(AddPastUsage.this, "Past usage record saved!",
                    Toast.LENGTH_LONG).show();
        } else {
            //we're working with a UsualSuspect, not a sub
            HashMap<Integer, Float> newUsageData = us.getSIDsAndDosages();

            Set newUsageDataSet = newUsageData.entrySet();
            Iterator iterator = newUsageDataSet.iterator();
            while (iterator.hasNext()) {
                Map.Entry mentry = (Map.Entry) iterator.next();

                Permanence.Admins.saveUsage(new Usage((int) mentry.getKey(),
                        (float) mentry.getValue(),
                        LocalDateTime.of(tmpCal.get(Calendar.YEAR), tmpCal.get(Calendar.MONTH) + 1,
                                tmpCal.get(Calendar.DAY_OF_MONTH), this.hour, this.minute),
                        us.getNotes()));
            }

            //here we need to redirect to AdminData, as we want to see the resuls of our usage
            //added to the graph for confirmation, most likely
            int subNdx = GlobalMisc.getSubListPositionBySid(
                    (int)us.getSIDsAndDosages().keySet().toArray()[0]);

            GlobalMisc.debugMsg("switchToAdminData", "working with subNdx: " +
                    subNdx);

            Intent intent = new Intent(AddPastUsage.this, AdminData.class);
            intent.putExtra("SUB_NDX", subNdx);
            startActivity(intent);
        }

        finish();
    }

    /**
     * Opens the ChooseTimeFragment to allow the user to select the time of day
     * that the dosage was to have taken place at.
     *
     * @param v View requiem for a button click handler
     */
    public void setAdministrationTime(View v) {
        ChooseTimeFragment chooseTimeFragment = new ChooseTimeFragment();
        chooseTimeFragment.show(getSupportFragmentManager(), "");
        Bundle args = new Bundle();
        chooseTimeFragment.setArguments(args);
    }

    /**
     * Listener (I think) for the ChooseTimeFragment; waits for the values in
     * there to be set and then sets the local values appropriately, depending
     * on whether or not they are successfully validated.
     *
     * @param hour int Hour of the day administration took place
     * @param minute int Minute of the day " " "
     * @param second int Second of the day " " "
     */
    public void onTimeSelected(int hour, int minute, int second) {
        EditText edtTime = findViewById(R.id.edtTime);

        this.hour = hour;
        this.minute = minute;

        if (!validateTime()) {
            GlobalMisc.showSimpleDialog(AddPastUsage.this, "Invalid date/time!",
                    "Your date/time combination is not in the past, which it must be " +
                    "for it to be a 'past usage'.  Try again!");

            return;
        }

        if (this.minute > 9) {
            edtTime.setText(getString(R.string.time_format, this.hour, this.minute));
        } else {
            edtTime.setText(getString(R.string.time_format_padded_minute, this.hour, this.minute));
        }
    }

    /**
     * Method validates that the date/time chosen is in the past, not in the
     * present or the future.
     *
     * @return boolean true if valid
     */
    private boolean validateTime() {
        Calendar nowCal = Calendar.getInstance();

        if (tmpCal.compareTo(nowCal) >= 0) {
            return false;   //invalid, it's in the future
        } else if ((tmpCal.get(Calendar.YEAR) == nowCal.get(Calendar.YEAR)) &&
                   (tmpCal.get(Calendar.MONTH) == nowCal.get(Calendar.MONTH)) &&
                   (tmpCal.get(Calendar.DAY_OF_MONTH) == (nowCal.get(Calendar.DAY_OF_MONTH)))) {
            //calendar dates are the same, let's check the clock hours; I didn't want to do this all
            //in one nasty 'else if' clause due to readability/comprehension issues
            return (nowCal.get(Calendar.HOUR_OF_DAY) > this.hour) ||
                    ((nowCal.get(Calendar.HOUR_OF_DAY) == this.hour) &&
                            (nowCal.get(Calendar.MINUTE) > this.minute));
        }

        return true;
    }
}
