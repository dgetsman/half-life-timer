package com.example.sprite.half_lifetimer;

public class MyBoolean {
    boolean     value;

    public MyBoolean() { }

    public MyBoolean(boolean value) {
        this.value = value;
    }

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean ouah) {
        this.value = ouah;
    }
}
