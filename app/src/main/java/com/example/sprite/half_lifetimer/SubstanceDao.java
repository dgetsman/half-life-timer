package com.example.sprite.half_lifetimer;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface SubstanceDao {
    @Query("SELECT * FROM Substance")
    List<Substance> getAll();

    @Query("SELECT * FROM Substance WHERE archived LIKE 0")
    List<Substance> getAllUnarchived();

    @Query("SELECT * FROM Substance WHERE archived LIKE 1")
    List<Substance> getAllArchived();

    @Query("SELECT * FROM Substance WHERE cname LIKE :cname LIMIT 1")
    Substance findByCName(String cname);

    @Query("SELECT * FROM Substance WHERE id LIKE :id LIMIT 1")
    Substance findSubByID(int id);

    @Query("SELECT * FROM Substance WHERE id LIKE :id AND archived LIKE 0")
    Substance findUnarchivedSubByID(int id);

    /*@Query("SELECT id FROM Substance INNER JOIN Usage WHERE Usage.sub_id LIKE " +
            "Substance.id AND Usage.timestamp >= :sts AND Usage.timestamp < :ets AND " +
            "Substance.archived LIKE 0")
    ArrayList<Integer> findUnarchivedSubsUsedInDuration(long sts, long ets);*/

    @Query("SELECT * FROM Substance WHERE sub_class LIKE :sClass AND archived LIKE 0")
    List<Substance> findUnarchivedSubsBySClass(int sClass);

    @Query("SELECT units FROM Substance WHERE id LIKE :id")
    DosageUnit getUnitsBySID(int id);

    @Query("SELECT * FROM Substance WHERE archived LIKE 0 AND lipid_soluble LIKE 1")
    List<Substance> getUnarchivedLipidSolubleSubs();

    @Insert
    void insertAll(List<Substance> substances);

    @Insert
    void insertSub(Substance substance);

    @Update
    void update(Substance substance);

    @Delete
    void delete(Substance substance);

    @Query("DELETE FROM Substance WHERE id LIKE :sid")
    void deleteById(int sid);
}

