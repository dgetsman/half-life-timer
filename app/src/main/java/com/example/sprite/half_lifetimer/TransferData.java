package com.example.sprite.half_lifetimer;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

public class TransferData {
    List<UsageISODateTime> allUsages;
    List<Substance> allSubstances;
    List<UsualSuspect> allUsualSuspects;
    List<UsualSuspectSubsCrossRef> allUSCrossRefs;
    List<TaperISODateTime> allTapers;
    List<SubstanceGoal> allGoals;
    List<SubClassification> allClasses;

    public TransferData(Context ctxt) {
        this.allUsages = serializeUsageLDT(Permanence.Admins.loadUnarchivedUsages());
        this.allSubstances = Permanence.Subs.loadUnarchivedSubstances(ctxt);
        this.allUsualSuspects = Permanence.US.loadAllUnarchivedUS();
        this.allUSCrossRefs = Permanence.CrossRef.loadCrossRefs();
        this.allTapers = serializeTaperLDT(Permanence.Tapers.loadAllValidUnarchivedTapers());
        this.allGoals = Permanence.SG.loadAllUnarchivedSubstanceGoals();
        this.allClasses = Permanence.SC.loadAllSC();
    }

    public TransferData(List<Usage> uses, List<Substance> subs, List<UsualSuspect> usualSuspects,
                        List<UsualSuspectSubsCrossRef> crossRefs, List<Taper> tapers,
                        List<SubstanceGoal> goals, List<SubClassification> classes) {
        this.allUsages = serializeUsageLDT(uses);
        this.allSubstances = subs;
        this.allUsualSuspects = usualSuspects;
        this.allUSCrossRefs = crossRefs;
        this.allTapers = serializeTaperLDT(tapers);
        this.allGoals = goals;
        this.allClasses = classes;
    }

    public List<Usage> getAllUsages() {
        List<Usage> pureUsages = new ArrayList<>();

        for (UsageISODateTime usageISODT : this.allUsages) {
            pureUsages.add(new Usage(usageISODT.getSub_id(), usageISODT.getDosage(),
                    usageISODT.getLDT(), usageISODT.getNotes(), usageISODT.isValid_entry()));
        }

        return pureUsages;
    }

    public void setAllUsages(List<Usage> allPureUsages) {
        for (Usage use : allPureUsages) {
            this.allUsages.add(new UsageISODateTime(use));
        }
    }

    public List<Substance> getAllSubstances() {
        return allSubstances;
    }

    public void setAllSubstances(List<Substance> allSubstances) {
        this.allSubstances = allSubstances;
    }

    public List<UsualSuspect> getAllUsualSuspects() {
        return allUsualSuspects;
    }

    public void setAllUsualSuspects(List<UsualSuspect> allUsualSuspects) {
        this.allUsualSuspects = allUsualSuspects;
    }

    public List<UsualSuspectSubsCrossRef> getAllUSCrossRefs() {
        return allUSCrossRefs;
    }

    public void setAllUSCrossRefs(List<UsualSuspectSubsCrossRef> allUSCrossRefs) {
        this.allUSCrossRefs = allUSCrossRefs;
    }

    public List<Taper> getAllTapers() {
        List<Taper> pureTapers = new ArrayList<>();

        for (TaperISODateTime taperISODT : this.allTapers) {
            pureTapers.add(new Taper(taperISODT.getName(), taperISODT.getTargetLDT(),
                    taperISODT.getStartLDT(), taperISODT.getSid(),
                    taperISODT.getDailyDosageTarget(), taperISODT.getDailyDosageStart(),
                    taperISODT.isValid(), taperISODT.getAdminsPerDay(), taperISODT.getStartLT(),
                    taperISODT.getEndLT(), taperISODT.getDosageInterval()));
        }

        return pureTapers;
    }

    public void setAllTapers(List<Taper> allPureTapers) {
        for (Taper taper : allPureTapers) {
            this.allTapers.add(new TaperISODateTime(taper));
        }
    }

    public List<SubstanceGoal> getAllGoals() {
        return allGoals;
    }

    public void setAllGoals(List<SubstanceGoal> allGoals) {
        this.allGoals = allGoals;
    }

    public List<SubClassification> getAllClasses() {
        return allClasses;
    }

    public void setAllClasses(List<SubClassification> allClasses) {
        this.allClasses = allClasses;
    }

    public List<UsageISODateTime> serializeUsageLDT(List<Usage> uses) {
        //since we've been having issues with this, we're going to try using the new
        //UsageISODateTime class, instead
        List<UsageISODateTime> allISOUsages = new ArrayList<>();

        for (Usage use : uses) {
            UsageISODateTime useISODateTime = new UsageISODateTime(use);
            allISOUsages.add(useISODateTime);
        }

        return allISOUsages;
    }

    public List<TaperISODateTime> serializeTaperLDT(List<Taper> tapers) {
        List<TaperISODateTime> allISOUTapers = new ArrayList<>();

        for (Taper taper : tapers) {
            TaperISODateTime taperISODateTime = new TaperISODateTime(taper);
            allISOUTapers.add(taperISODateTime);
        }

        return allISOUTapers;
    }

    public String generateJson() {
        Gson gson;
        //gson = new Gson();
        gson = new GsonBuilder().setPrettyPrinting().create();

        return gson.toJson(this);
    }
}
