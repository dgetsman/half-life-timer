package com.example.sprite.half_lifetimer;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.List;

public class FullAdminsDumpByID extends AppCompatActivity {
    private boolean reverse = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_admins_dump_by_id);

        updateDisplay();
    }

    private void updateDisplay() {
        GlobalMisc.showResultsBeingCompiledMessage(getApplicationContext());

        new Thread(new Runnable() {
            @Override
            public void run() {
                List<Usage> allAdmins;
                int cntr = 0;
                final Context fCtxt = getApplicationContext();
                boolean zebra = true;
                String adminText;

                //allAdmins = Permanence.Admins.loadUnarchivedUsages();
                if (reverse) {
                    allAdmins = Permanence.Admins.loadValidUnarchivedOrderedDescUsagesByUID();
                } else {
                    allAdmins = Permanence.Admins.loadValidUnarchivedOrderedUsagesByUID();
                }

                LinearLayout lloAllAdminsByID = findViewById(R.id.lloAllAdminsByID);
                TextView[] usesTextView = new TextView[allAdmins.size()];

                //in case of refresh
                lloAllAdminsByID.removeAllViewsInLayout();

                for (Usage admin : allAdmins) {
                    final Usage fAdmin = admin;

                    usesTextView[cntr] = new TextView(fCtxt);
                    usesTextView[cntr].setTextSize(18);
                    adminText = admin.toString();
                    usesTextView[cntr].setText(adminText);
                    if (adminText.contentEquals("invalid data")) {
                        usesTextView[cntr].setTextColor(Color.RED);
                    } else if (zebra) {
                        usesTextView[cntr].setTextColor(Color.BLACK);
                    } else {
                        usesTextView[cntr].setTextColor(Color.GRAY);
                    }
                    zebra = !zebra;
                    usesTextView[cntr].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //view notes
                            GlobalMisc.showSimpleDialog(FullAdminsDumpByID.this,
                                    fAdmin.getTimestamp().toString(), "Sub ID: " +
                                            fAdmin.getSub_id() + " " + fAdmin.getNotes());
                        }
                    });
                    usesTextView[cntr].setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            AlertDialog.Builder builder =
                                    new AlertDialog.Builder(FullAdminsDumpByID.this);
                            builder.setTitle("Delete " + fAdmin.getTimestamp() +
                                    "?  Are you sure?");
                            builder.setMessage(fAdmin.getNotes());
                            builder.setPositiveButton(getResources().getString(R.string.ok),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Permanence.Admins.wipeUsage(fAdmin);

                                            updateDisplay();

                                            dialog.dismiss();
                                        }
                                    });
                            builder.setNegativeButton(getResources().getString(R.string.cancel),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });

                            builder.show();

                            return true;
                        }
                    });
                    final LinearLayout fLloAllAdminsByID = lloAllAdminsByID;
                    final TextView fUseTextView = usesTextView[cntr++];

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            fLloAllAdminsByID.addView(fUseTextView);
                        }
                    });
                }
            }
        }).start();
    }

    public void reverseToggle(View v) {
        ToggleButton tbtnReverseSort = findViewById(R.id.tbtnReverseOrder);

        tbtnReverseSort.setChecked(reverse);
        reverse = !reverse;

        updateDisplay();
    }
}
