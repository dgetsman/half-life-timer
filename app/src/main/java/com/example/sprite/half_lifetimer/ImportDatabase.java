package com.example.sprite.half_lifetimer;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class ImportDatabase extends AppCompatActivity {
    private static final UUID MY_UUID = UUID.fromString("633cc67a-6f6f-4ed1-9d83-6c9044eaeb6a");

    private static final int REQUEST_ENABLE_BT = 1;
    BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    private BluetoothDevice mmDevice;
    ConnectedThread mConnectedThread;

    String TAG = "ImportDatabase";

    final private boolean debugging = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_import_database);

        ProgressBar prbBusyImporting = findViewById(R.id.prbImporting);
        prbBusyImporting.setVisibility(View.INVISIBLE);
        prbBusyImporting.setEnabled(false);

        if (bluetoothAdapter != null && !bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent =
                    new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        GlobalMisc.showDialogGetConfirmation(ImportDatabase.this,
                "Are you on a fresh database?",
                "You should be using the ImportDatabase functionality only if you have " +
                "not already created any data in Half-life Timer.",
                new GlobalMisc.OnDialogConfResultListener() {
                    public void onResult(boolean result) {
                        if (result) {
                            Toast.makeText(ImportDatabase.this,
                                    "Now accepting bluetooth connection.",
                                    Toast.LENGTH_LONG).show();

                            new Thread(new Runnable() {
                                public void run() {
                                    AcceptThread accept = new AcceptThread();
                                    accept.start();
                                }
                            }).start();
                        }
                    }
                });
    }

    /**
     * onClick handler for the import bluetooth button.  Brings up a dialog
     * to select the device to pair with and receive the import from, then
     * starts the connection.
     *
     * @param v View
     */
    public void importBluetooth(View v) {
        //NOTE: although not named as such, this method pairs the devices
        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

        Log.e(TAG, "" + pairedDevices.size());

        if (pairedDevices.size() > 0) {
            final Object[] devices = pairedDevices.toArray();
            String[] choices = new String[devices.length];
            for (int cntr = 0; cntr < devices.length; cntr++) {
                choices[cntr] = ((BluetoothDevice)devices[cntr]).getName();
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(ImportDatabase.this);
            builder.setTitle("Pick the device to receive JSON from.");
            builder.setItems(choices, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ProgressBar prbBusyImporting = findViewById(R.id.prbImporting);
                    prbBusyImporting.setVisibility(View.VISIBLE);
                    prbBusyImporting.setEnabled(true);
                    prbBusyImporting.setIndeterminate(true);
                    prbBusyImporting.setActivated(true);

                    Log.e(TAG, "" + (BluetoothDevice)devices[which]);

                    ConnectThread connect =
                            new ConnectThread((BluetoothDevice)devices[which], MY_UUID);
                    connect.start();
                }
            });

            builder.show();
        }
    }

    /**
     * Handles the creation of the connection with the other device.
     */
    private class ConnectThread extends Thread {
        private BluetoothSocket mmSocket;

        public ConnectThread(BluetoothDevice device, UUID uuid) {
            Log.d(TAG, "ConnectThread: started.");
            mmDevice = device;
        }

        /**
         * Creates the socket, then initiates the connection.
         */
        public void run(){
            BluetoothSocket tmp = null;
            Log.i(TAG, "RUN mConnectThread ");

            // Get a BluetoothSocket for a connection with the
            // given BluetoothDevice
            try {
                Log.d(TAG, "ConnectThread: Trying to create InsecureRfcommSocket using UUID: "
                        + MY_UUID);
                tmp = mmDevice.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) {
                Log.e(TAG, "ConnectThread: Could not create InsecureRfcommSocket " +
                        e.getMessage());
            }

            mmSocket = tmp;

            // Make a connection to the BluetoothSocket
            try {
                // This is a blocking call and will only return on a
                // successful connection or an exception
                mmSocket.connect();

            } catch (IOException e) {
                // Close the socket
                try {
                    mmSocket.close();
                    Log.d(TAG, "run: Closed Socket.");
                } catch (IOException e1) {
                    Log.e(TAG, "mConnectThread: run: Unable to close connection in socket " +
                            e1.getMessage());
                }
                Log.d(TAG, "run: ConnectThread: Could not connect to UUID: " + MY_UUID);
            }

            //will talk about this in the 3rd video
            connected(mmSocket);
        }

        /**
         * Aborts the connection attempt at the socket level.
         */
        public void cancel() {
            try {
                Log.d(TAG, "cancel: Closing Client Socket.");
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "cancel: close() of mmSocket in Connectthread failed. " +
                        e.getMessage());
            }
        }
    }

    /**
     * Takes the socket and uses it to start the thread managing the
     * connection & performing transmission.
     *
     * @param mmSocket BluetoothSocket
     */
    private void connected(BluetoothSocket mmSocket) {
        Log.d(TAG, "connected: Starting.");

        // Start the thread to manage the connection and perform transmissions
        mConnectedThread = new ConnectedThread(mmSocket);
        mConnectedThread.start();
    }

    /**
     * All of the methods for managing the connection & transmitting
     * the data.
     */
    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            Log.d(TAG, "ConnectedThread: Starting.");

            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                tmpIn = mmSocket.getInputStream();
                tmpOut = mmSocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        /**
         * Listens for incoming data, filling the incoming buffer and then
         * displaying the incoming JSON in the applicable TextView.
         */
        public void run() {
            byte[] buffer = new byte[1024];  // buffer store for the stream

            int bytes; // bytes returned from read()

            String incomingBuffer = "";
            // Keep listening to the InputStream until an exception occurs
            Log.d(TAG, "Listening via ConnectedThread.run()");

            while (true) {
                // Read from the InputStream
                try {
                    bytes = mmInStream.read(buffer);
                    final String incomingMessage = new String(buffer, 0, bytes);

                    incomingBuffer = incomingBuffer + incomingMessage;

                    Log.d(TAG, "Received: " + incomingBuffer.length());
                } catch (IOException e) {
                    Log.e(TAG, "write: Error reading Input Stream. " + e.getMessage() );

                    final ProgressBar prbBusyImporting = findViewById(R.id.prbImporting);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //return the progress bar to the ether
                            prbBusyImporting.setVisibility(View.INVISIBLE);
                            prbBusyImporting.setEnabled(false);
                            prbBusyImporting.setActivated(false);
                        }
                    });

                    break;
                }
            }

            final String fIncomingBuffer = incomingBuffer;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    TextView tvwIncomingJSON = findViewById(R.id.tvwIncomingJSON);
                    tvwIncomingJSON.setText(fIncomingBuffer);
                }
            });
        }

        /**
         * Method is called from the main activity to shutdown the connection
         */
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Toast.makeText(ImportDatabase.this, "Unable to close socket?!?",
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Class handles creating a listening server socket for the link.
     */
    private class AcceptThread extends Thread {
        // The local server socket
        private final BluetoothServerSocket mmServerSocket;

        public AcceptThread() {
            BluetoothServerSocket tmp = null ;

            // Create a new listening server socket
            try{
                tmp = bluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord("appname",
                        MY_UUID);

                Log.d(TAG, "AcceptThread: Setting up Server using: " + MY_UUID);
            }catch (IOException e){
                Log.e(TAG, "AcceptThread: IOException: " + e.getMessage() );
            }

            mmServerSocket = tmp;
        }

        /**
         * Method only returns on a successful connection or exception status.
         */
        public void run() {
            Log.d(TAG, "run: AcceptThread Running.");

            BluetoothSocket socket = null;

            try {
                // This is a blocking call and will only return on a
                // successful connection or an exception
                Log.d(TAG, "run: RFCOM server socket start.....");

                socket = mmServerSocket.accept();

                Log.d(TAG, "run: RFCOM server socket accepted connection.");

            } catch (IOException e) {
                Log.e(TAG, "AcceptThread: IOException: " + e.getMessage());
            }

            //talk about this is in the 3rd
            if (socket != null) {
                connected(socket);
            }

            Log.i(TAG, "END mAcceptThread ");
        }

        /**
         * Method cancels AcceptThread, closing the socket at the end.
         */
        public void cancel() {
            Log.d(TAG, "cancel: Canceling AcceptThread.");

            try {
                mmServerSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "cancel: Close of AcceptThread ServerSocket failed. " +
                        e.getMessage());
            }
        }
    }

    /**
     * Method is the main entry point for parsing the JSON that has been
     * received; also the handler for the 'parse JSON' button's onClick
     * attribute.
     *
     * @param v View
     */
    public void parseJSON(View v) {
        TextView tvwIncomingJSON = findViewById(R.id.tvwIncomingJSON);

        switch (parseJSONString(tvwIncomingJSON.getText().toString())) {
            case 0:
                Toast.makeText(ImportDatabase.this, "It's all good",
                        Toast.LENGTH_LONG).show();

                break;

            case -1:
                Toast.makeText(ImportDatabase.this, "Broke on substances",
                        Toast.LENGTH_LONG).show();

                break;

            case -2:
                Toast.makeText(ImportDatabase.this, "Broke on usages",
                        Toast.LENGTH_LONG).show();

                break;

            case -3:
                Toast.makeText(ImportDatabase.this, "Broke on usual suspects",
                        Toast.LENGTH_LONG).show();

                break;

            case -4:
                Toast.makeText(ImportDatabase.this, "Broke on cross references",
                        Toast.LENGTH_LONG).show();

                break;

            case -5:
                Toast.makeText(ImportDatabase.this, "Broke on tapers",
                        Toast.LENGTH_LONG).show();

                break;

            case -6:
                Toast.makeText(ImportDatabase.this, "Broke on goals",
                        Toast.LENGTH_LONG).show();

                break;

            case -7:
                Toast.makeText(ImportDatabase.this, "Broke on classes",
                        Toast.LENGTH_LONG).show();

                break;

            case -8:
            case -9:
                Toast.makeText(ImportDatabase.this, "General JSON Parsing Error: Please notify " +
                        "the developer!", Toast.LENGTH_LONG).show();

                break;

            default:
                Toast.makeText(ImportDatabase.this, "Looks like everything turned " +
                                "out alright", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * This method handles the nuts 'n bolts of breaking down the primary JSON
     * string into its 4 component JSON blobs, then breaks those down into
     * their constituent Lists of object types for passing off into the stuff*
     * methods which will pack them into the database.
     *
     * @param jsonString String
     * @return int error code
     */
    private int parseJSONString(String jsonString) {
        TransferData consolidatedData;
        Gson gson = new Gson();

        try {
            consolidatedData = gson.fromJson(jsonString, TransferData.class);
        } catch (com.google.gson.JsonSyntaxException ex) {
            GlobalMisc.showSimpleDialog(ImportDatabase.this, "JSON Syntax Exception",
                    "JSON parsing was hampered by malformed JSON; this is usually as a " +
                    "result of an incomplete bluetooth transmission attempt and/or bluetooth " +
                    "buffering issues.  Error: " + ex.toString());

            return -9;
        } catch (Exception ex) {
            GlobalMisc.showSimpleDialog(ImportDatabase.this, "Error parsing JSON",
                    "The following error is preventing JSON parsing: " + ex.toString());

            return -8;
        }

        TextView tvwImportStats = findViewById(R.id.tvwImportStats);
        tvwImportStats.setText(getResources().getString(R.string.subs_usages_us_cx,
                consolidatedData.getAllSubstances().size(),
                consolidatedData.getAllUsages().size(),
                consolidatedData.getAllUsualSuspects().size(),
                consolidatedData.getAllUSCrossRefs().size()));
        tvwImportStats.setTextSize(16);

        if (!stuffSubstances(consolidatedData.getAllSubstances())) {
            return -1;
        } else if (!stuffUsages(consolidatedData.getAllUsages())) {
            return -2;
        } else if (!stuffUsualSuspects(consolidatedData.getAllUsualSuspects())) {
            return -3;
        } else if (!stuffCrossReferences(consolidatedData.getAllUSCrossRefs())) {
            return -4;
        } else if (!stuffTapers(consolidatedData.getAllTapers())) {
            return -5;
        } else if (!stuffGoals(consolidatedData.getAllGoals())) {
            return -6;
        } else if (!stuffClasses(consolidatedData.getAllClasses())) {
            return -7;
        }

        return 0;
    }

    /**
     * Handles checking the Substances list for duplicates and, if they're
     * unique, packs them into the database.
     *
     * @param newSubstances List<Substance>
     * @return boolean success or no
     */
    private boolean stuffSubstances(List<Substance> newSubstances) {
        List<Substance> preexistingSubs =
                Permanence.Subs.loadSubstances(ImportDatabase.this);  //we are looking at
                                                                            //archived here, as well
        int duplicates = 0;
        int newEntries = 0;

        for (Substance maybeNewSub : newSubstances) {
            boolean found = false;
            boolean idCollision = false;

            for (Substance oldSub : preexistingSubs) {
                if (maybeNewSub.getCommon_name().toLowerCase().trim().contentEquals(
                        oldSub.getCommon_name().toLowerCase().trim()) ||
                    maybeNewSub.getSci_name().toLowerCase().trim().contentEquals(
                        oldSub.getSci_name().toLowerCase().trim())) {
                    //we have a duplicate here
                    found = true;
                    duplicates++;

                    break;
                } else if (maybeNewSub.getId() == oldSub.getId()) {
                    idCollision = true;

                    break;
                }
            }

            if (idCollision) {
                showIDCollisionError();

                return false;
            } else if (found) {    //we're not adding this duplicate
                continue;
            }

            if (!debugging) {
                Permanence.Subs.saveSubstance(ImportDatabase.this, maybeNewSub);
            }
            newEntries++;
        }

        TextView tvwImportStats = findViewById(R.id.tvwImportStats);
        tvwImportStats.setText(getResources().getString(R.string.old_text_new_subs,
                tvwImportStats.getText().toString(), newEntries, duplicates));

        return true;
    }

    /**
     * Handles checking the Usages list for duplicates and, if they're
     * unique, packs them into the database.
     *
     * @param newUsages List<Usage>
     * @return boolean success or no
     */
    private boolean stuffUsages(List<Usage> newUsages) {
        List<Usage> preexistingUsages = Permanence.Admins.loadUsages();

        int duplicates = 0;
        int newEntries = 0;

        for (Usage maybeNewUsage : newUsages) {
            boolean found = false;
            boolean idCollision = false;

            for (Usage oldUsage : preexistingUsages) {
                //we really need to do a mapping of json substance ID #s to the freshly imported
                //substance ID #s so that we can compare properly here instead of using 'notes' as a
                //backup comparison
                if (maybeNewUsage.getTimestamp().isEqual(oldUsage.getTimestamp()) &&
                    maybeNewUsage.getSub_id() == oldUsage.getSub_id()) {
                    found = true;
                    duplicates++;

                    break;
                } else if (maybeNewUsage.getId() == oldUsage.getId()) {
                    idCollision = true;
                }
            }

            if (idCollision) {
                showIDCollisionError();

                return false;
            } else if (found) {    //don't add the dupe
                continue;
            }

            if (!debugging) {
                Permanence.Admins.saveUsage(maybeNewUsage);
            }
            newEntries++;
        }

        TextView tvwImportStats = findViewById(R.id.tvwImportStats);
        tvwImportStats.setText(getResources().getString(R.string.old_text_new_usages,
                tvwImportStats.getText().toString(), newEntries, duplicates));

        return true;
    }

    /**
     * Handles checking the UsualSuspects list for duplicates and, if they're
     * unique, packs them into the database.
     *
     * @param newUsualSuspects List<UsualSuspect>
     * @return boolean success or no
     */
    private boolean stuffUsualSuspects(List<UsualSuspect> newUsualSuspects) {
        List<UsualSuspect> preexistingUsualSuspects = Permanence.US.loadAllUS();

        int duplicates = 0;
        int newEntries = 0;

        for (UsualSuspect maybeNewUsualSuspect : newUsualSuspects) {
            boolean found = false;
            boolean idCollision = false;

            for (UsualSuspect oldUsualSuspect : preexistingUsualSuspects) {
                if (maybeNewUsualSuspect.getName().toLowerCase().trim().contentEquals(
                        oldUsualSuspect.getName().toLowerCase().trim()) &&
                    maybeNewUsualSuspect.getNotes().toLowerCase().trim().contentEquals(
                        oldUsualSuspect.getNotes().toLowerCase().trim())) {
                    found = true;
                    duplicates++;

                    break;
                } else if (maybeNewUsualSuspect.getId() == oldUsualSuspect.getId()) {
                    idCollision = true;
                }
            }

            if (idCollision) {
                showIDCollisionError();

                return false;
            } else if (found) {
                continue;
            }

            if (!debugging) {
                Permanence.US.saveUS(maybeNewUsualSuspect);
            }
            newEntries++;
        }

        TextView tvwImportStats = findViewById(R.id.tvwImportStats);
        tvwImportStats.setText(getResources().getString(R.string.old_text_new_usual_suspects,
                tvwImportStats.getText().toString(), newEntries, duplicates));

        return true;
    }

    /**
     * Handles checking the CrossReferences list for duplicates and, if they're
     * unique, packs them into the database.
     *
     * @param newCrossRefs List<UsualSuspectSubsCrossRef>
     * @return boolean success or no
     */
    private boolean stuffCrossReferences(List<UsualSuspectSubsCrossRef> newCrossRefs) {
        List<UsualSuspectSubsCrossRef> preexistingCrossRefs = Permanence.CrossRef.loadCrossRefs();

        int duplicates = 0;
        int newEntries = 0;

        //NOTE: no id collision is necessary here as the 'usualSuspect' and 'sub' fields are used
        //as a composite key for the record
        for (UsualSuspectSubsCrossRef maybeNewCrossRef : newCrossRefs) {
            boolean found = false;

            for (UsualSuspectSubsCrossRef oldCrossRef : preexistingCrossRefs) {
                if ((maybeNewCrossRef.getUsualSuspect() == oldCrossRef.getUsualSuspect()) &&
                        (maybeNewCrossRef.getSub() == oldCrossRef.getSub()) &&
                        (maybeNewCrossRef.getDosage() == oldCrossRef.getDosage())) {
                    found = true;
                    duplicates++;

                    break;
                }
            }

            if (found) {
                continue;
            }

            if (!debugging) {
                Permanence.CrossRef.saveCrossRef(maybeNewCrossRef);
            }
            newEntries++;
        }

        TextView tvwImportStats = findViewById(R.id.tvwImportStats);
        tvwImportStats.setText(getResources().getString(R.string.old_text_new_cross_reference,
                tvwImportStats.getText().toString(), newEntries, duplicates));

        return true;
    }

    private boolean stuffTapers(List<Taper> newTapers) {
        List<Taper> preexistingTapers = Permanence.Tapers.loadAllTapers();

        int duplicates = 0;
        int newEntries = 0;

        for (Taper maybeNewTaper : newTapers) {
            boolean found = false;

            for (Taper oldTaper : preexistingTapers) {
                if ((maybeNewTaper.getName().toLowerCase().trim().contentEquals(
                        oldTaper.getName().toLowerCase().trim()))) {
                    //collision here
                    found = true;
                    duplicates++;

                    break;
                }
            }

            if (found) {
                continue;
            } else if (!debugging) {
                Permanence.Tapers.saveTaper(maybeNewTaper);
            }
            newEntries++;
        }

        TextView tvwImportStats = findViewById(R.id.tvwImportStats);
        tvwImportStats.setText(getResources().getString(R.string.old_text_new_tapers,
                tvwImportStats.getText().toString(), newEntries, duplicates));

        return true;
    }

    private boolean stuffGoals(List<SubstanceGoal> newGoals) {
        List<SubstanceGoal> preexistingGoals = Permanence.SG.loadAllSubstanceGoals();

        int duplicates = 0;
        int newEntries = 0;

        for (SubstanceGoal maybeNewGoal : newGoals) {
            boolean found = false;

            for (SubstanceGoal oldGoal : preexistingGoals) {
                if (maybeNewGoal.getSubId() == oldGoal.getSubId()) {
                    //collision
                    found = true;
                    duplicates++;

                    break;
                }
            }

            if (found) {
                continue;
            } else if (!debugging) {
                Permanence.SG.saveGoal(maybeNewGoal);
            }
            newEntries++;
        }

        TextView tvwImportStats = findViewById(R.id.tvwImportStats);
        tvwImportStats.setText(getResources().getString(R.string.old_text_new_goals,
                tvwImportStats.getText().toString(), newEntries, duplicates));

        return true;
    }

    private boolean stuffClasses(List<SubClassification> newClasses) {
        List <SubClassification> preexistingClasses = Permanence.SC.loadAllSC();

        int duplicates = 0;
        int newEntries = 0;

        for (SubClassification maybeNewClass : newClasses) {
            boolean found = false;

            for (SubClassification oldClass : preexistingClasses) {
                if ((maybeNewClass.getName().toLowerCase().trim().contentEquals(
                        oldClass.getName().toLowerCase().trim()))) {
                    //collision
                    found = true;
                    duplicates++;

                    break;
                }
            }

            if (found) {
                continue;
            } else if (!debugging) {
                Permanence.SC.saveSC(maybeNewClass);
            }
            newEntries++;
        }

        TextView tvwImportStats = findViewById(R.id.tvwImportStats);
        tvwImportStats.setText(getResources().getString(R.string.old_text_new_classes,
                tvwImportStats.getText().toString(), newEntries, duplicates));

        return true;
    }

    /**
     * Simply shows the user a 'simple dialog' regarding the ID collision error
     * and how it means that (for now) they really need to wipe their database
     * and only import the JSON into a fresh environment.
     */
    private void showIDCollisionError() {
        GlobalMisc.showSimpleDialog(ImportDatabase.this, "ID Collision Error",
                "There is already an alternate object in the database with the same " +
                "database ID #.  Until support is added, you must wipe the database first, if you" +
                " want to import your new data!");
    }
}