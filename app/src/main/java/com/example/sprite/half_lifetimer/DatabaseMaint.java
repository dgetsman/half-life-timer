package com.example.sprite.half_lifetimer;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class DatabaseMaint extends AppCompatActivity {

    private final Calendar tmpCal = Calendar.getInstance();
    private RadioGroup rgApplicableSubs;
    private ProgressBar progressBar;
    private int progressBarStatus = 0;
    private int records = 0;
    private Handler progressBarHandler = new Handler();

    /**
     * Method simply sets up the activity's UI and registers a listener for
     * the calendar.
     *
     * @param savedInstanceState requiem for an onCreate
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database_maint);

        progressBar = findViewById(R.id.progressBar);
        CalendarView cvwPurgeDate = findViewById(R.id.cvwPurgeDateSelector);
        Calendar calDT = Calendar.getInstance();
        LinearLayout lloSubList = findViewById(R.id.lloSubstanceSelection);
        RadioButton[] rbApplicableSub = new RadioButton[SubData.subList.size()];
        rgApplicableSubs = new RadioGroup(this);
        rgApplicableSubs.setOrientation(RadioGroup.VERTICAL);
        progressBar.setKeepScreenOn(false);
        progressBar.setEnabled(false);
        progressBar.setVisibility(ProgressBar.VISIBLE);

        //set up the radio buttons
        for (int cntr = 0; cntr < SubData.subList.size(); cntr++) {
            rbApplicableSub[cntr] = new RadioButton(this);
            rbApplicableSub[cntr].setText(SubData.subList.get(cntr).getCommon_name());
            rgApplicableSubs.addView(rbApplicableSub[cntr]);
        }

        lloSubList.addView(rgApplicableSubs);

        //set up the Calendar to snag dates from
        //get just the current date from calDT
        calDT.set(Calendar.HOUR_OF_DAY, 0);
        calDT.set(Calendar.MINUTE, 0);
        calDT.set(Calendar.SECOND, 0);
        calDT.set(Calendar.MILLISECOND, 0);

        //set up our calendarview
        cvwPurgeDate.setMaxDate(calDT.getTime().getTime());     //'ywhh
        //now we set the minimum date
        calDT.set(Calendar.YEAR, (calDT.get(Calendar.YEAR) - 50));
        cvwPurgeDate.setMinDate(calDT.getTime().getTime());     //'ywhh * 2

        //now we need to register a calendar listener
        cvwPurgeDate.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {
                tmpCal.set(year, month , dayOfMonth);
                tmpCal.set(Calendar.MILLISECOND, 0);
            }
        });
    }

    /**
     * Method obtains the date and substance selected from the calendar, then
     * reads all of the entries with applicable timestamps from the database.
     * After the user is sent through a confirmation dialog, the records are
     * then finally purged from the database (should the user complete the
     * confirmation dialog properly).
     *
     * @param v View (requiem for an onClick handler)
     */
    public void purgeDatabase(View v) {
        Switch swtAllEntries = findViewById(R.id.swtPurgeAllEntries);

        final long calDT = tmpCal.getTimeInMillis() / 1000;
        int subNdx = -1;

        //we need to get the value of the calendar in milliseconds and compare that to the value of
        //at least one timestamp from the database in order to verify that this comparison is
        //making any sense at all heah
        if (GlobalMisc.Debugging) {
            GlobalMisc.debugMsg("purgeDatabase",
                    "tmpCal time in epoch seconds: " + calDT +
                            " (Converted: " + Converters.toLocalDateTime(calDT) + ")");
        }

        if (rgApplicableSubs.getCheckedRadioButtonId() != -1 && !swtAllEntries.isChecked()) {
            subNdx = SubData.subList.get(rgApplicableSubs.indexOfChild(findViewById(
                    rgApplicableSubs.getCheckedRadioButtonId()))).getId();
        } else if (!swtAllEntries.isChecked()) {
            Toast.makeText(DatabaseMaint.this, "You must first select a substance to purge.",
                    Toast.LENGTH_LONG).show();

            return;
        }
        /* if these aren't activated then the switch is set to 'all entries' and subNdx is still
           equal to -1 */

        if (GlobalMisc.Debugging) {
            List<Long> priorTimestamps = Permanence.Admins.getPriorToTimestamps(subNdx, calDT);

            if (priorTimestamps.size() > 0) {
                long lastTS = priorTimestamps.get(priorTimestamps.size() - 1);
                long firstTS = priorTimestamps.get(0);

                GlobalMisc.debugMsg("purgeDatabase", "first matching DB entry " +
                        "timestamp: " + firstTS + " (Converted: " +
                        Converters.toLocalDateTime(firstTS) + ")");
                GlobalMisc.debugMsg("purgeDatabase", "last matching DB entry " +
                        "timestamp: " + lastTS + " Converted: " +
                        Converters.toLocalDateTime(lastTS) + ")");
                GlobalMisc.debugMsg("purgeDatabase", "total entries returned: " +
                        priorTimestamps.size());
            }
        }

        int countToFry;
        if (!swtAllEntries.isChecked() && subNdx != -1) {
            countToFry = Permanence.Admins.getUsagePriorToCount(subNdx, calDT);
        } else if (swtAllEntries.isChecked()) {
            countToFry = Permanence.Admins.getUsageFromAllPriorToCount(calDT);
        } else {
            Toast.makeText(DatabaseMaint.this, "You must first select a substance or toggle " +
                    "the switch for all substances!", Toast.LENGTH_LONG).show();

            return;
        }

        final int fSubNdx = subNdx;
        final int fCountToFry = countToFry;

        AlertDialog.Builder builder = new AlertDialog.Builder(DatabaseMaint.this);
        builder.setTitle("Delete Records?");
        builder.setMessage("This will toast " + countToFry + " records from the database " +
                "irreversibly!");

        //buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                wipeRecords(calDT, fCountToFry, fSubNdx);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    /**
     * Method handles the nitty gritty of deleting the records applicable to
     * the criteria specified by the user.
     *
     * @param purgePriorToDate (long) date to purge records prior to
     * @param fCountToFry count of records to be wiped
     * @param fSubNdx substance index if only working with 1 substance
     * @return
     */
    private boolean wipeRecords(final long purgePriorToDate, final int fCountToFry,
                                final int fSubNdx) {
        progressBar.setKeepScreenOn(true);
        progressBar.setEnabled(true);
        progressBar.setVisibility(ProgressBar.VISIBLE);

        if (fCountToFry > 0) {
            progressBar.setProgress(0, true);
        } else {
            Toast.makeText(DatabaseMaint.this, "No records to fry!", Toast.LENGTH_LONG).show();

            return false;
        }

        List<Usage> applicableRecords;
        try {
            if (fSubNdx != -1) {
                applicableRecords =
                        Permanence.Admins.getPriorToUsageRecords(fSubNdx, purgePriorToDate);
            } else {
                applicableRecords =
                        Permanence.Admins.getPriorToUsageRecordsFromAll(purgePriorToDate);
            }
        } catch (Exception ex) {
            Toast.makeText(DatabaseMaint.this, "Unable to obtain records: " + ex.toString(),
                    Toast.LENGTH_LONG).show();

            return false;
        }

        //now with threading!
        if (fSubNdx != -1) {
            final List<Usage> fApplicableRecords = applicableRecords;

            new Thread(new Runnable() {
                /**
                 * Method handles the call to doOneSubPrevRecordsWipe() and
                 * then updates the progressBar.
                 */
                public void run() {
                    //performing operation
                    final int fProgressStep = Math.round(fCountToFry / 10);

                    do {
                        progressBarStatus = doOneSubPrevRecordsWipe(fApplicableRecords,
                                fProgressStep, fCountToFry);

                        //updating the progress bar
                        progressBarHandler.post(new Runnable() {
                            public void run() {
                                progressBar.setProgress(progressBarStatus);
                                GlobalMisc.debugMsg("progressBarHandler.post()",
                                        "progressBar updated to: " + progressBarStatus);
                            }
                        });
                    } while (progressBarStatus < 100);

                    //doCleanUp();
                }

                /**
                 * Method handles the wiping of one substance's entries prior to the passed
                 * timestamp.
                 *
                 * @param fApplicableRecords List of records to be wiped
                 * @param fProgressStep how many records to wipe per progressBar update
                 * @param fCountToFry total number of entries to toast
                 * @return new progressBar's percentage complete
                 */
                public int doOneSubPrevRecordsWipe(final List<Usage> fApplicableRecords,
                                                   final int fProgressStep, final int fCountToFry) {
                    int newProgressStatus = 100;

                    while (records < fApplicableRecords.size()) {
                        try {
                            if (!GlobalMisc.Debugging) {
                                records += Permanence.Admins.wipeUsageWithResultRows(
                                        fApplicableRecords.get(records));
                            } else {
                                records++;
                            }
                        } catch (Exception ex) {
                            Toast.makeText(DatabaseMaint.this, "Unable to wipe records: " +
                                    ex.toString(), Toast.LENGTH_LONG).show();
                        }

                        if ((records % fProgressStep) == 0) {
                            newProgressStatus = Math.round((float)records / (float)fCountToFry * 100.0f);
                            GlobalMisc.debugMsg("wipeRecords.Runnable.run()",
                                    "Returning: " + newProgressStatus + "\n" +
                                            "Records: " + records +
                                            " (records / fCountToFry * 100): " +
                                            Integer.toString(
                                                    Math.round(
                                                            (float)records / (float)fCountToFry *
                                                                    100.0f)));

                            return newProgressStatus;
                        }

                        if (GlobalMisc.Debugging) {
                            try {
                                Thread.sleep(100);
                            } catch (Exception ex) {
                                GlobalMisc.debugMsg("doOneSubPrevRecordsWipe",
                                        "WTF, Thread.sleep() had an issue :-?(beep)");
                            }
                        }
                    }

                    return newProgressStatus;
                }
            }).start();
        } else {
            final long firstTimestamp = Permanence.Admins.getEarliestUsageTimestamp();
            final long fIntervalSize = purgePriorToDate - firstTimestamp;
            final long fStepSize = fIntervalSize / 10;

            new Thread(new Runnable() {
                /**
                 * Method handles the call to doAllSubsPrevRecordsWipe() and
                 * then updates the progressBar.
                 */
                public void run() {
                    //performing operation
                    long currentTimestamp = firstTimestamp;
                    long newTimestamp;

                    do {
                        newTimestamp = doAllSubsPrevRecordsWipe(currentTimestamp);
                        progressBarStatus = Math.round(((float)newTimestamp - (float)firstTimestamp)
                                / (float)fIntervalSize * 100.0f);
                        currentTimestamp = newTimestamp;

                        //updating the progress bar
                        progressBarHandler.post(new Runnable() {
                            public void run() {
                                progressBar.setProgress(progressBarStatus);
                                GlobalMisc.debugMsg("progressBarHandler.post()",
                                        "progressBar updated to: " + progressBarStatus);
                            }
                        });
                    } while (newTimestamp <= purgePriorToDate);
                }

                /**
                 * Handles wiping all of the entries prior to the passed timestamp.
                 *
                 * @param fCurrentTimestamp timestamp to wipe
                 * @return next timestamp that would be used in a loop with this method
                 */
                public long doAllSubsPrevRecordsWipe(final long fCurrentTimestamp) {
                    int cntr3;

                    try {
                        if (!GlobalMisc.Debugging) {
                            cntr3 = Permanence.Admins.wipePriorToTimestamp(fCurrentTimestamp);
                        } else {
                            cntr3 = Permanence.Admins.countPriorToTimestamp(fCurrentTimestamp);
                        }
                    } catch (Exception ex) {
                        Toast.makeText(DatabaseMaint.this,
                                "Unable to wipe records: " + ex.toString(),
                                Toast.LENGTH_LONG).show();

                        return -1;
                    }

                    GlobalMisc.debugMsg("wipeRecords",
                            "Wiped " + cntr3 + "; wiped prior to " +
                                    Converters.toLocalDateTime(fCurrentTimestamp).toString());

                    if (GlobalMisc.Debugging) {
                        try {
                            Thread.sleep(100);
                        } catch (Exception ex) {
                            GlobalMisc.debugMsg("wipeRecords",
                                    "sleep() gave us " + ex.toString());
                        }
                    }

                    if (GlobalMisc.Debugging) {
                        try {
                            Thread.sleep(100);
                        } catch (Exception ex) {
                            GlobalMisc.debugMsg("doAllSubsPrevRecordsWipe",
                                    "WTF, Thread.sleep() had an issue :-?(beep)");
                        }
                    }
                    return fCurrentTimestamp + fStepSize;
                }
            }).start();
        }

        doCleanUp();

        return true;
    }

    /**
     * Method handles resetting the controls of the activity.
     */
    private void doCleanUp() {
        rgApplicableSubs.clearCheck();
        Switch swtAllEntries = findViewById(R.id.swtPurgeAllEntries);
        swtAllEntries.setChecked(false);

        //progressbar shit
        progressBar.setProgress(0, false);
        progressBar.setEnabled(false);

        records = 0;
        progressBarStatus = 0;
    }

    /**
     * Method handles opening the new intent for dealing with a database
     * consolidation request.
     *
     * @param v requiem for an onClick handler
     */
    public void consolidateDatabase(View v) {
        if (rgApplicableSubs.getCheckedRadioButtonId() != -1) {
            Intent intent = new Intent(DatabaseMaint.this, ConsolidateDatabase.class);
            intent.putExtra("SUB_NDX", SubData.subList.get(
                    rgApplicableSubs.indexOfChild(findViewById(
                            rgApplicableSubs.getCheckedRadioButtonId()))).getId());
            startActivity(intent);
        } else {
            Toast.makeText(DatabaseMaint.this, "You must first select a substance " +
                    "to consolidate entries for!", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Method handles opening the new intent for dealing with a database
     * export request.
     *
     * @param v requiem for an onClick handler
     */
    public void exportDatabase(View v) {
        Intent intent = new Intent(DatabaseMaint.this, ExportDatabase.class);
        startActivity(intent);
    }

    public void importDatabase(View v) {
        Intent intent = new Intent(DatabaseMaint.this, ImportDatabase.class);
        startActivity(intent);
    }

    /**
     * Click handler for the 'Purge Invalid' button in the activity handles
     * checking all entries to make sure they are mapped to valid substances.
     * In cases where a usage/administration is mapped to a substance which
     * no longer exists (a problem that I will have to track down the source
     * of, as I thought the cascading database checks would've prevented it)
     * the entries are purged.
     *
     * This is now going to handle purging invalid cross reference records,
     * as well.  Going to modularize that portion of things; what's here will
     * have to be modularized as well at some point (maybe now).
     *
     * @param v View
     */
    public void onPurgeInvalidEntries(View v) {
        purgeInvalidUsageEntries();
        purgeInvalidUSCrossRefEntries();
    }

    /**
     * Purges invalid UsualSuspectSubsCrossRef entries.
     */
    private void purgeInvalidUSCrossRefEntries() {
        int purgeCount = 0;
        int failedPurges = 0;

        List<UsualSuspectSubsCrossRef> crossRefs = Permanence.CrossRef.loadCrossRefs();

        for (UsualSuspectSubsCrossRef crossRef : crossRefs) {
            try {
                GlobalMisc.debugMsg("purgeInvalidUSCrossRefEntries",
                        Permanence.US.loadUSByID(crossRef.getUsualSuspect()).getName());
            } catch (Exception ex) {
                //we're assuming that the exception is for an invalid substance ID at this point,
                //which is a bogus assumption to be making.  fix this at some point.
                try {
                    Permanence.CrossRef.deleteCrossRef(crossRef);
                } catch (Exception ex2) {
                    failedPurges++;
                }

                purgeCount++;
            }
        }

        showPurgeResults(purgeCount, failedPurges);
    }

    /**
     * Purges invalid usage entries.
     */
    private void purgeInvalidUsageEntries() {
        int purgeCount = 0;
        int failedPurges = 0;

        //make a list of valid sub ids for more rapid accessing instead of digging through subData
        List<Integer> sub_ids = new ArrayList<>();

        for (Substance sub : SubData.subList) {
            sub_ids.add(sub.getId());
        }

        for (Usage admin : Permanence.Admins.loadUsages()) {
            if (!sub_ids.contains(admin.getSub_id())) {
                purgeCount++;

                try {
                    Permanence.Admins.wipeUsage(admin);
                } catch (Exception ex) {
                    failedPurges++;
                }
            }
        }

        showPurgeResults(purgeCount, failedPurges);
    }

    /**
     * Method shows results of the database purge.
     *
     * @param purged number of records that were to be purged
     * @param failed number of records that were unable to be purged
     */
    private void showPurgeResults(int purged, int failed) {
        if (failed > 0) {
            GlobalMisc.showSimpleDialog(this, "Some purges failed",
                    "Out of " + purged + " records marked for purge, " +
                            failed + " failed and " + (purged - failed) +
                            "succeeded.");
        } else if (purged > 0) {
            GlobalMisc.showSimpleDialog(this, "Purges Successful",
                    "Purged " + purged + " invalid records.");
        } else {
            //purgeCount should be 0 here
            GlobalMisc.showSimpleDialog(this, "No purging necessary",
                    "Zero records were marked as invalid for purging.");
        }
    }
}
