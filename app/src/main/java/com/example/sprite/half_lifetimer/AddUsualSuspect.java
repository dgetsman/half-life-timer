package com.example.sprite.half_lifetimer;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static com.example.sprite.half_lifetimer.SubData.subList;

public class AddUsualSuspect extends AppCompatActivity
        implements AdapterView.OnItemSelectedListener {
    List<Substance> proposedSubs = new ArrayList<>();
    List<Float> proposedDosages = new ArrayList<>();
    int currentSubPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_usual_suspect);

        Spinner spnSubstanceNames = findViewById(R.id.spnSubstancesForUsualSuspect);
        String[] substanceNames = new String[subList.size()];

        for (int cntr = 0; cntr < subList.size(); cntr++) {
            substanceNames[cntr] = subList.get(cntr).getCommon_name();
        }

        ArrayAdapter<String> subAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, substanceNames);
        subAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnSubstanceNames.setAdapter(subAdapter);
        spnSubstanceNames.setOnItemSelectedListener(this);
    }

    /**
     * Handles listening for when the spinner has an item selected.
     *
     * @param parent AdapterView
     * @param view View
     * @param position int
     * @param id long
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        TextView tvwSubstanceDosagePrompt = findViewById(R.id.tvwUsualSuspectSubDosage);

        tvwSubstanceDosagePrompt.setText(getString(R.string.add_usual_suspect_substance_dosage,
                subList.get(position).getCommon_name(),
                GlobalMisc.dUnitsToString(subList.get(position).getUnits())));
        currentSubPosition = position;
    }

    /**
     * Requiem for OnItemSelectedListener.
     *
     * @param parent AdapterView
     */
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //ouah
    }

    /**
     * Handles handing off validation of separate form areas to the applicable routines and
     * notifyin the users if there are any issues.  If there are not, the UsualSuspect and its
     * cross-references are saved and the status line is updated with the message about it.
     *
     * @param v View
     */
    public void onCommitClick(View v) {
        EditText edtUsualSuspectName = findViewById(R.id.edtUsualSuspectName);
        EditText edtUsualSuspectNotes = findViewById(R.id.edtUsualSuspectNotes);
        TextView tvwStatusLine = findViewById(R.id.tvwStatusLine);

        if (!validateName()) {
            GlobalMisc.showSimpleDialog(AddUsualSuspect.this,
                    "Usual Suspect Already Exists",
                    "A Usual Suspect with that name already exists!");

            return;
        }

        if (!validateSubs()) {
            GlobalMisc.showSimpleDialog(AddUsualSuspect.this,
                    "Add Substances & Dosages First",
                    "You need to add substances and dosages for this Usual Suspect, first!");

            return;
        }

        //let's save the UsualSuspect
        UsualSuspect newSuspect = new UsualSuspect(edtUsualSuspectName.getText().toString().trim(),
                edtUsualSuspectNotes.getText().toString().trim());
        try {
            Permanence.US.saveUS(newSuspect);
        } catch (Exception ex) {
            tvwStatusLine.setText(getResources().getString(R.string.unable_to_save,
                    ex.toString()));
            tvwStatusLine.setTextColor(getResources().getColor(R.color.colorDarkRed));

            return;
        }

        //handle the cross references
        for (int cntr = 0; cntr < proposedSubs.size(); cntr++) {
            UsualSuspectSubsCrossRef crossRef =
                    new UsualSuspectSubsCrossRef(
                            Permanence.US.loadUSByName(newSuspect.getName()).getId(),
                            proposedSubs.get(cntr).getId(), proposedDosages.get(cntr));

            try {
                Permanence.CrossRef.saveCrossRef(crossRef);
            } catch (Exception ex) {
                //this could leave the database with dangling crossreferences and should be fixed
                tvwStatusLine.setText(getResources().getString(R.string.unable_to_save,
                        ex.toString()));
                tvwStatusLine.setTextColor(getResources().getColor(R.color.colorDarkRed));

                return;
            }
        }

        //if we made it this far with no errors we're done
        tvwStatusLine.setText(getResources().getString(R.string.usual_suspect_commit_successful));
        tvwStatusLine.setTextColor(getResources().getColor(R.color.colorDarkGreen));

        wipeEverythingDown();
    }

    /**
     * This method handles committing one substsance and its dosage into the lineup for the Usual
     * Suspect that the user is working on, to be saved later at full Usual Suspect committal.
     *
     * @param v View
     */
    public void onSubstanceCommitClick(View v) {
        EditText edtDosage = findViewById(R.id.edtUsualSuspectSubDosage);

        //first make sure we're not doubling up
        boolean alreadyThere = false;
        if (proposedSubs.size() > 0) {
            for (Substance oldSub : proposedSubs) {
                if (oldSub.getCommon_name().toLowerCase().contentEquals(
                        subList.get(currentSubPosition).getCommon_name().toLowerCase())) {
                    alreadyThere = true;
                    break;
                }
            }

            if (alreadyThere) {
                GlobalMisc.showSimpleDialog(AddUsualSuspect.this,
                        "Substance Already Exists Here",
                        "The usual suspect that you are creating already has an entry " +
                        "for " + subList.get(currentSubPosition).getCommon_name() + "!");

                edtDosage.setText("");

                return;
            }
        }

        float dosage;
        try {
            dosage = Float.parseFloat(edtDosage.getText().toString());
        } catch (Exception ex) {
            GlobalMisc.showSimpleDialog(AddUsualSuspect.this,
                    "Unable to parse dosage",
                    "Not sure what happened, but I'm unable to parse your dosage, please" +
                    " try again!");

            return;
        }

        if (!validateDosage(dosage)) {
            GlobalMisc.showSimpleDialog(AddUsualSuspect.this,
                    "Invalid dosage",
                    "Please enter a dosage between " + GlobalMisc.DoseRangeMin + " and " +
                    GlobalMisc.DoseRangeMax + "!");

            return;
        }

        //we should be good to go to add it to the list now
        proposedSubs.add(subList.get(currentSubPosition));
        proposedDosages.add(dosage);
        edtDosage.setText("");

        Toast.makeText(AddUsualSuspect.this, "Added " +
                subList.get(currentSubPosition).getCommon_name() + " to the lineup for this " +
                "Usual Suspect.", Toast.LENGTH_LONG).show();
    }

    /**
     * Method makes certain that the name for the UsualSuspect wasn't already taken.
     *
     * @return boolean true for good to proceed, false for duplicated name
     */
    private boolean validateName() {
        EditText edtName = findViewById(R.id.edtUsualSuspectName);
        String proposedName = edtName.getText().toString().trim();

        for (UsualSuspect uSuspect : Permanence.US.loadAllUS()) {
            if (proposedName.toLowerCase().contentEquals(uSuspect.getName().toLowerCase())) {
                return false;
            }
        }

        return true;
    }

    /**
     * Method makes certain that the dosage falls within the appropriate range of limitation that
     * we have set in GlobalMisc.
     *
     * @param dosage float dosage to be tested
     * @return boolean true for within parameters, false for out of bounds
     */
    private boolean validateDosage(float dosage) {
        return ((dosage < GlobalMisc.DoseRangeMax) && (dosage > GlobalMisc.DoseRangeMin));
    }

    /**
     * Method at this point simply makes sure that one substance has at least been set and that it
     * has a dosage set with it.
     *
     * @return true for properly set, false for no
     */
    private boolean validateSubs() {
        return ((proposedSubs.size() > 0) && (proposedDosages.size() > 0));
    }

    /**
     * Method just clears all of the previous data once a Usual Suspect has been entered and wipes
     * all of the EditText form elements down so that previous data isn't duplicated if another
     * UsualSuspect is to be added.
     */
    private void wipeEverythingDown() {
        proposedDosages = null;
        proposedSubs = null;
        currentSubPosition = -1;

        EditText edtName = findViewById(R.id.edtUsualSuspectName);
        EditText edtDosage = findViewById(R.id.edtUsualSuspectSubDosage);
        EditText edtUsualSuspectNotes = findViewById(R.id.edtUsualSuspectNotes);
        edtName.setText("");
        edtDosage.setText("");
        edtUsualSuspectNotes.setText("");
    }
}