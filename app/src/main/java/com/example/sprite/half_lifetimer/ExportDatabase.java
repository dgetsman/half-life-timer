package com.example.sprite.half_lifetimer;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static androidx.core.content.FileProvider.getUriForFile;

/**
 * Class contains all of the methods necessary for exporting the database's contents to a pair of
 * JSON files (usages.json and substances.json) stored locally on the device for sharing to another
 * device/computer.  An Intent for sharing is then opened in order to facilitate getting the local
 * files off of this device and onto [presumably] a computer for import into LastTime or other
 * applications.
 */
public class ExportDatabase extends AppCompatActivity {
    String consolidatedExportFN = "consolidated_export.json";

    /**
     * Method handles calling updateDisplay() primarily.
     *
     * @param savedInstanceState requiem for onCreate
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_export_database);

        ProgressBar progressBar = findViewById(R.id.progressBar2);
        progressBar.setKeepScreenOn(false);
        progressBar.setEnabled(false);
        progressBar.setVisibility(ProgressBar.VISIBLE);

        updateDisplay();
    }

    /**
     * Removes previous components to the layout in case viewing options are being toggled, then
     * replaces them with the appropriate content.
     */
    private void updateDisplay() {
        final LinearLayout lloJSONtxt = findViewById(R.id.lloDBDump);
        final LinearLayout lloDumpStats = findViewById(R.id.lloDumpStats);
        final TextView tvwJSON = new TextView(this);
        final TextView tvwDumpStats = new TextView(this);

        Toast.makeText(ExportDatabase.this, "Compiling database entries, please wait . . .",
                Toast.LENGTH_LONG).show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                String json = generateJSON();

                tvwJSON.setText(json);
                final String fJson = json;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        lloJSONtxt.removeAllViewsInLayout();
                        lloJSONtxt.addView(tvwJSON);

                        lloDumpStats.removeAllViewsInLayout();
                        tvwDumpStats.setText(getResources().getString(R.string.total_json_size,
                                fJson.length()));
                        lloDumpStats.addView(tvwDumpStats);
                    }
                });
            }
        }).start();
    }

    /**
     * Handles the generation of JSON for both all of the usages and all of the substances in the
     * database.  After writing them to the previous defined location for the files, it also returns
     * the content (along with titles to separate the JSON blobs) in the form of a String to be
     * displayed by other components here.
     *
     * @return String containing titles for as well as content of the JSON blobs
     */
    private String generateJSON() {
        TransferData consolidatedExportData = new TransferData(ExportDatabase.this);

        //this is why it breaks; if we had the data structure organized differently I think things
        //would be just fine; maybe something to keep in mind for 2.0
        String consolidatedExportDataJSON = consolidatedExportData.generateJson();

        try {
            File filesDirPath = new File(getFilesDir(), "files");
            filesDirPath.mkdirs();

            File consolidatedExportDataFile = new File(filesDirPath, consolidatedExportFN);

            FileOutputStream consolidatedExportDataFileIS =
                    new FileOutputStream(consolidatedExportDataFile);

            OutputStreamWriter consolidatedExportDataOSW =
                    new OutputStreamWriter(consolidatedExportDataFileIS);

            Writer consolidatedExportDataW = new BufferedWriter(consolidatedExportDataOSW);

            consolidatedExportDataW.write(consolidatedExportDataJSON);

            consolidatedExportDataW.close();
        } catch (FileNotFoundException ex) {
            GlobalMisc.showSimpleDialog(this, "Exception", "File not found " +
                    "exception!\n" + ex.toString());
        } catch (IOException ex) {
            GlobalMisc.showSimpleDialog(this, "Exception", "Input/Output " +
                    "exception!\n" + ex.toString());
        } catch (Exception ex) {
            GlobalMisc.debugMsg("generateJSON", "Issue creating files dir, " +
                    "or something else unforeseen: " + ex.toString());
        }

        return "Consolidated Export Data JSON\n\n" + consolidatedExportDataJSON;
    }

    /**
     * Handles setting up the intents for being a FileProvider of the aforementioned JSON files.
     *
     * @param v requiem for an onClick handler
     */
    public void shareJSON(View v) {
        File filesDirPath = new File(getFilesDir(), "files");

        File consolidatedExportDataFile = new File(filesDirPath, consolidatedExportFN);

        Uri consolidatedExportDataUri = getUriForFile(getApplicationContext(),
                "com.example.sprite.fileprovider", consolidatedExportDataFile);

        Intent intentShareJSON = new Intent(Intent.ACTION_SEND);
        intentShareJSON.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intentShareJSON.setType("text/json");
        intentShareJSON.putExtra(Intent.EXTRA_STREAM, consolidatedExportDataUri);

        startActivity(Intent.createChooser(intentShareJSON, "Share Consolidated Export Data" +
                " JSON"));
    }

    //the following is all class-level crap for the bluetooth sharing capabilities; there's
    //certainly a much tidier way to do this...
    private static final UUID MY_UUID = UUID.fromString("633cc67a-6f6f-4ed1-9d83-6c9044eaeb6a");

    private static final int REQUEST_ENABLE_BT = 1;
    BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    ConnectedThread mConnectedThread;

    String TAG = "ExportDatabase";

    /**
     * This is the onClick handler for Bluetooth Export.  It handles setting up a thread to accept
     * the connection, pops open a completely unnecessary thread for watching for anything coming
     * back from the other side of the link, then calls SendMessage() to transmit the contents of
     * jsonBuffer to the other device.  Afterwards it cancels the connected thread, thus terminating
     * the link.
     *
     * @param v View: requiem for an onClick handler
     */
    public void bluetoothShareJSON(View v) {
        if (bluetoothAdapter != null && !bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent =
                    new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        new Thread(new Runnable() {
            public void run() {
                AcceptThread accept = new AcceptThread();
                accept.start();
            }
        }).start();

        File filesDirPath = new File(getFilesDir(), "files");

        File consolidatedExportDataFile = new File(filesDirPath, consolidatedExportFN);
        FileInputStream fis;
        try {
             fis = new FileInputStream(consolidatedExportDataFile);
        } catch (FileNotFoundException fnfex) {
            GlobalMisc.showSimpleDialog(ExportDatabase.this, "JSON Not Found",
                    "The JSON file was not found: " + fnfex.toString());

            return;
        }

        String jsonBuffer = "";

        int intChar = 0;
        try {
            while ((intChar = fis.read()) != -1) {
                jsonBuffer = jsonBuffer + (char)intChar;
            }
        } catch (Exception ex) {
            GlobalMisc.showSimpleDialog(ExportDatabase.this, "File Reading Issue",
                    "Issue reading file: " + ex.toString());

            return;
        }

        //Log.d("ExportDatabase.bluetoothShareJSON", jsonBuffer);
        SendMessage(jsonBuffer);

        try {
            fis.close();
        } catch (Exception ex) {
            GlobalMisc.showSimpleDialog(ExportDatabase.this, "File Closing Issue",
                    "Issue closing file: " + ex.toString());
        }

        //mConnectedThread.cancel();
    }

    /**
     * Method starts the thread to manage the connection (mmConnectedThread), which performs
     * the data transmission.
     *
     * @param mmSocket BluetoothSocket
     */
    private void connected(BluetoothSocket mmSocket) {
        Log.d(TAG, "connected: Starting.");

        // Start the thread to manage the connection and perform transmissions
        mConnectedThread = new ConnectedThread(mmSocket);
        mConnectedThread.start();
    }

    /**
     * Class holds the components for ConnectedThread for managing the connection and performing
     * data transmission.
     */
    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            Log.d(TAG, "ConnectedThread: Starting.");

            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                tmpIn = mmSocket.getInputStream();
                tmpOut = mmSocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        /**
         * Method would handle receiving transmission data, if it were not gutted.  There doesn't
         * even need to be this much crap in there any more, it's all a waste, unless we move to
         * some more bidirectional chat protocol.
         */
        public void run() {
            byte[] buffer = new byte[1024];  // buffer store for the stream

            int bytes; // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs
            while (true) {
                // Read from the InputStream
                try {
                    bytes = mmInStream.read(buffer);
                    final String incomingMessage = new String(buffer, 0, bytes);
                    Log.d(TAG, "InputStream: " + incomingMessage);
                } catch (IOException e) {
                    Log.e(TAG, "write: Error reading Input Stream. " + e.getMessage());

                    break;
                }
            }
        }

        /**
         * Method writes to the mmOutStream, transmitting the data to the other device.
         *
         * @param bytes byte[] array of bytes to transmit
         */
        public void write(final byte[] bytes) {
            String text = new String(bytes, Charset.defaultCharset());
            Log.d(TAG, "write: Writing to outputstream: " + text);
            final ProgressBar progressBar = findViewById(R.id.progressBar2);

            progressBar.setKeepScreenOn(true);
            progressBar.setEnabled(true);
            progressBar.setVisibility(ProgressBar.VISIBLE);
            final Handler progressBarHandler = new Handler();
            progressBar.setProgress(0, true);

            new Thread(new Runnable() {
                public void run() {
                    int chunkSize = (bytes.length / 10);
                    int iteration = 0;

                    try {
                        for (int bytesSent = 0; bytesSent < bytes.length; bytesSent += chunkSize) {
                            if ((bytesSent + chunkSize) < bytes.length) {
                                mmOutStream.write(bytes, bytesSent, chunkSize);
                            } else {
                                mmOutStream.write(bytes, bytesSent, (bytes.length - bytesSent));
                            }

                            final int fIteration = iteration;
                            //updating the progress bar
                            progressBarHandler.post(new Runnable() {
                                public void run() {
                                    progressBar.setProgress(fIteration);
                                }
                            });

                            iteration += 10;
                            Log.d("ExportDatabase.ConnectedThread.write", "written: " +
                                    bytesSent + "\t" + iteration + "%");

                            //Thread.sleep(50);
                        }

                        cancel();
                    } catch (IOException /*| InterruptedException*/ e) {
                        Log.e(TAG, "write: Error writing to output stream. " + e.getMessage());
                    }
                }
            }).start();
        }

        /**
         * Call this from the main activity in order to shut down the link.
         */
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                //ouah
            }
        }
    }

    /**
     * Method handles transforming a String message into a byte array suitable for transmission and
     * then calls mConnectedThread.write() in order to transmit it.
     *
     * @param message String
     */
    private void SendMessage(String message) {
        byte[] bytes = message.getBytes(Charset.defaultCharset());
        mConnectedThread.write(bytes);
    }

    /**
     * Class handles creating a listening server socket for the link.
     */
    private class AcceptThread extends Thread {
        // The local server socket
        private final BluetoothServerSocket mmServerSocket;

        public AcceptThread(){
            BluetoothServerSocket tmp = null;

            // Create a new listening server socket
            try {
                tmp = bluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord("hlt",
                        MY_UUID);

                Log.d(TAG, "AcceptThread: Setting up Server using: " + MY_UUID);
            } catch (IOException e){
                Log.e(TAG, "AcceptThread: IOException: " + e.getMessage() );
            }

            mmServerSocket = tmp;
        }

        /**
         * Method only returns on a successful connection or exception status.
         */
        public void run() {
            Log.d(TAG, "run: AcceptThread Running.");

            BluetoothSocket socket = null;

            try{
                // This is a blocking call and will only return on a
                // successful connection or an exception
                Log.d(TAG, "run: RFCOM server socket start.....");

                socket = mmServerSocket.accept();

                Log.d(TAG, "run: RFCOM server socket accepted connection.");

            } catch (IOException e){
                Log.e(TAG, "AcceptThread: IOException: " + e.getMessage());
            }

            //talk about this is in the 3rd
            if (socket != null) {
                connected(socket);
            }

            Log.i(TAG, "END mAcceptThread ");
        }

        /**
         * Method cancels AcceptThread, closing the socket at the end.
         */
        public void cancel() {
            Log.d(TAG, "cancel: Canceling AcceptThread.");

            try {
                mmServerSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "cancel: Close of AcceptThread ServerSocket failed. " +
                        e.getMessage());
            }
        }
    }
}
