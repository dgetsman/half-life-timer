package com.example.sprite.half_lifetimer;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@Entity
public class SubstanceGoal {
    @PrimaryKey(autoGenerate = true)
    private int     id;
    @ColumnInfo(name="sub_id")
    private int     subId;
    @ColumnInfo(name="target_units_per_day")
    private float   unitsPerDay;

    public SubstanceGoal() { }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSubId() {
        return subId;
    }

    public void setSubId(int subId) {
        this.subId = subId;
    }

    public float getUnitsPerDay() {
        return unitsPerDay;
    }

    public void setUnitsPerDay(float unitsPerDay) {
        this.unitsPerDay = unitsPerDay;
    }

    public DosageUnit getUnits() {
        return Permanence.Subs.getUnitsBySID(this.subId);
    }

    public String toString() {
        return Permanence.Subs.loadSubstanceById(this.subId).getCommon_name() + " limit to: " +
                this.unitsPerDay + Permanence.Subs.getUnitsBySID(this.subId) + "/day";
    }

    /**
     * Method will pull the administrations of this substance since midnight, tally up the total
     * amount that's been administered in that amount of time, and determine what percentage of the
     * day's goal has already been consumed.  Percentage of the day's goal consumed is returned.
     *
     * @return float percentage of the day's goal administered already
     */
    public float getPercentageOfDaysGoal() {
        float tmpTotalDosage = 0.0f;

        LocalTime midnight = LocalTime.MIDNIGHT;
        LocalDate today = LocalDate.now();
        LocalDateTime todayMidnight = LocalDateTime.of(today, midnight);

        List<Usage> todaysUses = Permanence.Admins.getPostToTSUsageRecords(this.subId,
                Converters.fromLocalDateTime(todayMidnight));

        for (Usage use : todaysUses) {
            tmpTotalDosage += use.getDosage();
        }

        return tmpTotalDosage / Permanence.SG.loadSubGoalBySID(this.getSubId()).getUnitsPerDay();
    }

    /**
     * Method will pull the administrations of this substance since midnight, tally up the total
     * amount that's been administered in that amount of time, along with whatever dosage is to be
     * added, and determine what percentage of the day's goal is going to be consumed at this point.
     * Percentage of the day's goal consumed is returned.
     *
     * @param dosageAddition amount considered to be administered
     * @return float percentage of the day's goal that would be administered
     */
    public float getPercentageOfDaysGoal(float dosageAddition) {
        float tmpTotalDosage = dosageAddition;

        LocalTime midnight = LocalTime.MIDNIGHT;
        LocalDate today = LocalDate.now();
        LocalDateTime todayMidnight = LocalDateTime.of(today, midnight);

        List<Usage> todaysUses = Permanence.Admins.getPostToTSUsageRecords(this.subId,
                Converters.fromLocalDateTime(todayMidnight));

        for (Usage use : todaysUses) {
            tmpTotalDosage += use.getDosage();
        }

        return tmpTotalDosage / Permanence.SG.loadSubGoalBySID(this.getSubId()).getUnitsPerDay();
    }
}
