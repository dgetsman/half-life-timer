package com.example.sprite.half_lifetimer;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Our poorly named primary display.
 */
public class SubData extends AppCompatActivity {
    public static List<Substance> subList = new ArrayList<>();    //ouah

    private String m_Text = "";
    Substance tmpSub = new Substance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_data);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fabAddSub = findViewById(R.id.fabAddSub);
        fabAddSub.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent addSubIntent = new Intent(SubData.this, AddSubstance.class);
                startActivity(addSubIntent);
            }
        });

        subList = Permanence.Subs.loadUnarchivedSubstances(getApplicationContext());
        updateDisplay();
    }

    /**
     * Method handles repopulating the display after the user switches back to it from another
     * activity or app or whatever.  Without this the colors don't update properly.
     *
     * @param hasFocus whether or not the window is now in focus
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus) {
            updateDisplay(/*Permanence.Subs.loadUnarchivedSubstances(getApplicationContext())*/);
        }
    }

    /**
     * Method handles refreshing the window when a user returns to this parent activity from
     * another.  onWindowFocusChange() doesn't handle that, at least when you use the back arrow
     * to return, I guess.
     */
    @Override
    public void onRestart() {
        super.onRestart();
        updateDisplay();
    }

    /**
     * I'm afraid I'm not really clear on what, exactly, this method is used for.  :|
     * Well, other than inflating the menu, of course.
     *
     * @param menu Menu defined in menu_sub_data.xml
     * @return boolean
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // so I believe this is where we are to start adding substances to the data base
        getMenuInflater().inflate(R.menu.menu_sub_data, menu);
        return true;
    }

    /**
     * Method handles toggling whether or not some of the debugging options are visible within the
     * options menu depending on the status of GlobalMisc.Debugging.
     *
     * @param menu the ereh menu
     * @return boolean not sure what this is good for...
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (GlobalMisc.Debugging) {
            menu.findItem(R.id.admin_dump).setEnabled(true)
                    .setVisible(true);
            menu.findItem(R.id.cx_dump).setEnabled(true)
                    .setVisible(true);
        }

        return true;
    }

    /**
     * Method handles creating the intent and opening the view for whatever MenuItem is selected.
     *
     * @param item MenuItem selected
     * @return true if an existing activity is selected, and I'm unclear on if/when false
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent = null;

        if (id == R.id.usual_suspects) {
            intent = new Intent(SubData.this, UsualSuspects.class);
        } else if (id == R.id.ranked_by_last) {
            intent = new Intent(SubData.this, SubsRankedByLastUsage.class);
        } else if (id == R.id.admin_dump && GlobalMisc.Debugging) {
            intent = new Intent(SubData.this, FullAdminsDump.class);
        } else if (id == R.id.sub_class) {
            intent = new Intent(SubData.this, SubClass.class);
        } else if (id == R.id.database_maint) {
            intent = new Intent(SubData.this, DatabaseMaint.class);
        } else if (id == R.id.goals) {
            intent = new Intent(SubData.this, SubstanceGoals.class);
        } else if (id == R.id.cx_dump && GlobalMisc.Debugging) {
            intent = new Intent(SubData.this, FullCrossRefsDump.class);
        } else if (id == R.id.tapering) {
            intent = new Intent(SubData.this, Tapering.class);
        } else if (id == R.id.search) {
            intent = new Intent(SubData.this, SubIndependentNotesSearch.class);
        } else if (id == R.id.period_details) {
            intent = new Intent(SubData.this, PeriodDetails.class);
        } else if (id == R.id.archival) {
            intent = new Intent(SubData.this, ManageArchived.class);
        }

        if (intent != null) {
            startActivity(intent);

            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Handles updating the display of available substances and sets up the
     * various click handlers for each entry.
     */
    private void updateDisplay() {
        Context context = getBaseContext();
        LinearLayout ll = this.findViewById(R.id.lloSubList);
        subList = Permanence.Subs.loadUnarchivedSubstances(getApplicationContext());
        final List<Substance> localSubList = subList;
        TextView[] subsTextView = new TextView[subList.size()];

        ll.removeAllViewsInLayout();

        //add the substances
        for (int cntr = 0; cntr < subList.size(); cntr++) {
            SubstanceGoal goal = null;
            try {
                goal = Permanence.SG.loadSubGoalBySID(subList.get(cntr).getId());
            } catch (Exception ex) {
                //some testing should probably be made here to make sure we're catching the right
                //kinds of exceptions :P
                GlobalMisc.debugMsg("updateDisplay", "goal exception: " +
                        ex.toString());
            }

            subsTextView[cntr] = new TextView(context);

            //before we start working with Taper data we really need to make sure that we're only
            //looking at the valid ones here
            GlobalMisc.markInvalidTapers(getApplicationContext());

            Taper taper = Permanence.Tapers.loadOneValidTaperBySid(subList.get(cntr).getId());
            if (goal != null) {
                String subTextViewLine;
                String percentageString = "G: " +
                        new DecimalFormat("###.#").format(
                                goal.getPercentageOfDaysGoal() * 100);

                subTextViewLine = getResources().getString(R.string.sub_and_percentage,
                        subList.get(cntr).getCommon_name(),
                        /*goal.getPercentageOfDaysGoal() * 100*/ percentageString);

                if (taper != null) {
                    percentageString = new DecimalFormat("###.#").format(
                            taper.findTodaysDosageMax());
                    subTextViewLine += " T: " + percentageString + Permanence.Subs.getUnitsBySID(
                            subList.get(cntr).getId());
                    subTextViewLine += " DT: " + subList.get(cntr).getTotalDosageToday();
                }

                subsTextView[cntr].setText(subTextViewLine);
            } else {
                if (taper == null) {
                    subsTextView[cntr].setText(subList.get(cntr).getCommon_name());
                } else {
                    subsTextView[cntr].setText(getResources().getString(
                            R.string.sub_data_display_line_w_taper_no_goal,
                            subList.get(cntr).getCommon_name(),
                            new DecimalFormat("###.#").format(taper.findTodaysDosageMax()),
                            Permanence.Subs.getUnitsBySID(subList.get(cntr).getId())));
                }
            }
            subsTextView[cntr].setTag(cntr);    //holds the TextView & subList array position
            subsTextView[cntr].setTextSize(20);

            //do some color coding in case we're getting close to our daily goals on this substance
            if (goal != null) {
                float percentageAdminned = goal.getPercentageOfDaysGoal();

                if (percentageAdminned >= 1) {
                    subsTextView[cntr].setTextColor(Color.RED);
                } else if (percentageAdminned >= 0.75) {
                    subsTextView[cntr].setTextColor(
                            ContextCompat.getColor(SubData.this, R.color.colorWarnYellow));
                } else {
                    subsTextView[cntr].setTextColor(
                            ContextCompat.getColor(SubData.this, R.color.colorDarkGreen));
                }
            }

            final int fCntr = cntr;

            //implement our click handlers
            //record administration or see substance's statistics (new activity)
            subsTextView[cntr].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(SubData.this, AdminData.class);
                    intent.putExtra("SUB_NDX", fCntr);
                    startActivity(intent);
                }
            });

            //view deets, edit half-life data of, or delete substance
            subsTextView[cntr].setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    String[] choices = {
                            getResources().getString(R.string.view_details_title),
                            getResources().getString(R.string.edit_hlife_and_class_title),
                            getResources().getString(R.string.delete_entry_title),
                            getResources().getString(R.string.archive_entry)
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(SubData.this);
                    final View innerView = v;
                    final int pos = Integer.parseInt(innerView.getTag().toString());

                    builder.setTitle("View details, edit half-lives, or delete substance: '" +
                            localSubList.get(pos).getCommon_name() + "'?");
                    builder.setItems(choices, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0: //view details
                                    AlertDialog.Builder deetsBuilder =
                                            new AlertDialog.Builder(SubData.this);

                                    Substance sub = localSubList.get(pos);

                                    deetsBuilder.setTitle(sub.getCommon_name() + " Details");
                                    deetsBuilder.setMessage(sub.toString());
                                    deetsBuilder.setPositiveButton("OK",
                                            new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });

                                    deetsBuilder.show();
                                    break;

                                case 1: //edit half-life information
                                    changeDHalflife(pos);
                                    changeHalflife(pos);
                                    changeSClass(pos);

                                    break;
                                case 2:
                                    try {
                                        //wipe the deleted data
                                        GlobalMisc.showDialogGetConfirmation(SubData.this,
                                                "Delete Substance & Associated Usages?",
                                                "Are you sure you want to delete " +
                                                        localSubList.get(pos).getCommon_name() +
                                                        " and its associated administrations?",
                                                new GlobalMisc.OnDialogConfResultListener() {
                                                    @Override
                                                    public void onResult(boolean result) {
                                                        if (result) {
                                                            subList = Substance.wipeEntry(
                                                                    SubData.this, pos);
                                                        }
                                                    }
                                                });

                                    } catch (Exception ex) {
                                        Toast.makeText(SubData.this,
                                                "Issue deleting substance: " + ex,
                                                Toast.LENGTH_LONG).show();
                                        subList = Permanence.Subs.loadUnarchivedSubstances(
                                                getApplicationContext());
                                    }
                                    updateDisplay();

                                    break;
                                case 3: //archive substance
                                    Substance arcSub = localSubList.get(pos);
                                    arcSub.setArchived(true);

                                    try {
                                        //save our freshly archived substance
                                        Permanence.Subs.updateSub(arcSub);
                                    } catch (Exception ex) {
                                        Toast.makeText(SubData.this,
                                                "Issue updating substance: " + ex,
                                                Toast.LENGTH_LONG).show();
                                    }

                                    subList = Permanence.Subs.loadUnarchivedSubstances(
                                            getApplicationContext());

                                    updateDisplay();
                            }
                        }
                    });

                    builder.show();

                    return false;
                }
            });

            ll.addView(subsTextView[cntr]);
        }
    }

    /**
     * Method obtains the active half-life information from the user in hours, for the purpose of
     * changing this information in the current Substance record.
     *
     * @param position subList position of the substance in question
     */
    private void changeHalflife(int position) {
        final List<Substance> localSubList = subList;
        final int pos = position;

        //get the active half-life information
        AlertDialog.Builder builder = new AlertDialog.Builder(SubData.this);
        builder.setTitle(getResources().getString(R.string.add_sub_dhalflife));
        builder.setMessage(getResources().getString(R.string.add_sub_phalflife_message));

        final EditText input = new EditText(SubData.this);

        input.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
        input.setText(Float.toString(localSubList.get(pos).getHalflife()));
        builder.setView(input);

        //buttons
        builder.setPositiveButton(getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Substance sub = localSubList.get(pos);

                        try {
                            sub.setdHalflife(Float.parseFloat(input.getText().toString()));
                        } catch (Exception ex) {
                            Toast.makeText(SubData.this,
                                    "Unable to set half-life information!\n" +
                                    ex.toString(), Toast.LENGTH_LONG).show();

                            GlobalMisc.debugMsg("changeHalflife",
                                    "Unable to set halflife: " + ex.toString());

                            dialog.cancel();
                        }

                        Permanence.Subs.updateSub(sub);
                    }
                });
        builder.setNegativeButton(getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        builder.show();
    }

    /**
     * Method obtains the detectable metabolite's half-life information from the user in hours, for
     * the purpose of changing the information in the Substance record.
     *
     * @param position subList position of the substance in question
     */
    private void changeDHalflife(int position) {
        final int pos = position;
        final List<Substance> localSubList = subList;

        //get the detectable half-life information
        AlertDialog.Builder builder = new AlertDialog.Builder(SubData.this);
        builder.setTitle(getResources().getString(R.string.add_sub_phalflife));
        builder.setMessage(getResources().getString(R.string.add_sub_phalflife_message));

        final EditText input = new EditText(SubData.this);

        input.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
        input.setText(Float.toString(localSubList.get(pos).getdHalflife()));
        builder.setView(input);

        //buttons
        builder.setPositiveButton(getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Substance sub = localSubList.get(pos);

                        try {
                            sub.setHalflife(Float.parseFloat(input.getText().toString()));
                        } catch (Exception ex) {
                            Toast.makeText(SubData.this,
                                    "Unable to set half-life information!\n" +
                                    ex.toString(), Toast.LENGTH_LONG).show();

                            GlobalMisc.debugMsg("changeDHalflife",
                                    "Unable to set dHalflife: " + ex.toString());

                            dialog.cancel();
                        }

                        Permanence.Subs.updateSub(sub);
                    }
                });
        builder.setNegativeButton(getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        builder.show();
    }

    /**
     * Method allows reclassification of the substance.
     *
     * @param position subList position of the substance in question
     */
    private void changeSClass(int position) {
        final int pos = position;
        final List<Substance> localSubList = subList;

        //multiple-choice selection here
        final List<SubClassification> allClasses = Permanence.SC.loadAllSC();
        String[] subClasses = new String[allClasses.size() + 1];
        int cntr = 0;

        for (SubClassification subClass : allClasses) {
            subClasses[cntr++] = subClass.getName();
        }
        subClasses[cntr] = "no classification";

        final int fCntr = cntr;

        AlertDialog.Builder builder = new AlertDialog.Builder(SubData.this);
        builder.setTitle(getResources().getString(R.string.sub_class))
                .setItems(subClasses, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Substance sub = localSubList.get(pos);

                        if (which != fCntr) {
                            sub.setSClass(allClasses.get(which).getId());
                        } else {
                            sub.setSClass(-1);
                        }

                        Permanence.Subs.updateSub(sub);
                    }
                });

        builder.show();
    }
}
