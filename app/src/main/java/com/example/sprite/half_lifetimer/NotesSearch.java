package com.example.sprite.half_lifetimer;

import android.app.AlertDialog;
import android.content.DialogInterface;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

public class NotesSearch extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes_search);

        updateDisplay();
    }

    private void updateDisplay() {
        Toast.makeText(NotesSearch.this, "Searching notes . . .",Toast.LENGTH_LONG).show();

        new Thread(new Runnable() {

            @Override
            public void run() {
                final LinearLayout lloEntries = findViewById(R.id.lloSubstringMatches);
                final TextView tvwTitle = findViewById(R.id.tvwNotesSearchTitle);
                tvwTitle.setText(getString(R.string.search_for,
                        getIntent().getExtras().getString("SUBSTR")));
                tvwTitle.setTextSize(20);

                /*runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        lloEntries.removeAllViewsInLayout();
                        lloEntries.addView(tvwTitle);
                    }
                });*/

                List<Usage> hits = Permanence.Admins.getUsagesWithSubstring(
                        getIntent().getExtras().getInt("SUB_NDX"),
                        getIntent().getExtras().getString("SUBSTR")
                );
                final HashMap<Integer, String> fSubsList =
                        GlobalMisc.getSubIndexNNames(NotesSearch.this);

                TextView[] entriesTextView = new TextView[hits.size() * 2];

                int cntr = 0;
                for (Usage hit : hits) {
                    final Usage fHit = hit;

                    entriesTextView[cntr] = new TextView(NotesSearch.this);
                    entriesTextView[cntr + 1] = new TextView(NotesSearch.this);
                    entriesTextView[cntr].setText(hit.getTimestamp().toString());
                    entriesTextView[cntr + 1].setText(hit.getNotes());
                    entriesTextView[cntr].setTextSize(16);
                    entriesTextView[cntr + 1].setTextSize(15);

                    entriesTextView[cntr].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AlertDialog.Builder builder =
                                    new AlertDialog.Builder(NotesSearch.this);
                            builder.setTitle(getString(R.string.at_middle,
                                    fSubsList.get(fHit.getSub_id()),
                                    fHit.getTimestamp().toString()));
                            builder.setMessage("Admin #: " + fHit.getId() + "\n" +
                                    "Dosage: " + fHit.getDosage() + "\n" +
                                    "Notes: " + fHit.getNotes());
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                            builder.show();
                        }
                    });
                    entriesTextView[cntr + 1].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AlertDialog.Builder builder =
                                    new AlertDialog.Builder(NotesSearch.this);
                            builder.setTitle(getString(R.string.at_middle,
                                    fSubsList.get(fHit.getSub_id()),
                                    fHit.getTimestamp().toString()));
                            builder.setMessage("Admin #: " + fHit.getId() + "\n" +
                                    "Dosage: " + fHit.getDosage() + "\n" +
                                    "Notes: " + fHit.getNotes());
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                            builder.show();
                        }
                    });

                    final TextView entryTV1 = entriesTextView[cntr++];
                    final TextView entryTV2 = entriesTextView[cntr++];

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            lloEntries.addView(entryTV1);
                            lloEntries.addView(entryTV2);
                        }
                    });
                }
            }
        }).start();
    }
}
