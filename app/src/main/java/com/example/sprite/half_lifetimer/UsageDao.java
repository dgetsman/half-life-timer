package com.example.sprite.half_lifetimer;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface UsageDao {
    @Query("SELECT * FROM Usage")
    List<Usage> getAll();

    @Query("SELECT Usage.* FROM Usage INNER JOIN Substance WHERE Usage.sub_id LIKE Substance.id " +
            "AND Substance.archived LIKE 0")
    List<Usage> getAllUnarchived();

    @Query("SELECT * FROM Usage WHERE sub_id LIKE :sid")
    List<Usage> getBySid(int sid);

    @Query("SELECT * FROM Usage WHERE valid_entry LIKE 1 AND sub_id LIKE :sid")
    List<Usage> getValidBySid(int sid);

    //chronological order
    @Query("SELECT * FROM Usage WHERE valid_entry LIKE 1 AND sub_id LIKE :sid ORDER BY timestamp" +
            " DESC LIMIT 1")
    Usage getLastValidUsage(int sid);

    @Query("SELECT * FROM Usage WHERE id LIKE :id")
    Usage getById(int id);

    @Query("SELECT * FROM Usage WHERE sub_id LIKE :sid ORDER BY timestamp")
    List<Usage> getUsagesByChrono(int sid);

    @Query("SELECT * FROM Usage WHERE sub_id LIKE :sid AND valid_entry LIKE 1 ORDER BY timestamp")
    List<Usage> getValidUsagesByChrono(int sid);

    @Query("SELECT Usage.* FROM Usage INNER JOIN Substance WHERE Usage.sub_id LIKE Substance.id " +
            "AND Usage.valid_entry LIKE 1 AND Substance.archived LIKE 0 ORDER BY Usage.id")
    List<Usage> getValidUnarchivedUsagesByUID();

    @Query("SELECT Usage.* FROM Usage INNER JOIN Substance WHERE Usage.sub_id LIKE Substance.id " +
            "AND Usage.valid_entry LIKE 1 AND Substance.archived LIKE 0 ORDER BY Usage.id DESC")
    List<Usage> getValidUnarchivedUsagesDescByUID();

    @Query("SELECT * FROM Usage WHERE sub_id LIKE :sid ORDER BY timestamp DESC")
    List<Usage> getDescUsagesByChrono(int sid);

    @Query("SELECT Usage.* FROM Usage INNER JOIN Substance WHERE Usage.sub_id LIKE Substance.id " +
            "AND Usage.valid_entry LIKE 1 AND Substance.archived LIKE 0 AND Usage.sub_id " +
            "LIKE :sid ORDER BY Usage.id")
    List<Usage> getValidUnarchivedUsagesBySID(int sid);

    @Query("SELECT Usage.* FROM Usage INNER JOIN Substance WHERE Usage.sub_id LIKE Substance.id " +
            "AND Usage.valid_entry LIKE 1 AND Substance.archived LIKE 0 AND Usage.sub_id " +
            "LIKE :sid ORDER BY Usage.id DESC")
    List<Usage> getValidUnarchivedUsagesDescBySID(int sid);

    @Query("SELECT * FROM Usage WHERE sub_id LIKE :sid AND valid_entry LIKE 1 ORDER BY timestamp " +
            "DESC")
    List<Usage> getValidDescUsagesByChrono(int sid);

    @Query("SELECT id FROM Usage WHERE sub_id LIKE :sid ORDER BY timestamp LIMIT 1")
    int getMinUsageId(int sid);

    @Query("SELECT id FROM Usage WHERE sub_id LIKE :sid ORDER BY timestamp DESC LIMIT 1")
    int getMaxUsageId(int sid);

    @Query("SELECT COUNT(*) FROM Usage WHERE sub_id LIKE :sid AND timestamp < :ts")
    int getPriorToDateUsageCount(int sid, long ts);

    @Query("SELECT COUNT(*) FROM Usage INNER JOIN Substance ON Usage.sub_id = Substance.id WHERE " +
            "Usage.timestamp < :ts AND Substance.archived = 0")
    int getPriorToTSUOrderedUsageRecordsFromAllSubsCount(long ts);

    @Query("SELECT timestamp FROM Usage WHERE sub_id LIKE :sid AND timestamp < :ts")
    List<Long> getPriorToDateTimestamps(int sid, long ts);

    @Query("SELECT * FROM Usage WHERE sub_id LIKE :sid AND timestamp < :ts")
    List<Usage> getPriorToTSUsageRecords(int sid, long ts);

    @Query("SELECT * FROM Usage WHERE sub_id LIKE :sid AND timestamp > :ts")
    List<Usage> getPostToTSUsageRecords(int sid, long ts);

    @Query("SELECT * FROM Usage WHERE sub_id LIKE :sid AND timestamp > :ts ORDER BY timestamp")
    List<Usage> getPostToTSOrderedUsageRecords(int sid, long ts);

    @Query("SELECT * FROM Usage INNER JOIN Substance ON Usage.sub_id = Substance.id WHERE " +
            "Usage.timestamp < :ts AND Substance.archived = 0")
    List<Usage> getPriorToTSOrderedUsageRecordsFromAllSubs(long ts);

    @Query("SELECT * FROM Usage WHERE valid_entry LIKE 1 AND sub_id LIKE :sid AND " +
            "timestamp >= :sts AND timestamp < :ets ORDER BY timestamp")
    List<Usage> getDurationValidUsageRecordsForSub(int sid, long sts, long ets);

    @Query("SELECT COUNT(*) FROM Usage WHERE valid_entry LIKE 1 AND sub_id LIKE :sid AND " +
            "timestamp >= :sts AND timestamp < :ets LIMIT 1")
    int countDurationValidUsageRecordFlagForSub(int sid, long sts, long ets);

    @Query("SELECT * FROM Usage WHERE valid_entry LIKE 1 AND timestamp >= :sts AND " +
            "timestamp < :ets ORDER BY timestamp")
    List<Usage> getDurationValidUsageRecords(long sts, long ets);

    @Query("SELECT * FROM Usage INNER JOIN Substance WHERE Usage.sub_id LIKE Substance.id AND " +
            "Substance.archived LIKE 0 AND Usage.timestamp < :ets AND Usage.timestamp >= :sts" +
            " ORDER BY timestamp")
    List<Usage> getDurationValidUnarchivedUsageRecords(long sts, long ets);

    @Query("SELECT * FROM Usage WHERE valid_entry LIKE 1 AND sub_id LIKE :sid AND " +
            "notes LIKE :substr")
    List<Usage> getUsagesWithNotesSubstr(int sid, String substr);

    @Query("SELECT * FROM Usage WHERE valid_entry LIKE 1 AND notes LIKE :substr")
    List<Usage> getAllUsagesWithNotesSubstr(String substr);

    @Query("SELECT COUNT(*) FROM Usage")
    int getTotalUsageCount();

    @Insert
    void insertUsage(Usage use);

    @Update
    void update(Usage use);

    @Delete
    void delete(Usage use);

    @Delete
    int deleteWithResultRows(Usage use);

    @Query("DELETE FROM Usage WHERE id = :uid")
    int deleteByUsageID(int uid);

    @Query("DELETE FROM Usage WHERE timestamp < :ts")
    int deletePriorToTimestamp(long ts);

    @Query("SELECT COUNT(*) FROM Usage WHERE timestamp < :ts")
    int countPriorToTimestamp(long ts);

    @Query("SELECT COUNT(*) FROM Usage WHERE timestamp >= :ts AND sub_id LIKE :sid")
    int countPostToTimestampForSubstance(long ts, int sid);

    @Query("SELECT COUNT(*) FROM Usage WHERE timestamp >= :sts AND " +
            "timestamp < :ets AND sub_id LIKE :sid")
    int countInDurationForSubstance(int sid, long sts, long ets);

    @Query("DELETE FROM Usage WHERE sub_id = :sid")
    void deleteAllBySID(int sid);

    @Query("SELECT timestamp FROM Usage ORDER BY timestamp LIMIT 1")
    long getEarliestUsageTimestamp();

    @Query("SELECT timestamp FROM Usage WHERE sub_id = :sid ORDER BY timestamp LIMIT 1")
    long getEarliestUsageTimestampBySID(int sid);

    @Query("SELECT timestamp FROM Usage WHERE sub_id = :sid ORDER BY timestamp DESC LIMIT 1")
    long getLatestUsageTimestampBySID(int sid);

    @Query("SELECT COUNT(*) FROM Usage WHERE sub_id = :sid")
    int getUsageCountForSubstance(int sid);
}
