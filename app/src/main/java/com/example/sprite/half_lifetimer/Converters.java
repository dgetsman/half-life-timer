package com.example.sprite.half_lifetimer;

import androidx.room.TypeConverter;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;

public class Converters {
    private static final DosageUnit[]   Units = {DosageUnit.MCG, DosageUnit.MG, DosageUnit.G,
            DosageUnit.ML, DosageUnit.TSP, DosageUnit.TBSP, DosageUnit.DOSE};

    @TypeConverter
    public static DosageUnit toDosageUnit(int value) {
        //ah, this should be necessary only if the database was in an invalid state, ie from the
        //addition of a new DosageUnit without the above array being updated :P
        //if (value == -1) {  //and this is happening WHY, exactly?
        //    return null;
        //}
        return Units[value];
    }

    @TypeConverter
    public static int fromDosageUnit(DosageUnit unit) {
        for (int cntr = 0; cntr < Units.length; cntr++) {
            if (unit == Units[cntr]) {
                return cntr;
            }
        }

        return -1;
    }

    //NOTE: the following code is timezone unaware
    @TypeConverter
    public static LocalDateTime toLocalDateTime(long value) {
        return LocalDateTime.ofEpochSecond(value, 0, ZoneOffset.UTC);
    }

    @TypeConverter
    public static long fromLocalDateTime(LocalDateTime ldt) {
        return ldt.toEpochSecond(ZoneOffset.UTC);
    }

    @TypeConverter
    public static int fromLocalTime(LocalTime lt) {
        if (lt != null) {
            return lt.toSecondOfDay();
        } else {
            return 0;
        }
    }

    @TypeConverter
    public static LocalTime toLocalTime(int sod) {
        return LocalTime.ofSecondOfDay(sod);
    }
}
