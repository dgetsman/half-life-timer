package com.example.sprite.half_lifetimer;

import android.graphics.Color;
import android.graphics.Typeface;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ConsolidateDatabase extends AppCompatActivity {
    List<Usage> allUsages = new ArrayList<>();
    List<String> wiped = new ArrayList<>();
    List<String> added = new ArrayList<>();
    boolean localDebugging = false;
    int consolidatedTimes = 0;
    int entriesDeleted = 0;
    int entriesInserted = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consolidate_database);
    }

    /**
     * So yeah this is the button we click to initiate database consolidation
     * and like stuff.
     *
     * @param v View
     */
    public void consolidateOnClick(View v) {
        EditText edtMin = findViewById(R.id.edtMinToConsolidateBy);

        //verify that the value is within a sane range; should probably wrap this in a try/catch
        //just in case somebody can get something stupid in that field somehow, too
        int min = Integer.parseInt(edtMin.getText().toString());

        if (validateConsolidationMinutes(min)) {
            consolidateDatabase(min);
        }

        //display the results of our consolidation attempt
        GlobalMisc.showSimpleDialog(ConsolidateDatabase.this, "Consolidation Results",
                "Consolidation attempts: \t" + consolidatedTimes + "\n" +
                "Entries wiped: \t" + entriesDeleted + "\n" + "Entries inserted: \t" +
                entriesInserted);

        updateResults(wiped, added);

        consolidatedTimes = 0;
        entriesDeleted = 0;
        entriesInserted = 0;
    }

    /**
     * Validates that the user-entered # of minutes is within a sane range (as
     * specified within GlobalMisc).
     *
     * @param min user's specified minutes between dosages to count for
     *            consolidation
     * @return whether or not validation was successful
     */
    private boolean validateConsolidationMinutes(int min) {
        if (min < GlobalMisc.ConsolidateMin || min > GlobalMisc.ConsolidateMax) {
            Toast.makeText(ConsolidateDatabase.this, /*"Consolidation Minutes Out of Range",*/
                    "You must pick a value between " + GlobalMisc.ConsolidateMin + " and " +
                    GlobalMisc.ConsolidateMax + " minutes!", Toast.LENGTH_LONG).show();

            return false;
        }

        return true;
    }

    /**
     * Handles dummy wiping (user output of what would be done) or real wiping
     * of whatever Usages are in the uses List.
     *
     * @param uses List of Usages to be wiped
     */
    private void wipeFromDBAndList(List<Usage> uses) {
        for (Usage use : uses) {
            if (use == null) {
                continue;
            }

            //wipe from the database
            if (!localDebugging) {
                //we're now going to go with nondestructive consolidation, so we need to update the
                //entry to show that it's no longer valid, then update the entry in the DB
                use.setValid_entry(false);

                Permanence.Admins.updateUsage(use);
                GlobalMisc.debugMsg("wipeFromDBAndList", "updated usage: " +
                        use.toStringSansDT());
            }

            //add to our results display list
            wiped.add(use.toString());

            //erase from the list
            allUsages.remove(use);
        }

        if (localDebugging) {
            if (uses.size() == 3) {
                showDatabaseRemoval(uses.get(0), uses.get(1), uses.get(2));
            } else {
                showDatabaseRemoval(uses.get(0), uses.get(1), null);
            }
        }

        if (uses.get(0) == null || uses.get(2) == null) {
            entriesDeleted += 2;
        } else {
            entriesDeleted += 3;
        }
    }

    /**
     * Handles dummy or real addition of the new consolidated record to the
     * database, depending on whether or not we are in localDebugging.
     *
     * @param use new Usage to add to the database created from previous Usages
     */
    private void addConsolidatedToDatabase(Usage use) {
        use.setValid_entry(true);   //not sure why this wasn't handled elsewhere :-?(beep)

        if (!localDebugging) {
            Permanence.Admins.saveUsage(use);
        } else {
            showDatabaseAddition(use);
        }

        //add to our results display list for later
        added.add(use.toString());

        entriesInserted += 1;
    }

    /**
     * Handles the maths of tallying up the dosages between Usages to be
     * consolidated, averaging the timestamps to be consolidated, and creating
     * the rest of the record to be saved as the new consolidated Usage.
     *
     * @param usesToAverage List of Usages to average info from
     * @return Usage of our newly consolidated information
     */
    private Usage averageUsages(List<Usage> usesToAverage) {
        Usage averagedUsage = new Usage();
        float totalDosage = 0;
        long totalTSSeconds = 0;

        for (Usage use : usesToAverage) {
            totalDosage += use.getDosage();
            totalTSSeconds += Converters.fromLocalDateTime(use.getTimestamp());
        }

        averagedUsage.setSub_id(usesToAverage.get(0).getSub_id());
        averagedUsage.setNotes("Consolidated Entry");
        averagedUsage.setDosage(totalDosage);
        averagedUsage.setTimestamp(Converters.toLocalDateTime(
                totalTSSeconds / usesToAverage.size()));

        consolidatedTimes += 1;

        return averagedUsage;
    }

    /**
     * Determines what scenario we're in for averaging (ie averaging current,
     * pre & post entries, or just current and one of the pre or post), then
     * calls the appropriate method for handling each scenerio.
     *
     * @param min user specified minutes betwixt entries to be eligible for
     *            consolidation
     */
    private void consolidateDatabase(int min) {
        final int fSubNdx = getIntent().getExtras().getInt("SUB_NDX");
        final int fIntervalSec = min * 60;

        Toast.makeText(getApplicationContext(), "Please wait; optimizing the database . . .",
                Toast.LENGTH_LONG).show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                allUsages = Permanence.Admins.loadValidOrderedUsagesBySubid(fSubNdx);

                for (int cntr = 0; cntr < allUsages.size(); cntr++) {
                    if (cntr > 0 && cntr < allUsages.size() - 1) {
                        cntr -= testAndConsolidateThree(fIntervalSec, cntr);
                    } else if (cntr > 0) {
                        //fix the counter
                        cntr -= testAndConsolidateTwoPre(fIntervalSec, cntr);
                    } else if (cntr == 0) {
                        //fix the counter
                        cntr -= testAndConsolidateTwoPost(fIntervalSec, cntr);
                    } else {
                        Toast.makeText(ConsolidateDatabase.this,
                                "Not sure wtf happened; cntr: " + cntr, Toast.LENGTH_LONG).show();
                    }
                }
            }
        }).start();
    }

    /**
     * Displays message showing what would be added to the database.
     *
     * @param use Usage that would be added
     */
    private void showDatabaseAddition(Usage use) {
        /* GlobalMisc.showSimpleDialog(ConsolidateDatabase.this,
                "Debugging: Database Addition", use.toString()); */
        GlobalMisc.debugMsg("showDatabaseAddition", "Database Addition: " +
                use.toString());
    }

    /**
     * Displays a message showing what would be removed from the database.
     * Note that since this can have either 2 or 3 parameters in various
     * orders, I'm using 3 Usages to be passed to it, where all 3 can be
     * valid, or either the first or the last Usage can be null in order to
     * signify that the averaging was done over only the other 2 Usages.
     *
     * @param use1 first Usage (may be null)
     * @param use2 current Usage
     * @param use3 last Usage (may be null, but not if 1st Usage is null)
     */
    private void showDatabaseRemoval(Usage use1, Usage use2, Usage use3) {
        if (use1 != null && use3 != null) {
            /*GlobalMisc.showSimpleDialog(ConsolidateDatabase.this,
                    "Debugging: Database Removal", "1) " +
                            use1.toString() + "\n2) " +
                            use2.toString() + "\n3) " +
                            use3.toString());*/
            GlobalMisc.debugMsg("showDatabaseRemoval", "Database Removal:\n1) "
                    + use1.toString() + "\n2) " + use2.toString() + "\n3) " + use3.toString());
        } else if (use1 != null) {
            /*GlobalMisc.showSimpleDialog(ConsolidateDatabase.this,
                    "Debugging: Database Removal", "1) " +
                    use1.toString() + "\n2) " + use2.toString());*/
            GlobalMisc.debugMsg("showDatabaseRemoval", "Database Removal:\n1) "
                    + use1.toString() + "\n2) " + use2.toString());
        } else {
            /*GlobalMisc.showSimpleDialog(ConsolidateDatabase.this,
                    "Debugging: Database Removal", "1) " +
                    use2.toString() + "\n2) " + use3.toString());*/
            GlobalMisc.debugMsg("showDatabaseRemoval", "Database Removal:\n1) "
                    + use2.toString() + "\n2) " + use3.toString());
        }
    }

    /**
     * Tests three records to determine whether or not they're eligible for
     * consolidation and handles calling the consolidation routine that is
     * applicable for the case if such is warranted.
     *
     * @param intervalSec user specified minutes * 60
     * @param cntr current position in the allUsages List
     * @return 0 or how many positions to subtract from the allUsages pointer
     */
    private int testAndConsolidateThree(long intervalSec, int cntr) {
        //here we can test both the preceding and following usages to see if we can
        //consolidate into the middle
        long medianTS = Converters.fromLocalDateTime(allUsages.get(cntr).getTimestamp());
        long preTS = Converters.fromLocalDateTime(allUsages.get(cntr - 1).getTimestamp());
        long postTS = Converters.fromLocalDateTime(allUsages.get(cntr + 1).getTimestamp());

        if ((medianTS - preTS) <= intervalSec && (postTS - medianTS) <= intervalSec) {
            //average all 3 in the middle
            Usage newUsage =
                    averageUsages(Arrays.asList(allUsages.get(cntr - 1), allUsages.get(cntr),
                            allUsages.get(cntr + 1)));

            //save the new entry (should this come after the wipings, maybe?)
            addConsolidatedToDatabase(newUsage);

            //wipe the old entries then remove from the list
            //wipeFromDBAndList(allUsages.subList(cntr -1, cntr + 1));
            wipeFromDBAndList(Arrays.asList(allUsages.get(cntr - 1), allUsages.get(cntr),
                    allUsages.get(cntr + 1)));

            //don't forget to fix the counter
            return 3;
        } else if ((medianTS - preTS) <= intervalSec) {
            //preceding and current should be averaged
            //Usage newUsage = averageUsages(allUsages.subList(cntr - 1, cntr));
            Usage newUsage =
                    averageUsages(Arrays.asList(allUsages.get(cntr - 1), allUsages.get(cntr)));

            addConsolidatedToDatabase(newUsage);

            //wipe old from database and from list
            wipeFromDBAndList(Arrays.asList(allUsages.get(cntr - 1), allUsages.get(cntr), null));

            //fix the counter
            return 2;
        } else if ((postTS - medianTS) <= intervalSec) {
            //current and following should be averaged
            //List<Usage> curUsages = ;
            //curUsages = allUsages.subList(cntr, cntr + 1);    //not sure why this doesn't work... wtf
            Usage newUsage =
                    averageUsages(Arrays.asList(allUsages.get(cntr), allUsages.get(cntr + 1)));

            //save
            addConsolidatedToDatabase(newUsage);

            //wipe old from database and from the list
            wipeFromDBAndList(Arrays.asList(null, allUsages.get(cntr), allUsages.get(cntr + 1)));

            //fix the counter
            return 2;
        } else {
            return 0;
        }
    }

    /**
     * Tests for consolidation applicability and calls consolidation for the
     * current and following Usages.
     *
     * @param intervalSec user specified min * 60
     * @param cntr current allUsages position
     * @return pointer modification
     */
    private int testAndConsolidateTwoPost(long intervalSec, int cntr) {
        //here we can test the current with the following
        long medianTS = Converters.fromLocalDateTime(allUsages.get(cntr).getTimestamp());
        long postTS = Converters.fromLocalDateTime(allUsages.get(cntr + 1).getTimestamp());
        int cnt = 0;

        if ((postTS - medianTS) <= intervalSec) {
            //current and following should be averaged
            Usage newUsage =
                    averageUsages(Arrays.asList(allUsages.get(cntr), allUsages.get(cntr + 1)));

            //save (after we're sure that this is working correctly)
            addConsolidatedToDatabase(newUsage);

            //wipe old
            //wipeFromDBAndList(allUsages.subList(cntr, cntr + 1));
            wipeFromDBAndList(Arrays.asList(allUsages.get(cntr), allUsages.get(cntr + 1)));
            cnt = 2;
        }

        return cnt;
    }

    /**
     * Tests for consolidation applicability and calls consolidation routines
     * for the current and previous Usages.
     *
     * @param intervalSec user specified min * 60
     * @param cntr current allUsages position
     * @return pointer modification
     */
    private int testAndConsolidateTwoPre(long intervalSec, int cntr) {
        //and here we test the current only with the preceding
        long medianTS = Converters.fromLocalDateTime(allUsages.get(cntr).getTimestamp());
        long preTS = Converters.fromLocalDateTime(allUsages.get(cntr - 1).getTimestamp());
        int cnt = 0;

        if ((medianTS - preTS) <= intervalSec) {
            //preceding and current should be averaged
            Usage newUsage =
                    averageUsages(Arrays.asList(allUsages.get(cntr - 1), allUsages.get(cntr)));

            //save
            addConsolidatedToDatabase(newUsage);

            //wipe old
            //wipeFromDBAndList(allUsages.subList(cntr - 1, cntr));
            wipeFromDBAndList(Arrays.asList(allUsages.get(cntr - 1), allUsages.get(cntr)));
            cnt = 2;
        }

        return cnt;
    }

    /**
     * Method displays the results of the consolidation; primarily the listing of what records have
     * been removed, what ones have been added, and the totals for each.
     *
     * @param wipedList list of wiped entries
     * @param addedList list of added entries
     */
    private void updateResults(List<String> wipedList, List<String> addedList) {
        //LinearLayout lloWiped = findViewById(R.id.lloConsolidationRemovals);
        LinearLayout lloAdded = findViewById(R.id.lloConsoldationAdditions);
        //TextView[] tvwWiped = new TextView[wipedList.size()];
        TextView[] tvwAdded = new TextView[addedList.size() + wipedList.size() + 2];

        /*for (int cntr = 0; cntr < wipedList.size(); cntr++) {
            tvwWiped[cntr] = new TextView(ConsolidateDatabase.this);
            tvwWiped[cntr].setText(wipedList.get(cntr));

            lloWiped.addView(tvwWiped[cntr]);
        }*/

        tvwAdded[0] = new TextView(ConsolidateDatabase.this);
        tvwAdded[0].setText(getString(R.string.entries_added));
        tvwAdded[0].setTypeface(Typeface.DEFAULT_BOLD);
        lloAdded.addView(tvwAdded[0]);

        for (int cntr = 0; cntr < addedList.size(); cntr++) {
            tvwAdded[cntr + 1] = new TextView(ConsolidateDatabase.this);
            tvwAdded[cntr + 1].setText(addedList.get(cntr));
            tvwAdded[cntr + 1].setTextColor(Color.GREEN);

            lloAdded.addView(tvwAdded[cntr + 1]);
        }

        tvwAdded[addedList.size() + 1] = new TextView(ConsolidateDatabase.this);
        tvwAdded[addedList.size() + 1].setText(getString(R.string.entries_wiped));
        tvwAdded[addedList.size() + 1].setTypeface(Typeface.DEFAULT_BOLD);
        lloAdded.addView(tvwAdded[addedList.size() + 1]);

        for (int cntr = 0; cntr < wipedList.size(); cntr ++) {
            tvwAdded[cntr + addedList.size() + 2] =
                    new TextView(ConsolidateDatabase.this);
            tvwAdded[cntr + addedList.size() + 2].setText(wipedList.get(cntr));
            tvwAdded[cntr + addedList.size() + 2].setTextColor(Color.RED);

            lloAdded.addView(tvwAdded[cntr + addedList.size() + 2]);
        }

        //don't forget to set the consolidate button & text entry field to inactive here to avoid
        //the user doing something that we're not ready to handle here
        EditText edtMin = findViewById(R.id.edtMinToConsolidateBy);
        Button btnConsolidate = findViewById(R.id.btnConsolidate);

        edtMin.setEnabled(false);
        btnConsolidate.setEnabled(false);
    }
}
