package com.example.sprite.half_lifetimer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NotificationDismissalReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.hasExtra("TAPER")) {
            Taper dismissedTaper =
                    Permanence.Tapers.loadOneValidTaperBySid(intent.getExtras().getInt(
                            "SUB_ID"));

            NotificationService.firedNotifications.replace(dismissedTaper.getId(), false);

            GlobalMisc.debugLog("NotificationDismissalReceiver.onReceive",
                    "Toggled firedNotifications for " + dismissedTaper.getName());
        }
    }
}
