package com.example.sprite.half_lifetimer;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class UsageISODateTime {
    private int         sub_id;
    private float       dosage;
    private String      timestamp;
    private String      notes;
    private boolean     valid_entry;

    public UsageISODateTime(Usage useRecord) {
        //copy the easy fields
        this.sub_id = useRecord.getSub_id();
        this.dosage = useRecord.getDosage();
        this.notes = useRecord.getNotes();
        this.valid_entry = useRecord.isValid_entry();

        //now we'll convert the LocalDateTime to an ISO compatible string
        DateTimeFormatter ldtFormatter = DateTimeFormatter.ISO_DATE_TIME;
        this.timestamp = ldtFormatter.format(useRecord.getTimestamp());
    }

    public int getSub_id() {
        return sub_id;
    }

    public void setSub_id(int sub_id) {
        this.sub_id = sub_id;
    }

    public float getDosage() {
        return dosage;
    }

    public void setDosage(float dosage) {
        this.dosage = dosage;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public boolean isValid_entry() {
        return valid_entry;
    }

    public void setValid_entry(boolean valid_entry) {
        this.valid_entry = valid_entry;
    }

    public LocalDateTime getLDT() {
        DateTimeFormatter dtFmt = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

        return LocalDateTime.from(dtFmt.parse(this.timestamp));
    }

    public String toString() {
        return "Sub ID: " + this.sub_id + " (" + this.dosage + " units)\tat: " +
            this.timestamp + "\tValid: " + this.valid_entry;
    }
}
