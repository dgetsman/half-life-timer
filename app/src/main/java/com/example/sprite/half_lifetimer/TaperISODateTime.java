package com.example.sprite.half_lifetimer;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class TaperISODateTime {
    private int             id;
    private String          name;
    private String          targetDate;
    private String          startDate;
    private int             sid;
    private float           dailyDosageTarget;
    private float           dailyDosageStart;
    private boolean         valid = true;
    private int             adminsPerDay;
    private String          startHour;
    private String          endHour;
    private float           dosageInterval;

    public TaperISODateTime(Taper pureTaper) {
        this.name = pureTaper.getName();

        //now we'll convert the LocalDateTime to an ISO compatible string
        DateTimeFormatter ldtFormatter = DateTimeFormatter.ISO_DATE_TIME;
        this.targetDate = ldtFormatter.format(pureTaper.getTargetDate());
        this.startDate = ldtFormatter.format(pureTaper.getStartDate());

        this.sid = pureTaper.getSid();
        this.dailyDosageTarget = pureTaper.getDailyDosageTarget();
        this.dailyDosageStart = pureTaper.getDailyDosageStart();
        this.valid = pureTaper.getValid();
        this.adminsPerDay = pureTaper.getAdminsPerDay();

        DateTimeFormatter ltFormatter = DateTimeFormatter.ISO_TIME;
        this.startHour = ltFormatter.format(pureTaper.getStartHour());
        this.endHour = ltFormatter.format(pureTaper.getEndHour());

        this.dosageInterval = pureTaper.getDosageInterval();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(String targetDate) {
        this.targetDate = targetDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public float getDailyDosageTarget() {
        return dailyDosageTarget;
    }

    public void setDailyDosageTarget(float dailyDosageTarget) {
        this.dailyDosageTarget = dailyDosageTarget;
    }

    public float getDailyDosageStart() {
        return dailyDosageStart;
    }

    public void setDailyDosageStart(float dailyDosageStart) {
        this.dailyDosageStart = dailyDosageStart;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public int getAdminsPerDay() {
        return adminsPerDay;
    }

    public void setAdminsPerDay(int adminsPerDay) {
        this.adminsPerDay = adminsPerDay;
    }

    public String getStartHour() {
        return startHour;
    }

    public void setStartHour(String startHour) {
        this.startHour = startHour;
    }

    public String getEndHour() {
        return endHour;
    }

    public void setEndHour(String endHour) {
        this.endHour = endHour;
    }

    public float getDosageInterval() {
        return dosageInterval;
    }

    public void setDosageInterval(float dosageInterval) {
        this.dosageInterval = dosageInterval;
    }

    public LocalDateTime getTargetLDT() {
        DateTimeFormatter dtFmt = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

        return LocalDateTime.from(dtFmt.parse(this.targetDate));
    }

    public LocalDateTime getStartLDT() {
        DateTimeFormatter dtFmt = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

        return LocalDateTime.from(dtFmt.parse(this.startDate));
    }

    public LocalTime getEndLT() {
        return LocalTime.parse(this.endHour);
    }

    public LocalTime getStartLT() {
        return LocalTime.parse(this.startHour);
    }
}
