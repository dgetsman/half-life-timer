package com.example.sprite.half_lifetimer;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database(entities = {Substance.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class SubDbase extends RoomDatabase {
    public abstract SubstanceDao substanceDao();

//    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
//        @Override
//        public void migrate(SupportSQLiteDatabase database) {
//            database.execSQL("ALTER TABLE Substance ADD COLUMN halflife INTEGER");
//            database.execSQL("ALTER TABLE Substance ADD COLUMN detectable_halflife INTEGER");
//        }
//    };
}
