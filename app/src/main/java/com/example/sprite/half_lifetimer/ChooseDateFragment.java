package com.example.sprite.half_lifetimer;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;

public class ChooseDateFragment extends DialogFragment implements View.OnClickListener {

    private DatePicker datePicker;
    private int year;
    private int month;
    private int day;
    private boolean isDateSet = false;
    private DateListener dateListener;

    public interface DateListener {
        public void onDateSelected(int year, int month, int day);
    }

    public ChooseDateFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_choose_date, container, false);

        if (getArguments().getBoolean("startCompleted")) {
            getDialog().setTitle("End Date for Range");
        } else {
            getDialog().setTitle("Start Date for Range");
        }

        if (getArguments().getBoolean("dateSet")) {
            isDateSet = true;
        }

        datePicker = (DatePicker) rootView.findViewById(R.id.datepicker);
        Button button = (Button) rootView.findViewById(R.id.btnSetStart);
        button.setOnClickListener(this);
        int[] ymd = getArguments().getIntArray("ymd");

        if (isDateSet) {
            datePicker.updateDate(ymd[0], ymd[1], ymd[2]);
        }

        return rootView;
    }

    @Override
    public void onClick(View view) {
        int year = datePicker.getYear();
        int month;
            month = datePicker.getMonth() + 1;

        int day = datePicker.getDayOfMonth();

        dateListener.onDateSelected(year, month, day);

        this.dismiss();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dateListener = (DateListener) context;
    }
}
