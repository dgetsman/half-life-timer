package com.example.sprite.half_lifetimer;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

public class SubIndependentNotesSearch extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_independent_notes_search);

        getSearchSubstring();
    }

    private void getSearchSubstring() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SubIndependentNotesSearch.this);
        builder.setTitle(getString(R.string.search_substring));

        final EditText input = new EditText(SubIndependentNotesSearch.this);

        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        //buttons
        builder.setPositiveButton(getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        updateDisplay(input.getText().toString());
                    }
                });
        builder.setNegativeButton(getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        builder.show();
    }

    private void updateDisplay(final String substring) {
        Toast.makeText(SubIndependentNotesSearch.this, "Searching notes . . .",
                Toast.LENGTH_LONG).show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                List<Usage> applicableUsages =
                        Permanence.Admins.getAllUsagesWithSubstring(substring);
                final LinearLayout lloResults = findViewById(R.id.lloSearchResults);

                if (applicableUsages.size() < 1) {
                    GlobalMisc.showSimpleDialog(SubIndependentNotesSearch.this, "No results!",
                            "No results have been found for your search!");

                    return;
                }

                final HashMap<Integer, String> fSubsList =
                        GlobalMisc.getSubIndexNNames(SubIndependentNotesSearch.this);

                TextView[] usageTextViews = new TextView[applicableUsages.size() * 2];
                int cntr = 0;
                for (Usage use : applicableUsages) {
                    usageTextViews[cntr] = new TextView(SubIndependentNotesSearch.this);
                    usageTextViews[cntr + 1] = new TextView(SubIndependentNotesSearch.this);

                    usageTextViews[cntr].setText(getString(R.string.at_middle,
                            fSubsList.get(use.getSub_id()), use.getTimestamp().toString()));
                    usageTextViews[cntr + 1].setText(use.getNotes());
                    usageTextViews[cntr].setTextSize(16);
                    usageTextViews[cntr].setTypeface(usageTextViews[cntr].getTypeface(),
                            Typeface.BOLD);
                    usageTextViews[cntr + 1].setTextSize(15);

                    final Usage fUse = use;
                    usageTextViews[cntr].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AlertDialog.Builder builder =
                                    new AlertDialog.Builder(SubIndependentNotesSearch.this);
                            builder.setTitle(fSubsList.get(fUse.getSub_id()) + " Administration");
                            builder.setMessage("Admin #: " + fUse.getId() + "\n" +
                                    "Dosage: " + fUse.getDosage() + "\n" +
                                    "Administered At: " + fUse.getTimestamp().toString() + "\n" +
                                    "Notes: " + fUse.getNotes());
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                            builder.show();
                        }
                    });
                    usageTextViews[cntr + 1].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AlertDialog.Builder builder =
                                    new AlertDialog.Builder(SubIndependentNotesSearch.this);
                            builder.setTitle(fSubsList.get(fUse.getSub_id()) + " Administration");
                            builder.setMessage("Admin #: " + fUse.getId() + "\n" +
                                    "Dosage: " + fUse.getDosage() + "\n" +
                                    "Administered At: " + fUse.getTimestamp().toString() + "\n" +
                                    "Notes: " + fUse.getNotes());
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });

                            builder.show();
                        }
                    });

                    final TextView usageTV1 = usageTextViews[cntr++];
                    final TextView usageTV2 = usageTextViews[cntr++];

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            lloResults.addView(usageTV1);
                            lloResults.addView(usageTV2);
                        }
                    });
                }
            }
        }).start();
    }
}
