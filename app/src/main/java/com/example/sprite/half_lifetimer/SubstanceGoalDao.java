package com.example.sprite.half_lifetimer;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface SubstanceGoalDao {
    @Query("SELECT * FROM SubstanceGoal")
    List<SubstanceGoal> getAll();

    @Query("SELECT SubstanceGoal.* FROM SubstanceGoal INNER JOIN Substance WHERE " +
           "SubstanceGoal.sub_id LIKE Substance.id AND Substance.archived LIKE 0")
    List<SubstanceGoal> getAllUnarchived();

    @Query("SELECT * FROM SubstanceGoal WHERE id LIKE :id")
    SubstanceGoal getSubstanceGoalById(int id);

    @Query("SELECT * FROM SubstanceGoal WHERE sub_id LIKE :sid LIMIT 1")
    SubstanceGoal getSubstanceGoalBySid(int sid);

    @Update
    void updateSubstanceGoal(SubstanceGoal subGoal);

    @Insert
    void insertSubstanceGoal(SubstanceGoal subGoal);

    @Delete
    void delete(SubstanceGoal subGoal);
}
