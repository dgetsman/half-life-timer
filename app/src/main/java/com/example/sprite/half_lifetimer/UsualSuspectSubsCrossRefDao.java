package com.example.sprite.half_lifetimer;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface UsualSuspectSubsCrossRefDao {
    @Query("SELECT * FROM UsualSuspectSubsCrossRef")
    List<UsualSuspectSubsCrossRef> loadUsualSuspectCrossRefs();

    @Query("SELECT * FROM UsualSuspectSubsCrossRef WHERE sub LIKE :sid")
    List<UsualSuspectSubsCrossRef> loadUsualSuspectAndSubsBySID(int sid);

    @Query("SELECT * FROM UsualSuspectSubsCrossRef WHERE usualSuspect LIKE :USId")
    List<UsualSuspectSubsCrossRef> loadUsualSuspectAndSubsByUS(int USId);

    @Delete
    void delete(UsualSuspectSubsCrossRef crossRef);

    @Insert
    void insertUsualSuspectSubsCrossRef(UsualSuspectSubsCrossRef crossRef);
}
