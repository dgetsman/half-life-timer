package com.example.sprite.half_lifetimer;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface SubClassificationDao {
    @Query("SELECT * FROM SubClassification")
    List<SubClassification> getAll();

    @Query("SELECT * FROM SubClassification WHERE id LIKE :id LIMIT 1")
    SubClassification findClassById(int id);

    @Query("SELECT * FROM SubClassification WHERE name LIKE :name")
    SubClassification findClassByName(String name);

    @Insert
    void insertSubClass(SubClassification sClass);

    @Delete
    void delete(SubClassification sClass);
}
