package com.example.sprite.half_lifetimer;

import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Class provides a full dumping of usage/administrations, organized by substance.
 */
public class FullAdminsDump extends AppCompatActivity {
    boolean reverse = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_admins_dump);

        GlobalMisc.showResultsBeingCompiledMessage(FullAdminsDump.this);
        updateDisplay();
    }

    private void updateDisplay() {
        final LinearLayout fLloAllAdmins = findViewById(R.id.lloAllAdmins);
        fLloAllAdmins.removeAllViewsInLayout();

        new Thread(new Runnable() {
            @Override
            public void run() {
                LocalDateTime curDate = LocalDateTime.MIN.plusDays(1).withHour(0)
                        .withMinute(0)  //I know plusDays looks weird, but it may be necessary
                        .withSecond(0); //depending on when exactly LocalDateTime.MIN is

                for (Substance sub :
                        Permanence.Subs.loadUnarchivedSubstances(FullAdminsDump.this)) {
                    boolean newSubHeading = true;
                    boolean newDateHeading = true;

                    //new substance, new heading
                    final TextView subHeading = new TextView(FullAdminsDump.this);
                    subHeading.setText(sub.getCommon_name());
                    subHeading.setTextSize(22);

                    //are there any usage entries for this substance?  if not, notify user
                    if (Permanence.Admins.getUsageCountForSubstance(sub.getId()) < 1) {
                        final TextView nothingApplicable = new TextView(FullAdminsDump.this);
                        nothingApplicable.setText(getString(R.string.no_results_found,
                                sub.getCommon_name()));
                        nothingApplicable.setTextSize(20);
                        nothingApplicable.setPaintFlags(nothingApplicable.getPaintFlags() |
                                Paint.UNDERLINE_TEXT_FLAG);
                        nothingApplicable.setTypeface(nothingApplicable.getTypeface(),
                                Typeface.BOLD);

                        runOnUiThread(new Runnable() {
                            public void run() {
                                fLloAllAdmins.addView(nothingApplicable);
                            }
                        });

                        continue;
                    }

                    List<Usage> allAdmins;
                    if (reverse) {
                        allAdmins =
                                Permanence.Admins.getValidUnarchivedUsagesDescBySID(sub.getId());
                    } else {
                        allAdmins = Permanence.Admins.getValidUnarchivedUsagesBySID(sub.getId());
                    }

                    //for (Usage use : Permanence.Admins.loadOrderedUsagesBySubid(sub.getId())) {
                    for (Usage use : allAdmins) {
                        //do we need a new date heading?
                        final TextView dateHeading = new TextView(FullAdminsDump.this);
                        if (use.getTimestamp().isAfter(curDate.plusDays(1))) {
                            newDateHeading = true;
                            //new date heading needed
                            curDate = use.getTimestamp().withHour(0)
                                    .withMinute(0)
                                    .withSecond(0);

                            dateHeading.setText(curDate.toString());
                            dateHeading.setTextSize(20);
                            dateHeading.setPaintFlags(dateHeading.getPaintFlags() |
                                    Paint.UNDERLINE_TEXT_FLAG);
                            //fLloAllAdmins.addView(dateHeading);
                        }

                        //print the entry crapola
                        final TextView entryText = new TextView(FullAdminsDump.this);
                        entryText.setText(use.toString());
                        entryText.setTextSize(18);
                        //click handlers go here


                        final boolean fNewSubHeading = newSubHeading;
                        final boolean fNewDateHeading = newDateHeading;
                        //add everything to the layout
                        runOnUiThread(new Runnable() {
                            public void run() {
                                if (fNewSubHeading) {
                                    fLloAllAdmins.addView(subHeading);
                                }
                                if (fNewDateHeading) {
                                    fLloAllAdmins.addView(dateHeading);
                                }
                                fLloAllAdmins.addView(entryText);
                            }
                        });

                        newSubHeading = false;
                        newDateHeading = false;
                    }

                    curDate = LocalDateTime.MIN.plusDays(0).withHour(0)
                            .withMinute(0)
                            .withSecond(0);
                }
            }
        }).start();
    }

    /**
     * Method starts the FullAdminsDumpByID, if the applicable button is clicked.
     *
     * @param v View
     */
    public void dumpByIDOnClick(View v) {
        Intent intent = new Intent(FullAdminsDump.this, FullAdminsDumpByID.class);
        startActivity(intent);
    }

    public void reverseSort(View v) {
        reverse = !reverse;

        updateDisplay();
    }
}
